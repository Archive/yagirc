# Note that this is NOT a relocatable package
%define ver      0.66.2
%define rel      1
%define prefix   /usr

Summary: yagIRC -- Yet Another GTK+ IRC Client
Name: yagirc
Version: %ver
Release: %rel
Copyright: GPL
Group: X11/Applications
Source: http://yagirc.home.ml.org/yagirc-%{ver}.tar.gz
BuildRoot: /tmp/yagirc-root
Packager: Marcus Brubaker <spoon@elpaso.net>
URL: http://yagirc.home.ml.org/
Requires: imlib
Requires: glib >= 1.1.4
Requires: gtk+ >= 1.1.3
Requires: gnome-libs >= 0.20
Requires: perl >= 5.004
Docdir: %{prefix}/doc

%description
yagIRC is "Yet Another GTK+ based IRC client".

%prep
%setup -n yagirc-%{ver}

%build
# Needed for snapshot releases.
if [ ! -f configure ]; then
  CFLAGS="$RPM_OPT_FLAGS" ./autogen.sh --prefix=%prefix
else
  CFLAGS="$RPM_OPT_FLAGS" ./configure --prefix=%prefix
fi

make

%install
rm -rf $RPM_BUILD_ROOT

./configure --prefix=$RPM_BUILD_ROOT%{prefix}
make install

%clean
rm -rf $RPM_BUILD_ROOT

%post

%postun

%files
%defattr(-, root, root)

%{prefix}/bin/*
%{prefix}/etc/*
%{prefix}/share/yagirc/*

%doc COPYING ChangeLog README TODO ChangeLog.old NEWS
