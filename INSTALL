yagIRC -- INSTALL

** Required for installation **

In order to install the GUI version of yagIRC you will need a version of GTK+ >= 1.1.3 and a corresponding GLIB version.  For people on Redhat systems or any other system with package management, make sure that you upgrade both GLIB and GTK+ and that you have the glib-devel and gtk+-devel packages.  Scripting requires that you have PERL installed.  Most Linux distrobutions come with this preinstalled, and it is available for download for other platforms.  Imlib is recommended as I think there are some bugs that show up only when running without Imlib, plus you are minus some features.  EsounD will be supported once sound support has been finished and implemented.  If you are using GNOME, a version >= 0.20 is recommended.  As a rule of thumb, I would use the most recent one as the earlier ones are slightly unstable.

NOTE:  Even if you are planning on using yagIRC with the text interface, or w/o any interface you STILL NEED GLIB.  GLIB can be downloaded seperate of GTK+ now, so this is not a huge deal.

You can get the programs and libraries mentioned above at the following locations.

GTK+, GLIB -- http://www.gtk.org
Imlib      -- http://www.labs.redhat.com/imlib
Perl       -- http://www.perl.org
EsounD     -- http://www.netcom.com/~ericmit/EsounD.html
GNOME      -- http://www.gnome.org

Daily Snapshots of the developmental versions of Imlib, EsounD, GNOME, GTK+ and GLIB are available at ftp://ftp.jimpick.com/pub/gnome/snap


** How to get it installed **

Well, in a perfect world ./configure ; make ; su root ; make install and you're done.  Actually, for most cases that should do the trick.  If you are running on a non Linux box (or an older or unusual Linux distro) you will most likely have to run ./autogen.sh first.  All autogen.sh does for you is aclocal -I macros ; autoconf ; automake.


** Configure options **

You can get a complete list of configure options by running ./configure --help.  The more important ones are as follows...

--prefix=PREFIX           Install non-binaries under this prefix
--exec-prefix=EPREFIX     Install binaries and the like under this prefix
--with-gtk-prefix=PFX     If you have multiple copies of GTK on your system
                          or are having trouble getting the configure script
                          to detect GTK use this option.
--disable-[gnome, pthread, xpm, imlib, esd, socks, script]
                          Disable any of the above features


** Troubleshooting **

Below is a list of common problems, their most likely cause, and how to fix them.  If you are experiencing a problem that isn't listed, go ahead and send us a bug report.  Contact info is in the README file. 

Problem:

I get errors about undefined references to g_strup or g_*.

Probable Cause:

You have forgotten to upgrade to a more recent version of GLIB, or have multiple version of GLIB.  If you are on a RPM based system run rpm -q glib and rpm -q gtk+.  If you have installed from source, neither of those should come up with anything, or, if you have upgraded via RPMs, make sure that the glib version is at least 1.0.3.

Fix:

Remove the older version of GLIB and rerun /sbin/ldconfig.  If this still doesn't do the trick, remove all traces of GLIB from your system (effectively, rm -rf /usr/lib/*glib* /usr/include/*glib* /usr/local/lib/*glib* /usr/local/include/*glib* should take care of it for you, but if that deletes something important, it's NOT my fault ;) and then reinstall.


Problem:

It's not detecting GTK+/GLIB/IMLIB/PERL/Etc...

Probable Cause:

If you installed from source you, you most likely forgot to run /sbin/ldconfig.

Fix:

Run /sbin/ldconfig.


Problem:

It crashed/hung/etc when I *INSERT BUG HERE*

Probable Cause:

We screwed up somewhere :T

Fix:

If you can reproduce the crash/hang/etc, send us (contact info located in REAME file) a detailed bug report of exactly what you were doing, your system specs (libs, distro/os etc) and, if it is a crash, run yagirc through [insert debugger here, ie gdb] and send us the output along with a backtrace and any other debugger info you find pertinant.  Also include any other general info that you might find pertinant.

Or, if you want to, can, and have the time to, fix the bug and send us a diff.  We will love you forever if you do this.  :) 

