/*

 gui_setup_outlook.c : Outlook notebook entry for setup .. hm.. ok, this is
                       probably the worst code of yagirc so far... ;)

    Copyright (C) 1998 Timo Sirainen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "config.h"

#include <stdio.h>
#include <string.h>

#include <gtk/gtk.h>
#include <glib.h>

#include "os.h"
#include "irc.h"
#include "gui.h"
#include "intl.h"

GList *themes;

static GtkWidget *backg_text;

static GdkColor tempcolor;
static GtkWidget *colorseldlg;

static GtkWidget *color_buttons[17];

static GtkList *themelist;
static SETUP_THEME_REC *curtheme;

static void fsel_ok(GtkWidget *widget, GtkWidget *fsel)
{
    if (default_text_pixmap != NULL) g_free(default_text_pixmap);
    default_text_pixmap = g_strdup(gtk_file_selection_get_filename(GTK_FILE_SELECTION(fsel)));

    gtk_entry_set_text(GTK_ENTRY(backg_text), default_text_pixmap);

    gtk_widget_destroy(fsel);
}

static void select_back(GtkWidget *widget)
{
    GtkWidget *fsel;

    fsel = gtk_file_selection_new(_("Select background image"));
    gtk_signal_connect_object (GTK_OBJECT(fsel), "destroy",
                               GTK_SIGNAL_FUNC (gtk_widget_destroy),
                               GTK_OBJECT(fsel));
    gtk_signal_connect_object (GTK_OBJECT (GTK_FILE_SELECTION (fsel)->ok_button), "clicked",
                               GTK_SIGNAL_FUNC (fsel_ok), GTK_OBJECT(fsel));
    gtk_signal_connect_object (GTK_OBJECT (GTK_FILE_SELECTION (fsel)->cancel_button), "clicked",
                               (GtkSignalFunc) gtk_widget_destroy,
                               GTK_OBJECT (fsel));
    if (default_text_pixmap != NULL)
        gtk_file_selection_set_filename(GTK_FILE_SELECTION(fsel), default_text_pixmap);

    gtk_grab_add (GTK_WIDGET (fsel));
    gtk_widget_show(fsel);
}

static void color_selected(GtkWidget *widget, GtkWidget *button)
{
    GdkColor *color;
    GtkStyle style;

    color = gtk_object_get_data(GTK_OBJECT(button), "color");
    if (color != NULL) memcpy(color, &tempcolor, sizeof(GdkColor));

    gdk_color_alloc(gdk_window_get_colormap (button->window), color);
    memcpy(&style, gtk_widget_get_style(button), sizeof(GtkStyle));
    memcpy(&style.bg[0], color, sizeof(GdkColor));
    gtk_widget_set_style(button, &style);

    gtk_widget_destroy(colorseldlg);

    curtheme->colors[15] = curtheme->fg;
}

static void color_changed(GtkWidget *widget, GtkWidget *text)
{
    GtkStyle style;
    gdouble col[3];

    gtk_color_selection_get_color(GTK_COLOR_SELECTION(widget), col);
    tempcolor.red = (guint16) (col[0]*65535.0);
    tempcolor.green = (guint16) (col[1]*65535.0);
    tempcolor.blue = (guint16) (col[2]*65535.0);

    memcpy(&style, gtk_widget_get_style(text), sizeof(GtkStyle));
    style.text[0] = tempcolor;
    gtk_widget_set_style(text, &style);

    gtk_text_freeze(GTK_TEXT(text));
    gtk_text_set_point(GTK_TEXT(text), 0);
    gtk_text_forward_delete(GTK_TEXT(text), gtk_text_get_length(GTK_TEXT(text)));
    gtk_text_insert (GTK_TEXT(text), NULL, NULL, NULL, _("Test color"), -1);
    gtk_text_thaw(GTK_TEXT(text));
}

static GtkWidget *create_test_color(GtkBox *box)
{
    GtkWidget *text;
    GtkStyle style;

    text = gtk_text_new(NULL, NULL);
    gtk_widget_set_usize(text, -1, 30);
    gtk_box_pack_start(box, text, FALSE, FALSE, 0);

    gtk_widget_realize(text);

    memcpy(&style, gtk_widget_get_style(text), sizeof(GtkStyle));
    style.bg[0] = curtheme->bg;
    if (curtheme->pixmap != NULL)
    {
        GdkPixmap *pixmap;
        GdkBitmap *mask;

        pixmap = gdk_pixmap_create_from_xpm(text->window, &mask, NULL, curtheme->pixmap);
        style.bg_pixmap[0] = pixmap;
    }
    gtk_widget_set_style(text, &style);

    return text;
}

static void select_color(GtkWidget *widget, GtkWidget *button)
{
    GtkWidget *colorsel;
    GtkWidget *testcol, *but;
    GdkColor *color;
    gdouble colors[3];

    color = gtk_object_get_data(GTK_OBJECT(button), "color");
    colors[0] = color->red/65535.0;
    colors[1] = color->green/65535.0;
    colors[2] = color->blue/65535.0;

    colorseldlg = gtk_dialog_new();
    gtk_signal_connect (GTK_OBJECT (colorseldlg), "delete_event",
                        GTK_SIGNAL_FUNC(gtk_widget_destroy), NULL);

    testcol = create_test_color(GTK_BOX(GTK_DIALOG(colorseldlg)->vbox));
    gtk_widget_show(testcol);

    colorsel = gtk_color_selection_new();
    gtk_signal_connect(GTK_OBJECT (colorsel), "color_changed",
                       GTK_SIGNAL_FUNC(color_changed), testcol);
    gtk_box_pack_start(GTK_BOX(GTK_DIALOG(colorseldlg)->vbox), colorsel, TRUE, TRUE, 0);
    gtk_widget_show(colorsel);

    but = gtk_button_new_with_label (_("Ok"));
    gtk_box_pack_start (GTK_BOX (GTK_DIALOG(colorseldlg)->action_area), but, TRUE, TRUE, 10);
    gtk_signal_connect_object (GTK_OBJECT (but), "clicked",
                               GTK_SIGNAL_FUNC (color_selected), GTK_OBJECT(button));
    /*GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT);
    gtk_widget_grab_default (button);*/
    gtk_widget_show (but);

    but = gtk_button_new_with_label (_("Cancel"));
    gtk_box_pack_start (GTK_BOX (GTK_DIALOG(colorseldlg)->action_area), but, TRUE, TRUE, 10);
    gtk_signal_connect_object (GTK_OBJECT (but), "clicked",
                               GTK_SIGNAL_FUNC (gtk_widget_destroy), GTK_OBJECT(colorseldlg));
    gtk_widget_show (but);

    gtk_color_selection_set_color(GTK_COLOR_SELECTION(colorsel), colors);
    color_changed(colorsel, testcol);

    gtk_grab_add (GTK_WIDGET (colorseldlg));
    gtk_widget_show(colorseldlg);

}

static void add_color_label(int colpos, GtkWidget *box, char *text)
{
    GtkWidget *label;
    GtkWidget *button;
    GtkWidget *hbox;

    hbox = gtk_hbox_new(FALSE, 0);
    gtk_container_border_width (GTK_CONTAINER (hbox), 2);
    gtk_box_pack_start(GTK_BOX(box), hbox, FALSE, FALSE, 0);
    gtk_widget_show(hbox);

    button = gtk_button_new();
    gtk_signal_connect (GTK_OBJECT (button), "clicked",
                        GTK_SIGNAL_FUNC(select_color), button);
    gtk_widget_set_usize(button, 20, -1);
    gtk_box_pack_start(GTK_BOX(hbox), button, FALSE, FALSE, 0);
    gtk_widget_show(button);

    gtk_widget_realize(button);
    color_buttons[colpos] = button;

    label = gtk_label_new(text);
    gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 5);
    gtk_widget_show(label);
}

static void refresh_colors(void)
{
    GtkStyle style;
    GdkColor *color;
    int n;

    if (color_buttons[0] == NULL) return;

    memcpy(&style, gtk_widget_get_style(color_buttons[0]), sizeof(GtkStyle));
    for (n = 0; n < sizeof(color_buttons)/sizeof(color_buttons[0]); n++)
    {
        if (n == 0)
            color = &curtheme->fg;
        else if (n == 1)
            color = &curtheme->bg;
        else
            color = &curtheme->colors[n-2];
        memcpy(&style.bg[0], color, sizeof(GdkColor));
        gtk_widget_set_style(color_buttons[n], &style);
        gtk_object_set_data(GTK_OBJECT(color_buttons[n]), "color", color);
    }

    if (curtheme->pixmap == NULL)
        gtk_entry_set_text(GTK_ENTRY(backg_text), "");
    else
        gtk_entry_set_text(GTK_ENTRY(backg_text), curtheme->pixmap);
}

/* add theme record to GList with sort */
int gui_add_theme_rec(GList **themes, char *data)
{
    SETUP_THEME_REC *theme;
    GList *glist;
    int pos;

    glist = g_list_first(*themes);
    for (pos = 0; glist != NULL; pos++, glist = glist->next)
    {
        theme = (SETUP_THEME_REC *) glist->data;
        if (strcasecmp(theme->name, data) > 0) break;
    }

    theme = g_new(SETUP_THEME_REC, 1);
    theme->name = g_strdup(data);
    theme->fg = default_color;
    theme->bg = default_bgcolor;
    theme->pixmap = default_text_pixmap == NULL ? NULL : g_strdup(default_text_pixmap);
    memcpy(theme->colors, yag_colors, sizeof(yag_colors));

    *themes = g_list_insert(*themes, theme, pos);

    return pos;
}

/* remove theme record from list */
void gui_remove_theme_rec(GList **themes, SETUP_THEME_REC *theme)
{
    if (theme->pixmap != NULL) g_free(theme->pixmap);
    g_free(theme->name);

    *themes = g_list_remove(*themes, theme);
    g_free(theme);
}

/* add theme record to GtkList and GList */
static void add_theme(GtkList *list, char *data)
{
    GtkWidget *li;
    GList *glist;
    int pos;

    pos = gui_add_theme_rec(&themes, data);

    li = gtk_list_item_new_with_label(data);
    gtk_object_set_data(GTK_OBJECT(li), "theme", g_list_nth(themes, pos)->data);
    gtk_widget_show(li);

    glist = g_list_append(NULL, li);
    gtk_list_insert_items(list, glist, pos);

    gtk_list_select_child(list, li);
}

/* signal: ok button pressed in theme dialog */
static void sig_theme_ok(GtkWidget *window)
{
    GtkCombo *combo;
    GtkEntry *entry;
    GList *tmp;
    char *theme;
    int found;

    combo = gtk_object_get_data(GTK_OBJECT(window), "data");
    entry = gtk_object_get_data(GTK_OBJECT(window), "entry");

    theme = gtk_entry_get_text(entry);
    found = 0;
    for (tmp = g_list_first(themes); tmp != NULL; tmp = tmp->next)
    {
        SETUP_THEME_REC *rec;

        rec = (SETUP_THEME_REC *) tmp->data;
        if (strcasecmp(rec->name, theme) == 0)
        {
            found = 1;
            break;
        }
    }

    if (*theme != '\0' && !found)
    {
        if (default_text_pixmap != NULL) g_free(default_text_pixmap);
        default_text_pixmap = curtheme->pixmap == NULL ? NULL :
            g_strdup(curtheme->pixmap);
        default_color = curtheme->fg;
        default_bgcolor = curtheme->bg;
        memcpy(yag_colors, curtheme->colors, sizeof(yag_colors));

        add_theme(GTK_LIST(combo->list), theme);
    }

    gtk_widget_destroy(window);
}

/* signal: new theme button pressed */
static void new_theme(GtkWidget *widget, GtkCombo *combo)
{
    gui_entry_dialog(_("Theme name:"), NULL,
                     GTK_SIGNAL_FUNC(sig_theme_ok), combo);
}

/* signal: delete theme button pressed */
static void delete_theme(GtkWidget *widget, GtkCombo *combo)
{
    SETUP_THEME_REC *theme;
    GtkList *list;
    GList *glist;

    g_return_if_fail(combo != NULL);

    list = GTK_LIST(combo->list);
    if (list->selection == NULL || list->children->next == NULL) return;

    theme = gtk_object_get_data(GTK_OBJECT(list->selection->data), "theme");

    glist = g_list_append(NULL, list->selection->data);
    gtk_list_remove_items(list, glist);

    gui_remove_theme_rec(&themes, theme);
}

/* signal: theme changed */
static void sig_listchange(GtkList *list, GtkWidget *child)
{
    SETUP_THEME_REC *newtheme;

    if (child == NULL) return;

    newtheme = gtk_object_get_data(GTK_OBJECT(child), "theme");
    if (newtheme == curtheme) return;

    if (curtheme != NULL)
    {
        if (curtheme->pixmap != NULL) g_free(curtheme->pixmap);
        gui_get_label(curtheme->pixmap, backg_text);
        if (*curtheme->pixmap == '\0')
        {
            g_free(curtheme->pixmap);
            curtheme->pixmap = NULL;
        }
    }

    curtheme = newtheme;
    refresh_colors();
}

void gui_setup_outlook(GtkWidget *vbox)
{
    GtkWidget *frame;
    GtkWidget *combo;
    GtkWidget *vbox2, *hbox;
    GtkWidget *button;
    int n;

    color_buttons[0] = NULL;

    /* theme-frame */
    frame = gtk_frame_new(_("Theme"));
    gtk_box_pack_start(GTK_BOX(vbox), frame, TRUE, TRUE, 0);
    gtk_widget_show(frame);

    vbox2 = gtk_vbox_new(FALSE, 0);
    gtk_container_border_width(GTK_CONTAINER(vbox2), 5);
    gtk_container_add(GTK_CONTAINER(frame), vbox2);
    gtk_widget_show(vbox2);

    combo = gtk_combo_new();
    themelist = GTK_LIST(GTK_COMBO(combo)->list);
    gtk_entry_set_editable(GTK_ENTRY(GTK_COMBO(combo)->entry), FALSE);
    gtk_box_pack_start(GTK_BOX(vbox2), combo, FALSE, FALSE, 0);
    gtk_widget_show(combo);

    hbox = gtk_hbox_new(FALSE, 0);
    gtk_box_pack_start(GTK_BOX(vbox2), hbox, TRUE, TRUE, 10);
    gtk_widget_show(hbox);

    button = gtk_button_new_with_label(_("New"));
    gtk_signal_connect (GTK_OBJECT (button), "clicked",
                        GTK_SIGNAL_FUNC(new_theme), combo);
    gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 10);
    gtk_widget_show(button);

    button = gtk_button_new_with_label(_("Delete"));
    gtk_signal_connect (GTK_OBJECT (button), "clicked",
                        GTK_SIGNAL_FUNC(delete_theme), combo);
    gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 10);
    gtk_widget_show(button);

    /* Background image - frame */
    frame = gtk_frame_new(_("Background image"));
    gtk_box_pack_start(GTK_BOX(vbox2), frame, TRUE, TRUE, 0);
    gtk_widget_show(frame);

    hbox = gtk_hbox_new(FALSE, 0);
    gtk_container_border_width(GTK_CONTAINER(hbox), 5);
    gtk_container_add(GTK_CONTAINER(frame), hbox);
    gtk_widget_show(hbox);

    backg_text = gtk_entry_new();
    gtk_box_pack_start(GTK_BOX(hbox), backg_text, TRUE, TRUE, 0);
    gtk_widget_show(backg_text);

    button = gtk_button_new_with_label(_("browse"));
    gtk_signal_connect (GTK_OBJECT (button), "clicked",
                        GTK_SIGNAL_FUNC(select_back), NULL);
    gtk_box_pack_start(GTK_BOX(hbox), button, FALSE, FALSE, 10);
    gtk_widget_show(button);

    if (themes == NULL)
        add_theme(GTK_LIST(GTK_COMBO(combo)->list), _("default theme"));
    else
    {
        GList *tmp, *glist;
        GtkWidget *li, *sel;

        sel = NULL;
        for (tmp = g_list_first(themes); tmp != NULL; tmp = tmp->next)
        {
            SETUP_THEME_REC *data;

            data = (SETUP_THEME_REC *) tmp->data;

            li = gtk_list_item_new_with_label(data->name);
            gtk_object_set_data(GTK_OBJECT(li), "theme", data);
            gtk_widget_show(li);
            if (sel == NULL || data == default_theme) sel = li;

            glist = g_list_append(NULL, li);
            gtk_list_append_items(GTK_LIST(GTK_COMBO(combo)->list), glist);
        }

        gtk_list_select_child(GTK_LIST(GTK_COMBO(combo)->list), sel);
    }
    curtheme = default_theme != NULL ? default_theme :
        g_list_first(themes)->data;

    for (n = 0; n < sizeof(color_buttons)/sizeof(color_buttons[0]); n++)
    {
        GdkColor *color;

        if (n == 0)
            color = &curtheme->fg;
        else if (n == 1)
            color = &curtheme->bg;
        else
            color = &curtheme->colors[n-2];
        color->pixel = (gulong)
            ((color->red & 0xff00)*256 +
             (color->green & 0xff00) +
             (color->blue & 0xff00)/256);
        gdk_color_alloc(gtk_widget_get_colormap(vbox), color);
    }

    /* Channel colors - frame */
    frame = gtk_frame_new(_("Channel colors"));
    gtk_box_pack_start(GTK_BOX(vbox2), frame, TRUE, TRUE, 5);
    gtk_widget_show(frame);

    hbox = gtk_hbox_new(FALSE, 0);
    gtk_container_add(GTK_CONTAINER(frame), hbox);
    gtk_widget_show(hbox);

    vbox2 = gtk_vbox_new(FALSE, 0);
    gtk_box_pack_start(GTK_BOX(hbox), vbox2, TRUE, TRUE, 5);
    gtk_widget_show(vbox2);

    add_color_label(0, vbox2, _("Default text color"));
    add_color_label(1, vbox2, _("Default background"));
    add_color_label(14, vbox2, _("Server notices"));
    add_color_label(16, vbox2, _("Channel notices (join, quit, etc)"));
    add_color_label(13, vbox2, _("Error messages"));
    add_color_label(12, vbox2, _("yagIRC's notices"));
    add_color_label(3, vbox2, _("Public message to you"));
    add_color_label(4, vbox2, _("Channel name in <nick:#chan>"));
    add_color_label(2, vbox2, _("Nick color for text you've written"));

    vbox2 = gtk_vbox_new(FALSE, 0);
    gtk_box_pack_start(GTK_BOX(hbox), vbox2, TRUE, TRUE, 5);
    gtk_widget_show(vbox2);

    add_color_label(11, vbox2, _("CTCP messages"));
    add_color_label(15, vbox2, _("DCC messages"));
    add_color_label(5, vbox2, _("/msg to you"));
    add_color_label(6, vbox2, _(" - second color"));
    add_color_label(7, vbox2, _("You sent /msg"));
    add_color_label(8, vbox2, _(" - second color"));
    add_color_label(9, vbox2, _("/me color"));
    add_color_label(10, vbox2, _(" - second color"));

    gtk_signal_connect (GTK_OBJECT (GTK_COMBO(combo)->list), "select_child",
                        GTK_SIGNAL_FUNC(sig_listchange), combo);
    refresh_colors();
}

/* OK button pressed in dialog - accept changes */
void gui_setup_outlook_accept(void)
{
    if (default_text_pixmap != NULL) g_free(default_text_pixmap);

    if (themelist->selection != NULL)
        default_theme = gtk_object_get_data(GTK_OBJECT(themelist->selection->data), "theme");

    if (curtheme->pixmap != NULL) g_free(curtheme->pixmap);
    gui_get_label(curtheme->pixmap, backg_text);
    if (*curtheme->pixmap == '\0')
    {
        g_free(curtheme->pixmap);
        curtheme->pixmap = NULL;
    }

    default_text_pixmap = default_theme->pixmap == NULL ? NULL :
        g_strdup(default_theme->pixmap);
    default_color = default_theme->fg;
    default_bgcolor = default_theme->bg;
    memcpy(yag_colors, default_theme->colors, sizeof(yag_colors));
}
