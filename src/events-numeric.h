#include "config.h"

#ifndef __EVENTS_NUMERIC_H
#define __EVENTS_NUMERIC_H

#include "data.h"

/* numeric events */
void eirc_welcome(char *data);
void eirc_connected(char *data);
void eirc_ison(char *data);
void eirc_nick_in_use(char *data);
void eirc_nick_unavailable(char *data);
void eirc_names_list(char *data);
void eirc_end_of_names(char *data);
void eirc_topic(char *data);
void eirc_topic_info(char *data);
void eirc_who(char *data);
void eirc_end_of_who(char *data);
void eirc_ban_list(char *data);
void eirc_end_of_banlist(char *data);
void eirc_too_many_channels(char *data);
void eirc_channel_is_full(char *data);
void eirc_invite_only(char *data);
void eirc_banned(char *data);
void eirc_bad_channel_key(char *data);
void eirc_bad_channel_mask(char *data);
void eirc_channel_mode(char *data);
void eirc_channel_created(char *data);
void eirc_away(char *data);
void eirc_unaway(char *data);
void eirc_whois(char *data);
void eirc_whois_idle(char *data);
void eirc_whois_server(char *data);
void eirc_whois_oper(char *data);
void eirc_whois_channels(char *data);
void eirc_end_of_whois(char *data);
void eirc_list_start(char *data);
void eirc_list(char *data);
void eirc_list_end(char *data);

#endif
