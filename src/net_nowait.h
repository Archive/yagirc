#include "config.h"

#ifndef __NET_NOWAIT_H
#define __NET_NOWAIT_H

typedef void (*NET_CALLBACK) (int, void *);

int net_nowait_connect(char *server, int port, NET_CALLBACK func, void *data);

int net_nowait_connect_new (char *server,
			    int port,
			    int pipe);

#endif
