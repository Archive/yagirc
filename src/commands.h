#include "config.h"

#ifndef __COMMANDS_H
#define __COMMANDS_H

int irccmd_server(char *data);
int irccmd_servers(char *data);
int irccmd_disconnect(char *data);
int irccmd_quit(char *data);

int irccmd_dcc(char *data);
int irccmd_window(char *data);
int irccmd_load(char *data);
int irccmd_unload(char *data);
int irccmd_quote(char *data);
int irccmd_clear(char *data);
int irccmd_log(char *data);
int irccmd_logs(char *data);
int irccmd_echo(char *data);
int irccmd_play(char *data);

int irccmd_join(char *data);
int irccmd_whois(char *data);
int irccmd_query(char *data);
int irccmd_unquery(char *data);
int irccmd_part(char *data);
int irccmd_mode(char *data);
int irccmd_modes(char *data);
int irccmd_topic(char *data);
int irccmd_kill(char *data);
int irccmd_away(char *data);
int irccmd_awayall(char *data);
int irccmd_wallops(char *data);
int irccmd_oper(char *data);
int irccmd_say(char *data);

int irccmd_ison(char *data);
int irccmd_ctcp(char *data);
int irccmd_msg(char *data);
int irccmd_notice(char *data);
int irccmd_me(char *data);

int irccmd_op(char *data);
int irccmd_deop(char *data);
int irccmd_voice(char *data);
int irccmd_devoice(char *data);
int irccmd_kick(char *data);
int irccmd_kickban(char *data);
int irccmd_knockout(char *data);
int irccmd_ban(char *data);
int irccmd_banstat(char *data);
int irccmd_wallop(char *data);
int irccmd_list(char *data);

#endif
