#include "config.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

#include <glib.h>
#include <gtk/gtk.h>

#include "intl.h"
#include "data.h"
#include "irc.h"
#include "gui_urllist.h"
#include "misc.h"

static int doubleclick;
static int select_row;

void graburls(char **data, char *nick, char *channel)
{
  char *item, *temp, *datatmp, final[2048];

  if ((*data == NULL) || !(strstr(*data, "http://") || strstr(*data, "ftp://") ||
	strchr(*data, '@') || strstr(*data, " #"))) return;

  final[0] = '\0';
  datatmp = g_strdup(*data);
  while(datatmp != NULL) 
  {
    item = g_strdup(datatmp);
    temp = strchr(item, ' ');
    if (temp != NULL) *temp = '\0';
    item = g_realloc(item, strlen(item)+1);
    datatmp = strchr(datatmp, ' ');
    if (!strncmp(item, "http://", 7) || !strncmp(item, "ftp://", 6) || 
	strchr(item, '@') || *item == '#') 
    {
      strcat(final, "%B%_%!"); strcat(final, item); strcat(final, "%n%_%!");
      if (strchr(item, '@')) 
      {
         temp = g_malloc(strlen(item)+8);
         strcpy(temp, "mailto:");
         strcat(temp, item);
         catalogurl(temp, nick, channel);
      }
      else
         catalogurl(item, nick, channel);
    } 
    else
      strcat(final, item);
    g_free(item);
    if (datatmp) 
    {
      datatmp++;
      strcat(final, " ");
    }
  }
  g_free(datatmp);
  final[strlen(final)] = '\0';
  *data = g_malloc(strlen(final));
  strcpy(*data, final);
}

void catalogurl(char *data, char *nick, char *channel)
{
  URL_REC *temp;

  /* dont catalog if url catcher is turned off */
  if (!global_settings->urlcatch) return;

  temp = g_malloc(sizeof(URL_REC));

  if (channel == NULL) channel = strdup(nick);

  temp->url = strdup(data);
  temp->nick = strdup(nick);
  temp->channel = strdup(channel);

  urllist = g_list_append(urllist, temp);
}

extern int irccmd_join(char *);

/* browse to url */
int url_browse(char *url)
{
  int ret=0;
  char *tempexe;

  if (*url == '#')
    irccmd_join(url);
  else {
    if (global_settings->browserpath == NULL || url == NULL) return(0);
    tempexe = g_new0(char, strlen(global_settings->browserpath)+strlen(url)+5);
    sprintf(tempexe, "%s %s &", global_settings->browserpath, url);
    ret = system(tempexe);
    g_free(tempexe);
  }

  return(ret);
}

/* add a url to the url catcher window */
void gui_urllist_add(URLWIN_REC *urlwin, URL_REC *url)
{
  char *titles[] = { url->url, url->nick, url->channel };
  g_return_if_fail(urlwin != NULL && url != NULL);
  gtk_clist_append(GTK_CLIST(urlwin->list), titles);
}

void gui_urllist_select(GtkWidget *clist, gint row, gint column, GdkEventButton *event, gpointer data)
{
  select_row = row;
}

void gui_urllist_press(GtkCList *list, GdkEventButton *event)
{
  int ass;
  gchar *url;
  if (event->button == 1) {
    if (!doubleclick) doubleclick = 1;
    else {
      doubleclick = 0;
      gtk_clist_get_text(list, select_row, 0, &url);
      ass = url_browse(url);
      if (ass < 0 || ass == 127) drawtext(NULL, NULL, LEVEL_YAGNOTICE, _("Error browsing URL: %s\n"), strerror(errno));
    }
  }
}

void gui_urllist_release(GtkCList *list, GdkEventButton *event)
{
  if (event->button == 1) doubleclick = 0;
}

/* create window with caught urls in it */
void gui_urllist_show(GList *urls)
{
  GList *url;
  URLWIN_REC *urlwin;
  char *titles[] = { N_("URL"), N_("Nick"), N_("Channel") };
  GtkWidget *sw;

/*  if (urls == NULL)
  {
    drawtext(NULL, NULL, LEVEL_YAGNOTICE, "No URLs to list!@~\n");
    return;
  }
*/

  /* setup the widgetz */
  urlwin = g_new0(URLWIN_REC, 1);
  urlwin->urls = urls;
  urlwin->win = gtk_window_new(GTK_WINDOW_TOPLEVEL);

#ifdef ENABLE_NLS
  titles[0]=_(titles[0]);
  titles[1]=_(titles[1]);
  titles[2]=_(titles[2]);
#endif

  urlwin->list = gtk_clist_new_with_titles(3, titles);
  urlwin->mainbox = gtk_vbox_new(FALSE, 0);
  urlwin->buttonbox = gtk_hbox_new(TRUE, 5);
  urlwin->clearbutt = gtk_button_new_with_label(_("Clear"));
  urlwin->closebutt = gtk_button_new_with_label(_("Close"));

  sw = gtk_scrolled_window_new(NULL, NULL);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(sw), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

  /* configure the list box */
  gtk_clist_set_selection_mode(GTK_CLIST(urlwin->list), GTK_SELECTION_BROWSE);
  gtk_clist_column_titles_passive(GTK_CLIST(urlwin->list));
  gtk_clist_set_column_width(GTK_CLIST(urlwin->list), 0, 140);
  gtk_clist_set_column_width(GTK_CLIST(urlwin->list), 1, 60);

  /* configure the other shit */
  gtk_object_set_data(GTK_OBJECT(urlwin->win), "rec", urlwin);
  gtk_window_set_title(GTK_WINDOW(urlwin->win), _("URL Listing"));
  gtk_signal_connect(GTK_OBJECT(urlwin->win), "delete_event", GTK_SIGNAL_FUNC(gui_urllist_kill), NULL);
  gtk_signal_connect_object(GTK_OBJECT(urlwin->closebutt), "clicked", GTK_SIGNAL_FUNC(gui_urllist_kill), GTK_OBJECT(urlwin->win));
  gtk_signal_connect_object(GTK_OBJECT(urlwin->clearbutt), "clicked", GTK_SIGNAL_FUNC(gui_urllist_clear), GTK_OBJECT(urlwin->win));
  gtk_signal_connect(GTK_OBJECT(urlwin->list), "button_press_event", GTK_SIGNAL_FUNC(gui_urllist_press), NULL);
  gtk_signal_connect(GTK_OBJECT(urlwin->list), "button_release_event", GTK_SIGNAL_FUNC(gui_urllist_release), NULL);
  gtk_signal_connect(GTK_OBJECT(urlwin->list), "select_row", GTK_SIGNAL_FUNC(gui_urllist_select), NULL);

  /* lay out widgets */
  gtk_box_pack_start(GTK_BOX(urlwin->buttonbox), urlwin->clearbutt, TRUE, TRUE, 5);
  gtk_box_pack_start(GTK_BOX(urlwin->buttonbox), urlwin->closebutt, TRUE, TRUE, 5);
  //cc gtk_container_add (GTK_CONTAINER (sw), urlwin->list);
  gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(sw), urlwin->list);
  gtk_box_pack_start(GTK_BOX(urlwin->mainbox), sw, TRUE, TRUE, 0);
  gtk_box_pack_start(GTK_BOX(urlwin->mainbox), urlwin->buttonbox, FALSE, TRUE, 0);
  gtk_container_add(GTK_CONTAINER(urlwin->win), urlwin->mainbox);

  /* add urls to the listbox */
  if (urls != NULL)
  {
    GLIST_FOREACH(url, urls)
    {
      gui_urllist_add(urlwin, url->data);
    }
  }

  /* display the widgets */
  gtk_widget_show(urlwin->list);
  gtk_widget_show(urlwin->clearbutt);
  gtk_widget_show(urlwin->closebutt);
  gtk_widget_show(urlwin->buttonbox);
  gtk_widget_show(urlwin->mainbox);
  gtk_widget_set_usize(urlwin->win, 300, 200);
  gtk_widget_show(urlwin->win);
}

/* destroy url catcher window */
void gui_urllist_kill(GtkWidget *widget, void *dizata)
{
  URLWIN_REC *rec = gtk_object_get_data(GTK_OBJECT(widget), "rec");
  gtk_widget_destroy(rec->win);
  rec->win = NULL; rec->list = NULL; rec->closebutt = NULL; rec->clearbutt = NULL; rec->mainbox = NULL; rec->buttonbox = NULL; rec->urls = NULL;
  g_free(rec);
}

/* clear url list */
void gui_urllist_clear(GtkWidget *widget, gpointer data)
{
  URLWIN_REC *rec = gtk_object_get_data(GTK_OBJECT(widget), "rec");
/*  if (rec->urls != NULL) g_list_free(rec->urls);*/
  rec->urls = NULL;
  urllist = NULL;
  gtk_clist_clear(GTK_CLIST(rec->list));
}
