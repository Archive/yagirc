/*

 gui_menu.c : Menu commands

    Copyright (C) 1998 Timo Sirainen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "config.h"

#include <stdio.h>

#include "os.h"
#include "gui.h"
#include "gui_urllist.h"
#include "irc.h"
#include "version.h"
#include "commands.h"
#include "intl.h"

/* add menuitem to menu */
void add_popup(GtkWidget *menu, char *label, void *func, void *data)
{
    GtkWidget *menu_item;

    g_return_if_fail(menu != NULL);

    if (label != NULL)
        menu_item = gtk_menu_item_new_with_label(label);
    else
        menu_item = gtk_menu_item_new();

    gtk_menu_append(GTK_MENU (menu), menu_item);

    if (func != NULL)
    {
        gtk_signal_connect_object(GTK_OBJECT(menu_item), "activate",
                                  GTK_SIGNAL_FUNC(func),
                                  (GtkObject *) data);
    }
    else
        gtk_widget_set_sensitive(menu_item, FALSE);

    gtk_widget_show(menu_item);
}

/* add submenu to menu */
void add_popup_sub(GtkWidget *menu, char *label, GtkWidget *submenu)
{
    GtkWidget *menu_item;

    g_return_if_fail(menu != NULL);
    g_return_if_fail(label != NULL);

    menu_item = gtk_menu_item_new_with_label(label);

    gtk_menu_append(GTK_MENU(menu), menu_item);
    gtk_menu_item_set_submenu(GTK_MENU_ITEM(menu_item), submenu);
    gtk_widget_show(menu_item);
}


void menu_irc_connect(GtkWidget *widget, gpointer data)
{
    gui_connect_dialog();
}

void menu_irc_disconnect(GtkWidget *widget, gpointer data)
{
    gui_servers_dialog();
}

void menu_irc_setup(GtkWidget *widget, gpointer data)
{
    gui_setup();
}

void menu_irc_quit(GtkWidget *widget, gpointer data)
{
    irccmd_quit("\0");
}

void menu_command_join(GtkWidget *widget, gpointer data)
{
    if (cserver != NULL)
        gui_join_dialog(cserver);
}

void menu_command_part(GtkWidget *widget, gpointer data)
{
    irccmd_part("");
}

void menu_window_new(GtkWidget *widget, gpointer data)
{
    char tmp[4] = "NEW";

    irccmd_window(tmp);
}

void menu_window_new_hidden(GtkWidget *widget, gpointer data)
{
    char tmp[9] = "NEW HIDE";

    irccmd_window(tmp);
}

void menu_window_close(GtkWidget *widget, gpointer data)
{
    char tmp[6] = "CLOSE";

    irccmd_window(tmp);
}

void menu_window_select_server(GtkWidget *widget, gpointer data)
{
    gui_servers_dialog();
}

void menu_window_show_urllist(GtkWidget *widget, gpointer data)
{
  gui_urllist_show(urllist);
}

void menu_help_about(GtkWidget *widget, gpointer data)
{
#ifdef USE_GNOME
    GtkWidget *about;
    gchar *authors[] = { "Marcus Brubaker <spoon@elpaso.net>",
                         "Ben Pierce <croooow@together.net>", 
                         "Timo Sirainen <a@sicom.fi>", NULL };

    about = gnome_about_new(_("yagIRC"), IRC_VERSION_NUMBER,
                            /* copyrigth notice */
                            _("(C) 1998 Timo Sirainen"),
                            authors,
                            /* another comments */
                            _("Yet Another GTK+ based IRC client"),
                            NULL);
    gtk_widget_show(about);
#else
    GtkWidget *dialog;
    GtkWidget *label;
    GtkWidget *button;

    dialog = gtk_dialog_new();

    label = gtk_label_new(_("Yet Another GTK+ based IRC client\n(C) 1998 Timo Sirainen <a@sicom.fi>\nMaintainers:\n  Marcus Brubaker <spoon@elpaso.net>\n  Ben Pierce <croooow@together.net>\n\nhttp://yagirc.home.ml.org"));
    gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->vbox), label, TRUE, TRUE, 10);
    gtk_widget_show(label);

    button = gtk_button_new_with_label (_("Ok"));
    gtk_box_pack_start (GTK_BOX (GTK_DIALOG(dialog)->action_area), button, TRUE, TRUE, 10);
    gtk_signal_connect_object (GTK_OBJECT (button), "clicked",
                               GTK_SIGNAL_FUNC (gtk_widget_destroy), GTK_OBJECT(dialog));
    GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT);
    gtk_widget_grab_default (button);
    gtk_widget_show (button);

    gtk_widget_show(dialog);
#endif
}
