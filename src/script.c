/*

 script.c : PERL script interface

    Copyright (C) 1998 Jere Sanisalo <jeress@iname.com> and Timo Sirainen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "config.h"

#ifdef USE_SCRIPT
#include <stdio.h>
#include <string.h>
#include <sys/param.h>

#ifndef HAS_BOOL
#define HAS_BOOL
#define bool char
#endif

#include <EXTERN.h>
#include <XSUB.h>

#include <glib.h>

#include "os.h"
#include "script_txt.h"
#include "irc.h"
#include "ctcp.h"
#include "script.h"
#include "events.h"
#include "commands.h"
#include "params.h"
#include "options.h"
#include "version.h"
#include "misc.h"

#undef PACKAGE
#undef _
#include <perl.h>

enum
{
    SCRIPT_EVENT_UNKNOWN,
    SCRIPT_EVENT_COMMAND,
    SCRIPT_EVENT_NUMERIC,

    SCRIPT_EVENT_ACTION,
    SCRIPT_EVENT_CONNECTED,
    SCRIPT_EVENT_CTCP,
    SCRIPT_EVENT_DCCMSG,
    SCRIPT_EVENT_DISCONNECTED,
    SCRIPT_EVENT_INVITE,
    SCRIPT_EVENT_IRCJOIN,
    SCRIPT_EVENT_IRCPART,
    SCRIPT_EVENT_JOIN,
    SCRIPT_EVENT_KICK,
    SCRIPT_EVENT_MODE,
    SCRIPT_EVENT_PART,
    SCRIPT_EVENT_PRGQUIT,
    SCRIPT_EVENT_PRIVMSG,
    SCRIPT_EVENT_PUBMSG,
    SCRIPT_EVENT_SERVERMSG,
    SCRIPT_EVENT_TOPIC,
    SCRIPT_EVENT_QUIT,
};

typedef struct
{
    int type;
    char *name;

    int sender_pos;
    int sender_host_pos;
}
EVENT_LIST_REC;

static EVENT_LIST_REC events[] =
{
    { SCRIPT_EVENT_ACTION, "ACTION", 1, 2 },
    { SCRIPT_EVENT_CONNECTED, "CONNECTED", 0, 0 },
    { SCRIPT_EVENT_CTCP, "CTCP", 1, 2 },
    { SCRIPT_EVENT_DCCMSG, "DCCMSG", 1, 2 },
    { SCRIPT_EVENT_DISCONNECTED, "DISCONNECTED", 0, 0 },
    { SCRIPT_EVENT_INVITE, "INVITE", 1, 2 },
    { SCRIPT_EVENT_IRCJOIN, "IRCJOIN", 1, 2 },
    { SCRIPT_EVENT_IRCPART, "IRCPART", 1, 2 },
    { SCRIPT_EVENT_JOIN, "JOIN", 1, 2 },
    { SCRIPT_EVENT_KICK, "KICK", 1, 0 },
    { SCRIPT_EVENT_MODE, "MODE", 1, 0 },
    { SCRIPT_EVENT_PART, "PART", 1, 2 },
    { SCRIPT_EVENT_PRGQUIT, "PRGQUIT", 0, 0 },
    { SCRIPT_EVENT_PRIVMSG, "PRIVMSG", 1, 2 },
    { SCRIPT_EVENT_PUBMSG, "PUBMSG", 1, 0 },
    { SCRIPT_EVENT_SERVERMSG, "SERVERMSG", 0, 0 },
    { SCRIPT_EVENT_TOPIC, "TOPIC", 1, 2 },
    { SCRIPT_EVENT_QUIT, "QUIT", 1, 2 },
    { 0, NULL }
};

static EVENT_LIST_REC event_numeric =
{ SCRIPT_EVENT_NUMERIC, NULL, 1, 2 };

static EVENT_LIST_REC event_command =
{ SCRIPT_EVENT_COMMAND, NULL, 0, 0 };

typedef struct
{
    int id;
    int type;
    int num;
    int server;
    int timeouttag;
    char *name;
    char *spec;
    char *func;
}
EVENT_REC;

typedef struct
{
    int id;
    int type;
    int server;
    char *name;
    char *spec;
    char *func;
}
MENU_REC;

typedef struct
{
    int id;
    unsigned long timeout;
    char *func;
    char **args;
}
TIMEOUT_REC;

static PerlInterpreter *perl_script;

static GList *event_list;
static int event_ids;

static GList *popup_menu;
static int popup_ids;

static GList *timeout_list;

static int call_perl(char *name, char *args[]);
static void xs_init();

static SERVER_REC *sserver;

/* remove menu item from list */
static void remove_menu(MENU_REC *menu)
{
    popup_menu = g_list_remove(popup_menu, menu);
    g_free(menu->name);
    g_free(menu->spec);
    g_free(menu->func);
    g_free(menu);
}

/* remove timeout from list */
static void remove_timeout(TIMEOUT_REC *rec)
{
    int pos;

    timeout_list = g_list_remove(timeout_list, rec);

    gui_timeout_remove(rec->id);
    g_free(rec->func);
    for (pos = 0; rec->args[pos] != NULL; pos++)
        g_free(rec->args[pos]);
    g_free(rec->args);
    g_free(rec);
}

/* remove event from list */
static void remove_event(EVENT_REC *event)
{
    event_list = g_list_remove(event_list, event);

    gui_timeout_remove(event->timeouttag);

    g_free(event->spec);
    g_free(event->func);
    g_free(event);
}

void script_init(void)
{
    perl_script = NULL;
    event_list = NULL; event_ids = 0;
    popup_menu = NULL; popup_ids = 0;
    timeout_list = NULL;
    sserver = NULL;
}

void script_deinit(void)
{
    script_event(eserver, NULL, NULL, "PRGQUIT", "");

    if(perl_script!=NULL)
    {
        perl_destruct(perl_script);
        perl_free(perl_script);

        perl_script=NULL;
    }

    while (event_list != NULL)
        remove_event((EVENT_REC *) event_list->data);

    while (popup_menu != NULL)
        remove_menu((MENU_REC *) popup_menu->data);

    while (timeout_list != NULL)
        remove_timeout((TIMEOUT_REC *) timeout_list->data);
}

int script_load(char *name)
{
    char *arglist[] = { "perl", "-w", NULL, NULL, NULL };
    struct stat statbuf;

    g_return_val_if_fail(name != NULL, RET_ERR_PARAM);

    script_deinit();

    perl_script=perl_alloc();
    if(perl_script==NULL)
    {
        g_warning("perl_alloc() failed\n");
        return RET_ERROR;
    }
    perl_construct(perl_script);

    arglist[2] = g_new(char, strlen(SCRIPT_PREFIX)+3);
    strcpy(arglist[2], "-I" SCRIPT_PREFIX);

    if (*name == '/' || (*name == '~' && name[1] == '/'))
        arglist[3] = convhome(name);
    else
    {
        /* No absolute path given, use $(prefix) */
        arglist[3] = g_new(char, strlen(SCRIPT_PREFIX)+strlen(name)+1);
        sprintf(arglist[3], SCRIPT_PREFIX "%s", name);
        if (stat(arglist[3], &statbuf) != 0)
        {
            /* file not found .. try without any prefixes */
            g_free(arglist[3]);
            arglist[3] = g_strdup(name);
        }
    }
    if (stat(arglist[3], &statbuf) != 0)
    {
        g_free(arglist[2]);
        g_free(arglist[3]);
        drawtext(NULL, NULL, LEVEL_YAGERROR, IRCTXT_SCRIPT_NOT_FOUND, name);
        script_deinit();
        return RET_ERROR;
    }

    if(perl_parse(perl_script,xs_init,3,arglist,(char **)NULL))
    {
        g_free(arglist[2]);
        g_free(arglist[3]);
        drawtext(NULL, NULL, LEVEL_YAGERROR, IRCTXT_SCRIPT_LOAD_ERROR, name);
        script_deinit();
        return RET_ERROR;
    }
    g_free(arglist[2]);
    g_free(arglist[3]);

    if(perl_run(perl_script))
    {
        g_warning("perl_run() failed\n");
        script_deinit();
        return RET_ERROR;
    }

    return RET_OK;
}

int script_event(SERVER_REC *sendserver, char *sendnick, char *sendaddr, char *event, char *data)
{
    EVENT_LIST_REC *erec;
    GList *tmp;
    int num;
    char *ptr, tmpnum[INTEGER_LENGTH+1];

    g_return_val_if_fail(event != NULL, 1);
    g_return_val_if_fail(data != NULL, 1);

    if (perl_script == NULL) return 1;
    if (event_list == NULL) return 1; /* there's no events grabbed.. */

    if (sscanf(event, "%d", &num) == 1)
    {
        /* numeric event */
        erec = &event_numeric;
    }
    else if (sendnick == NULL)
    {
        /* /command */
        erec = &event_command;
    }
    else
    {
        /* named event.. */
        if (strcasecmp(event, "PRIVMSG") == 0)
        {
            ptr = strchr(data, ' ');
            if (ptr != NULL && *(ptr+1) == 1)
            {
                /* CTCP message */
                if (strncmp(ptr+1, "ACTION ", 7) == 0)
                    event = "ACTION";
                else
                    event = "CTCP";
            }
            else if (*data == '#' || *data == '&')
            {
                /* this was public message */
                event = "PUBMSG";
            }
        }

        erec = NULL;
        for (num = 0; events[num].name != NULL; num++)
        {
            if (strcasecmp(event, events[num].name) == 0)
            {
                erec = &events[num];
                break;
            }
        }
        if (erec == NULL)
        {
            /* event wasn't recognized */
            return 1;
        }
    }

    /* now find every function that wants this event.. */
    GLIST_FOREACH(tmp, event_list)
    {
        EVENT_REC *ev;

        ev = (EVENT_REC *) tmp->data;
        if (ev->type == erec->type &&
            (erec->type != SCRIPT_EVENT_NUMERIC || num == ev->num) &&
            (erec->type != SCRIPT_EVENT_COMMAND || strcasecmp(ev->name, event) == 0))
        {
            /* found one! */
            int ret, chancheck;
            char *args[10], *datap, *startdata;

            startdata = datap = g_strdup(data);

            if (erec->sender_pos != 0)
                args[erec->sender_pos-1] = sendnick == NULL ? "" : sendnick;
            if (erec->sender_host_pos != 0)
                args[erec->sender_host_pos-1] = sendaddr == NULL ? "" : sendaddr;

            chancheck = -1; /* default: don't check if channel name matches */
            switch (ev->type)
            {
                case SCRIPT_EVENT_COMMAND:
                    args[0] = event;
                    args[1] = datap;
                    args[2] = NULL;
                    chancheck = 0;
                    break;
                case SCRIPT_EVENT_SERVERMSG:
                case SCRIPT_EVENT_CONNECTED:
                    sprintf(tmpnum, "%d", eserver->handle);
                    args[0] = tmpnum; /* server handle */
                    args[1] = datap; /* data */
                    args[2] = NULL;
                    chancheck = 0;
                    break;
                case SCRIPT_EVENT_DISCONNECTED:
                    args[0] = event_get_param(&datap);
                    args[1] = datap;
                    args[2] = NULL;
                    chancheck = 0;
                    break;
                case SCRIPT_EVENT_JOIN:
                    args[2] = event_get_param(&datap); /* channel */
                    args[3] = NULL;
                    chancheck = 2;
                    break;
                case SCRIPT_EVENT_PART:
                    args[2] = event_get_param(&datap); /* channel */
                    args[3] = datap; /* reason */
                    args[4] = NULL;
                    chancheck = 2;
                    break;
                case SCRIPT_EVENT_QUIT:
                    {
                        GList *tmp, *chans;
                        char *pos;
                        int len;

                        chans = NULL; len = 0;
                        GLIST_FOREACH(tmp, winlist)
                        {
                            WINDOW_REC *winrec;
                            GList *chan;

                            winrec = (WINDOW_REC *) tmp->data;
                            GLIST_FOREACH(chan, winrec->chanlist)
                            {
                                CHAN_REC *ch;

                                ch = (CHAN_REC *) chan->data;
                                if (find_nick_rec(ch, sendnick) != NULL)
                                {
                                    /* was joined here */
                                    len += strlen(ch->name)+1;
                                    chans = g_list_append(chans, chan->data);
                                }
                            }
                        }
                        args[2] = g_new(char, len); /* channel names user was joined.. */
                        pos = args[2];
                        GLIST_FOREACH(tmp, chans)
                        {
                            CHAN_REC *chan;

                            chan = (CHAN_REC *) tmp->data;
                            if (pos == args[2])
                                pos += sprintf(pos, "%s", chan->name);
                            else
                                pos += sprintf(pos, " %s", chan->name);
                        }
                        *pos = '\0';
                        g_list_free(chans);

                        args[3] = datap; /* reason */
                        args[4] = NULL;
                        break;
                    }
                case SCRIPT_EVENT_PRIVMSG:
                    args[2] = event_get_param(&datap); /* text */
                    args[3] = NULL;
                    chancheck = 0;
                    break;
                case SCRIPT_EVENT_PUBMSG:
                    args[2] = event_get_param(&datap); /* channel */
                    args[3] = datap; /* text */
                    args[4] = NULL;
                    chancheck = 2;
                    break;
                case SCRIPT_EVENT_ACTION:
                    args[2] = event_get_param(&datap); /* channel */
                    datap += 8; /* remove "\001ACTION "*/
                    ptr = strchr(datap, 1);
                    if (ptr != NULL) *ptr = '\0'; /* remove last \001 */
                    args[3] = datap; /* data */
                    args[4] = NULL;
                    chancheck = 2;
                    break;
                case SCRIPT_EVENT_CTCP:
                    args[2] = event_get_param(&datap); /* channel */
                    datap++; /* remove first \001 */
                    ptr = strchr(datap, 1);
                    if (ptr != NULL) *ptr = '\0'; /* remove last \001 */
                    args[3] = datap; /* data */
                    args[4] = NULL;
                    chancheck = 2;
                    break;
                case SCRIPT_EVENT_INVITE:
                    event_get_param(&datap); /* skip your name */
                    args[2] = event_get_param(&datap); /* channel */
                    args[3] = NULL;
                    args[4] = NULL;
                    chancheck = 2;
                    break;
                case SCRIPT_EVENT_KICK:
                    {
                        CHAN_REC *chan;
                        NICK_REC *nick;

                        args[1] = event_get_param(&datap); /* channel */
                        args[2] = event_get_param(&datap); /* kicked nick */
                        chan = channel_joined(sendserver, args[1]);
                        nick = chan == NULL ? NULL : find_nick_rec(chan, args[1]);
                        args[3] = nick == NULL ? "" : nick->host; /* kicked nick host */
                        args[4] = datap; /* reason */
                        args[5] = NULL;
                        chancheck = 1;
                        break;
                    }
                case SCRIPT_EVENT_MODE:
                    args[1] = event_get_param(&datap); /* channel */
                    args[2] = datap; /* mode */
                    args[3] = NULL;
                    chancheck = 1;
                    break;
                case SCRIPT_EVENT_TOPIC:
                    args[2] = event_get_param(&datap); /* channel */
                    args[3] = datap; /* topic */
                    args[4] = NULL;
                    chancheck = 2;
                    break;
                default:
                    args[0] = datap;
                    args[1] = NULL;
                    break;
            }

            if (chancheck != -1 && *ev->spec != '\0' && strcasecmp(ev->spec, args[chancheck]) != 0)
            {
                /* this event isn't supposed to be sent in this channel.. */
                ret = 1;
            }
            else
            {
                sserver = sendserver;
                ret = !call_perl(ev->func, args);
                sserver = NULL;
            }

            if (ev->type ==  SCRIPT_EVENT_QUIT)
                g_free(args[2]);

            g_free(startdata);

            if (!ret) return 0;
        }
    }

    return 1;
}

typedef struct
{
    MENU_REC *menu;
    char *data;
}
SCRIPT_PARAMS_REC;

static GList *script_params = NULL;

static void script_menu(SCRIPT_PARAMS_REC *sp)
{
    char *args[2] = { NULL, NULL };

    args[0] = sp->data;
    call_perl(sp->menu->func, args);
}

void script_add_popups(GtkWidget *menu, int menutype, char *data)
{
    GtkWidget *smenu;
    GList *tmp;

    g_return_if_fail(menu != NULL);
    g_return_if_fail(data != NULL);

    g_list_foreach(script_params, (GFunc) g_free, NULL);
    g_list_free(script_params);

    smenu = NULL;
    for (tmp = g_list_first(popup_menu); tmp != NULL; tmp = tmp->next)
    {
        MENU_REC *menu;

        menu = (MENU_REC *) tmp->data;
        if (menu->type == menutype &&
            (*menu->spec == '\0' || strcasecmp(menu->spec, data) == 0))
        {
            SCRIPT_PARAMS_REC *sp;

            if (smenu == NULL) smenu = gtk_menu_new();

            sp = g_new(SCRIPT_PARAMS_REC, 1);
            sp->menu = menu;
            sp->data = data;
            g_list_append(script_params, sp);

            add_popup(smenu, menu->name, script_menu, sp);
        }
    }

    if (smenu != NULL) add_popup_sub(menu, "Script", smenu);
}

/* C-commands used by perl */

/*  NOTES ON PERL INTERFACE
---
Types:
.         IV - Integer
.         NV - Double
.         PV - String
.         SV - Scalar

Creates new values:
.         SV*  newSViv(IV);
.         SV*  newSVnv(double);
.         SV*  newSVpv(char*, int);
.         SV*  newSVpvf(const char*, ...);
.         SV*  newSVsv(SV*);

Sets the values: (length can be set to 0!)
.         void  sv_setiv(SV*, IV);
.         void  sv_setnv(SV*, double);
.         void  sv_setpv(SV*, char*);
.         void  sv_setpvn(SV*, char*, int)
.         void  sv_setpvf(SV*, const char*, ...);
.         void  sv_setsv(SV*, SV*);

Converts the values to C:
.         SvIV(SV*)
.         SvNV(SV*)
.         SvPV(SV*,STRLEN len) (here the length is placed in len)

Checks if the value is TRUE:
.         SvTRUE(SV*)
Checks if a value is ok:
.         SvOK(SV*)

Checks if the value is of a type:
.         SvIOK(SV*)
.         SvNOK(SV*)
.         SvPOK(SV*)

.If you know the name of a scalar variable, you can get a
.pointer to its SV by using the following:
.         SV*  perl_get_sv("package::varname", FALSE);
.This returns NULL if the variable does not exist.

Some predefined values:
.         sv_no
.         sv_undef
.         sv_yes
---
NOTE ON STRINGS!

The strings may contains NUL-characters. The last char of the strings is not
necessarily a NUL, so be careful!
---
XS(xxx) predefined values:
.         ax - unknown
.         items - Number of items (use stack ST(x) to find 'em out (x=0..items))

The XS form goes like this (the dots at the start of the line are not
included):
XS(name)
{
.         dXSARGS;

...CODE...

...Return method (eg. XS_RETURN_YES)...
}

Return methods:
.         XSRETURN(v)         - Returns with v args (must be mortal values in
.                               at the start of the stack)
.         XSRETURN_IV(v)
.         XSRETURN_NV(v)
.         XSRETURN_PV(v)
.         XSRETURN_NO
.         XSRETURN_YES
.         XSRETURN_UNDEF
.         XSRETURN_EMPTY
*/

static char *dup0(char *str, int len)
{
    char *tmp;

    g_return_val_if_fail(str != NULL, NULL);

    if (len < 0) return NULL;
    tmp = g_new(char, len+1);

    memcpy(tmp, str, len);
    tmp[len] = '\0';

    return tmp;
}

#define XS_GETSTR(num,name) { char *tmp; size_t len; tmp=(char *)SvPV(ST(num),len); name=dup0(tmp,len); }
#define XS_FREESTR(name) g_free(name);

#define GET_SERVER(a, b) \
{ \
    if (items == a+1) \
    { \
        char *tmps; \
        XS_GETSTR(a, tmps); \
        if (sscanf(tmps, "%d", &b) != 1) \
            b = -1; \
        XS_FREESTR(tmps) \
    } \
    else \
        b = -1; \
}

static int call_perl(char *name, char *args[])
{
    dSP;
    int retcount,num,i,ret;

    ENTER;
    SAVETMPS;

    g_return_val_if_fail(name != NULL, 0);
    g_return_val_if_fail(args != NULL, 0);

    if (sserver == NULL) sserver = cserver;
    PUSHMARK(sp);
    while (*args != NULL)
    {
        XPUSHs(sv_2mortal(newSVpv(*args, strlen(*args))));
        args++;
    }
    PUTBACK;
    
    retcount=perl_call_pv(name,G_EVAL|G_SCALAR);

    SPAGAIN;

    ret = 0;
    if(SvTRUE(GvSV(errgv)))
    {
        void *tmp;
        g_warning("perl error: %s\n",SvPV(GvSV(errgv),na));
        tmp = POPs;
    }
    else
    {
        if (retcount>0) ret = POPi;
        for(num=2;num<=retcount;num++) i=POPi;
    }
    
    PUTBACK;
    FREETMPS;
    LEAVE;

    sserver = NULL;
    return ret;
}

XS(ircBind)
{
    char *event_name, *event_spec, *bind_time, *func_name;
    EVENT_REC *event;
    int tmp, server;

    dXSARGS;

    if (items < 4 || items > 5) XSRETURN_EMPTY;

    XS_GETSTR(0, event_name);
    XS_GETSTR(1, event_spec);
    XS_GETSTR(2, bind_time);
    XS_GETSTR(3, func_name);
    GET_SERVER(4, server);

    event = g_new(EVENT_REC, 1);
    event->id = ++event_ids;
    event->type = SCRIPT_EVENT_UNKNOWN;
    event->timeouttag = -1;
    event->server = server;
    event->name = NULL;
    event->spec = event_spec;
    event->func = func_name;

    if (sscanf(bind_time, "%d", &tmp) == 1 && tmp > 0)
    {
        /* set bind timeout */
        event->timeouttag = gui_timeout_new(tmp, (GUI_TIMEOUT_FUNC) remove_event, event);
    }

    if (*event_name == '/')
    {
        /* binding command.. */
        event->type = SCRIPT_EVENT_COMMAND;
        event->name = g_strdup(event_name+1);
    }
    else if (sscanf(event_name, "%d", &event->num) == 1)
    {
        /* numeric event */
        event->type = SCRIPT_EVENT_NUMERIC;
    }
    else
    {
        /* named event */
        for (tmp = 0; events[tmp].name != NULL; tmp++)
        {
            if (strcasecmp(event_name, events[tmp].name) == 0)
            {
                event->type = events[tmp].type;
                break;
            }
        }
    }

    XS_FREESTR(event_name);
    XS_FREESTR(bind_time);

    if (event->type == SCRIPT_EVENT_UNKNOWN)
    {
        /* unknown event */
        event_ids--;
        g_free(event);
        XS_FREESTR(event_spec);
        XS_FREESTR(func_name);
        XSRETURN_EMPTY;
    }

    event_list = g_list_append(event_list, event);

    XSRETURN_IV(event->id);
}

XS(ircCmd)
{
    char *chan, *data, *tmp;
    int server;
    SERVER_REC *servrec;
    CHAN_REC *ch;

    dXSARGS;

    if (items < 1 || items > 3) XSRETURN_NO;

    XS_GETSTR(0, data);
    if (items == 1)
        chan = g_strdup("");
    else
    {
        XS_GETSTR(1, chan);
    }
    GET_SERVER(2, server);

    if (server == -1)
        servrec = NULL;
    else
    {
        servrec = irc_get_server(server);
        if (servrec == NULL)
        {
            XS_FREESTR(chan);
            XS_FREESTR(data);
            XSRETURN_NO;
        }
    }

    if (*chan == '\0')
    {
        if (curwin->curchan != NULL &&
            (servrec == NULL || curwin->curchan->server == servrec))
        {
            /* use current channel.. */
            ch = curwin->curchan;
        }
        else if (servrec != NULL && servrec->defwin != NULL)
        {
            /* use server's default window */
            ch = servrec->defwin->curchan;
        }
        else
        {
            /* No channel, send command to wherever you like... */
            ch = NULL;
        }
    }
    else
    {
        ch = channel_joined(servrec, chan);
        if (ch == NULL)
        {
            XS_FREESTR(chan);
            XS_FREESTR(data);
            XSRETURN_NO;
        }
    }

    if (ch != NULL)
    {
        servrec = cserver;
        cserver = ch->server;
    }

    tmp = g_new(char, strlen(data)+2);
    sprintf(tmp, "%c%s", CMD_CHAR, data);

    irc_parse_outgoing(ch, tmp);

    g_free(tmp);
    if (ch != NULL) cserver = servrec;

    XS_FREESTR(chan);
    XS_FREESTR(data);

    XSRETURN_YES;
}

XS(ircText)
{
    char *chan, *data;
    int server;

    dXSARGS;

    if (items < 1 || items > 3) XSRETURN_NO;

    XS_GETSTR(0, data);
    if (items == 1)
        chan = g_strdup("");
    else
    {
        XS_GETSTR(1, chan);
    }
    GET_SERVER(2, server);

    if (server == -1)
        drawtext(NULL, chan, LEVEL_YAGNOTICE, data);
    else
        drawtext(irc_get_server(server), chan, LEVEL_YAGNOTICE, data);

    XS_FREESTR(chan);
    XS_FREESTR(data);

    XSRETURN_YES;
}

XS(ircCTCPSend)
{
    char *chan, *data, *tmp;
    SERVER_REC *old;
    int server;

    dXSARGS;

    if (items < 2 || items > 3) XSRETURN_NO;

    XS_GETSTR(0, chan);
    XS_GETSTR(1, data);
    GET_SERVER(2, server);

    old = cserver; 
    if (server != -1)
    {
        cserver = irc_get_server(server);
        if (cserver == NULL)
        {
            cserver = old;
            XS_FREESTR(chan);
            XS_FREESTR(data);
            XSRETURN_NO;
        }
    }

    tmp = g_new(char, strlen(chan)+strlen(data)+3);
    sprintf(tmp, "%s %s\n", chan, data);
    irccmd_ctcp(tmp);
    g_free(tmp);

    cserver = old;

    XS_FREESTR(chan);
    XS_FREESTR(data);

    XSRETURN_YES;
}

XS(ircCTCPReply)
{
    char *chan, *data, *tmp;
    SERVER_REC *server;
    int servnum;

    dXSARGS;

    if (items < 2 || items > 3) XSRETURN_NO;

    XS_GETSTR(0, chan);
    XS_GETSTR(1, data);
    GET_SERVER(2, servnum);

    server = cserver;
    if (servnum != -1)
    {
        server = irc_get_server(servnum);
        if (server == NULL)
        {
            XS_FREESTR(chan);
            XS_FREESTR(data);
            XSRETURN_NO;
        }
    }

    tmp = g_new(char, strlen(chan)+strlen(data)+12);
    sprintf(tmp, "NOTICE %s \001%s\001\n", chan, data);
    ctcp_send_reply(server, tmp);
    g_free(tmp);

    XS_FREESTR(chan);
    XS_FREESTR(data);

    XSRETURN_YES;
}

XS(ircGetChannelList)
{
    SERVER_REC *servrec;
    int server, ret;
    GList *tmp;

    dXSARGS;

    if (items > 1) XSRETURN_EMPTY;

    GET_SERVER(0, server);

    if (server == -1)
        servrec = cserver;
    else
    {
        servrec = irc_get_server(server);
        if (servrec == NULL) XSRETURN_EMPTY;
    }

    ret = 0;
    GLIST_FOREACH(tmp, winlist)
    {
        GList *ch;

        GLIST_FOREACH(ch, ((WINDOW_REC *) tmp->data)->chanlist)
        {
            CHAN_REC *chan;

            chan = (CHAN_REC *) ch->data;
            if (chan->server == servrec)
            {
                XST_mPV(ret++, chan->name);
            }
        }
    }

    XSRETURN(ret);
}

XS(ircGetClientInfo)
{
    char *str;
    int n, ret, flags;

    dXSARGS;

    n = 0; flags = 0;
    do
    {
        if (n == items)
            str = NULL;
        else
        {
            XS_GETSTR(n, str);
        }

        if (str == NULL || strcasecmp(str, "uptime") == 0)
            flags |= 0x01;
        if (str == NULL || strcasecmp(str, "ver") == 0)
            flags |= 0x02;
        if (str == NULL || strcasecmp(str, "veronly") == 0)
            flags |= 0x04;

        if (n < items) XS_FREESTR(str);
        n++;
    }
    while (n < items);

    ret = 0;

    if (flags & 0x01)
    {
        struct tms buf;

        /* FIXME: how do I this? this didn't seem to work :( */
        times(&buf);
        XST_mPV(ret++, "uptime");
        XST_mIV(ret++, buf.tms_stime/CLOCKS_PER_SEC);
    }

    if (flags & 0x02)
    {
        XST_mPV(ret++, "ver");
        XST_mPV(ret++, IRC_VERSION);
    }

    if (flags & 0x04)
    {
        XST_mPV(ret++, "veronly");
        XST_mPV(ret++, IRC_VERSION_NUMBER);
    }

    XSRETURN(ret);
}

XS(ircGetCurrentChannel)
{
    char *ret = NULL;

    dXSARGS;

    if (items > 0) XSRETURN_IV(-1);

    if (curwin)
      if (curwin->curchan)
    	ret = curwin->curchan->name == NULL ? "" : curwin->curchan->name;
    if (ret == NULL)
      ret = "";
    XSRETURN_PV(ret);
}

XS(ircGetCurrentServer)
{
    int ret;

    dXSARGS;

    if (items > 0) XSRETURN_IV(-1);

    ret = cserver == NULL ? -1 : cserver->handle;
    XSRETURN_IV(ret);
}

XS(ircGetEventServer)
{
    int ret;

    dXSARGS;

    if (items > 0) XSRETURN_IV(-1);

    if (sserver != NULL)
        ret = sserver->handle;
    else
        ret = cserver == NULL ? -1 : cserver->handle;
    XSRETURN_IV(ret);
}

XS(ircGetLocalUserInfo)
{
    SERVER_REC *servrec;
    int server;

    dXSARGS;

    if (items > 1) XSRETURN_UNDEF;

    GET_SERVER(0, server);

    servrec = server == -1 ? cserver : irc_get_server(server);
    if (servrec == NULL) XSRETURN_UNDEF;

    XST_mPV(0, servrec->nick);
    XST_mPV(1, ""); /* FIXME: servrec->hostname */
    XST_mPV(2, ""); /* FIXME: servrec->realname */

    XSRETURN(3);
}

XS(ircGetNameList)
{
    SERVER_REC *servrec;
    CHAN_REC *chan;
    char *channel;
    int server, ret;
    GList *tmp;

    dXSARGS;

    if (items < 1 || items > 2) XSRETURN_EMPTY;

    XS_GETSTR(0, channel);
    GET_SERVER(1, server);

    servrec = NULL;
    if (server != -1)
    {
        servrec = irc_get_server(server);
        if (servrec == NULL)
        {
            XS_FREESTR(channel);
            XSRETURN_EMPTY;
        }
    }

    chan = channel_joined(servrec, channel);
    XS_FREESTR(channel);
    if (chan == NULL) XSRETURN_EMPTY;

    ret = 0;
    GLIST_FOREACH(tmp, chan->nicks)
    {
        XST_mPV(ret++, tmp->data);
    }

    XSRETURN(ret);
}

XS(ircGetServerIdList)
{
    int ret;
    GList *tmp;

    dXSARGS;

    if (items > 0) XSRETURN_EMPTY;

    ret = 0;
    GLIST_FOREACH(tmp, servlist)
    {
        SERVER_REC *server;

        server = (SERVER_REC *) tmp->data;
        XST_mIV(ret++, server->handle);
        XST_mPV(ret++, server->name);
    }

    XSRETURN(ret);
}

XS(ircGetTopic)
{
    SERVER_REC *servrec;
    CHAN_REC *chan;
    char *channel, *ret;
    int server;
    dXSARGS;

    if (items < 1 || items > 2) XSRETURN_EMPTY;

    XS_GETSTR(0, channel);
    GET_SERVER(1, server);

    servrec = NULL;
    if (server != -1)
    {
        servrec = irc_get_server(server);
        if (servrec == NULL)
        {
            XS_FREESTR(channel);
            XSRETURN_UNDEF;
        }
    }

    chan = channel_joined(servrec, channel);
    XS_FREESTR(channel);
    if (chan == NULL) XSRETURN_UNDEF;

    ret = chan->topic == NULL ? "" : chan->topic;
    XSRETURN_PV(ret);
}

XS(ircMenuAdd)
{
    char *menu_type, *menu_name, *menu_spec, *func_name;
    MENU_REC *menu;
    int server;

    dXSARGS;

    if (items < 4 || items > 5) XSRETURN_NO;

    XS_GETSTR(0, menu_type);
    XS_GETSTR(1, menu_name);
    XS_GETSTR(2, menu_spec);
    XS_GETSTR(3, func_name);
    GET_SERVER(4, server);

    menu = g_new(MENU_REC, 1);
    menu->id = ++popup_ids;
    menu->name = menu_name;
    menu->spec = menu_spec;
    menu->func = func_name;
    menu->server = server;

    if (strcasecmp(menu_type, "nick") == 0)
        menu->type = POPUPMENU_NICK;
    else if (strcasecmp(menu_type, "channel") == 0)
        menu->type = POPUPMENU_CHAN;
    else if (strcasecmp(menu_type, "global") == 0)
        menu->type = POPUPMENU_MAIN;
    else
    {
        /* unknown menu type */
        popup_ids--;
        g_free(menu);
        XS_FREESTR(menu_type);
        XS_FREESTR(menu_name);
        XS_FREESTR(menu_spec);
        XS_FREESTR(func_name);
        XSRETURN_NO;
    }
    XS_FREESTR(menu_type);

    popup_menu = g_list_append(popup_menu, menu);

    XSRETURN_IV(menu->id);
}

XS(ircMenuRemove)
{
    GList *tmp;
    char *idstr;
    int id, ok;

    dXSARGS;

    if(items!=1) XSRETURN_NO;

    XS_GETSTR(0, idstr);
    if (sscanf(idstr, "%d", &id) != 1)
    {
        XS_FREESTR(idstr);
        XSRETURN_NO;
    }

    ok = 0;
    GLIST_FOREACH(tmp, popup_menu)
    {
        MENU_REC *menu;

        menu = (MENU_REC *) tmp->data;
        if (menu->id == id)
        {
            remove_menu(menu);
            ok = 1;
            break;
        }
    }

    XS_FREESTR(idstr);

    if (ok)
        XSRETURN_YES;
    else
        XSRETURN_NO;
}

XS(ircMenuRemoveMatch)
{
    char *menu_type, *menu_name, *menu_spec, *func_name;
    int server, ok;
    GList *tmp;

    dXSARGS;

    if (items < 4 || items > 5) XSRETURN_NO;

    XS_GETSTR(0, menu_type);
    XS_GETSTR(1, menu_name);
    XS_GETSTR(2, menu_spec);
    XS_GETSTR(3, func_name);
    GET_SERVER(4, server);

    ok = 0;
    GLIST_FOREACH(tmp, popup_menu)
    {
        MENU_REC *menu;

        menu = (MENU_REC *) tmp->data;
        if (menu->server == server &&
            strcmp(menu->name, menu_type) == 0 &&
            strcmp(menu->spec, menu_spec) == 0 &&
            strcmp(menu->func, func_name) == 0)
        {
            remove_menu(menu);
            ok = 1;
            break;
        }
    }

    XS_FREESTR(menu_type);
    XS_FREESTR(menu_name);
    XS_FREESTR(menu_spec);
    XS_FREESTR(func_name);

    if (ok)
        XSRETURN_YES;
    else
        XSRETURN_NO;
}

XS(ircMsg)
{
    char *channel, *text, *tmp;
    SERVER_REC *old;
    int server;

    dXSARGS;

    if (items < 2 || items > 3) XSRETURN_NO;

    XS_GETSTR(0, channel);
    XS_GETSTR(1, text);
    GET_SERVER(2, server);

    old = cserver;
    if (server != -1)
    {
        cserver = irc_get_server(server);
        if (cserver == NULL)
        {
            cserver = old;
            XSRETURN_NO;
        }
    }

    tmp = g_new(char, strlen(channel) + strlen(text)+2);
    sprintf(tmp, "%s %s", channel, text);
    irccmd_msg(tmp);

    cserver = old;

    XS_FREESTR(channel);
    XS_FREESTR(text);

    XSRETURN_YES;
}

XS(ircNotice)
{
    char *channel, *text, *tmp;
    SERVER_REC *old;
    int server;

    dXSARGS;

    if (items < 2 || items > 3) XSRETURN_NO;

    XS_GETSTR(0, channel);
    XS_GETSTR(1, text);
    GET_SERVER(2, server);

    old = cserver;
    if (server != -1)
    {
        cserver = irc_get_server(server);
        if (cserver == NULL)
        {
            cserver = old;
            XSRETURN_NO;
        }
    }

    tmp = g_new(char, strlen(channel) + strlen(text)+2);
    sprintf(tmp, "%s %s", channel, text);
    irccmd_notice(tmp);

    cserver = old;

    XS_FREESTR(channel);
    XS_FREESTR(text);

    XSRETURN_YES;
}

XS(ircServerCmd)
{
    SERVER_REC *servrec;
    char *data;
    int server;

    dXSARGS;

    if (items < 1 || items > 2) XSRETURN_NO;

    XS_GETSTR(0, data);
    GET_SERVER(1, server);

    if (server == -1)
        servrec = cserver;
    else
        servrec = irc_get_server(server);

    if (servrec == NULL)
    {
        XS_FREESTR(data);
        XSRETURN_NO;
    }

    irc_send_cmd(servrec, data);
    XS_FREESTR(data);

    XSRETURN_YES;
}

static int timeout_func(TIMEOUT_REC *rec)
{
    if (!call_perl(rec->func, rec->args))
        remove_timeout(rec);
    return 1;
}

XS(ircTimerCallback)
{
    char *func, *times;
    unsigned long timeout;
    TIMEOUT_REC *rec;
    int n;

    dXSARGS;

    if(items < 2) XSRETURN_IV(-1);

    XS_GETSTR(0, func);
    XS_GETSTR(1, times);

    if (sscanf(times, "%lu", &timeout) != 1)
    {
        XS_FREESTR(func);
        XS_FREESTR(times);
        XSRETURN_IV(-1);
    }

    rec = g_new(TIMEOUT_REC, 1);
    rec->timeout = timeout;
    rec->func = func;

    rec->args = g_new(char *, items);
    rec->args[0] = times;
    for (n = 2; n < items; n++)
    {
        char *str;

        XS_GETSTR(n, str);
        rec->args[n-1] = str;
    }
    rec->args[items-1] = NULL;

    timeout_list = g_list_append(timeout_list, rec);

    n = gui_timeout_new(timeout, (GUI_TIMEOUT_FUNC) timeout_func, rec);
    rec->id = n;

    XSRETURN_IV(n);
}

XS(ircTimerRemove)
{
    GList *tmp;
    char *idstr;
    int id, ok;

    dXSARGS;

    if(items!=1) XSRETURN_NO;

    XS_GETSTR(0, idstr);
    if (sscanf(idstr, "%d", &id) != 1)
    {
        XS_FREESTR(idstr);
        XSRETURN_NO;
    }

    ok = 0;
    GLIST_FOREACH(tmp, timeout_list)
    {
        TIMEOUT_REC *rec;

        rec = (TIMEOUT_REC *) tmp->data;
        if (rec->id == id)
        {
            remove_timeout(rec);
            ok = 1;
            break;
        }
    }

    XS_FREESTR(idstr);

    if (ok)
        XSRETURN_YES;
    else
        XSRETURN_NO;
}

XS(ircUnBind)
{
    GList *tmp;
    char *idstr;
    int id, ok;

    dXSARGS;

    if (items != 1) XSRETURN_NO;

    XS_GETSTR(0, idstr);
    if (sscanf(idstr, "%d", &id) != 1) XSRETURN_NO;

    ok = 0;
    GLIST_FOREACH(tmp, event_list)
    {
        EVENT_REC *event;

        event = (EVENT_REC *) tmp->data;
        if (event->id == id)
        {
            remove_event(event);
            ok = 1;
            break;
        }
    }

    XS_FREESTR(idstr);

    if (ok)
        XSRETURN_YES;
    else
        XSRETURN_NO;
}

XS(ircUnBindMatch)
{
    char *event_name, *event_spec, *bind_time, *func_name;
    char *name;
    int timeout, server, type, num, ok;

    dXSARGS;

    if (items < 4 || items > 5) XSRETURN_EMPTY;

    XS_GETSTR(0, event_name);
    XS_GETSTR(1, event_spec);
    XS_GETSTR(2, bind_time);
    XS_GETSTR(3, func_name);
    GET_SERVER(4, server);

    name = NULL; num = 0;
    if (sscanf(bind_time, "%d", &timeout) != 1)
        timeout = -1;

    type = SCRIPT_EVENT_UNKNOWN;
    if (*event_name == '/')
    {
        /* binding command.. */
        type = SCRIPT_EVENT_COMMAND;
        name = event_name;
    }
    else if (sscanf(event_name, "%d", &num) == 1)
    {
        /* numeric event */
        type = SCRIPT_EVENT_NUMERIC;
    }
    else
    {
        int tmp;

        /* named event */
        for (tmp = 0; events[tmp].name != NULL; tmp++)
        {
            if (strcmp(event_name, events[tmp].name) == 0)
            {
                type = events[tmp].type;
                break;
            }
        }
    }

    ok = 0;
    if (type != SCRIPT_EVENT_UNKNOWN)
    {
        GList *tmp;

        GLIST_FOREACH(tmp, event_list)
        {
            EVENT_REC *event;

            event = (EVENT_REC *) tmp->data;
            if (event->type == type &&
                event->server == server &&
                strcmp(event->spec, event_spec) == 0 &&
                strcmp(event->func, func_name) == 0)
            {
                if ((type == SCRIPT_EVENT_NUMERIC && event->num == num) ||
                    (type == SCRIPT_EVENT_COMMAND && strcmp(event->name, name) == 0))
                {
                    remove_event(event);
                    ok = 1;
                    break;
                }
            }
        }
    }

    XS_FREESTR(event_name);
    XS_FREESTR(event_spec);
    XS_FREESTR(bind_time);
    XS_FREESTR(func_name);

    if (ok)
        XSRETURN_YES;
    else
        XSRETURN_NO;
}

static void xs_init()
{
    newXS("ircBind",ircBind,__FILE__);
    newXS("ircCmd",ircCmd,__FILE__);
    newXS("ircCTCPSend",ircCTCPSend,__FILE__);
    newXS("ircCTCPReply",ircCTCPReply,__FILE__);
    newXS("ircGetChannelList",ircGetChannelList,__FILE__);
    newXS("ircGetClientInfo",ircGetClientInfo,__FILE__);
    newXS("ircGetCurrentChannel", ircGetCurrentChannel, __FILE__);
    newXS("ircGetCurrentServer",ircGetCurrentServer,__FILE__);
    newXS("ircGetEventServer",ircGetEventServer,__FILE__);
    newXS("ircGetLocalUserInfo",ircGetLocalUserInfo,__FILE__);
    newXS("ircGetNameList",ircGetNameList,__FILE__);
    newXS("ircGetServerIdList",ircGetServerIdList,__FILE__);
    newXS("ircGetTopic",ircGetTopic,__FILE__);
    newXS("ircMenuAdd",ircMenuAdd,__FILE__);
    newXS("ircMenuRemove",ircMenuRemove,__FILE__);
    newXS("ircMenuRemoveMatch",ircMenuRemoveMatch,__FILE__);
    newXS("ircMsg",ircMsg,__FILE__);
    newXS("ircNotice",ircNotice,__FILE__);
    newXS("ircServerCmd",ircServerCmd,__FILE__);
    newXS("ircText",ircText,__FILE__);
    newXS("ircTimerCallback",ircTimerCallback,__FILE__);
    newXS("ircTimerRemove",ircTimerRemove,__FILE__);
    newXS("ircUnBind",ircUnBind,__FILE__);
    newXS("ircUnBindMatch",ircUnBindMatch,__FILE__);
}
#endif
