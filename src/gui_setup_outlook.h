#include "config.h"

#ifndef __GUI_SETUP_OUTLOOK_H
#define __GUI_SETUP_OUTLOOK_H

typedef struct
{
    char *name;

    char *pixmap;
    GdkColor fg, bg;
    GdkColor colors[16];
}
SETUP_THEME_REC;

extern GList *themes;

void gui_setup_outlook(GtkWidget *vbox);
void gui_setup_outlook_accept(void);

/* add theme record to GList with sort */
int gui_add_theme_rec(GList **themes, char *data);
void gui_remove_theme_rec(GList **themes, SETUP_THEME_REC *theme);

#endif
