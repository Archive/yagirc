#include "config.h"

#ifndef __LOG_H
#define __LOG_H

/* Append/create to log file, data = logging info */
LOG_REC *log_file_open(char *fname, char *data);

/* Close log file */
void log_file_close(LOG_REC *rec);

/* Write to log file */
void log_file_write(char *chan, int level, char *data);

/* Find a log file record */
LOG_REC *log_file_find(char *fname);

#endif
