#include "config.h"

#ifndef __CTCP_H
#define __CTCP_H

/* CTCP sound */
int ctcp_sound(char *sender, char *target, char *data);
/* CTCP version */
int ctcp_version(char *sender, char *target, char *data);
/* CTCP ping */
int ctcp_ping(char *sender, char *target, char *data);
/* CTCP action = /ME command */
int ctcp_action(char *sender, char *target, char *data);
/* CTCP DCC */
int ctcp_dcc(char *sender, char *target, char *data);
/* CTCP PAGE */
int ctcp_page(char *sender, char *target, char *data);
/* CTCP FINGER */
int ctcp_finger(char *sender, char *target, char *data);
/* CTCP TIME */
int ctcp_time(char *sender, char *target, char *data);
/* CTCP ECHO */
int ctcp_echo(char *sender, char *target, char *data);

/* CTCP reply */
int ctcp_reply(char *sender, char *data);

/* Send CTCP reply with flood protection */
void ctcp_send_reply(SERVER_REC *server, char *data);

/* CTCP reply sending function */
int ctcp_timeout_func(SERVER_REC *server);

#endif
