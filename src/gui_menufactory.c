/*

 gui_menufactory.c : creates main menu..

    This file is mostly cut & pasted from GTK+ tutorial so I think it's
    copyrighted by Ian Main <imain@gtk.org> and Tony Gale <gale@gtk.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "config.h"

#include <gtk/gtk.h>
#include <gtk/gtkfeatures.h>
#include <strings.h>

#include "script.h"
#include "gui_menu.h"
#include "intl.h"

#if 0
static void menus_remove_accel(GtkWidget * widget, gchar * signal_name, gchar * path);
static gint menus_install_accel(GtkWidget * widget, gchar * signal_name, gchar key, gchar modifiers, gchar * path);
#endif
static void menus_create(WINDOW_REC *win, GtkMenuEntry *entries, int nmenu_entries);


/* this is the GtkMenuEntry structure used to create new menus.  The
 * first member is the menu definition string.  The second, the
 * default accelerator key used to access this menu function with
 * the keyboard.  The third is the callback function to call when
 * this menu item is selected (by the accelerator key, or with the
 * mouse.) The last member is the data to pass to your callback function.
*/

static GtkMenuEntry menu_items[] =
{
    {N_("<Main>/IRC/Connect..."), NULL, (GtkMenuCallback) menu_irc_connect, NULL},
    {N_("<Main>/IRC/Disconnect..."), NULL, (GtkMenuCallback) menu_irc_disconnect, NULL},
    {N_("<Main>/IRC/Setup..."), NULL, (GtkMenuCallback) menu_irc_setup, NULL},
    {N_("<Main>/IRC/<separator>"), NULL, NULL, NULL},
    {N_("<Main>/IRC/Quit"), NULL, (GtkMenuCallback) menu_irc_quit, NULL},
    {N_("<Main>/Commands/Join..."), NULL, (GtkMenuCallback) menu_command_join, NULL},
    {N_("<Main>/Commands/Part"), NULL, (GtkMenuCallback) menu_command_part, NULL},
    {N_("<Main>/Window/New"), NULL, (GtkMenuCallback) menu_window_new, NULL},
    {N_("<Main>/Window/New hidden"), NULL, (GtkMenuCallback) menu_window_new_hidden, NULL},
    {N_("<Main>/Window/Close"), NULL, (GtkMenuCallback) menu_window_close, NULL},
    {N_("<Main>/Window/<separator>"), NULL, NULL, NULL},
    {N_("<Main>/Window/Select server..."), NULL, (GtkMenuCallback) menu_window_select_server, NULL},
    {N_("<Main>/Window/<separator>"), NULL, NULL, NULL},
    {N_("<Main>/Window/URL List"), NULL, (GtkMenuCallback) menu_window_show_urllist, NULL},
    {N_("<Main>/Help/About"), NULL, (GtkMenuCallback) menu_help_about, NULL}
};

/* calculate the number of menu_item's */
static int nmenu_items = sizeof(menu_items) / sizeof(menu_items[0]);

void gui_menu_create(WINDOW_REC *win)
{
    GtkMenuFactory *menu;

    g_return_if_fail(win != NULL);

    win->gui->parent->entry_ht = NULL;
    win->gui->parent->factory = gtk_menu_factory_new(GTK_MENU_FACTORY_MENU_BAR);
    menu = gtk_menu_factory_new(GTK_MENU_FACTORY_MENU_BAR);

#ifdef ENABLE_NLS
    {
	int i=0;
	for (i=0;i<nmenu_items;i++) menu_items[i].path=_(menu_items[i].path);
    }
#endif

    gtk_menu_factory_add_subfactory(win->gui->parent->factory, menu, "<Main>");
    menus_create(win, menu_items, nmenu_items);

    win->gui->parent->menubar = menu->widget;
    win->gui->parent->group = menu->accel_group;
}

void gui_menu_destroy(WINDOW_REC *win)
{
    /*gtk_menu_factory_destroy(win->gui->parent->factory);
    gtk_widget_destroy(win->gui->parent->menubar);*/
}

static void menus_create(WINDOW_REC *win, GtkMenuEntry *entries, int nmenu_entries)
{

#if 0
    char *accelerator;
    int i;
#endif

    g_return_if_fail(win != NULL);
    g_return_if_fail(entries != NULL);

#if 0
    if (win->gui->parent->entry_ht)
        for (i = 0; i < nmenu_entries; i++) {
            accelerator = g_hash_table_lookup(win->gui->parent->entry_ht, entries[i].path);
            if (accelerator) {
                if (accelerator[0] == '\0')
                    entries[i].accelerator = NULL;
                else
                    entries[i].accelerator = accelerator;
            }
        }
#endif
    gtk_menu_factory_add_entries(win->gui->parent->factory, entries, nmenu_entries);
    //script_add_popups(win->gui->parent->factory->widget, POPUPMENU_MAIN, (char *) win);

#if 0
    for (i = 0; i < nmenu_entries; i++)
        if (entries[i].widget) {
            gtk_signal_connect(GTK_OBJECT(entries[i].widget), "install_accelerator",
                               (GtkSignalFunc) menus_install_accel,
                               entries[i].path);
            gtk_signal_connect(GTK_OBJECT(entries[i].widget), "remove_accelerator",
                               (GtkSignalFunc) menus_remove_accel,
                               entries[i].path);
        }
#endif
}

#if 0
static gint menus_install_accel(GtkWidget *widget, gchar *signal_name, gchar key, gchar modifiers, gchar *path)
{
    char accel[64];
    char *t1, t2[2];

    accel[0] = '\0';
    if (modifiers & GDK_CONTROL_MASK)
        strcat(accel, "<control>");
    if (modifiers & GDK_SHIFT_MASK)
        strcat(accel, "<shift>");
    if (modifiers & GDK_MOD1_MASK)
        strcat(accel, "<alt>");

    t2[0] = key;
    t2[1] = '\0';
    strcat(accel, t2);

    if (win->gui->parent->entry_ht) {
        t1 = g_hash_table_lookup(win->gui->parent->entry_ht, path);
        g_free(t1);
    } else
        win->gui->parent->entry_ht = g_hash_table_new(g_str_hash, g_str_equal);

    g_hash_table_insert(win->gui->parent->entry_ht, path, g_strdup(accel));

    return TRUE;
}

static void menus_remove_accel(GtkWidget *widget, gchar *signal_name, gchar *path)
{
    char *t;

    if (win->gui->parent->entry_ht) {
        t = g_hash_table_lookup(win->gui->parent->entry_ht, path);
        g_free(t);

        g_hash_table_insert(win->gui->parent->entry_ht, path, g_strdup(""));
    }
}
#endif

void menus_set_sensitive(WINDOW_REC *win, char *path, int sensitive)
{
    GtkMenuPath *menu_path;

    g_return_if_fail(win != NULL);
    g_return_if_fail(path != NULL);

    menu_path = gtk_menu_factory_find(win->gui->parent->factory, path);
    if (menu_path)
        gtk_widget_set_sensitive(menu_path->widget, sensitive);
    else
        g_warning("Unable to set sensitivity for menu which doesn't exist: %s", path);
}
