#include "config.h"

#ifndef __SOUND_H
#define __SOUND_H

#ifdef WE_ARE_NUTS

#include "misc.h"

#define FILEPATH_LEN_MAX 4096

#ifdef USE_ESD
#include <esd.h>

typedef struct _sound
  {
     int                 rate;
     int                 format;
     int                 samples;
     short              *data;
     int                 id;
  } Sound;

int esound_started,
    esound_working,
    sound_fd;
#else
typedef _sound FILE Sound;

FILE *sound_fd;
#endif

enum
{
    SOUND_FAIL,
    SOUND_FMT_UNKNWN,
    SOUND_INIT_DSP,
    SOUND_INIT_FAIL,
    SOUND_INIT_OK,
    SOUND_NOFILE,
    SOUND_NOTAUDIO,
    SOUND_OK
} SoundEnum;

typedef struct _WAVFormatChunk
  {
     char                chunkID[4];
     int                 chunkSize;

     short               wFormatTag;
     unsigned short      wChannels;
     unsigned int        dwSamplesPerSec;
     unsigned int        dwAvgBytesPerSec;
     unsigned short      wBlockAlign;
     unsigned short      wBitsPerSample;
  }
WAVFormatChunk;

Sound *sound_load(char *file);
int sound_play(Sound *s);
void sound_destroy(Sound *s);

int sound_init();
void sound_deinit();
#endif
#endif
