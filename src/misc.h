#include "config.h"

#ifndef __MISC_H
#define __MISC_H

#include "data.h"

#define GLIST_FOREACH(a, b) for (a = g_list_first(b); a != NULL; a = a->next)
GList *glist_find_string(GList *list, char *str);
GList *glist_case_find_string(GList *list, char *str);

/* Read line from somewhere..

    socket : 0 = read from file/pipe, 1 = read from socket
    handle : file/socket handle to read from
    str    : where to put line
    buf    : temp buffer
    bufsize: temp buffer size
    bufpos : current position in temp buffer
*/
int read_line(int socket, int handle, char *str, char *buf, int bufsize, int *bufpos);

/* Add new nick to list */
NICK_REC *insert_nick_rec(CHAN_REC *chan, char *nick);
/* remove nick from list */
void remove_nick_rec(CHAN_REC *chan, NICK_REC *nick);
/* Find nick record from list */
NICK_REC *find_nick_rec(CHAN_REC *chan, char *nick);

/* Add new ban */
BAN_REC *add_ban(CHAN_REC *chan, char *ban, char *nick, time_t time);
/* Remove ban */
void remove_ban(CHAN_REC *chan, char *ban);

/* Parse channel mode string */
void parse_channel_mode(CHAN_REC *chan, char *modestr);

/* File exists */
int exists(char *s);

/* Execute a program */
int execprog(void *params);

/* Search path */
char *pathtofile(char *file, char *searchpath);

#ifdef WE_ARE_NUTS
/* Is file audio */
int fileisaudio(char *file);
#endif

#endif
