/*

 gui_connect.c : Connect server dialog

    Copyright (C) 1998 Timo Sirainen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "config.h"

#include <stdio.h>
#include <string.h>

#include <glib.h>

#include "os.h"
#include "gui.h"
#include "irc.h"
#include "commands.h"
#include "options.h"
#include "intl.h"

static GtkWidget *servdlg = NULL;
static int doubleclick;

/* signal: cancel button pressed */
static void sig_cancel(GtkWidget *dialog)
{
    servdlg = NULL;

    gui_setup_deinit();
    gui_setup_init();

    gui_read_config();
    gtk_widget_destroy(dialog);
}

/* signal: close button pressed */
static void sig_close(GtkWidget *dialog)
{
    servdlg = NULL;
    gui_write_config();
    gtk_widget_destroy(dialog);
}

/* signal: connect button pressed */
static void sig_connect(GtkWidget *dialog)
{
    SETUP_SERVER_REC *server;
    GtkList *list;
    char *tmp;

    list = gtk_object_get_data(GTK_OBJECT(servdlg), "list");
    if (list->selection == NULL) return;

    server = gtk_object_get_data(GTK_OBJECT(list->selection->data), "server");

    tmp = g_new(char, strlen(server->name)+INTEGER_LENGTH+10);
    sprintf(tmp, "SERVER %s:%d", server->name, server->port);
    irccmd_window(tmp);
    g_free(tmp);

    sig_close(dialog);
}

/* signal: mouse button pressed */
static void sig_mousedown(GtkList *list, GdkEventButton *event)
{
    if (event->button == 1)
    {
        if (!doubleclick)
            doubleclick = 1;
        else
        {
            doubleclick = 2;
        }
    }
}

/* signal: mouse button released */
static void sig_mouseup(GtkList *list, GdkEventButton *event, GtkWidget *dialog)
{
    if (doubleclick == 2)
    {
        sig_connect(dialog);
        doubleclick = 0;
    }

    if (event->button == 1)
    {
        doubleclick = 0;
    }
}

void gui_connect_dialog(void)
{
    GtkWidget *button;
    GtkWidget *list;

    if (servdlg != NULL)
    {
        /* server dialog already created */
        gdk_window_raise(servdlg->window);
        return;
    }
    doubleclick = 0;

    servdlg = gtk_dialog_new();
    gtk_widget_set_usize(servdlg, 300, 200);

	gtk_window_position (GTK_WINDOW (servdlg), GTK_WIN_POS_MOUSE);

    gtk_signal_connect_object (GTK_OBJECT (servdlg), "delete_event",
                               GTK_SIGNAL_FUNC (sig_cancel), GTK_OBJECT(servdlg));

    list = create_local_servers(GTK_DIALOG(servdlg)->vbox, TRUE);
    gtk_object_set_data(GTK_OBJECT(servdlg), "list", list);
    gtk_signal_connect (GTK_OBJECT (list), "button_press_event",
                        GTK_SIGNAL_FUNC(sig_mousedown), servdlg);
    gtk_signal_connect (GTK_OBJECT (list), "button_release_event",
                        GTK_SIGNAL_FUNC(sig_mouseup), servdlg);

    button = gtk_button_new_with_label (_("Close"));
    gtk_box_pack_start (GTK_BOX (GTK_DIALOG(servdlg)->action_area), button, TRUE, TRUE, 10);
    gtk_signal_connect_object (GTK_OBJECT (button), "clicked",
                               GTK_SIGNAL_FUNC (sig_close), GTK_OBJECT(servdlg));
    gtk_widget_show (button);

    button = gtk_button_new_with_label (_("Cancel"));
    gtk_box_pack_start (GTK_BOX (GTK_DIALOG(servdlg)->action_area), button, TRUE, TRUE, 10);
    gtk_signal_connect_object (GTK_OBJECT (button), "clicked",
                               GTK_SIGNAL_FUNC (sig_cancel), GTK_OBJECT(servdlg));
    gtk_widget_show (button);

    button = gtk_button_new_with_label (_("Connect"));
    gtk_box_pack_start (GTK_BOX (GTK_DIALOG(servdlg)->action_area), button, TRUE, TRUE, 10);
    gtk_signal_connect_object (GTK_OBJECT (button), "clicked",
                               GTK_SIGNAL_FUNC (sig_connect), GTK_OBJECT(servdlg));
    gtk_widget_show (button);

    gtk_widget_show(servdlg);
}

static void select_server(GtkWidget *dialog)
{
    GtkList *list;

    list = gtk_object_get_data(GTK_OBJECT(dialog), "list");

    if (list->selection != NULL)
    {
        SERVER_REC *server;
        char tmp[30];

        server = gtk_object_get_data(GTK_OBJECT(list->selection->data), "server");

        sprintf(tmp, "SERVER %d", server->handle);
        irccmd_window(tmp);
    }
    gtk_widget_destroy(dialog);
}

static void disconnect_server(GtkWidget *dialog)
{
    GtkList *list;

    list = gtk_object_get_data(GTK_OBJECT(dialog), "list");

    if (list->selection != NULL)
    {
        SERVER_REC *server;
        char tmp[20];

        server = gtk_object_get_data(GTK_OBJECT(list->selection->data), "server");

        sprintf(tmp, "%d", server->handle);
        irccmd_disconnect(tmp);
    }
    gtk_widget_destroy(dialog);
}

/*static void selection_changed(GtkList *list, GtkWidget *ch, GtkDialog *dialog)
{
    GList *children;
    GtkBoxChild *child;
    int sensitive;

    children = GTK_BOX(dialog->action_area)->children;
    sensitive = ch != NULL;

    child = (GtkBoxChild *) children->data;
    gtk_widget_set_sensitive(child->widget, sensitive);
    child = (GtkBoxChild *) children->next->data;
    gtk_widget_set_sensitive(child->widget, sensitive);
}*/

void gui_servers_dialog(void)
{
    GtkWidget *dialog;
    GtkWidget *list;
    GtkWidget *scrollbox;
    GtkWidget *button;
    GList *tmp, *items;
    int selected, pos;
    char str[200];

    dialog = gtk_dialog_new();

	gtk_window_position (GTK_WINDOW (dialog), GTK_WIN_POS_MOUSE);
            
    scrollbox = gtk_scrolled_window_new(NULL, NULL);
    gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrollbox),
                                   GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
    gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->vbox), scrollbox, TRUE, TRUE, 10);
    gtk_widget_show(scrollbox);

    list = gtk_list_new();
    gtk_object_set_data(GTK_OBJECT(dialog), "list", list);
    gtk_list_set_selection_mode (GTK_LIST(list), GTK_SELECTION_BROWSE);
    gtk_container_add(GTK_CONTAINER(scrollbox), list);
    gtk_widget_show(list);

    items = NULL; selected = 0;
    for (pos = 0, tmp = g_list_first(servlist); tmp != NULL; tmp = tmp->next, pos++)
    {
        GtkWidget *li;
        SERVER_REC *server;

        server = (SERVER_REC *) tmp->data;
        if (server == curwin->defserv) selected = pos;

        sprintf(str, "(%d) %s", server->handle, server->name);
        li = gtk_list_item_new_with_label(str);
        gtk_object_set_data(GTK_OBJECT(li), "server", server);
        gtk_widget_show(li);

        items = g_list_append(items, li);
    }
    gtk_list_append_items(GTK_LIST(list), items);
    gtk_list_select_item(GTK_LIST(list), selected);

    button = gtk_button_new_with_label (_("Disconnect"));
    gtk_box_pack_start (GTK_BOX (GTK_DIALOG(dialog)->action_area), button, TRUE, TRUE, 10);
    gtk_signal_connect_object (GTK_OBJECT (button), "clicked",
                               GTK_SIGNAL_FUNC (disconnect_server), GTK_OBJECT(dialog));
    /*GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT);
    gtk_widget_grab_default (button);*/
    if (items == NULL) gtk_widget_set_sensitive(button, FALSE);
    gtk_widget_show (button);

    button = gtk_button_new_with_label (_("Use in window"));
    gtk_box_pack_start (GTK_BOX (GTK_DIALOG(dialog)->action_area), button, TRUE, TRUE, 10);
    gtk_signal_connect_object (GTK_OBJECT (button), "clicked",
                               GTK_SIGNAL_FUNC (select_server), GTK_OBJECT(dialog));
    if (items == NULL) gtk_widget_set_sensitive(button, FALSE);
    gtk_widget_show (button);

    button = gtk_button_new_with_label (_("Close"));
    gtk_box_pack_start (GTK_BOX (GTK_DIALOG(dialog)->action_area), button, TRUE, TRUE, 10);
    gtk_signal_connect_object (GTK_OBJECT (button), "clicked",
                               GTK_SIGNAL_FUNC (gtk_widget_destroy), GTK_OBJECT(dialog));
    gtk_widget_show (button);

    /*gtk_signal_connect_object (GTK_OBJECT (list), "select_child",
                               GTK_SIGNAL_FUNC (selection_changed), GTK_OBJECT(dialog));*/

    gtk_widget_show(dialog);
}
