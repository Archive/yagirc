/*

 gui_setup_lists.c : Notify and ignore list notebook entries for setup

    Copyright (C) 1998 Timo Sirainen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "config.h"

#include <stdio.h>
#include <string.h>

#include <gtk/gtk.h>
#include <glib.h>

#include "os.h"
#include "irc.h"
#include "gui.h"
#include "intl.h"

static void sig_list_ok(GtkWidget *widget, GtkWidget *dialog)
{
    GtkWidget *li;
    GtkList *list;
    GtkEntry *entry;
    GList *item, **glist;
    char *text;

    list = (GtkList *) gtk_object_get_data(GTK_OBJECT(dialog), "data");
    glist = (GList **) gtk_object_get_data(GTK_OBJECT(list), "list");
    entry = (GtkEntry *) gtk_object_get_data(GTK_OBJECT(dialog), "entry");

    text = gtk_entry_get_text(entry);
    *glist = g_list_append(*glist, g_strdup(text));

    li = gtk_list_item_new_with_label(text);
    gtk_object_set_data(GTK_OBJECT(li), "item", *glist);
    gtk_widget_show(li);
    item = g_list_append(NULL, li);
    gtk_list_append_items(list, item);

    gtk_widget_destroy(dialog);
}

static void add_list(GtkWidget *widget, GtkWidget *list)
{
    gui_entry_dialog(gtk_object_get_data(GTK_OBJECT(list), "new_title"), NULL,
                     GTK_SIGNAL_FUNC(sig_list_ok), list);
}

static void delete_list(GtkWidget *widget, GtkList *list)
{
    GList **glist, *sel;

    if (list->selection == NULL) return;

    glist = (GList **) gtk_object_get_data(GTK_OBJECT(list), "list");
    sel = gtk_object_get_data(GTK_OBJECT(list->selection->data), "item");

    gtk_list_remove_items(list, list->selection);

    g_free(sel->data);
    *glist = g_list_remove_link(*glist, sel);
}

/* draw alias list section */
static void gui_setup_list(GtkWidget *vbox, GList **list, char *topic)
{
    GtkWidget *hbox;
    GtkWidget *button;
    GtkWidget *scrollwin;
    GtkWidget *wlist;
    GList *tmp, *itemlist;

    g_return_if_fail(vbox != NULL);
    g_return_if_fail(list != NULL);
    g_return_if_fail(topic != NULL);

    hbox = gtk_hbox_new(FALSE, 0);
    gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);
    gtk_widget_show(hbox);

    /* create alias list widget here for buttons.. */
    wlist = gtk_list_new();
    gtk_object_set_data(GTK_OBJECT(wlist), "new_title", topic);
    gtk_object_set_data(GTK_OBJECT(wlist), "list", list);

    /* add/edit/delete buttons */
    button = gtk_button_new_with_label(_("Add"));
    gtk_signal_connect (GTK_OBJECT (button), "clicked",
                        GTK_SIGNAL_FUNC(add_list), wlist);
    gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 10);
    gtk_widget_show(button);
    button = gtk_button_new_with_label(_("Delete"));
    gtk_signal_connect (GTK_OBJECT (button), "clicked",
                        GTK_SIGNAL_FUNC(delete_list), wlist);
    gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 10);
    gtk_widget_show(button);

    scrollwin = gtk_scrolled_window_new(NULL, NULL);
    gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrollwin),
                                   GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
    gtk_box_pack_start(GTK_BOX(vbox), scrollwin, TRUE, TRUE, 10);
    gtk_widget_show(scrollwin);

    /* alias list */
    gtk_list_set_selection_mode (GTK_LIST(wlist), GTK_SELECTION_BROWSE);
    // gtk_container_add(GTK_CONTAINER(scrollwin), wlist);
    gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(scrollwin), wlist);
    gtk_widget_show(wlist);

    itemlist = NULL;
    for (tmp = g_list_first(*list); tmp != NULL; tmp = tmp->next)
    {
        GtkWidget *li;

        li = gtk_list_item_new_with_label((char *) tmp->data);
        gtk_object_set_data(GTK_OBJECT(li), "item", tmp);
        gtk_widget_show(li);
        itemlist = g_list_append(itemlist, li);
    }
    gtk_list_append_items(GTK_LIST(wlist), itemlist);
}


void gui_setup_ignorelist(GtkWidget *vbox)
{
    gui_setup_list(vbox, &ignores, _("Add new member to ignore list in format\n\"<nick> [ALL] [CTCP] [PUBLIC] [PRIVATE]\""));
}

void gui_setup_notifylist(GtkWidget *vbox)
{
    gui_setup_list(vbox, &notifies, _("Add new nick to notify list"));
}
