#ifndef __IGNORE_TXT_H
#define __IGNORE_TXT_H

#include "gui.h"
#include "intl.h"

static char *  IRCTXT_AUTOIGNORE=N_("Flood detected from %_%s%_, autoignoring for %_%d%_ minutes\n");
static char *  IRCTXT_AUTOUNIGNORE=N_("Unignoring %_%s\n");

#endif