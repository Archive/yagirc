#include "config.h"

#ifndef __IRC_H
#define __IRC_H

#include <glib.h>

#include "data.h"

#define LEVEL_CRAP            0x0001
#define LEVEL_CHAN            0x0002
#define LEVEL_PUBLIC          0x0004
#define LEVEL_MSGS            0x0008
#define LEVEL_NOTICES         0x0010
#define LEVEL_WALLOPS         0x0020
#define LEVEL_SNOTES          0x0040
#define LEVEL_ACTIONS         0x0080
#define LEVEL_DCC             0x0100
#define LEVEL_CTCP            0x0200
#define LEVEL_YAGNOTICE       0x0400
#define LEVEL_YAGERROR        0x0800
#define LEVEL_ALL             0xffff

#define LEVELS 9
extern char *levels[];

#define isircflag(a) ((a) == '@' || (a) == '+' || (a) == '-' || (a) == '~')
#define is_channel(a) ((a) == '#' || (a) == '&')

void irc_init(void);
void irc_init_after(void);
void irc_deinit(void);

/* Write text to window, convert color codes. */
void drawtext(SERVER_REC *server, char *chan, int type, char *str, ...);

/* Send command to IRC server */
int irc_send_cmd(SERVER_REC *serv, char *cmd);
/* Parse outgoing line */
int irc_parse_outgoing(CHAN_REC *chan, char *line);

/* Connect to IRC server */
SERVER_REC *irc_server_connect(char *name, int port);
/* Disconnect from IRC server */
void irc_server_disconnect(SERVER_REC *server);

/* Create new IRC window, if win == NULL new top level window is created, if
   not, new window for same toplevel window as "win" is created. */
WINDOW_REC *irc_window_new(SERVER_REC *server, WINDOW_REC *win);
/* Close IRC window */
void irc_window_close(WINDOW_REC *win);
/* Window got focus */
void irc_window_focus(WINDOW_REC *win);

/* Select new "default server window" for server messages */
void irc_select_new_server_window(WINDOW_REC *win);

/* Destroy IRC channel record */
void irc_chan_free(CHAN_REC *chan);
/* Find server record */
SERVER_REC *irc_get_server(int handle);
/* Find channel record of chan in server serv, if serv==NULL, find from all servers */
CHAN_REC *channel_joined(SERVER_REC *serv, char *chan);
/* IRC nick comparision for sort functions */
int irc_nicks_compare(char *p1, char *p2);
/* Select any other channel in window except the one we are now */
int irc_select_new_channel(WINDOW_REC *window);
/* Get nick's host mask */
char *irc_get_nickmask(CHAN_REC *chan, char *nick);
/* Tab style nick completion */
char *irc_nick_tabcompletion(CHAN_REC *chan, char *line, char *out);

void irc_channel_new(char *name, WINDOW_REC *window, SERVER_REC *server, char *key);

/* Convert level string to bits */
int irc_level2bits(char *str);
/* Convert level bits to string */
char *irc_bits2level(int bits);

extern GList *winlist; /* List of windows */
extern GList *servlist; /* List of servers */
extern GList *scriptlist; /* List of scripts loaded */
extern GList *aliases; /* List of command aliases */
extern GList *ignores; /* Ignore list */
extern GList *notifies; /* Notify list */
extern GList *logs; /* Log file list */
extern WINDOW_REC *curwin; /* current window */
extern SERVER_REC *cserver; /* current server */
extern GLOBAL_SETTINGS *global_settings; /* global settings */

#endif
