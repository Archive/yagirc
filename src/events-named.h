#include "config.h"

#ifndef __EVENTS_NAMED_H
#define __EVENTS_NAMED_H

/* named events */
void eirc_privmsg(char *data);
void eirc_notice(char *data);
void eirc_nick(char *data);
void eirc_join(char *data);
void eirc_part(char *data);
void eirc_ping(char *data);
void eirc_pong(char *data);
void eirc_quit(char *data);
void eirc_mode(char *data);
void eirc_kick(char *data);
void eirc_invite(char *data);
void eirc_new_topic(char *data);
void eirc_error(char *data);
void eirc_wallops(char *data);

#endif
