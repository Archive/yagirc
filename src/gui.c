/*

 gui.c : User interface

    Copyright (C) 1998 Timo Sirainen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "config.h"

#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <ctype.h>
#include <stdlib.h>

#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <glib.h>
#include <gtk/gtkfeatures.h>


#ifdef USE_IMLIB
#include <gdk_imlib.h>
#endif

#include "os.h"
#include "gui.h"
#include "gui_txt.h"
#include "irc.h"
#include "commands.h"
#include "options.h"
#include "misc.h"
#include "intl.h"
#include "sound.h"

#include "../pixmaps/notfound.xpm"

enum {
	TBNORMALBUTTON,
	TBTOGGLEBUTTON,
	TBSPACE,
	TBSPECIALBUTTON,
};

typedef struct {
	int flag;
	char *lab1;
	char *lab2;
	char *lab3;
	GtkMenuCallback callback;
} TOOLBARCORE;


static TOOLBARCORE toolbar_rec[] =
{
	{ TBNORMALBUTTON, N_("Con"), N_("Connect"), "connect", (GtkSignalFunc) gui_connect_dialog},
	{ TBNORMALBUTTON, N_("Dis"), N_("Disconnect"), "disconnect", (GtkSignalFunc) gui_servers_dialog},
	{ TBNORMALBUTTON, N_("Setup"), N_("Setup"), "setup", (GtkSignalFunc) gui_setup},
	{ TBNORMALBUTTON, N_("Quit"), N_("Quit"), "quit", (GtkSignalFunc) gui_exit},
	{ TBSPACE, NULL, NULL, NULL, NULL},
	{ TBNORMALBUTTON, N_("Join"), N_("Join"), "join", (GtkSignalFunc) menu_command_join},
	{ TBNORMALBUTTON, N_("Part"), N_("Part"), "part", (GtkSignalFunc) menu_command_part},
	{ TBSPACE, NULL, NULL, NULL, NULL},
	{ TBNORMALBUTTON, N_("New"), N_("New Window"), "new", (GtkSignalFunc) menu_window_new},
	{ TBNORMALBUTTON, N_("Newh"), N_("New Window Hidden"), "newh", (GtkSignalFunc) menu_window_new_hidden},
	{ TBNORMALBUTTON, N_("Close"), N_("Close"), "close", (GtkSignalFunc) menu_window_close},
	{ TBNORMALBUTTON, N_("Serv"), N_("Select server"), "serv", (GtkSignalFunc) menu_window_select_server},
	{ TBNORMALBUTTON, N_("Url"), N_("Url list"), "url", (GtkSignalFunc) menu_window_show_urllist},
	{ TBSPACE, NULL, NULL, NULL, NULL },
	{ TBNORMALBUTTON, N_("About"), N_("About"), "about", (GtkSignalFunc) menu_help_about},
	{ TBSPACE, NULL, NULL, NULL, NULL},
	{ TBTOGGLEBUTTON, N_("raise"), N_("Auto raise"), "autoraise", (GtkSignalFunc) autoraise_flag},
	{ TBSPECIALBUTTON, N_("Away"), N_("Away"), "away", (GtkSignalFunc) awayclicked_new}
};

static int ntoolbar_rec;

/* default colors */
GdkColor colors[] =
{
    { 0, 0, 0, 0 },
    { 0, 0, 0, 0 },
    { 0, 0, 0, 0x7fff },
    { 0, 0, 0x7fff, 0 },
    { 0, 0, 0x7fff, 0x7fff },
    { 0, 0x7fff, 0, 0 },
    { 0, 0x7fff, 0, 0x7fff },
    { 0, 0x7fff, 0x7fff, 0 },
    { 0, 0xbfff, 0xbfff, 0xbfff },
    { 0, 0x3fff, 0x3fff, 0x3fff },
    { 0, 0, 0, 0xffff },
    { 0, 0, 0xffff, 0 },
    { 0, 0, 0xffff, 0xffff },
    { 0, 0xffff, 0, 0 },
    { 0, 0xffff, 0, 0xffff },
    { 0, 0xffff, 0xffff, 0 },
    { 0, 0xffff, 0xffff, 0xffff },
};

GdkColor default_color, default_bgcolor;
GdkColor yag_colors[16];

/* command history */
#define MAX_CMDHIST_LINES 100
static GList *cmdhist, *histpos;
static int histlines;

static int setup_open;
static int away_toggling;

#define FLAG_BOLD 1
#define FLAG_REVERSE 2
#define FLAG_UNDERLINE 4

static GdkFont *font_normal, *font_bold, *font_italic, *font_bolditalic;

static char ansitab[8] = { 0, 4, 2, 6, 1, 5, 3, 7 };

/* parse ANSI color string */
static char *convert_ansi(char *str, int *fgcolor, int *bgcolor, int *flags)
{
    char *start;
    int fg, bg, fl, num;

    if (*str != '[') return str;

    start = str;
    fg = *fgcolor-1; bg = *bgcolor; fl = *flags;
    if (bg != -1) bg--;

    str++; num = 0;
    for (;; str++)
    {
        if (*str == '\0') return start;

        if (isdigit(*str))
        {
            num = num*10 + (*str-'0');
            continue;
        }

        if (*str != ';' && *str != 'm') return start;

        switch (num)
        {
            case 0:
                /* reset colors back to default */
                fg = 7;
                bg = -1;
                break;
            case 1:
                /* hilight */
                fg |= 8;
                break;
            case 5:
                /* blink */
                bg = bg == -1 ? 8 : bg | 8;
                break;
            case 7:
                /* reverse */
                fl |= FLAG_REVERSE;
                break;
            default:
                if (num >= 30 && num <= 37)
                    fg = (fg & 0xf8) + ansitab[num-30];
                if (num >= 40 && num <= 47)
                {
                    if (bg == -1) bg = 0;
                    bg = (bg & 0xf8) + ansitab[num-40];
                }
                break;
        }
        num = 0;

        if (*str == 'm')
        {
            *fgcolor = fg+1;
            *bgcolor = bg == -1 ? -1 : bg+1;
            *flags = fl;
            str++;
            break;
        }
    }

    return str;
}

int gui_is_sb_down(WINDOW_REC *win)
{
    GtkAdjustment *adj;

    g_return_val_if_fail(win != NULL, 1);
    g_return_val_if_fail(win->gui != NULL, 1);

    adj = win->gui->text->vadj;

    /* kinda ugly code was here, so I rewrote it to be more readable. [pel]
     * adj->value is the current position, while adj->upper - adj->lower -
     * adj->page_size is the absolute bottom of the window. 
    */
    if((adj->value + 10) < (adj->upper - adj->lower - adj->page_size))
	    return TRUE; /* scrollbar is not down, leave it alone */
    return FALSE; /* scrollbar is down, move it down again after printing */
}

void gui_set_sb_down(WINDOW_REC *win)
{
    GtkAdjustment *adj;

    g_return_if_fail(win != NULL);
    g_return_if_fail(win->gui != NULL);

    adj = win->gui->text->vadj;
    gtk_adjustment_set_value(adj, adj->upper - adj->lower - adj->page_size);
}

/* Write text to window */
static void printtext(char *str, WINDOW_REC *win)
{
    char *ptr, type;
    GdkColor *fg, *bg;
    int fgcolor, bgcolor;
    int flags, scroll_down;

    g_return_if_fail(str != NULL);
    g_return_if_fail(win != NULL);
    g_return_if_fail(win->gui != NULL);

    if (win->gui->parent->araiseset)
    {
        /* Auto raise toggle set, so raise it! */
        gdk_window_show(win->gui->parent->mainwin->window);
    }

    scroll_down = gui_is_sb_down(win);

    gtk_text_freeze (win->gui->text);
    gtk_text_insert (win->gui->text, NULL, NULL, NULL, "\n", 1);
    if (win->gui->lines < MAX_TEXTLINES_HISTORY)
    {
        win->gui->linelen[win->gui->lines++] = strlen(str)+1;
    }
    else
    {
        gtk_text_set_point(win->gui->text, 0);
        gtk_text_forward_delete(win->gui->text, win->gui->linelen[0]);
        gtk_text_set_point(win->gui->text, gtk_text_get_length(win->gui->text));
        memmove(win->gui->linelen, &win->gui->linelen[1],
                (MAX_TEXTLINES_HISTORY-1)*sizeof(win->gui->linelen[0]));
        win->gui->linelen[MAX_TEXTLINES_HISTORY-1] = strlen(str)+1;
    }

    flags = 0;
    fg = &default_color; fgcolor = -2; bgcolor = -1; type = '\0';
    while (*str != '\0')
    {
        for (ptr = str; *ptr != '\0'; ptr++)
            if (*ptr == 2 || *ptr == 3 || *ptr == 4 || *ptr == 22 || *ptr == 27 || *ptr == 31)
            {
                type = *ptr;
                *ptr++ = '\0';
                break;
            }

        if (*str != '\0')
        {
            GdkFont *font;

            if (flags & FLAG_BOLD)
                font = flags & FLAG_UNDERLINE ? font_bolditalic : font_bold;
            else if (flags & FLAG_UNDERLINE)
                font = font_italic;
            else
                font = font_normal;

            if (flags & FLAG_REVERSE)
            {
                if (fgcolor == -2) bg = fg; else bg = &colors[fgcolor];
                fg = bgcolor == -1 ? &default_bgcolor : &colors[bgcolor];
            }
            else
            {
                if (fgcolor != -2) fg = &colors[fgcolor];
                bg = bgcolor == -1 ? NULL : &colors[bgcolor];
            }
            gtk_text_insert (win->gui->text, font, fg, bg, str, -1);
        }
        if (*ptr == '\0') break;

        if (type == '\0')
        {
            /* ... */
        }
        else if (type == 2)
        {
            /* bold */
            flags ^= FLAG_BOLD;
        }
        else if (type == 22)
        {
            /* reverse */
            flags ^= FLAG_REVERSE;
        }
        else if (type == 31)
        {
            /* underline, display as italic.. */
            flags ^= FLAG_UNDERLINE;
        }
        else if (type == 27)
        {
            /* ansi color code */
            ptr = convert_ansi(ptr, &fgcolor, &bgcolor, &flags);
        }
        else if (type == 4)
        {
            /* user specific colors */
            fg = &yag_colors[((int) *ptr++)-1];
            fgcolor = -2;
            bgcolor = -1;
        }
        else if (type == 3)
        {
            /* color! */
            if (*ptr < 16)
            {
                fgcolor = *ptr++;
                bgcolor = -1;
            }
            else
            {
                fgcolor = 0; bgcolor = -1;
                if (!isdigit(*ptr))
                    fgcolor = -2;
                else
                {
                    /* foreground color */
                    fgcolor = *ptr++-'0';
                    if (isdigit(*ptr))
                        fgcolor = fgcolor*10 + (*ptr++-'0');
                    fgcolor++;
                    if (*ptr == ',')
                    {
                        /* back color */
                        bgcolor = 0;
                        if (!isdigit(*++ptr))
                            bgcolor = -1;
                        else
                        {
                            bgcolor = *ptr++-'0';
                            if (isdigit(*ptr))
                                bgcolor = bgcolor*10 + (*ptr++-'0');
                            bgcolor++;
                        }
                    }
                }
            }
        }

        str = ptr;
    }
    gtk_text_thaw (win->gui->text);

    if (!scroll_down) gui_set_sb_down(win);
}

/* signal: ok pressed in password dialog */
void gui_passwd_okfunc(GtkWidget *window)
{
    GtkEntry *entry;
    char *text;
    GUI_PASSWORD_FUNC callback;
    void *data;

    callback = gtk_object_get_data(GTK_OBJECT(window), "callback");
    data = gtk_object_get_data(GTK_OBJECT(window), "data");
    entry = gtk_object_get_data(GTK_OBJECT(window), "entry");
    text = gtk_entry_get_text(entry);

    callback(text, data);

    gtk_widget_destroy(window);
}

/* signal: cancel pressed in password dialog */
void gui_passwd_cancelfunc(GtkWidget *window)
{
    GUI_PASSWORD_FUNC callback;
    void *data;

    callback = gtk_object_get_data(GTK_OBJECT(window), "callback");
    data = gtk_object_get_data(GTK_OBJECT(window), "data");

    callback(NULL, data);
    gtk_widget_destroy(window);
}

/* Create dialog to ask password */
void gui_ask_password(char *label, GUI_PASSWORD_FUNC func, void *data)
{
    GtkWidget *win;
    GtkWidget *hbox;
    GtkWidget *labelw;
    GtkWidget *entry;
    GtkWidget *button;

    win = gtk_dialog_new();
    gtk_signal_connect_object (GTK_OBJECT (win), "delete_event",
                               GTK_SIGNAL_FUNC(gui_passwd_cancelfunc), GTK_OBJECT(win));
    gtk_object_set_data(GTK_OBJECT(win), "callback", func);
    gtk_object_set_data(GTK_OBJECT(win), "data", data);

    labelw = gtk_label_new(label);
    gtk_box_pack_start(GTK_BOX(GTK_DIALOG(win)->vbox), labelw, FALSE, FALSE, 5);
    gtk_widget_show(labelw);

    hbox = gtk_hbox_new(FALSE, 0);
    gtk_container_border_width(GTK_CONTAINER(hbox), 3);
    gtk_box_pack_start(GTK_BOX(GTK_DIALOG(win)->vbox), hbox, FALSE, FALSE, 0);
    gtk_widget_show(hbox);

    entry = gtk_entry_new();
    gtk_entry_set_visibility(GTK_ENTRY(entry), FALSE);
    gtk_object_set_data(GTK_OBJECT(win), "entry", entry);
    gtk_box_pack_start(GTK_BOX(hbox), entry, FALSE, FALSE, 0);
    gtk_signal_connect_object (GTK_OBJECT (entry), "activate",
                               GTK_SIGNAL_FUNC(gui_passwd_okfunc), GTK_OBJECT(win));
    gtk_widget_show(entry);

    button = gtk_button_new_with_label (_("Ok"));
    gtk_box_pack_start (GTK_BOX (GTK_DIALOG(win)->action_area), button, TRUE, FALSE, 0);
    gtk_signal_connect_object (GTK_OBJECT (button), "clicked",
                               GTK_SIGNAL_FUNC(gui_passwd_okfunc), GTK_OBJECT(win));
    GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT);
    gtk_widget_grab_default (button);
    gtk_widget_show (button);

    button = gtk_button_new_with_label (_("Cancel"));
    gtk_box_pack_start (GTK_BOX (GTK_DIALOG(win)->action_area), button, TRUE, FALSE, 0);
    gtk_signal_connect_object (GTK_OBJECT (button), "clicked",
                               GTK_SIGNAL_FUNC(gui_passwd_cancelfunc), GTK_OBJECT(win));
    gtk_widget_show (button);
    gtk_widget_grab_focus(entry);

    gtk_grab_add (GTK_WIDGET (win));
    gtk_widget_show(win);
}

/* Create dialog with entry field */
void gui_entry_dialog(char *label, char *oldtext, GtkSignalFunc okfunc, gpointer data)
{
    GtkWidget *win;
    GtkWidget *hbox;
    GtkWidget *labelw;
    GtkWidget *entry;
    GtkWidget *button;

    win = gtk_dialog_new();
    gtk_object_set_data(GTK_OBJECT(win), "data", data);

    labelw = gtk_label_new(label);
    gtk_box_pack_start(GTK_BOX(GTK_DIALOG(win)->vbox), labelw, FALSE, FALSE, 5);
    gtk_widget_show(labelw);

    hbox = gtk_hbox_new(FALSE, 0);
    gtk_container_border_width(GTK_CONTAINER(hbox), 3);
    gtk_box_pack_start(GTK_BOX(GTK_DIALOG(win)->vbox), hbox, FALSE, FALSE, 0);
    gtk_widget_show(hbox);

    entry = gtk_entry_new();
    gtk_object_set_data(GTK_OBJECT(win), "entry", entry);
    if (oldtext != NULL) gtk_entry_set_text(GTK_ENTRY(entry), oldtext);
    gtk_box_pack_start(GTK_BOX(hbox), entry, FALSE, FALSE, 5);
    gtk_signal_connect_object (GTK_OBJECT (entry), "activate",
                               okfunc, GTK_OBJECT(win));
    gtk_widget_show(entry);

    button = gtk_button_new_with_label (_("Ok"));
    gtk_box_pack_start (GTK_BOX (GTK_DIALOG(win)->action_area), button, TRUE, FALSE, 0);
    gtk_signal_connect_object (GTK_OBJECT (button), "clicked",
                               okfunc, GTK_OBJECT(win));
    GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT);
    gtk_widget_grab_default (button);
    gtk_widget_show (button);

    button = gtk_button_new_with_label (_("Cancel"));
    gtk_box_pack_start (GTK_BOX (GTK_DIALOG(win)->action_area), button, TRUE, FALSE, 0);
    gtk_signal_connect_object (GTK_OBJECT (button), "clicked",
                               GTK_SIGNAL_FUNC (gtk_widget_destroy),
                               GTK_OBJECT(win));
    gtk_widget_show (button);
    gtk_widget_grab_focus(entry);

    gtk_grab_add (GTK_WIDGET (win));
    gtk_widget_show(win);
}

/* Update status bar */
void gui_update_statusbar(WINDOW_REC *win)
{
    char str[200];
    int connected;
    GList *tmp;
    int windows;
    char *act, *ptr;

    if (win != NULL && win != win->gui->parent->selected)
    {
        /* window is hidden - status doesn't need to be displayed */
        return;
    }

    /* find which windows have new text */
    windows = g_list_length(winlist);

    if (windows == 0)
        act = NULL;
    else
    {
        act = g_new0(char, windows*(sprintf(str, "%d", windows)+1)+10);

        ptr = act;
        for (tmp = g_list_first(winlist); tmp != NULL; tmp = tmp->next)
        {
            WINDOW_REC *w;

            w = (WINDOW_REC *) tmp->data;
            if (w->new_data)
            {
                if (act == ptr) ptr += sprintf(ptr, "[ act: ");
                ptr += sprintf(ptr, "%d ", w->num);
            }
        }
        if (ptr != act) strcpy(ptr, "]");
    }

    for (tmp = g_list_first(winlist); tmp != NULL || win != NULL; tmp = tmp->next)
    {
        WINDOW_REC *w;
        SERVER_REC *server;

        w = win != NULL ? win : (WINDOW_REC *) tmp->data;

        if (win == NULL && w != w->gui->parent->selected)
        {
            /* window is hidden - status doesn't need to be displayed */
            continue;
        }

        server = w->curchan == NULL ? w->defserv : w->curchan->server;
        if (server != NULL) connected = server->connected;
        else connected = FALSE;
        g_snprintf(str, 200, _("[ window %d ] [ %s ] [ %s%s ] %s"),
                   w->num,
                   connected ? server->nick : default_nick,
                   connected ? _("Connected to ") : _("Not connected"),
                   connected ? server->name : "",
                   act == NULL ? "" : act);

        gtk_statusbar_pop(GTK_STATUSBAR(w->gui->parent->statusbar), 1);
        gtk_statusbar_push(GTK_STATUSBAR(w->gui->parent->statusbar), 1, str);
        if (win != NULL) break;
    }
    if (act != NULL) g_free(act);
}

/* allocate colors */
static void init_colors(GtkWidget *window)
{
    int n;

    g_return_if_fail(window != NULL);

    for (n = 0; n < COLORS; n++)
    {
        colors[n].pixel =
            (gulong)((colors[n].red & 0xff00)*256 +
                     (colors[n].green & 0xff00) +
                     (colors[n].blue & 0xff00)/256);
        gdk_color_alloc(gtk_widget_get_colormap(window), &colors[n]);
    }

    for (n = 0; n < sizeof(yag_colors)/sizeof(yag_colors[0]); n++)
    {
        yag_colors[n].pixel =
            (gulong)((yag_colors[n].red & 0xff00)*256 +
                     (yag_colors[n].green & 0xff00) +
                     (yag_colors[n].blue & 0xff00)/256);
        gdk_color_alloc(gtk_widget_get_colormap(window), &yag_colors[n]);
    }

    gdk_color_alloc(gtk_widget_get_colormap(window), &default_color);
    gdk_color_alloc(gtk_widget_get_colormap(window), &default_bgcolor);
}

/* Set window style */
static void gui_set_style(WINDOW_REC *win)
{
    GtkWidget *widget;
    GtkStyle style;
    GdkPixmap *pixmap;
    GdkBitmap *mask;

    g_return_if_fail(win != NULL);

    widget = GTK_WIDGET(win->gui->text);

    memcpy(&style, gtk_widget_get_style(widget), sizeof(GtkStyle));

    /* single color */
    style.bg[0] = default_bgcolor;
    gdk_color_alloc(gtk_widget_get_colormap(widget), &default_bgcolor);

    if (default_text_pixmap == NULL)
    {
        style.bg_pixmap[0] = NULL;

        gdk_window_set_background(GTK_TEXT(win->gui->text)->text_area, &default_bgcolor);
    }
    else
    {
        /* pixmap */
#ifdef USE_IMLIB
        GdkImlibImage *img;

        mask = NULL;
        img = gdk_imlib_load_image(default_text_pixmap);
        if (img == NULL)
            pixmap = NULL;
        else
        {
            gdk_imlib_render(img, img->rgb_width, img->rgb_height);
            pixmap = gdk_imlib_move_image(img); mask = NULL;
            gdk_imlib_destroy_image(img);
        }
#else
        pixmap = gdk_pixmap_colormap_create_from_xpm(NULL, 
					    gtk_widget_get_colormap(widget),
					    &mask, NULL,
        default_text_pixmap);
#endif
        style.bg_pixmap[0] = pixmap;
        gtk_widget_set_style(widget, &style);
    }

}

/* Set window style for all windows */
void gui_refresh_windows(void)
{
    g_list_foreach(winlist, (GFunc) gui_set_style, NULL);
}

/* Toggle window autoraising */
void gui_set_autoraise(WINDOW_REC *win, int on)
{
    gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(win->gui->parent->autoraise), on);
}

/* Set away buttons' toggle state */
static void set_away_toggles(GUI_TOPWIN_REC *top)
{
    GList *tmp;

    away_toggling = 1;
    GLIST_FOREACH(tmp, winlist)
    {
        WINDOW_REC *win;

        win = (WINDOW_REC *) tmp->data;
        if (win->gui->parent->awayset != top->awayset)
        {
            win->gui->parent->awayset = top->awayset;
            gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(win->gui->parent->away), top->awayset);
        }
    }
    away_toggling = 0;
}

/* signal: ok pressed in away reason */
static void sig_away_ok(GtkWidget *dialog)
{
    GUI_TOPWIN_REC *top;
    GtkEntry *entry;
    char *data;

    g_return_if_fail(dialog != NULL);

    top = gtk_object_get_data(GTK_OBJECT(dialog), "data");
    entry = gtk_object_get_data(GTK_OBJECT(dialog), "entry");
    data = gtk_entry_get_text(entry);


    data = g_strdup(data);
    gtk_widget_destroy(dialog);

    if (*data != '\0') irccmd_awayall(data);
}

/* signal: auto raise button clicked */
/*static void autoraise_clicked(GtkWidget *widget, GUI_TOPWIN_REC *top)
{
    top->araiseset = !top->araiseset;
}*/

/* signal: away button clicked */
/*static void away_clicked(GtkWidget *widget, GUI_TOPWIN_REC *top)
{
    g_return_if_fail(top != NULL);

    if (away_toggling) return;

    away_toggling = 1;
    gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(top->away), top->awayset);
    away_toggling = 0;

    if (top->awayset)
    {
        irccmd_awayall("");
        set_away_toggles(top);
    }
    else
    {
        gui_entry_dialog(_("Away reason:"), NULL, GTK_SIGNAL_FUNC(sig_away_ok), top);
    }
}*/

/* Set away state */
void gui_set_away(int away)
{
    WINDOW_REC *win;
    GUI_TOPWIN_REC *top;

    win = (WINDOW_REC *) winlist->data;
    if (win == NULL) return;

    top = win->gui->parent;
    top->awayset = away;
    away_toggling = 1;
    gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(top->away), away);
    away_toggling = 0;

    set_away_toggles(top);
}

/* save line for history */
static void add_cmdhistory(CHAN_REC *chan, char *text, int prepend)
{
    /* see if we should add to the current channel's history or if there is no current channel, the global history */
    GList **tmpcmdhist = (chan == NULL) ? &cmdhist : &chan->cmdhist;
    int *tmphistlines = (chan == NULL) ? &histlines : &chan->histlines;
 
    if (*tmphistlines < MAX_CMDHIST_LINES)
        (*tmphistlines)++;
    else
      {
        g_free(g_list_first(*tmpcmdhist)->data);
        *tmpcmdhist = g_list_remove_link(*tmpcmdhist, g_list_first(*tmpcmdhist));
      }

    if (prepend)
        *tmpcmdhist = g_list_prepend(*tmpcmdhist, g_strdup(text));
    else
        *tmpcmdhist = g_list_append(*tmpcmdhist, g_strdup(text));
}

/* signal: enter pressed - process entry */
static int enter_text(GtkEntry *entry)
{
    char *str, *ptr;

    /* pasting text into entry can add some newlines in there, so we have to
       parse them here.. */
    str = gtk_entry_get_text(entry);
    while (str != NULL && *str != '\0')
    {
        ptr = strchr(str, '\n');
        if (ptr != NULL) *ptr++ = '\0';

        add_cmdhistory(curwin->curchan, str, FALSE);

        irc_parse_outgoing(curwin->curchan, str);

        str = ptr;
    }

    gtk_entry_set_text(entry, "");

    if (curwin->curchan == NULL) histpos = NULL;
    else curwin->curchan->histpos = NULL;

    return 0;
}

#define KEYSTATMASK (GDK_SHIFT_MASK|GDK_CONTROL_MASK|GDK_MOD1_MASK)

/* signal: key pressed in entry field */
int keyevent(GtkWidget *widget, GdkEventKey *event, GtkEntry *entry)
{
    GList *pos;
    GList **tmphistpos = (curwin->curchan == NULL) ? &histpos : &curwin->curchan->histpos;
    GList **tmpcmdhist = (curwin->curchan == NULL) ? &cmdhist : &curwin->curchan->cmdhist;
    gtk_widget_grab_focus(GTK_WIDGET(entry));
    switch (event->keyval)
    {
        case GDK_Up:
            /* previous line */
            pos = *tmphistpos;
            if (*tmphistpos == NULL)
                *tmphistpos = g_list_last(*tmpcmdhist);
            else
               *tmphistpos = (*tmphistpos)->prev;
            if (*entry->text != '\0' &&
                (pos == NULL || strcmp(pos->data, gtk_entry_get_text(entry)) != 0))
            {
                /* save the old entry to history */
                add_cmdhistory(curwin->curchan, gtk_entry_get_text(entry), FALSE);
            }
            gtk_entry_set_text(entry, *tmphistpos == NULL ? "" : (*tmphistpos)->data);
            break;

        case GDK_Down:
            /* next line */
            pos = *tmphistpos;
            if (*tmphistpos == NULL)
                *tmphistpos = g_list_first(*tmpcmdhist);
            else
                *tmphistpos = (*tmphistpos)->next;

            if (*gtk_entry_get_text(entry) != '\0' &&
                (pos == NULL || strcmp(pos->data, gtk_entry_get_text(entry)) != 0))
            {
                /* save the old entry to history */
                add_cmdhistory(curwin->curchan, gtk_entry_get_text(entry), TRUE);
            }
            gtk_entry_set_text(entry, *tmphistpos == NULL ? "" : (*tmphistpos)->data);
            break;

        case GDK_Page_Up:
            /* scrollback buffer up.. */
            {
                GtkAdjustment *adj;
                int val;

                adj = curwin->gui->text->vadj;
                val = adj->value - (adj->page_size/2);
                gtk_adjustment_set_value(adj, val > 0 ? val : 0);
            }
            break;

        case GDK_Page_Down:
            /* scrollback buffer down.. */
            {
                GtkAdjustment *adj;
                int val, max;

                adj = curwin->gui->text->vadj;
                val = adj->value + (adj->page_size/2);
                max = adj->upper - adj->lower - adj->page_size;
                gtk_adjustment_set_value(adj, val <= max ? val : max);
            }
            break;

        case GDK_Tab:
            if ((event->state & KEYSTATMASK) == 0)
            {
                /* Just tab */
                char newtext[512];

                irc_nick_tabcompletion(curwin->curchan, gtk_entry_get_text(GTK_ENTRY(curwin->gui->parent->entry)), newtext);
                gtk_entry_set_text(GTK_ENTRY(curwin->gui->parent->entry), newtext);
            }
            break;
			
        case 'N':
            if ((event->state & KEYSTATMASK) == GDK_MOD1_MASK &&
                curwin->curchan != NULL && curwin->chanlist->next != NULL)
            {
                /* Alt+N */
                GList *pos;
                CHAN_REC *chan;
                char *tmp;

                pos = g_list_find(curwin->chanlist, curwin->curchan);
                if (pos == NULL || pos->next == NULL)
                    chan = curwin->chanlist->data;
                else
                    chan = pos->next->data;

                tmp = g_new(char, strlen(chan->name)+INTEGER_LENGTH+2);
                sprintf(tmp, "%d %s", chan->server->handle, chan->name);

                if (*chan->name == '&' || *chan->name == '#')
                    irccmd_join(tmp);
                else
                    irccmd_query(tmp);

                g_free(tmp);
            }
            

        default:
            if (event->keyval >= '0' && event->keyval <= '9' && (event->state & KEYSTATMASK) == GDK_MOD1_MASK)
            {
                /* alt-1..0 pressed */
                GList *tmp;

                tmp = g_list_nth(winlist, event->keyval == '0' ? 9 : event->keyval-'0'-1);
                if (tmp != NULL) gui_window_select((WINDOW_REC *) tmp->data);
            }
            return 0;
    }

    /* key was used, stop signal */
    gtk_signal_emit_stop_by_name(GTK_OBJECT(widget), "key_press_event");
    return 1;
}

/* signal: window closed */
static int delete_event(GtkWidget *window, GtkWidget *t, GUI_TOPWIN_REC *win)
{
    /* close all subwindows in this window */
    while (win->windows)
        irc_window_close(win->windows->data);
    return 0;
}

/* signal: got focus for some widget */
static void got_focus(GtkWindow *a, GtkWidget *b, GUI_TOPWIN_REC *top)
{
    gtk_widget_grab_focus(GTK_WIDGET(top->entry));
    irc_window_focus(top->selected);
}

static void size_request(GtkWidget *win, GtkRequisition *req)
{
    if (win->allocation.width < 50) req->width = gdk_screen_width()*2/3;
    if (win->allocation.height <= 1) req->height = gdk_screen_height()/2;
req->width=640;
req->height=480;
}


/* create_pixmap
 */
static GtkWidget *
create_pixmap(GtkWidget *window, gchar **data)
{
  GtkStyle *style;
  GdkBitmap *mask;
  GdkPixmap *gdk_pixmap;
  GtkWidget *gtk_pixmap;

  style = gtk_widget_get_style(window);

  gdk_pixmap = gdk_pixmap_colormap_create_from_xpm_d(NULL,
						     gtk_widget_get_colormap(window),
	&mask, &style->bg[GTK_STATE_NORMAL],data);
  gtk_pixmap = gtk_pixmap_new(gdk_pixmap, mask);
  gtk_widget_show(gtk_pixmap);

  gtk_style_unref(style);

  return(gtk_pixmap);
}



static 
GtkWidget* new_pixmap (char *filename, GtkWidget *window, GdkColor *background)
	{
	GtkWidget *wpixmap;
	GdkPixmap *pixmap;
	GdkBitmap *mask;
	pixmap = gdk_pixmap_colormap_create_from_xpm (NULL, 
					     gtk_widget_get_colormap (window),
					     &mask, background, filename);
	wpixmap = gtk_pixmap_new (pixmap, mask);
	return wpixmap;
	}


void awayclicked_new(GtkWidget *widget, gpointer data)
{
GUI_TOPWIN_REC *top;
top=(GUI_TOPWIN_REC*) data;

    g_return_if_fail(top != NULL);

    if (away_toggling) return;

    away_toggling = 1;
    gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(top->away), top->awayset);
    away_toggling = 0;

    if (top->awayset)
    {
        /* Not away */
        irccmd_awayall("");
        set_away_toggles(top);
    }
    else
    {
        /* Away, ask reason */
        gui_entry_dialog(_("Away reason:"), NULL, GTK_SIGNAL_FUNC(sig_away_ok), top);
    }
}

void autoraise_flag(GtkWidget *widget, gpointer data)
{
WINDOW_REC *win;
win=(WINDOW_REC*) data;
win->gui->parent->araiseset=!win->gui->parent->araiseset;
}


/* Create new toplevel window */
static void gui_top_window_new(WINDOW_REC *win)
{
    GUI_TOPWIN_REC *top;
    GtkWidget *window;
    GtkWidget *vbox, *hbox;
    GtkWidget *entry;
    GtkWidget *statusbar;
    GtkWidget *nickscroll;
    gchar *user_dir;
	char file_temp[128];
	char file_temp2[128];
	char file_temp3[128];
	int n,m;

    g_return_if_fail(win != NULL);

    top = g_new0(GUI_TOPWIN_REC, 1);
    win->gui->parent = top;

    top->windows = g_list_append(NULL, win);

    /* main window */
#ifdef USE_GNOME
    top->mainwin = window = gnome_app_new("yagIRC", PROGRAM_TITLE);
#else
    top->mainwin = window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), PROGRAM_TITLE);
#endif
    gtk_signal_connect (GTK_OBJECT (window), "delete_event",
                        GTK_SIGNAL_FUNC(delete_event), top);
    gtk_signal_connect (GTK_OBJECT (window), "focus_in_event",
                        GTK_SIGNAL_FUNC(got_focus), top);
    gtk_signal_connect (GTK_OBJECT (window), "size_request",
                        GTK_SIGNAL_FUNC(size_request), window);
    /*
    setting size here doesn't work - if window is resized and nicklist changes
    it's always resized back to startup size, don't know why.. so size_request
    signal works for now.

    gtk_widget_set_usize(window, gdk_screen_width()*2/3, gdk_screen_height()/2);
    */
    gtk_window_set_policy(GTK_WINDOW(window), TRUE, TRUE, FALSE);

    vbox = gtk_vbox_new(FALSE, 0);
#ifdef USE_GNOME
    gnome_app_set_contents(GNOME_APP(window), vbox);
#else
    gtk_container_add(GTK_CONTAINER(window), vbox);
#endif
    gtk_widget_show(vbox);

    /* menu */
    gui_menu_create(win);
    /*gtk_window_add_accelerator_table(GTK_WINDOW(window), top->table);*/
#ifdef USE_GNOME
    gnome_app_set_menus(GNOME_APP(window), GTK_MENU_BAR(top->menubar));
#else
    gtk_box_pack_start(GTK_BOX(vbox), top->menubar, FALSE, TRUE, 0);
    gtk_widget_show(top->menubar);
#endif

/* Add a toolbar */

	top->toolbar = gtk_toolbar_new(GTK_ORIENTATION_HORIZONTAL, GTK_TOOLBAR_ICONS);
#ifdef USE_GNOME
    gnome_app_set_toolbar ( GNOME_APP(window), GTK_TOOLBAR (top->toolbar) );
#else
	gtk_box_pack_start(GTK_BOX(vbox), top->toolbar, FALSE, TRUE, 0);

	gtk_toolbar_set_button_relief (GTK_TOOLBAR (top->toolbar), GTK_RELIEF_NONE);
#endif

/* first look for pixmaps in ~/.yagirc/pixmaps/ , else use the default in 
SCRIPT_PREFIX/pixmaps */

	user_dir = getenv("HOME");
	snprintf(file_temp, 127, "%s/%s/", user_dir, CONFIG_DIR);
	snprintf(file_temp2, 127, "%s%s", file_temp, PIXMAPS_DIR);

    snprintf(file_temp3, 127, "%s/%s", file_temp2, global_settings->icon[0]);
    
	if (!exists(file_temp3)) {
	snprintf(file_temp2, 127, "%s%s", SCRIPT_PREFIX, PIXMAPS_DIR);
	snprintf(file_temp3, 127, "%s/%s", file_temp2, global_settings->icon[0]);
	}

	n = 0;
	m = 0;
	ntoolbar_rec = sizeof(toolbar_rec) / sizeof(toolbar_rec[0]);

	for ( n = 0; n < ntoolbar_rec; n++) 
 {
	snprintf(file_temp3, 127, "%s/%s", file_temp2, global_settings->icon[m]);

	if (toolbar_rec[n].flag == TBNORMALBUTTON )
	{
    	if (exists(file_temp3)) {
 			gtk_toolbar_append_item(GTK_TOOLBAR(top->toolbar), _(toolbar_rec[n].lab1),
			_(toolbar_rec[n].lab2), toolbar_rec[n].lab3,
			new_pixmap (file_temp3, win->gui->parent->mainwin,
			&window->style->bg[GTK_STATE_NORMAL]), toolbar_rec[n].callback, top->toolbar);
		}
		else { /* icon file not found use notfound.xpm */
			fprintf (stderr, _("file %s doesn't exist\n"), file_temp3);
			gtk_toolbar_append_item(GTK_TOOLBAR(top->toolbar), _(toolbar_rec[n].lab1),
			_(toolbar_rec[n].lab2), _(toolbar_rec[n].lab1),
			create_pixmap(window, notfound_xpm), toolbar_rec[n].callback, top->toolbar);
			}
		
	}
	if (toolbar_rec[n].flag == TBTOGGLEBUTTON ) {
		gtk_toolbar_append_element(GTK_TOOLBAR(top->toolbar), GTK_TOOLBAR_CHILD_TOGGLEBUTTON,
		NULL,_(toolbar_rec[n].lab1), _(toolbar_rec[n].lab2), toolbar_rec[n].lab3,
		new_pixmap (file_temp3, window,
		&window->style->bg[GTK_STATE_NORMAL]), toolbar_rec[n].callback , win);
	}
	
	m++; /* global_settings->icon[] is linear , toolbar_rec[] is not */
	if (toolbar_rec[n].flag == TBSPACE ) {
	gtk_toolbar_append_space(GTK_TOOLBAR(top->toolbar));
	m--;
	}

	if (toolbar_rec[n].flag == TBSPECIALBUTTON ) {
		top->away = gtk_toolbar_append_element(GTK_TOOLBAR(top->toolbar), GTK_TOOLBAR_CHILD_TOGGLEBUTTON,
		NULL,_(toolbar_rec[n].lab1), _(toolbar_rec[n].lab2), toolbar_rec[n].lab3,
		new_pixmap (file_temp3, window,
		&window->style->bg[GTK_STATE_NORMAL]), toolbar_rec[n].callback , top);
	}
 }
                                
	gtk_widget_show (top->toolbar);

/* initial look of the tool bar */

	switch (global_settings->toolbar_style) {
	case 0:
		gtk_toolbar_set_style(GTK_TOOLBAR(top->toolbar), GTK_TOOLBAR_ICONS);
		break;
	case 1:
		gtk_toolbar_set_style(GTK_TOOLBAR(top->toolbar), GTK_TOOLBAR_TEXT);
		break;
	case 2:
		gtk_toolbar_set_style(GTK_TOOLBAR(top->toolbar), GTK_TOOLBAR_BOTH);
		break;
	case 3:
        /* disable toolbar */
		gtk_widget_hide (top->toolbar);
	}


    /* channel list */
    top->chanbox = gtk_hbox_new(FALSE, 0);
    gtk_box_pack_start(GTK_BOX(vbox), top->chanbox, FALSE, FALSE, 0);
    gtk_widget_show(top->chanbox);

/*    top->away = gtk_toggle_button_new_with_label(_("Away"));
    gtk_signal_connect (GTK_OBJECT (top->away), "clicked",
                        GTK_SIGNAL_FUNC(away_clicked), top);
    gtk_box_pack_end(GTK_BOX(top->chanbox), top->away, FALSE, FALSE, 0);
    gtk_widget_show(top->away);

    top->autoraise = gtk_toggle_button_new_with_label(_("Auto raise"));
    gtk_signal_connect (GTK_OBJECT (top->autoraise), "clicked",
                        GTK_SIGNAL_FUNC(autoraise_clicked), top);
    gtk_box_pack_end(GTK_BOX(top->chanbox), top->autoraise, FALSE, FALSE, 0);
    gtk_widget_show(top->autoraise);
*/
    top->hbox = hbox = gtk_hbox_new(FALSE, 0);
    gtk_box_pack_start(GTK_BOX(vbox), hbox, TRUE, TRUE, 0);
    gtk_widget_show(hbox);

    /* nick list */
    top->nickscroll = nickscroll = gtk_scrolled_window_new(NULL, NULL);
    
    gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(nickscroll),
                                   GTK_POLICY_AUTOMATIC, GTK_POLICY_ALWAYS);
    gtk_box_pack_start(GTK_BOX(hbox), nickscroll, FALSE, FALSE, 0);
    gtk_widget_show (nickscroll);

    top->nicklist = gtk_list_new();
    gtk_list_set_selection_mode (GTK_LIST(top->nicklist), GTK_SELECTION_MULTIPLE);
    gtk_signal_connect (GTK_OBJECT (top->nicklist), "button_press_event",
                        GTK_SIGNAL_FUNC(signick_button_pressed), win);
    gtk_signal_connect (GTK_OBJECT (top->nicklist), "button_release_event",
                        GTK_SIGNAL_FUNC(signick_button_released), win);
    gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(nickscroll), top->nicklist);
    gtk_widget_show(top->nicklist);
    top->nicklistlen = 0;

    /* entry field */
    top->entry = entry = gtk_entry_new_with_max_length(490);
    gtk_signal_connect (GTK_OBJECT (entry), "activate",
                        GTK_SIGNAL_FUNC(enter_text), NULL);
    gtk_box_pack_start(GTK_BOX(vbox), entry, FALSE, FALSE, 0);
    gtk_widget_show(entry);

    /* redirect all key presses in window to entry field */
    gtk_signal_connect (GTK_OBJECT (window), "key_press_event",
                        GTK_SIGNAL_FUNC(keyevent), entry);

    top->statusbar = statusbar = gtk_statusbar_new();
    gtk_statusbar_push(GTK_STATUSBAR(top->statusbar), 1, "\n");

#ifdef USE_GNOME
    gnome_app_set_statusbar ( GNOME_APP(window), top->statusbar );
#else
    gtk_box_pack_start(GTK_BOX(vbox), statusbar, FALSE, FALSE, 0);
#endif

    gtk_widget_show(statusbar);

    init_colors(window);

    gtk_widget_show(window);
}

static void sig_change_window(GtkWidget *widget, GdkEventButton *event, WINDOW_REC *window)
{
    if (event->button == 1)
        gui_window_select(window);
}

void gui_window_init(WINDOW_REC *win, WINDOW_REC *parent)
{
    GUI_TOPWIN_REC *top;
    GtkWidget *text;
    GtkWidget *textscroll;
    char tmp[INTEGER_LENGTH];

    win->drawfunc = (DRAW_FUNC) printtext;
    win->drawfuncdata = win;

    win->gui = g_new0(GUI_WINDOW_REC, 1);

    if (parent == NULL)
    {
        gui_top_window_new(win);
    }
    else
    {
        win->gui->parent = parent->gui->parent;
        win->gui->parent->windows =
            g_list_append(win->gui->parent->windows, win);
    }
    top = win->gui->parent;

    /* add window number button.. */
    win->gui->box = gtk_hbox_new(FALSE, 0);
    gtk_box_pack_start(GTK_BOX(top->chanbox), win->gui->box, FALSE, FALSE, 0);
    gtk_widget_show(win->gui->box);

    win->gui->eventbox = gtk_event_box_new();
    gtk_signal_connect (GTK_OBJECT (win->gui->eventbox), "button_release_event",
                        GTK_SIGNAL_FUNC(sig_change_window), win);
    gtk_box_pack_start(GTK_BOX(win->gui->box), win->gui->eventbox, FALSE, FALSE, 5);
    gtk_widget_show(win->gui->eventbox);

    sprintf(tmp, "%d", win->num);
    win->gui->label = gtk_label_new(tmp);
    gtk_container_add(GTK_CONTAINER(win->gui->eventbox), win->gui->label);
    gtk_widget_show(win->gui->label);

    /* irc text field */
    text = gtk_text_new(NULL, NULL);
    win->gui->text = GTK_TEXT(text);
    gtk_signal_connect (GTK_OBJECT (text), "button_release_event",
                        GTK_SIGNAL_FUNC(sigchantext_button_released), win);
    gtk_signal_connect (GTK_OBJECT (text), "focus_in_event",
                        GTK_SIGNAL_FUNC(got_focus), top);
    gtk_box_pack_start(GTK_BOX(top->hbox), text, TRUE, TRUE, 0);
    gtk_box_reorder_child(GTK_BOX(top->hbox), text, 0);
    gtk_text_set_word_wrap(GTK_TEXT(text), TRUE);

    win->gui->textscroll = textscroll =
        gtk_vscrollbar_new (GTK_TEXT (text)->vadj);
    gtk_box_pack_start(GTK_BOX(top->hbox), textscroll, FALSE, FALSE, 0);
    gtk_box_reorder_child(GTK_BOX(top->hbox), textscroll, 1);

    gui_window_select(win);

    gui_set_style(win);
}

/* Deinitialize IRC window */
void gui_window_deinit(WINDOW_REC *window)
{
    GList *tmp;
    char str[INTEGER_LENGTH];

    g_return_if_fail(window != NULL);
    if (window->gui == NULL) return;

    if (window->gui->parent->selected == window)
        window->gui->parent->selected = NULL;

    for (tmp = g_list_nth(winlist, window->num); tmp != NULL; tmp = tmp->next)
    {
        WINDOW_REC *win;

        win = (WINDOW_REC *) tmp->data;
        sprintf(str, "%d", win->num-1);
        gtk_label_set(GTK_LABEL(win->gui->label), str);
    }

    gtk_widget_destroy(GTK_WIDGET(window->gui->eventbox));
    gtk_widget_destroy(GTK_WIDGET(window->gui->text));
    gtk_widget_destroy(window->gui->textscroll);

    window->gui->parent->windows =
        g_list_remove(window->gui->parent->windows, window);

    if (window->gui->parent->windows == NULL)
    {
        /* Remove main window */
        gtk_widget_destroy(window->gui->parent->mainwin);
        g_free(window->gui->parent); window->gui->parent = NULL;
    }
    g_free(window->gui); window->gui = NULL;
}

/* Clear all text from window */
void gui_window_clear(WINDOW_REC *win)
{
    g_return_if_fail(win != NULL);

    gtk_text_set_point(win->gui->text, 0);
    gtk_text_forward_delete(win->gui->text, gtk_text_get_length(win->gui->text));
    win->gui->lines = 0;
}

/* IRC channel/window changed, update everything you can think of.. */
void gui_window_update(WINDOW_REC *win)
{
    g_return_if_fail(win != NULL);

    /* Enable/disable (dis)connect menu items */
    /*if (cserver == NULL)
    {
        menus_set_sensitive(win, _("<Main>/IRC/Connect..."), TRUE);
        menus_set_sensitive(win, _("<Main>/IRC/Disconnect..."), FALSE);
    }
    else
    {
        menus_set_sensitive(win, _("<Main>/IRC/Connect"), FALSE);
        menus_set_sensitive(win, _("<Main>/IRC/Disconnect"), TRUE);
    }*/
//    if (!gui_is_sb_down(win)) gui_set_sb_down(win);
}

/* Change focus to specified window */
void gui_window_select(WINDOW_REC *win)
{
    GUI_WINDOW_REC *gui;
    int scroll_bar;

    g_return_if_fail(win != NULL);

    scroll_bar = gui_is_sb_down(win);

    if (curwin != NULL && win->gui->parent != curwin->gui->parent)
    {
        /* not in same toplevel window .. we should set focus to that window,
        but how?? Didn't find any such function from GDK/GTK, XSetInputFocus
        changes to that window but WM doesn't notice the change so I think it
        should be done with some WM hints.. If there is such... */
        gdk_window_raise(win->gui->parent->mainwin->window);
        /*XSetInputFocus(GDK_WINDOW_XDISPLAY(win->gui->parent->mainwin->window),
                       GDK_WINDOW_XWINDOW(win->gui->parent->mainwin->window),
                       RevertToParent, CurrentTime);*/
        /*gtk_widget_grab_focus(win->gui->parent->mainwin);*/
    }

    if (win->gui->parent->selected != NULL)
    {
        /* Hide old window */
        gui = win->gui->parent->selected->gui;
        gtk_widget_hide(GTK_WIDGET(gui->text));
        gtk_widget_hide(gui->textscroll);
    }

    gtk_widget_set_usize(win->gui->parent->nickscroll, 1, -1);
    gtk_widget_show(GTK_WIDGET(win->gui->text));
    gtk_widget_show(win->gui->textscroll);
    win->gui->parent->selected = win;

    irc_window_focus(win);
    if (win->curchan != NULL)
        gui_select_channel(win, win->curchan);
    else
        gui_draw_channel(win, NULL);

    gui_update_statusbar(curwin);
    if (!scroll_bar) gui_set_sb_down(win);
}

/* Change to any new window in same top window */
void gui_window_select_new(WINDOW_REC *win)
{
    GList *tmp;

    int scroll_bar;

    /* select new hidden window */
    GLIST_FOREACH(tmp, winlist)
    {
        curwin = (WINDOW_REC *) tmp->data;
        if (curwin != win && curwin->gui->parent == win->gui->parent)
        {
            /* new window found, great. */
            break;
        }
    }

    scroll_bar = gui_is_sb_down(win);

    if (tmp == NULL)
    {
        /* this was the last hidden window .. any gui windows around? */
        GLIST_FOREACH(tmp, winlist)
        {
            curwin = (WINDOW_REC *) tmp->data;
            if (curwin != win)
            {
                /* new window found, great. */
                break;
            }
        }
        if (tmp == NULL)
        {
            /* This was the last window */
            irccmd_quit("");
        }
    }

    if (curwin != NULL)
        gui_window_select(curwin);
    if (!scroll_bar) gui_set_sb_down(win);
}

/* Quit program requested */
void gui_exit(void)
{
    gtk_main_quit();
}

/* Someone in notify list joined IRC */
void gui_notify_join(char *nick)
{
    drawtext(NULL, NULL, LEVEL_YAGNOTICE, IRCTXT_IRCJOIN, nick);
}

/* Someone in notify list left IRC */
void gui_notify_part(char *nick)
{
    drawtext(NULL, NULL, LEVEL_YAGNOTICE, IRCTXT_IRCPART, nick);
}

/* server got connected */
void gui_connected(SERVER_REC *serv, int state)
{
    gui_update_statusbar(NULL);
}

/* Initialize GUI */
void gui_init(void)
{
    cmdhist = NULL;
    histlines = 0;

    setup_open = 0;
    away_toggling = 0;

    if (!gui_read_config())
    {
        /* configuration file not created yet - run setup */
        setup_open = 1;
        gui_write_config();
    }

    font_normal = gdk_font_load("-adobe-helvetica-medium-r-normal--*-120-*-*-*-*-*-*");
    font_bold = gdk_font_load("-adobe-helvetica-bold-r-normal--*-120-*-*-*-*-*-*");
    font_italic = gdk_font_load("-adobe-helvetica-medium-o-normal--*-120-*-*-*-*-*-*");
    font_bolditalic = gdk_font_load("-adobe-helvetica-bold-o-normal--*-120-*-*-*-*-*-*");
}

/* deinitialize GUI */
void gui_deinit(void)
{
    if (cmdhist != NULL)
    {
        g_list_foreach(cmdhist, (GFunc) g_free, NULL);
        g_list_free(cmdhist);
    }
}

int main (int argc, char *argv[])
{
    WINDOW_REC *window;

#ifdef ENABLE_NLS
    bindtextdomain("yagirc",NLS_PREFIX);
    textdomain("yagirc");
#endif

#ifdef USE_GNOME
    gnome_init ("yagIRC", VERSION, argc, argv );
#else
    gtk_set_locale();
    gtk_init (&argc, &argv);
  #ifdef USE_IMLIB
    gdk_imlib_init();
  #endif
#endif

    gui_setup_init();
    gui_init();
    irc_init();

    window = irc_window_new(NULL, NULL);
    if (setup_open) gui_setup();

    /* initialisation that needs a window */
    irc_init_after();

#ifdef WE_ARE_NUTS
    if (global_settings->sound)
      {
        if (sound_init() == SOUND_INIT_FAIL) global_settings->sound = FALSE;
      }
#endif

    gtk_main ();

#ifdef WE_ARE_NUTS
    sound_deinit();
#endif

    irc_deinit();
    gui_deinit();
    gui_setup_deinit();

    return 0;
}
