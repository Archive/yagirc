#ifndef __CTCP_TXT_H
#define __CTCP_TXT_H

#include "gui.h"
#include "intl.h"

static char *  IRCTXT_CTCP_REPLY=N_("%9CTCP %_%s%_ reply from %_%s%_: %!%s\n");
static char *  IRCTXT_CTCP_PING_REPLY=N_("%9CTCP %_PING%_ reply from %_%s%_: %!%d seconds\n");

#endif
