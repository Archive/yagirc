#include "config.h"
#include <unistd.h>
#include <string.h>

#include <glib.h>

#ifdef WE_ARE_NUTS

#ifdef USE_ESD
#include <esd.h>
#endif

#include "sound.h"
#include "irc.h"

Sound *sound_load(char *file)
{

  FILE *f;
  Sound *s;
  char buf[4];
  char str[FILEPATH_LEN_MAX];
  WAVFormatChunk fmt;
  int skipl = 0;
  int skipr = 0;
  char bytes = 0;
  char stereo = 0;
  int len;

  if (!exists(file)) strcpy(str, pathtofile(file, global_settings->soundpath));
  else strcpy(str, file);

  g_warning("%s//", str);

#ifdef USE_ESD
  if (exists(str))
    {
      f = fopen(str, "r");
      if (!f)
         return(NULL);
      s = g_malloc(sizeof(Sound));
     if (!s)
        {
          fclose(f);
          return NULL;
        }
      s->rate = 44100;
      s->format = ESD_STREAM | ESD_PLAY;
      s->samples = 0;
      s->data = NULL;
      s->id = 0;
      fread(buf, 1, 4, f);
      if ((buf[0] != 'R') ||
          (buf[1] != 'I') ||
          (buf[2] != 'F') ||
          (buf[3] != 'F'))
       {
          /* not a RIFF WAV file */
          fclose(f);
          g_free(s);
          g_warning("File %s is not a valid audio format", file);
          return NULL;
        }
      fread(buf, 1, 4, f);
      fread(buf, 1, 4, f);
      fread(&fmt, sizeof(WAVFormatChunk), 1, f);
      if ((fmt.chunkID[0] == 'f') &&
          (fmt.chunkID[1] == 'm') &&
          (fmt.chunkID[2] == 't') &&
          (fmt.chunkID[3] == ' '))

        {
          if (fmt.wFormatTag != 1)
            {
              /* unknown WAV encoding format - exit */
              fclose(f);
              g_free(s);
              g_warning("File %s is not a valid audio format", file);
              return NULL;
            }
          skipl = 0;
          skipr = 0;
          bytes = 0;
          stereo = 0;
          if (fmt.wChannels == 1)
            s->format |= ESD_MONO;
          else if (fmt.wChannels == 2)
               {
                 stereo = 1;
                 s->format |= ESD_STEREO;
               }
          else
                 {
                    stereo = 1;
                    s->format |= ESD_STEREO;
                    if (fmt.wChannels == 3)
                      {
                         skipl = 0;
                         skipr = 1;
                      }
                    else if (fmt.wChannels == 4)
                          {
                             skipl = 0;
                            skipr = 2;
                          }
                    else if (fmt.wChannels == 6)
                          {
                             skipl = 3;
                             skipr = 1;
                          }
                    else
                          {
                             /* unknown channel encoding */
                             fclose(f);
                             g_free(s);
                             g_warning("File %s is not a valid audio format", file);
                             return NULL;
                          }
                 }
          s->rate = fmt.dwSamplesPerSec;
          if (fmt.wBitsPerSample <= 8)
            {
              bytes = 1;
              s->format |= ESD_BITS8;
            }
          else if (fmt.wBitsPerSample <= 16)
                  s->format |= ESD_BITS16;
          else
            {
              /* unknown bits encoding encoding */
              fclose(f);
              g_free(s);
              g_warning("File %s is not a valid audio format", file);
              return NULL;
            }
        } /* if ((fmt.chunkID[0] == 'f') && ... */
      for (;;)
        {
           if (fread(buf, 1, 4, f) &&
              fread(&len, 4, 1, f))
             {
               if ((buf[0] != 'd') ||
                   (buf[1] != 'a') ||
                   (buf[2] != 't') ||
                   (buf[3] != 'a'))
                 fseek(f, len, SEEK_CUR);
               else
                 {
                   s->data = g_malloc(len);
                   if (!s->data)
                    {
                       fclose(f);
                       g_free(s);
                       return(NULL);
                     }
                   if ((skipl == 0) && (skipr == 0) && (!bytes))
                     {
                        fread(s->data, len, 1, f);
                     }
                   s->samples = len;
                   if (stereo)
                     s->samples /= 2;
                   if (!bytes)
                    s->samples /= 2;
                   fclose(f);
                   return s;
                 }
             } /* if (fread(buf, 1, 4, f) && fread(&len, 4, 1, f)) */
         else
           {
             fclose(f);
             return NULL;
           }
        } /* for (;;) */
      fclose(f);
      g_free(s);
      if (s->data)
        g_free(s->data);
    }
  else 
    {
      g_warning("File %s not found", file);
      return NULL;
    }
#else
  if (exists(file))
    {
      if (!fileisaudio(file))
        {
          return NULL;
        }
      else
        {
          s = fopen(file, "r");
        }
    }
  else
    {
       return NULL;
    } 
#endif
}

int sound_play(Sound *s)
{
  if (s != NULL)
    {
#ifdef USE_ESD
      int size;
      int confirm = 0;
      if ((sound_fd < 0) || (!global_settings->sound) || (!s))
        return SOUND_FAIL;
      size = s->samples;
      if (s->format & ESD_STEREO)
        size *= 2;
      if (!s->id)
        {
          if (sound_fd >= 0)
            {
              if (s->data)
                {
//                  s->id = esd_sample_cache(sound_fd, s->format, s->rate,
//                                           size * 2, "yagIRC");
                  write(sound_fd, s->data, size * 2);
//                  confirm = esd_confirm_sample_cache(sound_fd);
//                  if (s->id <= 0 || confirm != s->id)
//                    {
//                      printf("SoundPlay: error caching sample <%d>!\n", s->id);
//                      s->id = 0;
//                    }
                  g_free(s->data);
                  s->data = NULL;
                }
            }
        }

//      if (s->id > 0)
//        {
//          esd_sample_play(sound_fd, s->id);
//          esd_sample_free(sound_fd, s->id);
//        }

      fsync(sound_fd);
    }
#else

#endif
  return SOUND_OK;
}

void sound_destroy(Sound *s)
{
  if (s != NULL)
    {
#ifdef USE_ESD
      if ((s->id != 0) && (sound_fd >= 0))
        {
          esd_sample_free(sound_fd, s->id);
        }
      if (s->data)
        g_free(s->data);
      g_free(s);
      return;
#else
      fclose(s);
      return;
#endif
    }
  else return;
}

int sound_init()
{
#ifdef USE_ESD
    int temp;
    temp = esd_open_sound(NULL);
    if (temp >= 0) 
      {
        sound_fd = temp;
        esound_working = TRUE;
        return SOUND_INIT_OK;
      }
    else
      {
        printf("EsounD is either not running or not operating properly, attempting to start it\n");
        execprog("esd");
        temp = esd_open_sound(NULL);
        if (temp < 0)
          {
            printf("EsounD either doesn't exist, isn't installed properly, not operating properly, or not in the path.  We will try to open /dev/dsp and use that.\n");
           }
        else
          {
            printf("EsounD succesfully started and connected to.  We will shut it down when we finish with it.\n");
            esound_started = TRUE;
            sound_fd = temp;
            esound_working = TRUE;
            return SOUND_INIT_OK;
          }
      }
    esound_working = FALSE;
#else
    FILE *temp;
    if (exists("/dev/dsp")) temp = fopen("/dev/dsp", "a");
    if (temp != NULL)
      {
        sound_fd = temp;
        g_free(temp);
        return SOUND_INIT_OK;
      }
    else
      {
        sound_fd = NULL;
        g_free(temp);
        return -1;
      }  
#endif
}

void sound_deinit()
{
#ifdef USE_ESD
  if (sound_fd >= 0) close(sound_fd);
#else
  if (sound_fd != NULL) fclose(sound_fd);
#endif
  return;
}

#endif
