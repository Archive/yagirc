/*

 gui_channels.c : User interface for handling channels

    Copyright (C) 1998 Timo Sirainen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "config.h"

#include <stdio.h>
#include <string.h>

#include <gtk/gtk.h>
#include <glib.h>

#include "os.h"
#include "dcc.h"
#include "irc.h"
#include "gui.h"
#include "commands.h"
#include "script.h"
#include "misc.h"
#include "options.h"
#include "intl.h"

enum {
    TARGET_CHANNEL,
    TARGET_ROOTWIN
};

static int chan_toggled = 0;

static void chanlist_popup(CHAN_REC *chan, GdkEventButton *event);
static void window_popup(WINDOW_REC *win, GdkEventButton *event);

/* Change channel button label */
void gui_channel_change_name(CHAN_REC *chan, char *name)
{
    GtkLabel *label;

    label = gtk_container_children(GTK_CONTAINER(chan->gui))->data;
    gtk_label_set(label, name);
}

/* Hilight channel button */
void gui_channel_hilight(CHAN_REC *chan)
{
    GtkStyle *style;
    GtkWidget *wid;

    g_return_if_fail(chan != NULL);

    /* hilight new selected channel */
    wid = gtk_container_children(GTK_CONTAINER(chan->gui))->data;
//    memcpy(&style, gtk_widget_get_style(wid), sizeof(style));
    style = gtk_widget_get_style ( wid );
    style->fg[0] = colors[BRED];
    gtk_widget_set_style(wid, style);
}

/* Dehilight channel button */
void gui_channel_dehilight(CHAN_REC *chan)
{
    GtkStyle style;
    GtkWidget *wid;

    g_return_if_fail(chan != NULL);

    /* hilight new selected channel */
    wid = gtk_container_children(GTK_CONTAINER(chan->gui))->data;
    memcpy(&style, gtk_widget_get_style(wid), sizeof(style));
    style.fg[0] = colors[BLACK];
    gtk_widget_set_style(wid, &style);
}

/* Channel changed in window - draw new topic, nicks, hilight channel button */
void gui_draw_channel(WINDOW_REC *win, CHAN_REC *chan)
{
    g_return_if_fail(win != NULL);

    gui_nicklist_redraw(win);

    if (win->gui->parent->highchan != NULL)
    {
        /* Some channel was already hilighted, change it back to black */
        chan_toggled = 1;
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(win->gui->parent->highchan->gui), FALSE);
        chan_toggled = 0;
    }

    if (chan == NULL)
    {
        /* just dehighlighted channel - no channel selected now.. */
        gtk_widget_set_usize(win->gui->parent->nickscroll, 1, -1);
        gtk_window_set_title(GTK_WINDOW(win->gui->parent->mainwin), PROGRAM_TITLE);
        win->gui->parent->highchan = NULL;
        return;
    }

    /* draw topic */
    gui_change_topic(chan);

    /* hilight new selected channel */
    win->gui->parent->highchan = chan;
    chan_toggled = 1;
    gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(chan->gui), TRUE);
    chan_toggled = 0;

    /* update the status bar, just in case... */
    gui_update_statusbar(win);

    /* check if we need to scroll down and if we do, do it */
    if (!gui_is_sb_down(win)) gui_set_sb_down(win);
}

/* signal: channel clicked */
static void sigchan_clicked(GtkWidget *widget, CHAN_REC *chan)
{
    char *tmp;

    if (chan_toggled) return;

    chan_toggled = 1;
    if (chan->window->gui->parent->highchan == chan)
    {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(chan->gui), TRUE);
        chan_toggled = 0;
        return;
    }

    if (chan->window != curwin) gui_window_select(chan->window);
    gui_draw_channel(chan->window, chan);

    /* gtk's clicked-signal changes this back to TRUE... */
    gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(chan->gui), TRUE);

    tmp = g_new(char, strlen(chan->name)+INTEGER_LENGTH+2);
    if (chan->server != NULL)
    {
      sprintf(tmp, "%d %s", chan->server->handle, chan->name);
    }
    else
    {
      sprintf(tmp, "%s", chan->name);
    }

    if (is_channel(*chan->name))
        irccmd_join(tmp);
    else
        irccmd_query(tmp);

    g_free(tmp);
    chan_toggled = 0;
}

/* signal: button released */
static void sigchan_button_released(GtkWidget *widget, GdkEventButton *event, CHAN_REC *chan)
{
    g_return_if_fail(chan != NULL);

    if (event->button == 3)
        chanlist_popup(chan, event);
}

static void  
sigchan_drag_get (GtkWidget          *widget,
		  GdkDragContext     *context,
		  GtkSelectionData   *selection_data,
		  guint               info,
		  guint               time,
		  CHAN_REC           *chan)
{
  WINDOW_REC *win;

  g_return_if_fail(chan != NULL);

  switch (info) {
  case TARGET_CHANNEL:
    gtk_selection_data_set (selection_data,
			    selection_data->target,
			    8, (guchar *)&chan, sizeof(chan));
    break;
  case TARGET_ROOTWIN:
    win = irc_window_new(cserver, NULL);
    win->curchan = chan;
    gui_select_channel(win, chan);
    break;
  }
}

static void sigchan_drag_received (GtkNotebook        *book,
				   GdkDragContext     *context,
				   GtkSelectionData   *selection_data,
				   guint               info,
				   guint               time,
				   CHAN_REC           *chan)
{
    CHAN_REC *from;

    g_return_if_fail(chan != NULL);
    g_return_if_fail(selection_data->length == sizeof(from));

    from = *((CHAN_REC **) selection_data->data);
//    if (from == curwin->curchan)
        gui_window_select(chan->window);
    curwin->curchan = from;
    gui_select_channel(curwin, from);
    irccmd_join(from->name);
}

/* Draw new channel button */
GtkWidget *gui_private_add_channel_button(GtkBox *box, CHAN_REC *chan)
{
  GtkWidget *but;
  static GtkTargetEntry channel_drag_types[] = { 
	{ NULL,                         0, TARGET_CHANNEL },
	{ "application/x-rootwin-drop", 0, TARGET_ROOTWIN }
  };
 
  g_return_val_if_fail(box != NULL, NULL);
  g_return_val_if_fail(chan != NULL, NULL);

    if (!channel_drag_types[0].target) {
      gchar hostname[128] = "\0", pid[12];
      
      gethostname(hostname, 127);
      sprintf(pid, "%d", getpid());
      
      channel_drag_types[0].target = g_malloc ( strlen (hostname) + strlen (pid) + 29 );
      sprintf(channel_drag_types[0].target, "%s%s%s",
	      "application/x-yagirc-channel-", hostname, pid);
    }

  /* create widget */
  but = gtk_toggle_button_new_with_label(chan->name);
  gtk_signal_connect (GTK_OBJECT (but), "button_release_event",
                      GTK_SIGNAL_FUNC(sigchan_button_released), chan);
  gtk_signal_connect (GTK_OBJECT (but), "clicked",
                      GTK_SIGNAL_FUNC(sigchan_clicked), chan);
  gtk_signal_connect (GTK_OBJECT (but), "drag_data_get",
                      GTK_SIGNAL_FUNC(sigchan_drag_get), chan);
  gtk_signal_connect (GTK_OBJECT(but), "drag_data_received",
                      GTK_SIGNAL_FUNC(sigchan_drag_received), chan);
  gtk_drag_dest_set(but,
                    GTK_DEST_DEFAULT_MOTION |
                    GTK_DEST_DEFAULT_HIGHLIGHT |
                    GTK_DEST_DEFAULT_DROP,
                    channel_drag_types, 1,
                    GDK_ACTION_COPY);
  gtk_drag_source_set(but,
                      GDK_BUTTON1_MASK,
                      channel_drag_types, 2,
                      GDK_ACTION_COPY);


  gtk_box_pack_start(GTK_BOX(chan->window->gui->box), but, FALSE, FALSE, 0);
  gtk_widget_show(but);

  return but;
}

/* Remove channel button */
void gui_private_remove_channel_button(CHAN_REC *chan)
{
    g_return_if_fail(chan != NULL);

    if (chan->window->gui->parent->highchan == chan)
    {
        /* this channel was highlighted.. */
        chan->window->gui->parent->highchan = NULL;
    }

    gtk_widget_destroy(chan->gui);
}

/* Select channel in specific window, if channel is in some other window,
   move it from there */
void gui_select_channel(WINDOW_REC *win, CHAN_REC *chan)
{
    GList *oldchan;

    g_return_if_fail(win != NULL);
    g_return_if_fail(chan != NULL);

    if (chan->window != win)
    {
        /* we need to move channel into this window.. */
        WINDOW_REC *oldwin;

        oldwin = chan->window;
        gui_private_remove_channel_button(chan);
        chan->window = win;
        chan->gui = gui_private_add_channel_button(GTK_BOX(win->gui->parent->chanbox), chan);

        for (oldchan = g_list_first(oldwin->chanlist); oldchan != NULL; oldchan = oldchan->next)
            if (oldchan->data == chan) break;

        win->chanlist = g_list_append(win->chanlist, oldchan->data);

        if (oldwin->curchan != chan)
            oldwin->chanlist = g_list_remove_link(oldwin->chanlist, oldchan);
        else
        {
            int ret;
            WINDOW_REC *win;

            win = curwin;

            /* channel was selected in it's old window */
            ret = irc_select_new_channel(oldwin);
            oldwin->chanlist = g_list_remove_link(oldwin->chanlist, oldchan);
            if (ret == 0)
            {
                /* no more channels in this window - remove it */
                irc_window_close(oldwin);
            }
            else
            {
                /* draw topic */
                gui_draw_channel(oldwin, oldwin->curchan);
            }
            curwin = win;
        }
    }

    gui_draw_channel(win, chan);
}

/* Change topic of channel */
void gui_change_topic(CHAN_REC *chan)
{
    char *tmp;

    g_return_if_fail(chan != NULL);
    if (chan->window->gui->parent->selected != chan->window) return;

    if (chan->topic == NULL || *chan->topic == '\0')
    {
        /* No topic in channel */
        tmp = g_new(char, strlen(chan->name)+PROGRAM_TITLE_SIZE+10);
        sprintf(tmp, "%s - "PROGRAM_TITLE, chan->name);
    }
    else
    {
        tmp = g_new(char, strlen(chan->name)+strlen(chan->topic)+PROGRAM_TITLE_SIZE+10);
        sprintf(tmp, "%s - %s - "PROGRAM_TITLE, chan->name, chan->topic);
    }
    gtk_window_set_title(GTK_WINDOW(chan->window->gui->parent->mainwin), tmp);
    g_free(tmp);
}

void sigchantext_button_released(GtkWidget *widget, GdkEventButton *event, WINDOW_REC *win)
{
    g_return_if_fail(widget != NULL);
    g_return_if_fail(event != NULL);
    g_return_if_fail(win != NULL);

    if (event->button == 3)
    {
        /* show popup menu */
        if (win->curchan != NULL)
            chanlist_popup(win->curchan, event);
        else
            window_popup(win, event);
    }
}

/* channel menu: move channel to new window */
static void move_new_window(CHAN_REC *chan)
{
    WINDOW_REC *win;

    win = irc_window_new(chan->server, NULL);
    win->curchan = chan;
    gui_select_channel(win, chan);
}

/* channel menu: move channel to new hidden window */
static void move_new_hidden(CHAN_REC *chan)
{
    WINDOW_REC *win;

    win = irc_window_new(chan->server, chan->window);
    win->curchan = chan;
    gui_select_channel(win, chan);
}

/* gtk_menu_popup() position function */
void posfunc(GtkMenu *menu, gint *x, gint *y, GdkEventButton *event)
{
    g_return_if_fail(x != NULL);
    g_return_if_fail(y != NULL);
    g_return_if_fail(event != NULL);

    *x = event->x_root;
    *y = event->y_root;
}

/* Channel popup menu */
static void chanlist_popup(CHAN_REC *chan, GdkEventButton *event)
{
    GtkWidget *menu, *movemenu, *ctcpmenu;

    g_return_if_fail(chan != NULL);
    g_return_if_fail(event != NULL);

    menu = gtk_menu_new();

    movemenu = gtk_menu_new();
    add_popup(movemenu, _("New window"), move_new_window, chan);
    add_popup(movemenu, _("New hidden window"), move_new_hidden, chan);

    if (is_channel(*chan->name))
    {
        add_popup(menu, _("Channel commands"), NULL, NULL);
        add_popup(menu, NULL, NULL, NULL);
        add_popup(menu, _("Part"), irccmd_part, chan->name);
        add_popup(menu, _("Modes"), irccmd_modes, chan->name);
#ifdef USE_SCRIPT
        script_add_popups(menu, POPUPMENU_CHAN, chan->name);
#endif
    }
    else
    {
        add_popup(menu, _("Query commands"), NULL, NULL);
        add_popup(menu, NULL, NULL, NULL);
        add_popup(menu, _("Close query"), irccmd_unquery, chan->name);

	ctcpmenu = gtk_menu_new();
	add_popup(ctcpmenu, _("Ping"), nicklist_ctcp_ping, chan->name);
	add_popup(ctcpmenu, _("Version"), nicklist_ctcp_version, chan->name);
	add_popup_sub(menu, _("CTCP"), ctcpmenu);
#ifdef USE_SCRIPT
	script_add_popups(menu, POPUPMENU_NICK, chan->name);
#endif
    }

    add_popup_sub(menu, _("Move"), movemenu);
    gtk_menu_popup (GTK_MENU(menu), NULL, NULL, (GtkMenuPositionFunc) posfunc, event, 0, event->time);
}

static void window_popup(WINDOW_REC *win, GdkEventButton *event)
{
    GtkWidget *menu;

    g_return_if_fail(win != NULL);
    g_return_if_fail(event != NULL);

    menu = gtk_menu_new();

    add_popup(menu, _("Window commands"), NULL, NULL);
    add_popup(menu, NULL, NULL, NULL);
    add_popup(menu, _("Connect..."), menu_irc_connect, NULL);
    add_popup(menu, _("Select server..."), menu_window_select_server, NULL);
    add_popup(menu, _("Close"), menu_window_close, NULL);

    gtk_menu_popup (GTK_MENU(menu), NULL, NULL, (GtkMenuPositionFunc) posfunc, event, 0, event->time);
}

static void list_bans(GtkList *list, GList *bans)
{
    GtkWidget *li;
    GList *tmp, *items;

    items = NULL;
    for (tmp = g_list_first(bans); tmp != NULL; tmp = tmp->next)
    {
        BAN_REC *ban;

        ban = (BAN_REC *) tmp->data;
        li = gtk_list_item_new_with_label(ban->ban);
        gtk_object_set_data(GTK_OBJECT(li), "ban", ban);
        items = g_list_append(items, li);
        gtk_widget_show(li);
    }
    gtk_list_append_items(list, items);
}

/* signal: apply button pressed in mode box dialog */
static void modebox_apply(GtkWidget *dialog)
{
    GtkEntry *topicentry, *limitentry, *keyentry;
    CHAN_REC *chan;
    char *topic, *limitstr, *key;
    char tmp[512], *ptr, *start;
    int limit, mode;

    chan = gtk_object_get_data(GTK_OBJECT(dialog), "chan");
    topicentry = gtk_object_get_data(GTK_OBJECT(dialog), "topic");
    limitentry = gtk_object_get_data(GTK_OBJECT(dialog), "limit");
    keyentry = gtk_object_get_data(GTK_OBJECT(dialog), "key");
    mode = *((int *) gtk_object_get_data(GTK_OBJECT(dialog), "mode"));

    topic = gtk_entry_get_text(topicentry);
    key = gtk_entry_get_text(keyentry);
    limitstr = gtk_entry_get_text(limitentry);
    if (*limitstr == '\0')
        limit = 0;
    else
    {
        if (sscanf(limitstr, "%d", &limit) != 1 || limit == 0) limit = -1;
    }

    if (strcmp(chan->topic != NULL ? chan->topic : "", topic) != 0)
    {
        /* topic changed */
        sprintf(tmp, "TOPIC %s :%s", chan->name, topic);
        irc_send_cmd(chan->server, tmp);
    }

    if (strcmp(chan->key != NULL ? chan->key : "", key) != 0)
    {
        /* key changed */
        if (chan->key != NULL)
        {
            /* remove old key */
            sprintf(tmp, "MODE %s -k %s", chan->name, chan->key);
            irc_send_cmd(chan->server, tmp);
        }

        if (*key != '\0')
        {
            /* set new key */
            sprintf(tmp, "MODE %s +k %s", chan->name, key);
            irc_send_cmd(chan->server, tmp);
        }
    }

    start = ptr = tmp+sprintf(tmp, "MODE %s ", chan->name);

    if ((mode & CHANMODE_INVITE) != (chan->mode & CHANMODE_INVITE))
        ptr += sprintf(ptr, "%ci", (mode & CHANMODE_INVITE) ? '+' : '-');
    if ((mode & CHANMODE_SECRET) != (chan->mode & CHANMODE_SECRET))
        ptr += sprintf(ptr, "%cs", (mode & CHANMODE_SECRET) ? '+' : '-');
    if ((mode & CHANMODE_PRIVATE) != (chan->mode & CHANMODE_PRIVATE))
        ptr += sprintf(ptr, "%cp", (mode & CHANMODE_PRIVATE) ? '+' : '-');
    if ((mode & CHANMODE_MODERATE) != (chan->mode & CHANMODE_MODERATE))
        ptr += sprintf(ptr, "%cm", (mode & CHANMODE_MODERATE) ? '+' : '-');
    if ((mode & CHANMODE_NOMSGS) != (chan->mode & CHANMODE_NOMSGS))
        ptr += sprintf(ptr, "%cn", (mode & CHANMODE_NOMSGS) ? '+' : '-');
    if ((mode & CHANMODE_OP_TOPIC) != (chan->mode & CHANMODE_OP_TOPIC))
        ptr += sprintf(ptr, "%ct", (mode & CHANMODE_OP_TOPIC) ? '+' : '-');

    if (limit != -1 && limit != chan->limit)
    {
        /* limit changed */
        if (limit == 0)
            ptr += sprintf(ptr, "-l");
        else
            ptr += sprintf(ptr, "+l %d", limit);
    }

    if (ptr != start) irc_send_cmd(chan->server, tmp);
}

/* signal: ok button pressed in mode box dialog */
static void modebox_ok(GtkWidget *dialog)
{
    modebox_apply(dialog);

    gtk_widget_destroy(dialog);
}

static void check_clicked(GtkWidget *button, CHAN_REC *chan)
{
    gint mask;
    gint *mode;

    mask = GPOINTER_TO_INT(gtk_object_get_data(GTK_OBJECT(button), "mask"));
    mode = gtk_object_get_data(GTK_OBJECT(button), "mode");
    *mode ^= mask;

    if ((*mode & CHANMODE_PRIVATE) && (*mode & CHANMODE_SECRET))
    {
        GtkToggleButton *but;

        /* can't be both private and secret */
        if (mask == CHANMODE_PRIVATE)
            but = gtk_object_get_data(GTK_OBJECT(button), "secretbut");
        else
            but = gtk_object_get_data(GTK_OBJECT(button), "privatebut");
        gtk_toggle_button_set_state(but, FALSE);
    }
}

/* create new check button for mode dialog */
static GtkWidget *new_check_button(GtkWidget *dialog, GtkWidget *vbox, char *text, gint mode)
{
    GtkWidget *button;
    CHAN_REC *chan;

    chan = gtk_object_get_data(GTK_OBJECT(dialog), "chan");

    button = gtk_check_button_new_with_label(text);
    gtk_object_set_data(GTK_OBJECT(button), "mask", GINT_TO_POINTER(mode));
    gtk_object_set_data(GTK_OBJECT(button), "mode",
                        gtk_object_get_data(GTK_OBJECT(dialog), "mode"));
    gtk_signal_connect(GTK_OBJECT(button), "clicked", GTK_SIGNAL_FUNC (check_clicked), chan);
    gtk_box_pack_start(GTK_BOX(vbox), button, FALSE, FALSE, 0);
    if (chan->mode & mode)
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(button), TRUE);

    return button;
}

/* signal: unban button pressed */
static void unban(GtkWidget *widget, GtkWidget *dialog)
{
    CHAN_REC *chan;
    GtkList *list;
    BAN_REC *ban;
    char *tmp;

    chan = gtk_object_get_data(GTK_OBJECT(dialog), "chan");
    list = gtk_object_get_data(GTK_OBJECT(dialog), "list");
    if (list->selection == NULL) return;

    ban = gtk_object_get_data(GTK_OBJECT(list->selection->data), "ban");
    tmp = g_new(char, strlen(ban->ban)+strlen(chan->name)+10);
    sprintf(tmp, "MODE %s -b %s", chan->name, ban->ban);
    irc_send_cmd(chan->server, tmp);
    g_free(tmp);
}

/* Display channel modes dialog */
void gui_channel_modes(CHAN_REC *chan)
{
    GtkWidget *dialog;
    GtkWidget *topicentry, *limitentry, *keyentry;
    GtkWidget *button;
    GtkWidget *secretbut, *privatebut;
    GtkWidget *label;
    GtkWidget *scroll;
    GtkWidget *list;
    GtkWidget *vbox, *hbox, *hbox2;
    GtkWidget *frame;
    static int mode;
    NICK_REC *nick;
    int op;

    if (chan->queries != 0) return; /* don't know channel mode yet.. */

    nick = find_nick_rec(chan, chan->server->nick);
    op = nick != NULL && *nick->nick == '@';

    dialog = gtk_dialog_new();
    gtk_widget_set_usize(dialog, 450, -1);
    gtk_window_set_title(GTK_WINDOW(dialog), chan->name);

    mode = 0;
    gtk_object_set_data(GTK_OBJECT(dialog), "mode", &mode);
    gtk_object_set_data(GTK_OBJECT(dialog), "chan", chan);

    /* topic */
    hbox = gtk_hbox_new(FALSE, 0);
    gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->vbox), hbox, FALSE, FALSE, 5);
    label = gtk_label_new(_("Topic: "));
    gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 5);
    topicentry = gtk_entry_new();
    gtk_box_pack_start(GTK_BOX(hbox), topicentry, TRUE, TRUE, 5);
    if (chan->topic != NULL)
        gtk_entry_set_text(GTK_ENTRY(topicentry), chan->topic);
    if (!op && chan->mode & CHANMODE_OP_TOPIC)
        gtk_entry_set_editable(GTK_ENTRY(topicentry), FALSE);

    hbox2 = gtk_hbox_new(FALSE, 0);
    gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->vbox), hbox2, TRUE, TRUE, 0);

    /* modes */
    frame = gtk_frame_new(_("Modes"));
    gtk_container_border_width(GTK_CONTAINER(frame), 10);
    gtk_box_pack_start(GTK_BOX(hbox2), frame, FALSE, FALSE, 0);

    vbox = gtk_vbox_new(FALSE, 0);
    gtk_container_add(GTK_CONTAINER(frame), vbox);

    button = new_check_button(dialog, vbox, _("Invite"), CHANMODE_INVITE);
    if (!op) gtk_widget_set_sensitive(button, FALSE);
    secretbut = new_check_button(dialog, vbox, _("Secret"), CHANMODE_SECRET);
    if (!op) gtk_widget_set_sensitive(secretbut, FALSE);
    privatebut = new_check_button(dialog, vbox, _("Private"), CHANMODE_PRIVATE);
    if (!op) gtk_widget_set_sensitive(privatebut, FALSE);
    button = new_check_button(dialog, vbox, _("Moderated"), CHANMODE_MODERATE);
    if (!op) gtk_widget_set_sensitive(button, FALSE);
    button = new_check_button(dialog, vbox, _("No external msgs"), CHANMODE_NOMSGS);
    if (!op) gtk_widget_set_sensitive(button, FALSE);
    button = new_check_button(dialog, vbox, _("Only ops change topic"), CHANMODE_OP_TOPIC);
    if (!op) gtk_widget_set_sensitive(button, FALSE);

    hbox = gtk_hbox_new(FALSE, 0);
    gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);
    label = gtk_label_new(_("User limit: "));
    gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 5);
    limitentry = gtk_entry_new();
    gtk_box_pack_start(GTK_BOX(hbox), limitentry, TRUE, TRUE, 5);
    if (chan->limit != 0)
    {
        char tmp[20];

        sprintf(tmp, "%d", chan->limit);
        gtk_entry_set_text(GTK_ENTRY(limitentry), tmp);
    }
    if (!op) gtk_entry_set_editable(GTK_ENTRY(limitentry), FALSE);

    hbox = gtk_hbox_new(FALSE, 0);
    gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 5);
    label = gtk_label_new(_("Channel key: "));
    gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 5);
    keyentry = gtk_entry_new();
    gtk_box_pack_start(GTK_BOX(hbox), keyentry, TRUE, TRUE, 5);
    if (chan->key != NULL)
        gtk_entry_set_text(GTK_ENTRY(keyentry), chan->key);
    if (!op) gtk_entry_set_editable(GTK_ENTRY(keyentry), FALSE);

    /* ban list */
    frame = gtk_frame_new(_("Banlist"));
    gtk_container_border_width(GTK_CONTAINER(frame), 10);
    gtk_box_pack_start(GTK_BOX(hbox2), frame, TRUE, TRUE, 0);

    vbox = gtk_vbox_new(FALSE, 0);
    gtk_container_add(GTK_CONTAINER(frame), vbox);

    scroll = gtk_scrolled_window_new(NULL, NULL);
    gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scroll),
                                   GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
    gtk_box_pack_start(GTK_BOX(vbox), scroll, TRUE, TRUE, 0);

    list = gtk_list_new();
    // gtk_container_add(GTK_CONTAINER(scroll), list);
    gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(scroll),list);

    list_bans(GTK_LIST(list), chan->banlist);

    button = gtk_button_new_with_label(_("Unban"));
    gtk_box_pack_start(GTK_BOX(vbox), button, FALSE, FALSE, 0);
    if (op)
    {
        /* unban only for ops :) */
        gtk_signal_connect_object (GTK_OBJECT (button), "clicked",
                                   GTK_SIGNAL_FUNC (unban), GTK_OBJECT(dialog));
    }

    /* ok/cancel/apply */
    button = gtk_button_new_with_label (_("Ok"));
    gtk_box_pack_start (GTK_BOX (GTK_DIALOG(dialog)->action_area), button, TRUE, TRUE, 10);
    gtk_signal_connect_object (GTK_OBJECT (button), "clicked",
                               GTK_SIGNAL_FUNC (modebox_ok), GTK_OBJECT(dialog));
    /*GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT);
    gtk_widget_grab_default (button);*/
    gtk_widget_show (button);

    button = gtk_button_new_with_label (_("Cancel"));
    gtk_box_pack_start (GTK_BOX (GTK_DIALOG(dialog)->action_area), button, TRUE, TRUE, 10);
    gtk_signal_connect_object (GTK_OBJECT (button), "clicked",
                               GTK_SIGNAL_FUNC (gtk_widget_destroy), GTK_OBJECT(dialog));
    gtk_widget_show (button);

    button = gtk_button_new_with_label (_("Apply"));
    gtk_box_pack_start (GTK_BOX (GTK_DIALOG(dialog)->action_area), button, TRUE, TRUE, 10);
    gtk_signal_connect_object (GTK_OBJECT (button), "clicked",
                               GTK_SIGNAL_FUNC (modebox_apply), GTK_OBJECT(dialog));
    gtk_widget_show (button);

    gtk_object_set_data(GTK_OBJECT(dialog), "chan", chan);
    gtk_object_set_data(GTK_OBJECT(dialog), "list", list);
    gtk_object_set_data(GTK_OBJECT(dialog), "topic", topicentry);
    gtk_object_set_data(GTK_OBJECT(dialog), "limit", limitentry);
    gtk_object_set_data(GTK_OBJECT(dialog), "key", keyentry);
    gtk_object_set_data(GTK_OBJECT(privatebut), "secretbut", secretbut);
    gtk_object_set_data(GTK_OBJECT(secretbut), "privatebut", privatebut);

    gtk_grab_add (GTK_WIDGET (dialog));
    gtk_widget_show_all(dialog);
}
