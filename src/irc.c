/*

 irc.c : Main IRC related file - all kinds of stuff

    Copyright (C) 1998 Timo Sirainen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "config.h"
#include <errno.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>



#include <glib.h>

#include "os.h"
#include "gui.h"
#include "irc.h"
#include "irc_txt.h"
#include "dcc.h"
#include "ctcp.h"
#include "flood.h"
#include "ignore.h"
#include "network.h"
#include "net_nowait.h"
#include "commands.h"
#include "events.h"
#include "script.h"
#include "log.h"
#include "params.h"
#include "misc.h"
#include "options.h"
#include "intl.h"

char *ret_texts[] =
{
    "", /* error */
    "", /* ok */
    N_("Not enough parameters given\n"),
    N_("Not connected to IRC server yet\n"),
    N_("Not joined to any channels yet\n"),
    N_("Error: getsockname() failed\n"),
    N_("Error: listen() failed\n"),
    N_("Multiple matches found, be more specific\n"),
    N_("Nick not found\n"),
    N_("Not joined to such channel\n"),
};

typedef int (*OUT_CALL_FUNC)(char *);
typedef void (*IN_CALL_FUNC)(char *);

struct aliasparam
{
    char *key;
    void *info;
} aliasparam;

struct OUT_CMD_STRUCT
{
    char *name;
    OUT_CALL_FUNC func;
} OUT_CMD_STRUCT;

struct IN_CMD_STRUCT
{
    char *name;
    IN_CALL_FUNC func;
} IN_CMD_STRUCT;

static struct OUT_CMD_STRUCT out_cmds[] =
{
    { "SERVER", (OUT_CALL_FUNC) irccmd_server },
    { "SERVERS", (OUT_CALL_FUNC) irccmd_servers },
    { "DISCONNECT", (OUT_CALL_FUNC) irccmd_disconnect },
    { "CTCP", (OUT_CALL_FUNC) irccmd_ctcp },
    { "ME", (OUT_CALL_FUNC) irccmd_me },
    { "DCC", (OUT_CALL_FUNC) irccmd_dcc },
    { "WINDOW", (OUT_CALL_FUNC) irccmd_window },
    { "LOAD", (OUT_CALL_FUNC) irccmd_load },
    { "UNLOAD", (OUT_CALL_FUNC) irccmd_unload },
    { "QUOTE", (OUT_CALL_FUNC) irccmd_quote },
    { "CLEAR", (OUT_CALL_FUNC) irccmd_clear },
    { "LOG", (OUT_CALL_FUNC) irccmd_log },
    { "LOGS", (OUT_CALL_FUNC) irccmd_logs },
    { "ECHO", (OUT_CALL_FUNC) irccmd_echo },
#ifdef WE_ARE_NUTS
    { "PLAY", (OUT_CALL_FUNC) irccmd_play },
#endif

    { "ISON", (OUT_CALL_FUNC) irccmd_ison },
    { "MSG", (OUT_CALL_FUNC) irccmd_msg },
    { "NOTICE", (OUT_CALL_FUNC) irccmd_notice },
    { "QUIT", (OUT_CALL_FUNC) irccmd_quit },
    { "JOIN", (OUT_CALL_FUNC) irccmd_join },
    { "WHOIS", (OUT_CALL_FUNC) irccmd_whois },
    { "QUERY", (OUT_CALL_FUNC) irccmd_query },
    { "UNQUERY", (OUT_CALL_FUNC) irccmd_unquery },
    { "PART", (OUT_CALL_FUNC) irccmd_part },
    { "MODE", (OUT_CALL_FUNC) irccmd_mode },
    { "MODES", (OUT_CALL_FUNC) irccmd_modes },
    { "TOPIC", (OUT_CALL_FUNC) irccmd_topic },
    { "KILL", (OUT_CALL_FUNC) irccmd_kill },
    { "AWAY", (OUT_CALL_FUNC) irccmd_away },
    { "AWAYALL", (OUT_CALL_FUNC) irccmd_awayall },
    { "WALLOPS", (OUT_CALL_FUNC) irccmd_wallops },
    { "OPER", (OUT_CALL_FUNC) irccmd_oper },
    { "SAY", (OUT_CALL_FUNC) irccmd_say },
    { "WALL", (OUT_CALL_FUNC) irccmd_wallop },

    { "OP", (OUT_CALL_FUNC) irccmd_op },
    { "DEOP", (OUT_CALL_FUNC) irccmd_deop },
    { "VOICE", (OUT_CALL_FUNC) irccmd_voice },
    { "DEVOICE", (OUT_CALL_FUNC) irccmd_devoice },
    { "KICK", (OUT_CALL_FUNC) irccmd_kick },
    { "KICKBAN", (OUT_CALL_FUNC) irccmd_kickban },
    { "KNOCKOUT", (OUT_CALL_FUNC) irccmd_knockout },
    { "BAN", (OUT_CALL_FUNC) irccmd_ban },
    { "BANSTAT", (OUT_CALL_FUNC) irccmd_banstat },
    { "LIST", (OUT_CALL_FUNC) irccmd_list },

    { NULL, NULL }
};

static struct IN_CMD_STRUCT in_cmds[] =
{
    { "PRIVMSG", (IN_CALL_FUNC) eirc_privmsg },
    { "NOTICE", (IN_CALL_FUNC) eirc_notice },
    { "NICK", (IN_CALL_FUNC) eirc_nick },
    { "QUIT", (IN_CALL_FUNC) eirc_quit },
    { "JOIN", (IN_CALL_FUNC) eirc_join },
    { "PART", (IN_CALL_FUNC) eirc_part },
    { "PING", (IN_CALL_FUNC) eirc_ping },
    { "PONG", (IN_CALL_FUNC) eirc_pong },
    { "MODE", (IN_CALL_FUNC) eirc_mode },
    { "KICK", (IN_CALL_FUNC) eirc_kick },
    { "INVITE", (IN_CALL_FUNC) eirc_invite },
    { "TOPIC", (IN_CALL_FUNC) eirc_new_topic },
    { "WALLOPS", (IN_CALL_FUNC) eirc_wallops },
    { "ERROR", (IN_CALL_FUNC) eirc_error },
    { NULL, NULL }
};

char *levels[] =
{
    "CRAP",
    "CHAN",
    "PUBLIC",
    "MSGS",
    "NOTICES",
    "WALLOPS",
    "SNOTES",
    "ACTIONS",
    "DCC",
    "CTCP",
    "YAGNOTICES",
    "YAGERRORS",
};

/* callback function list for numeric irc server replies */
#define MAX_NUMCMDS 600
static IN_CALL_FUNC in_numcmds[MAX_NUMCMDS];

static int ircwindows; /* number of currently existing windows */

GList *winlist; /* List of windows */
GList *servlist; /* List of servers */
/*GList *scriptlist;  List of scripts loaded */
GList *aliases; /* List of command aliases */
GList *ignores; /* Ignore list */
GList *notifies; /* Notify list */
GList *logs; /* Log file list */

WINDOW_REC *curwin; /* current window */
SERVER_REC *cserver; /* current server */
GLOBAL_SETTINGS *global_settings; /* global settings */

static void irc_server_remove_dependencies (SERVER_REC *server);


static char *strip_codes(char *input)
{
    char *tmp, *ptr, *tmppos;

    tmppos = tmp = g_strdup(input);
    for (ptr = input; *ptr != '\0'; ptr++)
    {
        if (*ptr == 3)
        {
            /* color,color; */
            if (*++ptr >= 16)
            {
                if (isdigit(*++ptr))
                {
                    if (isdigit(*++ptr))
                        ptr++;
                    if (*ptr == ',')
                    {
                        if (isdigit(*++ptr))
                        {
                            if (isdigit(*++ptr))
                                ptr++;
                        }
                    }
                }
                ptr--;
            }
        }
        else if (*ptr == 4)
        {
            /* color */
            ptr++;
        }
        else if (*ptr != 2 && *ptr != 4 && *ptr != 22 && *ptr != 27 && *ptr != 31)
        {
            /* real character, finally.. */
            *tmppos++ = *ptr;
        }
    }
    *tmppos = '\0';

    return tmp;
}

void irc_channel_new(char *name, WINDOW_REC *window, SERVER_REC *server, char *key)
{
  CHAN_REC *newchan;
  int firstjoin;

  g_return_if_fail(name != NULL || window != NULL || server != NULL);

  newchan = g_new0(CHAN_REC, 1);

  newchan->name = g_strdup(name);
  newchan->nicks = NULL;
  newchan->topic = NULL;
  newchan->window = window;
  newchan->server = server;
  newchan->cmdhist = NULL;
  newchan->histlines = 0;
  if (key != NULL) newchan->key = g_strdup(key);


/* if there is no channel in the selected window, put newchan in it */

if (window->chanlist != NULL) firstjoin = 1;
	else firstjoin = 0;
	
  window->chanlist = g_list_append(window->chanlist, newchan);
  curwin->curchan = newchan;
  gui_channel_join(newchan, NULL);

  /* if auto create window on (chan/query depending on the new window type) is set on, do it */

  if (firstjoin && \
   (is_channel(*name) ? global_settings->autowin_chan : global_settings->autowin_query)) {
    WINDOW_REC *newwin;
    
    newwin = irc_window_new(server, NULL);
    newwin->curchan = newchan;
    gui_select_channel(newwin, newchan);
  }
}

/* Write text to window - convert color codes */
void drawtext(SERVER_REC *server, char *chan, int type, char *str, ...)
{
    GList *tmp;
    WINDOW_REC *win;
    va_list args;
    char *out;
    int pros, newln, scroll_bar;

    g_return_if_fail(str != NULL);
    g_return_if_fail(curwin != NULL);

    va_start(args, str);
    
    win = curwin; /* default to current window */
    if (server != NULL)
    {
        /* use server window */
        if (server->defwin != NULL && (curwin->curchan == NULL || curwin->curchan->server != server))
            win = server->defwin;
    }
    /* check window levels */
    GLIST_FOREACH(tmp, winlist)
    {
        WINDOW_REC *rec;

        rec = (WINDOW_REC *) tmp->data;
        if (rec->defserv == server && rec->level & type)
        {
            /* level matches, use this window! */
            win = rec;
            break;
        }
    }
    if (chan != NULL)
    {
        /* maybe channel has own window? */
        CHAN_REC *ch;

        if (ignore_check(server, chan, type)) return;
        ch = channel_joined(server, chan);
        if (ch != NULL)
        {
            win = ch->window;
            if (!ch->new_data && win != curwin)
            {
                /* hilight channel button */
                ch->new_data = 1;
                gui_channel_hilight(ch);
            }
        }
    }
    g_return_if_fail(win != NULL);

    out = win->textbuf+win->textbuflen; pros = 0; newln = 0;
    if (win->textbuflen == 0)
    {
        switch (type)
        {
            case LEVEL_YAGNOTICE:
                out += sprintf(out, "\004%c - ", 11);
                break;
            case LEVEL_YAGERROR:
                out += sprintf(out, "\004%c - ", 12);
                break;
            case LEVEL_CRAP:
                out += sprintf(out, "\004%c - ", 13);
                break;
            case LEVEL_DCC:
                out += sprintf(out, "\004%c", 14);
                break;
            case LEVEL_CHAN:
                out += sprintf(out, "\004%c - ", 15);
                break;
        }
    }


    for (; *str != '\0'; str++)
    {
        win->textbuflen = (int) (out - win->textbuf);
        if (*str == '\n')
        {
            newln = 1;
            break;
        }

        if (win->textbuflen >= sizeof(win->textbuf)-INTEGER_LENGTH)
        {
            g_warning(_("drawtext() : text got too big - truncated"));
            break;
        }

        if (*str != '%')
        {
            *out++ = *str;
            continue;
        }

        if (*++str == '\0') break;
        switch (*str)
        {
            /* standard parameters */
            case 's':
                {
                    char *s = (char *) va_arg(args, char *);
                    if (s)
                    {
                        if (strlen(s) < (sizeof(win->textbuf) - win->textbuflen))
                            out += sprintf(out, "%s", s);
                        else
                            g_warning(_("drawtext() : %%s has too long string - ignored"));
                    }
                    break;
                }
            case 'd':
                {
                    int d = (int) va_arg(args, int);
                    out += sprintf(out, "%d", d);
                    break;
                }
            case 'f':
                {
                    double f = (double) va_arg(args, double);
                    out += sprintf(out, "%0.2f", f);
                    break;
                }
            case 'c':
                {
                    char c = (char )va_arg(args, int);
                    *out++ = c;
                    break;
                }
            case 'u':
                {
                    unsigned int d = (unsigned int) va_arg(args, unsigned int);
                    out += sprintf(out, "%u", d);
                    break;
                }
            case 'l':
                {
                    unsigned long d = (unsigned long) va_arg(args, unsigned long);
                    if (*++str != 'd' && *str != 'u')
                    {
                        out += sprintf(out, "%ld", d);
                        str--;
                    }
                    else
                    {
                        if (*str == 'd')
                            out += sprintf(out, "%ld", d);
                        else
                            out += sprintf(out, "%lu", d);
                    }
                    break;
                }

            /* colors, to reset color back to default use %n */
            case 'K':
                *out++ = 3;
                *out++ = BBLACK;
                break;
            case 'k':
                *out++ = 3;
                *out++ = BLACK;
                break;
            case 'B':
                *out++ = 3;
                *out++ = BBLUE;
                break;
            case 'b':
                *out++ = 3;
                *out++ = BLUE;
                break;
            case 'G':
                *out++ = 3;
                *out++ = BGREEN;
                break;
            case 'g':
                *out++ = 3;
                *out++ = GREEN;
                break;
            case 'A':
                *out++ = 3;
                *out++ = BCYAN;
                break;
            case 'a':
                *out++ = 3;
                *out++ = CYAN;
                break;
            case 'R':
                *out++ = 3;
                *out++ = BRED;
                break;
            case 'r':
                *out++ = 3;
                *out++ = RED;
                break;
            case 'M':
                *out++ = 3;
                *out++ = BMAGENTA;
                break;
            case 'm':
                *out++ = 3;
                *out++ = MAGENTA;
                break;
            case 'Y':
                *out++ = 3;
                *out++ = BYELLOW;
                break;
            case 'y':
                *out++ = 3;
                *out++ = YELLOW;
                break;
            case 'W':
                *out++ = 3;
                *out++ = BWHITE;
                break;
            case 'w':
                *out++ = 3;
                *out++ = WHITE;
                break;
            case 'n':
                *out++ = 4;
                *out++ = 16;
                break;

            /* others */
            case '!':
                /* Italic on/off */
                *out++ = 31;
                break;

            case '_':
                /* BOLD on/off */
                *out++ = 2;
                break;
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
            case '0':
                *out++ = 4;
                *out++ = (*str-'0')+1;
                break;
            case '%':
                *out++ = '%';
                break;

            default:
                *out++ = '%';
                *out++ = *str;
                break;
        }
    }
    va_end(args);
    *out = '\0';
    win->textbuflen = (int) (out - win->textbuf);

    if (newln)
    {
        /* really draw the stuff in screen */
        if (logs != NULL)
        {
            /* Check if line should be saved in logs */
            char *tmp;

            tmp = strip_codes(win->textbuf);
            log_file_write(chan, type, tmp);
            g_free(tmp);
        }

        scroll_bar = gui_is_sb_down(win);
        win->textbuflen = 0;
        win->drawfunc(win->textbuf, win->drawfuncdata);
        if (!scroll_bar) gui_set_sb_down(win);
    }

    if (curwin != win && win->new_data != 1)
    {
        /* statusbars should update [ act ]  */
        win->new_data = 1;
        gui_update_statusbar(NULL);
    }
}

/* clear buffer */
void cleartextbuf(WINDOW_REC *win)
{
    win->textbuflen = 0;
}

/* Destroy IRC channel record */
void irc_chan_free(CHAN_REC *chan)
{
    g_return_if_fail(chan != NULL);

    if (chan->name != NULL) g_free(chan->name);
    if (chan->topic != NULL) g_free(chan->topic);
    if (chan->key != NULL) g_free(chan->key);
    if (chan->nicks != NULL)
    {
        while (chan->nicks != NULL)
            remove_nick_rec(chan, chan->nicks->data);
    }
    if (chan->window != NULL && chan->window->chanlist != NULL)
        chan->window->chanlist = g_list_remove(chan->window->chanlist, chan);

    if (chan->cmdhist != NULL)
    {
        g_list_foreach(chan->cmdhist, (GFunc) g_free, NULL);
        g_list_free(chan->cmdhist);
    }

    g_free(chan);
}

/* Find server record */
SERVER_REC *irc_get_server(int handle)
{
    GList *tmp;

    for (tmp = g_list_first(servlist); tmp != NULL; tmp = tmp->next)
    {
        SERVER_REC *server;

        server = (SERVER_REC *) tmp->data;
        if (server->handle == handle) return server;
    }

    return NULL;
}

/* Find channel record of chan in server serv, if serv==NULL, find from all servers */
CHAN_REC *channel_joined(SERVER_REC *serv, char *chan)
{
    GList *link, *winl;

    g_return_val_if_fail(chan != NULL, NULL);

    if (*chan == '=')
        serv = NULL; /* Trying to find DCC query - server doesn't matter */

    GLIST_FOREACH(winl, winlist)
    {
        WINDOW_REC *win;

        win = (WINDOW_REC *) winl->data;
        GLIST_FOREACH(link, win->chanlist)
        {
            CHAN_REC *ch;

            ch = (CHAN_REC *) link->data;
            if ((serv == NULL || serv == ch->server) &&
                strcasecmp(chan, ch->name) == 0)
            {
                /* found one! */
                return ch;
            }
        }
    }

    return NULL;
}

/* Get nick's host mask */
char *irc_get_nickmask(CHAN_REC *chan, char *nick)
{
    NICK_REC *rec;
    char *ptr, *ptr2;

    g_return_val_if_fail(chan != NULL, NULL);
    g_return_val_if_fail(nick != NULL, NULL);

    rec = find_nick_rec(chan, nick);
    if (rec == NULL || rec->host == NULL) return NULL;

    nick = g_strdup(*rec->host == '-' || *rec->host == '~' ? rec->host+1 : rec->host);
    ptr = strchr(nick, '@');
    if (ptr == NULL)
    {
        g_free(nick);
        return NULL; /* what ??? */
    }
    *ptr++ = '\0';

    /* if more than one dot, skip the first (d123.blah.fi -> blah.fi) */
    ptr2 = strchr(ptr, '.');
    if (ptr2 != NULL && strchr(ptr2+1, '.') != NULL)
        ptr = ptr2+1;

    ptr2 = g_new(char, strlen(nick)+strlen(ptr)+6);
    sprintf(ptr2, "*!*%s@*%s", nick, ptr);
    g_free(nick);

    return ptr2;
}


/* IRC nick comparision for sort functions */
int irc_nicks_compare(char *p1, char *p2)
{
    if (p1 == NULL) return -1;
    if (p2 == NULL) return 1;

    if (*p1 == '@' && *p2 != '@') return -1;
    if (*p2 == '@' && *p1 != '@') return 1;

    return strcasecmp(p1, p2);
}

/* Parse command line sent by server */
static int irc_parse_line(SERVER_REC *serv, char *str)
{
    char *cmd;
    int cmdnum, n;
    WINDOW_REC *win;

    g_return_val_if_fail(serv != NULL, 0);
    g_return_val_if_fail(str != NULL, 0);

    if (curwin == NULL || (curwin->curchan != NULL && curwin->curchan->server == serv))
    {
        win = curwin;
    }
    else
    {
        int flag;

        if (serv->defwin != NULL)
        {
            win = serv->defwin;
            flag = win->curchan != NULL && win->curchan->server != win->defserv;
        }
        else
        {
            /* server has no default window.. */
            win = curwin;
            flag = 1;
        }

        /*if (flag)
            drawtext(NULL, win, TXT_TYPE_DEFAULT, "[%s(%d)] ", serv->name, serv->handle);*/
    }

    eserver = serv; edefwin = win;
    esendnick = NULL;
    esendaddr = NULL;

    if (*str == ':')
    {
        /* read prefix.. */
        esendnick = ++str;
        while (*str != '\0' && *str != ' ')
        {
            if (*str == '!')
            {
                *str = '\0';
                esendaddr = str+1;
            }
            str++;
        }
        if (*str == ' ')
        {
            *str++ = '\0';
            while (*str == ' ') str++;
        }
    }

    if (*str == '\0')
    {
        cleartextbuf(win);
        return 1; /* empty line */
    }

#ifdef USE_SCRIPT
    if (!script_event(eserver, NULL, NULL, "SERVERMSG", str))
    {
        /* script handled this event all by itself, don't call build in events */
        return 1;
    }
#endif

    /* get command.. */
    cmd = str;
    while (*str != '\0' && *str != ' ') str++;
    if (*str == ' ') *str++ = '\0';
    while (*str == ' ') str++;

#ifdef USE_SCRIPT
    /* Call script events */
    if (!script_event(eserver, esendnick == NULL ? "" : esendnick, esendaddr, cmd, str))
    {
        /* script handled this event all by itself, don't call build in events */
        return 1;
    }
#endif
    cmdnum = -1;
    if (isdigit(*cmd))
    {
        /* numeric command */
        if (sscanf(cmd, "%d", &cmdnum) != 1) cmdnum = -1;

        if (cmdnum < MAX_NUMCMDS && in_numcmds[cmdnum] != NULL)
        {
            in_numcmds[cmdnum](str);
            cleartextbuf(win);
            return 1;
        }

        /* skip targets */
        while (*str != '\0' && *str != ' ') str++;
        while (*str == ' ') str++;

        /*drawtext(edefwin, "%s ", cmd);*/
    }
    else
    {
        /* named command.. */
        for (n = 0; in_cmds[n].name != NULL; n++)
        {
            if (strcasecmp(in_cmds[n].name, cmd) == 0)
            {
                in_cmds[n].func(str);
//                cleartextbuf(win);
                return 1;
            }
        }
        drawtext(eserver, NULL, LEVEL_CRAP, "(%s)", cmd);
    }

    if (*str == ':')
        str++;
    else
    {
        char *ptr;

        ptr = strstr(str, " :");
        if (ptr != NULL)
        {
            *ptr = '\0';

            drawtext(eserver, NULL, LEVEL_CRAP, "%s %s\n", str, ptr+2);
            return 1;
        }
    }
    drawtext(eserver, NULL, LEVEL_CRAP, "%s\n", str);

    return 1;
}

/* Read line from server */
static int irc_receive_line(SERVER_REC *serv, char *str)
{
    int ret;

    g_return_val_if_fail(serv != NULL, -1);
    g_return_val_if_fail(str != NULL, -1);

    ret = read_line(1, serv->handle, str, serv->buf, sizeof(serv->buf), &serv->bufpos);
    if (ret == -1)
    {
        /* connection lost */
        if (curwin != NULL)
        {
            drawtext(serv, NULL, LEVEL_YAGNOTICE, IRCTXT_CONNECTION_LOST,
                     serv->name);
        }
        irc_server_disconnect(serv);
    }
    return ret;
}

/* Send command to IRC server */
int irc_send_cmd(SERVER_REC *serv, char *cmd)
{
    char str[512];
    int len;

    g_return_val_if_fail(cmd != NULL, 2);

    if (serv == NULL || !serv->connected)
    {
        drawtext(serv, NULL, LEVEL_YAGERROR, IRCTXT_NOT_CONNECTED);
        return 0;
    }

    /* just check that we don't send any longer commands than 512 bytes.. */
    len = 0;
    while (*cmd != '\0' && len < 510)
    {
        str[len] = *cmd++;
        len++;
    }
    str[len++] = 13; str[len] = 10;

    return net_transmit(serv->handle, str, len);
}

int irc_level2bits(char *str)
{
    char *ptr;
    int level;

    str = g_strdup(str);
    g_strup(str);

    level = 0;
    for (ptr = str; ; str++)
    {
        if (*str == ' ' || *str == '\0')
        {
            int neg, n;

            if (*str == ' ') *str++ = '\0';

            neg = *ptr == '-' ? 1 : 0;
            if (*ptr == '-' || *ptr == '+') ptr++;

            if (strcmp(ptr, "ALL") == 0)
            {
                if (!neg)
                    level |= LEVEL_ALL;
                else
                    level &= ~LEVEL_ALL;
            }
            else
            {
                for (n = 0; n < LEVELS; n++)
                {
                    if (strcmp(ptr, levels[n]) == 0)
                    {
                        if (!neg)
                            level |= 1 << n;
                        else
                            level &= ~(1 << n);
                        break;
                    }
                }
            }

            while (*str == ' ') str++;
            if (*str == '\0') break;
            ptr = str;
        }
    }

    return level;
}

char *irc_bits2level(int bits)
{
    static char tmp[200];
    int n;

    tmp[0] = '\0';
    for (n = 0; n < LEVELS; n++)
    {
        if (bits & (1 << n))
        {
            if (tmp[0] == '\0')
                strcpy(tmp, levels[n]);
            else
                sprintf(tmp+strlen(tmp), " %s", levels[n]);
        }
    }
    return tmp;
}

/* strip all extra characted from nick */
static char *strip_nick(char *nick)
{
    char *ptr;
    int n;

    g_return_val_if_fail(nick != NULL, NULL);

    for (n = 0, ptr = nick; *ptr != '\0'; ptr++)
        if (isalnum(*ptr)) nick[n++] = toupper(*ptr);

    nick[n] = '\0';

    return nick;
}

/* find best matching nick for nick completion */
static char *find_nick(CHAN_REC *chan, char *pnick)
{
    GList *list;
    char *tmp;
    int pnicklen;

    char *best, *besttmp;
    int bestlen;

    g_return_val_if_fail(chan != NULL, NULL);
    g_return_val_if_fail(pnick != NULL, NULL);

    if (chan->nicks == NULL) return NULL;

    besttmp = best = NULL; bestlen = 0;

    pnick = strip_nick(g_strdup(pnick));
    if (*pnick == '\0')
    {
        g_free(pnick);
        return NULL;
    }
    pnicklen = strlen(pnick);

    GLIST_FOREACH(list, chan->nicks)
    {
        NICK_REC *rec;

        rec = (NICK_REC *) list->data;
        tmp = g_strdup(rec->nick);
        strip_nick(tmp);
        if (strncmp(tmp, pnick, pnicklen) != 0)
            g_free(tmp);
        else
        {
            /* matches */
            if (((strlen(tmp) > bestlen ||
                  (strlen(tmp) == bestlen && strstr(besttmp, "BOT") != NULL))) && /* unless there's better matches, don't send to bots.. */
                strcasecmp(rec->nick, chan->window->defserv->nick) != 0)
            {
                best = rec->nick;
                if (besttmp != NULL) g_free(besttmp);
                besttmp = tmp;
                bestlen = strlen(tmp);
                if (bestlen == pnicklen)
                {
                    /* identical match */
                    break;
                }
            }
        }
    }
    if (besttmp != NULL) g_free(besttmp);
    g_free(pnick);
    if (best != NULL && isircflag(*best)) best++; /* skip @ or + */
    return best;
}

/* Tab style nick completion */
char *irc_nick_tabcompletion(CHAN_REC *chan, char *line, char *out)
{
    GList *list;
    char *first;
    int more;

    *out = '\0';
    if (*line == '\0') return out;

    if (strchr(line, ' ') != NULL)
    {
        strcpy(out, line);
        return out;
    }

    if (*line == '/')
    {
      /* Insert code to do tab completion for commands here */
      strcpy(out, line);
      return out;
    }

    first = NULL; more = 0;
    if (chan != NULL)
    {
      GLIST_FOREACH(list, chan->nicks)
      {
          NICK_REC *rec;
          char *nick;
  
          rec = (NICK_REC *) list->data;
          nick = isircflag(*rec->nick) ? rec->nick+1 : rec->nick;
          if (strncasecmp(nick, line, strlen(line)) == 0)
          {
             if (!first)
                  first = nick;
             else
               {
                  if (more)
                      drawtext(cserver, NULL, LEVEL_YAGNOTICE, "%s\n", nick);
                  else
                  {
                      more = 1;
                      drawtext(cserver, NULL, LEVEL_YAGNOTICE, "%s %s\n", first, nick);
                  }
              }
          }
      } /* GLIST_FOREACH */
    } /* if (chan != NULL) */

    if (first != NULL)
        strcpy(out, first);
    else
    {
        strcpy(out, line);
        if (more) drawtext(cserver, NULL, LEVEL_YAGNOTICE, "\n");
    }
    return out;
}

/* Write text to channel */
static int irc_write_text(CHAN_REC *chan, char *text)
{
    char str[512], *ptr;
    int len;

    g_return_val_if_fail(text != NULL, 0);

    if (chan == NULL)
    {
        drawtext(cserver, NULL, LEVEL_YAGERROR, IRCTXT_NOT_JOINED);
        return 0;
    }
    if (*text == '\0') return 1;

    /* first parameter: channel name */
    len = sprintf(str, "%s ", chan->name);

    /* check for nick completion */
    ptr = strchr(text, ' ');
    if (ptr != NULL && ptr != text && *(--ptr) == ':')
    {
        /* "nick: " found */
        char *nick;

        *ptr = '\0';
        nick = find_nick(chan, text);
        *ptr = ':';
        if (nick != NULL)
        {
            text = ptr+1;
            len += sprintf(str+len, "%s:", nick);
        }
    }

    strncpy(str+len, text, 510-len); str[510] = '\0';
    irccmd_msg(str);
    return 1;
}

/* Parse outgoing line */
int irc_parse_outgoing(CHAN_REC *chan, char *line)
{
    SERVER_REC *serv;
    char str[512], *cmd, *ptr;
    int n, ret;
    GList *tmp;

    g_return_val_if_fail(line != NULL, 0);

    if (*line != CMD_CHAR)
    {
        /* write text to channel */
        irc_write_text(chan, line);
        return 1;
    }

    /* must be some command.. */
    strncpy(str, line+1, sizeof(str)-1);
    str[sizeof(str)-1] = '\0';
    ptr = strchr(str, ' ');
    if (ptr != NULL) *ptr++ = '\0'; else ptr = "";
    cmd = str;

    if (*cmd == '/')
    {
        /* //command overrides any aliases/scripts */
        cmd++;
    }
    else
    {
        /* check if there's an alias for command */
        GLIST_FOREACH(tmp, aliases)
        {
            ALIAS_REC *al;

            al = (ALIAS_REC *) tmp->data;
            if (!strcasecmp(al->alias, cmd))
            {
                ptr = strchr(line, ' ');
                if (ptr != NULL) sprintf(cmd, "/%s%s", al->cmd, ptr);
                  else sprintf(cmd, "/%s", al->cmd); 
                n = irc_parse_outgoing(chan, cmd);
                return n;
            }
        }

#ifdef USE_SCRIPT
        if (!script_event(cserver, NULL, NULL, cmd, ptr)) return 1;
#endif
    }

    /* check if command is found */
    ret = RET_ERROR;
    for (n = 0; out_cmds[n].name != NULL; n++)
    {
        if (!strcasecmp(out_cmds[n].name, cmd))
        {
            ret = out_cmds[n].func(ptr);
            break;
        }
    }

    if (out_cmds[n].name == NULL)
    {
        /* command not found - just send it like it was typed */
        serv = chan == NULL ? curwin->defserv : chan->server;
        if (serv != NULL) irc_send_cmd(serv, line+1);
        return 1;
    }


    if (ret == RET_ERR_PARAM)
    {
        /* internal error! */
        drawtext(cserver, NULL, LEVEL_YAGERROR,
                 _("%_Internal error%_: g_return_val_if_fail() function failed!\n"));
    }
    else if (ret != RET_OK && ret != RET_ERROR)
    {
        /* print error message */
        drawtext(cserver, NULL, LEVEL_YAGERROR, _(ret_texts[ret]));
    }

    return 1;
}

/* Initialize new IRC window */
WINDOW_REC *irc_window_new(SERVER_REC *server, WINDOW_REC *win)
{
    WINDOW_REC *w;

    w = g_new0(WINDOW_REC, 1);

    w->num = ++ircwindows;
    w->textbuflen = 0;
    w->chanlist = NULL;
    w->curchan = NULL;
    w->defserv = NULL;
    w->defserv = server;

    gui_window_init(w, win);

    winlist = g_list_append(winlist, w);
    irc_window_focus(w);

    return w;
}

/* Select new "default server window" for server messages */
void irc_select_new_server_window(WINDOW_REC *win)
{
    GList *tmp;

    if (win->defserv == NULL) return;

    /* closing the default window for server .. need new one.. */
    GLIST_FOREACH(tmp, winlist)
    {
        WINDOW_REC *w;

        w = (WINDOW_REC *) tmp->data;
        if (w->defserv == win->defserv && w != win)
        {
            /* found one! */
            win->defserv->defwin = w;
            break;
        }
    }
    if (tmp == NULL)
    {
        /* none found.. */
        win->defserv->defwin = NULL;
    }
}

/* Close IRC window, sends part messages for every channel in window */
void irc_window_close(WINDOW_REC *win)
{
    GList *tmp;

    g_return_if_fail(win != NULL);

    if (win == curwin)
        gui_window_select_new(win);

    if (win->defserv != NULL && win->defserv->defwin == win)
        irc_select_new_server_window(win);

    /* Send PART command for every channel in this window */
    while (win->chanlist != NULL)
    {
        CHAN_REC *chan;

        chan = (CHAN_REC *) win->chanlist->data;
        if (chan->server != NULL && chan->server->connected)
	{
	  if (is_channel(*chan->name))
            {
              irccmd_part(chan->name);
              gui_channel_part(chan, NULL);
              irc_chan_free(chan);
            }
	  else irccmd_unquery(chan->name);
        }
    }

    gui_window_deinit(win);

    /* renumber windows */
    for (tmp = g_list_nth(winlist, win->num); tmp != NULL; tmp = tmp->next)
    {
        WINDOW_REC *w;

        w = (WINDOW_REC *) tmp->data;
        w->num--;
    }
    ircwindows--;

    winlist = g_list_remove(winlist, win);
    g_free(win);
}

/* Window got focus */
void irc_window_focus(WINDOW_REC *win)
{
    curwin = win;
    if (win == NULL)
    {
        /* umm... no window.. */
        cserver = NULL;
        return;
    }

    cserver = win->curchan != NULL ? win->curchan->server : win->defserv;
    gui_window_update(win);

    if (win->new_data)
    {
        GList *tmp;

        /* remove hilight from all channels in this window */
        GLIST_FOREACH(tmp, win->chanlist)
        {
            CHAN_REC *chan;

            chan = (CHAN_REC *) tmp->data;
            if (chan->new_data)
            {
                chan->new_data = 0;
                gui_channel_dehilight(chan);
            }
        }

        win->new_data = 0;
        gui_update_statusbar(NULL);
    }
}

void irc_init(void)
{
    winlist = NULL; servlist = NULL; /*scriptlist = NULL;*/
    ircwindows = 0;

    /* set numeric commands */
    memset(in_numcmds, 0, sizeof(in_numcmds));
    in_numcmds[1] = (IN_CALL_FUNC) eirc_welcome;
    in_numcmds[4] = (IN_CALL_FUNC) eirc_connected;
    in_numcmds[303] = (IN_CALL_FUNC) eirc_ison;
    in_numcmds[305] = (IN_CALL_FUNC) eirc_unaway;
    in_numcmds[306] = (IN_CALL_FUNC) eirc_away;
    in_numcmds[311] = (IN_CALL_FUNC) eirc_whois;
    in_numcmds[312] = (IN_CALL_FUNC) eirc_whois_server;
    in_numcmds[313] = (IN_CALL_FUNC) eirc_whois_oper;
    in_numcmds[315] = (IN_CALL_FUNC) eirc_end_of_who;
    in_numcmds[317] = (IN_CALL_FUNC) eirc_whois_idle;
    in_numcmds[318] = (IN_CALL_FUNC) eirc_end_of_whois;
    in_numcmds[319] = (IN_CALL_FUNC) eirc_whois_channels;
    in_numcmds[321] = (IN_CALL_FUNC) eirc_list_start;
    in_numcmds[322] = (IN_CALL_FUNC) eirc_list;
    in_numcmds[323] = (IN_CALL_FUNC) eirc_list_end;
    in_numcmds[324] = (IN_CALL_FUNC) eirc_channel_mode;
    in_numcmds[329] = (IN_CALL_FUNC) eirc_channel_created;
    in_numcmds[332] = (IN_CALL_FUNC) eirc_topic;
    in_numcmds[333] = (IN_CALL_FUNC) eirc_topic_info;
    in_numcmds[352] = (IN_CALL_FUNC) eirc_who;
    in_numcmds[353] = (IN_CALL_FUNC) eirc_names_list;
    in_numcmds[366] = (IN_CALL_FUNC) eirc_end_of_names;
    in_numcmds[367] = (IN_CALL_FUNC) eirc_ban_list;
    in_numcmds[368] = (IN_CALL_FUNC) eirc_end_of_banlist;
    in_numcmds[405] = (IN_CALL_FUNC) eirc_too_many_channels;
    in_numcmds[433] = (IN_CALL_FUNC) eirc_nick_in_use;
    in_numcmds[437] = (IN_CALL_FUNC) eirc_nick_unavailable;
    in_numcmds[471] = (IN_CALL_FUNC) eirc_channel_is_full;
    in_numcmds[473] = (IN_CALL_FUNC) eirc_invite_only;
    in_numcmds[474] = (IN_CALL_FUNC) eirc_banned;
    in_numcmds[475] = (IN_CALL_FUNC) eirc_bad_channel_key;
    in_numcmds[476] = (IN_CALL_FUNC) eirc_bad_channel_mask;

    events_init();
    dcc_init();
#ifdef USE_SCRIPT
    script_init();
#endif
}

void irc_init_after(void)
{
#ifdef USE_SCRIPT
  if (global_settings->loadscript)
    script_load (global_settings->loadscriptfile);
#endif
}
void irc_deinit(void)
{
#ifdef USE_SCRIPT
    script_deinit();
#endif

    while (servlist != NULL)
        irc_server_disconnect((SERVER_REC *) servlist->data);

    if (winlist != NULL)
    {
        /* close every unclosed window properly.. */
        while (winlist != NULL)
            irc_window_close((WINDOW_REC *) winlist->data);
    }

    events_deinit();
    dcc_deinit();

    while (logs != NULL)
        log_file_close((LOG_REC *) logs->data);
}

/* Select any other channel in window except the one we are now */
int irc_select_new_channel(WINDOW_REC *window)
{
    GList *tmp;

    g_return_val_if_fail(window != NULL, 0);

    GLIST_FOREACH(tmp, window->chanlist)
        if (tmp->data != window->curchan) break;

    window->curchan = tmp == NULL ? NULL : tmp->data;
    if (tmp == NULL)
    {
        /* no more channels */
        return 0;
    }

    if (*window->curchan->name == '#' || *window->curchan->name == '&')
        drawtext(window->curchan->server, window->curchan->name,
                 LEVEL_YAGNOTICE, IRCTXT_TALKING_IN, window->curchan->name);
    else
        drawtext(window->curchan->server, window->curchan->name,
                 LEVEL_YAGNOTICE, IRCTXT_QUERYING, window->curchan->name);
    irc_window_focus(window);
    return 1;
}

/* input function: handle incoming server messages */
static void irc_parse_incoming(SERVER_REC *server)
{
    char str[512];

    g_return_if_fail(server != NULL);

    while (irc_receive_line(server, str) > 0)
        irc_parse_line(server, str);
}

/* timeout function: send /ISON commands to server to check if someone in
   notify list is in IRC */
static int irc_timeout_func(SERVER_REC *serv)
{
    char tmp[512];
    int pos;
    GList *list;

    g_return_val_if_fail(serv != NULL, 0);

    if (notifies == NULL) return 1; /* no-one in notify list */

    if (serv->ison_reqs != 0)
    {
        /* still not received all replies to previous /ISON commands.. */
        return 1;
    }

    pos = sprintf(tmp, "ISON :");
    GLIST_FOREACH(list, notifies)
    {
        int len;

        len = strlen((char *) list->data);

        if (pos+len+1 > 510)
        {
            irc_send_cmd(serv, tmp);
            serv->ison_reqs++;
            pos = sprintf(tmp, "ISON :");
        }

        if (tmp[pos-1] == ':')
            pos += sprintf(tmp+pos, "%s", (char *) list->data);
        else
            pos += sprintf(tmp+pos, " %s", (char *) list->data);
    }

    irc_send_cmd(serv, tmp);
    serv->ison_reqs++;
    return 1;
}

static void
irc_server_cant_connect (SERVER_REC *serv)
{
    drawtext(NULL, NULL, LEVEL_YAGERROR, IRCTXT_CANT_CONNECT, serv->name, serv->port);
    irc_server_remove_dependencies(serv);
    g_free(serv->name);
    g_free(serv);
}

static void
connect_callback_2 (SERVER_REC *serv,
		    gint source,
		    GdkInputCondition condition)
{
    char tmp[512];
    int i, j = sizeof(i);

    gui_input_remove(serv->nbtag);

    if(0 == getsockopt(source, SOL_SOCKET, SO_ERROR, &i, &j)) {
	g_assert(j == sizeof(i));
	if(i == 0) {
	    drawtext(NULL, NULL, LEVEL_YAGERROR, IRCTXT_CONNECTION_ESTABLISHED, serv->name, serv->port);
            
	    serv->nick = g_strdup(default_nick);
	    serv->handle = source;
	    serv->readtag = gui_input_add(source, GUI_INPUT_READ, (GUI_INPUT_FUNC) irc_parse_incoming, serv);
	    serv->timetag = gui_timeout_new(NOTIFYLIST_TIMECHECK, (GUI_TIMEOUT_FUNC) irc_timeout_func, serv);
	    serv->ctcptag = gui_timeout_new(CTCP_TIMECHECK, (GUI_TIMEOUT_FUNC) ctcp_timeout_func, serv);
	    
	    serv->connected = 1;
	    sprintf(tmp, "NICK %s", serv->nick);
	    irc_send_cmd(serv, tmp);
	    sprintf(tmp, "USER %s - - :%s", user_name, real_name);
	    irc_send_cmd(serv, tmp);
	    serv->connected = 0;

	    init_flood(serv);
	    
	    servlist = g_list_append(servlist, serv);
	    return;
	}
    }

    irc_server_cant_connect(serv);
    net_disconnect(source);
}

static void
connect_callback_1 (SERVER_REC *serv,
		    gint source,
		    GdkInputCondition condition)
{
    int fd, ip;
    int p[2] = { serv->nbpipe[0], serv->nbpipe[1] };
    struct in_addr in;

    gui_input_remove(serv->nbtag);
    read(source, &fd, sizeof(fd));

    if(fd == -1) {
	irc_server_cant_connect(serv);
    } else {
	read(source, &ip, sizeof(ip));
	in.s_addr = ip;
	serv->nbtag = gui_input_add(fd, GUI_INPUT_WRITE, (GUI_INPUT_FUNC) connect_callback_2, serv);
	drawtext(NULL, NULL, LEVEL_YAGERROR, IRCTXT_CONNECTING, serv->name, inet_ntoa(in), serv->port);
    }

    close(p[0]);
    close(p[1]);
}

/* Connect to IRC server */
SERVER_REC *irc_server_connect(char *name, int port)
{
    SERVER_REC *serv;
    int e;

    g_return_val_if_fail(name != NULL, NULL);

    if (*name == '\0') return NULL;

    serv = g_new0(SERVER_REC, 1);

    if(0 > pipe(serv->nbpipe)) {
	g_free(serv);
	return NULL;
    }

    serv->name = g_strdup(name);
    serv->port = port;

    if (port == 0)
	port = 6667;

    e = net_nowait_connect_new(name, port, serv->nbpipe[1]);

    switch(e) {
    case 0:
	close(serv->nbpipe[0]);
	close(serv->nbpipe[1]);
        g_free(serv->name);
        g_free(serv);
        drawtext(NULL, NULL, LEVEL_YAGERROR, IRCTXT_CANT_CONNECT, name, port);
        return NULL;
    case 1:
	serv->nbtag = gui_input_add(serv->nbpipe[0], GUI_INPUT_READ, (GUI_INPUT_FUNC) connect_callback_1, serv);
	drawtext(NULL, NULL, LEVEL_YAGERROR, IRCTXT_LOOKING_UP, serv->name);
	break;
    default:
	g_assert_not_reached();
    }

    return serv;
}

static void
irc_server_remove_dependencies (SERVER_REC *server)
{
    GList *tmp, *nextwin;

    server->connected = 0;

    for (tmp = nextwin = g_list_first(winlist); nextwin != NULL; tmp = nextwin)
    {
        WINDOW_REC *win;
        GList *chan;
        int exists;

        nextwin = tmp->next;
        win = (WINDOW_REC *) tmp->data;
        if (win->defserv == server) win->defserv = NULL;

        exists = 0;
        for (chan = g_list_first(win->chanlist); chan != NULL;)
        {
            CHAN_REC *ch;

            ch = (CHAN_REC *) chan->data;
            if (ch->server == server && *ch->name != '=')
            {
                chan = chan->next;
                if (ch == win->curchan) win->curchan = NULL;
                gui_channel_part(ch, NULL);
                irc_chan_free(ch);
            }
            else
            {
                chan = chan->next;
                exists = 1;
            }
        }

        if (!exists)
        {
            /* no channels left in this window */
            if (g_list_first(winlist)->next != NULL) irc_window_close(win);
        }
        else
        {
            if (win->curchan == NULL) irc_select_new_channel(win);
        }
    }

    irc_window_focus(curwin);
    gui_connected(server, 0);
}

/* Disconnect from IRC server */
void irc_server_disconnect(SERVER_REC *server)
{
#ifdef USE_SCRIPT
    char *str;
#endif

    g_return_if_fail(server != NULL);

    if (server->handle != -1) net_disconnect(server->handle);
    gui_input_remove(server->readtag);
    gui_timeout_remove(server->timetag);
    gui_timeout_remove(server->ctcptag);

    irc_server_remove_dependencies(server);

#ifdef USE_SCRIPT
    str = g_new(char, strlen(server->name)+INTEGER_LENGTH+2);
    sprintf(str, "%d %s", server->handle, server->name);
    script_event(server, "", "", "DISCONNECTED", str);
    g_free(str);
#endif

    deinit_flood(server);

    servlist = g_list_remove(servlist, server);

    g_free(server->nick);
    g_free(server->name);
    g_free(server);
}
