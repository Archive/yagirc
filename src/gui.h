#include "config.h"

#include "data.h"

#ifndef __GUI_H
#define __GUI_H

#include <gdk/gdk.h>
#include <gtk/gtk.h>
#include <gtk/gtkfeatures.h>

#define PROGRAM_TITLE "yagIRC"
#define PROGRAM_TITLE_SIZE 6
#define CONFIG_DIR ".yagirc"
#define CONFIG_FILE "yagircrc"
#define CONFIG_LOOK "yaglook"
#define PIXMAPS_DIR "pixmaps"
#define SITE_CONFIG_FILE "yagirc.site"
#define GLOBAL_CONFIG_FILE "yagirc.conf"

#define MAX_TEXTLINES_HISTORY 1000

typedef struct
{
    GtkWidget *mainwin;
    GtkWidget *entry;
    GtkWidget *statusbar;
    GtkWidget *hbox;

    GtkWidget *away;
    int awayset;

    GtkWidget *autoraise;
    int araiseset;

	GtkWidget *toolbar;

    GtkWidget *chanbox; /* channel buttons */
    CHAN_REC *highchan; /* last hilighted channel button */

    GList *windows; /* list of child windows in this window */
    WINDOW_REC *selected; /* current selected child window */

    GtkWidget *nickscroll;
    GtkWidget *nicklist;
    int nicklistlen;

    /* menu */
    GtkMenuFactory *factory;
    GHashTable *entry_ht;
    GtkWidget *menubar;
    GtkAccelGroup *group;
}
GUI_TOPWIN_REC;

typedef struct
{
    GtkText *text;
    GtkWidget *textscroll;
    GtkWidget *eventbox;
    GtkWidget *label;
    GUI_TOPWIN_REC *parent;

    GtkWidget *box; /* subbox in channel list box */

    int lines;
    int linelen[MAX_TEXTLINES_HISTORY];
}
GUI_WINDOW_REC;

typedef struct
{
    GtkWidget *mainwin;
    GtkWidget *progbar;
    GtkWidget *infotext;
}
GUI_DCC_REC;

typedef GtkWidget GUI_CHAN_REC;

/* Input functions */
typedef GdkInputFunction GUI_INPUT_FUNC;
#define GUI_INPUT_READ GDK_INPUT_READ
#define GUI_INPUT_WRITE GDK_INPUT_WRITE
#define gui_input_add(a,b,c,d) gdk_input_add(a,b,c,d)
#define gui_input_remove(a) gdk_input_remove(a)

/* Timeout functions */
typedef GtkFunction GUI_TIMEOUT_FUNC;
#define gui_timeout_new(a,b,c) gtk_timeout_add(a,b,c)
#define gui_timeout_remove(a) gtk_timeout_remove(a)

extern GdkColor colors[];
extern GdkColor default_color, default_bgcolor;
extern GdkColor yag_colors[16];

enum
{
    NOCOLOR,
    BLACK,
    BLUE,
    GREEN,
    CYAN,
    RED,
    MAGENTA,
    YELLOW,
    WHITE,
    BBLACK,
    BBLUE,
    BGREEN,
    BCYAN,
    BRED,
    BMAGENTA,
    BYELLOW,
    BWHITE,
    COLORS
};

/* Initialize IRC window */
void gui_window_init(WINDOW_REC *win, WINDOW_REC *parent);

/* Deinitialize IRC window */
void gui_window_deinit(WINDOW_REC *win);

/* Clear all text from window */
void gui_window_clear(WINDOW_REC *win);

/* IRC channel/window changed, update everything you can think of.. */
void gui_window_update(WINDOW_REC *win);

/* Change focus to specified window */
void gui_window_select(WINDOW_REC *win);

/* Change to any new window in same top window */
void gui_window_select_new(WINDOW_REC *win);

/* Quit program requested */
void gui_exit(void);

/* auto raise function */
void autoraise_flag(GtkWidget *widget, gpointer data);

/* away function */
void awayclicked_new(GtkWidget *widget, gpointer data);

/* Inform GUI what connection state we are in - 0 = disconnected, 1 = socket
   connected, 2 = IRC server ready */
void gui_connected(SERVER_REC *serv, int state);

/* Someone in notify list joined IRC */
void gui_notify_join(char *nick);
/* Someone in notify list left IRC */
void gui_notify_part(char *nick);

/* Toggle window autoraising */
void gui_set_autoraise(WINDOW_REC *win, int on);

/* Update status bar */
void gui_update_statusbar(WINDOW_REC *win);

/* Set away state */
void gui_set_away(int away);

typedef void (*GUI_PASSWORD_FUNC) (char *, void *);

/* Create dialog to ask password */
void gui_ask_password(char *label, GUI_PASSWORD_FUNC func, void *data);

/* Create dialog with entry field */
void gui_entry_dialog(char *label, char *oldtext, GtkSignalFunc okfunc, gpointer data);

#include "gui_dcc.h"
#include "gui_join.h"
#include "gui_connect.h"
#include "gui_channels.h"
#include "gui_nicklist.h"
#include "gui_menu.h"
#include "gui_setup.h"

/* private */
int gui_is_sb_down(WINDOW_REC *win);
void gui_set_sb_down(WINDOW_REC *win);
void gui_refresh_windows(void);

#endif
