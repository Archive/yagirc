/*

 events-named.c : Functions to handle (named) IRC server replies

    Copyright (C) 1998 Timo Sirainen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "config.h"
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include <glib.h>

#include "intl.h"
#include "os.h"
#include "irc.h"
#include "events-named_txt.h"
#include "gui.h"
#include "gui_urllist.h"
#include "ctcp.h"
#include "ignore.h"
#include "flood.h"
#include "events.h"
#include "misc.h"
#include "flood.h"
#include "script.h"
#include "commands.h"

char *esendnick, *esendaddr;
SERVER_REC *eserver;
WINDOW_REC *edefwin;

typedef int (*CALL_FUNC)(char *, char *, char *);

struct CMD_STRUCT
{
    char *name;
    CALL_FUNC func;
} CMD_STRUCT;

static struct CMD_STRUCT ctcp_cmds[] =
{
    { "VERSION", (CALL_FUNC) ctcp_version },
    { "PING", (CALL_FUNC) ctcp_ping },
    { "ACTION", (CALL_FUNC) ctcp_action },
    { "DCC", (CALL_FUNC) ctcp_dcc },
    { "SOUND", (CALL_FUNC) ctcp_sound },
    { "PAGE", (CALL_FUNC) ctcp_page },
    { "FINGER", (CALL_FUNC) ctcp_finger },
    { "TIME", (CALL_FUNC) ctcp_time },
    { "ECHO", (CALL_FUNC) ctcp_echo },
    { NULL, NULL }
};

/* Remove record from list */
static void remove_nick(CHAN_REC *chan, char *nick)
{
    NICK_REC *rec;

    g_return_if_fail(chan != NULL);
    g_return_if_fail(nick != NULL);

    rec = find_nick_rec(chan, nick);
    if (rec != NULL)
        remove_nick_rec(chan, rec);
}

/* Change nick name in list */
static void change_nick(CHAN_REC *chan, char *oldnick, char *newnick)
{
    NICK_REC *rec, *newrec;
    char *data;

    g_return_if_fail(chan != NULL);
    g_return_if_fail(oldnick != NULL);
    g_return_if_fail(newnick != NULL);

    rec = find_nick_rec(chan, oldnick);
    if (rec == NULL) return;

    /* insert new nick to list */
    data = g_new(char, strlen(newnick)+2);
    if (isircflag(*rec->nick))
        sprintf(data, "%c%s", *rec->nick, newnick);
    else
        strcpy(data, newnick);

    newrec = insert_nick_rec(chan, data);
    if (rec->host != NULL) newrec->host = g_strdup(rec->host);
    g_free(data);

    /* remove old nick from list */
    remove_nick_rec(chan, rec);
}

void eirc_privmsg(char *data)
{
    char *channame, format[2048];
    int level;
    CHAN_REC *chan;

    g_return_if_fail(data != NULL);

    channame = event_get_param(&data); /* Channel or nick name */
    if (*data == ':') data++;

    if (esendnick == NULL) esendnick = "!server!";

    if (*data == 1)
    {
        /* ctcp message */
        char *ptr;
        int n, ret;

        if (ignore_check(eserver, esendnick, LEVEL_CTCP)) return;
        flood_newmsg(eserver, LEVEL_CTCP | is_channel(*channame) ? LEVEL_PUBLIC : LEVEL_MSGS, esendnick);

        /* remove the later \001 */
        ptr = strchr(++data, 1);
        if (ptr != NULL)
            *ptr = *(ptr+1) == '\0' ? '\0' : ' ';

        /* get ctcp command */
        for (ptr = data; *ptr != ' ' && *ptr != '\0'; ptr++) ;
        if (*ptr == ' ') *ptr++ = '\0';
	if (!*ptr) ptr = NULL;

        ret = 2;
        for (n = 0; ctcp_cmds[n].name != NULL; n++)
        {
            if (strcasecmp(ctcp_cmds[n].name, data) == 0)
            {
                ret = ctcp_cmds[n].func(esendnick, channame, ptr);
                break;
            }
        }

        if (ret)
        {
            drawtext(eserver, channame, LEVEL_CTCP, _("%9>>> %_%s%_ requested %s%!%_%s%_%! from %_%s\n"), esendnick,
                     ret == 1 ? "" : _("unknown ctcp "), data, channame);
        }
        return;
    }
    if (is_channel(*channame))
    {
        /* message to some channel */
        char *nick;
        int nicklen;

        if (esendaddr != NULL)
        {
            if (ignore_check(eserver, esendnick, LEVEL_PUBLIC)) return;
            flood_newmsg(eserver, LEVEL_PUBLIC, esendnick);
        }
        chan = channel_joined(eserver, channame);
        nick = chan != NULL ? chan->server->nick : eserver->nick;
        nicklen = strlen(nick);
        level = LEVEL_PUBLIC;
        graburls(&data, esendnick, chan->name);
        if (strstr(data, nick) != NULL && !isalnum(data[nicklen]))
        {
            if (chan != NULL && strcasecmp(channame, chan->window->curchan->name) == 0)
                sprintf(format, "<%%1%s%%n> %s\n", esendnick, data);
            else
                sprintf(format, "<%%1%s%%n:%%2%s%%n> %s\n", esendnick, channame, data);
        }
        else
        {
            if (chan != NULL && strcasecmp(channame, chan->window->curchan->name) == 0)
                sprintf(format, "<%s> %s\n", esendnick, data);
            else
                sprintf(format, "<%s:%%2%s%%n> %s\n", esendnick, channame, data);
        }
    }
    else
    {
        /* private message */
        if (esendaddr != NULL)
        {
            if (ignore_check(eserver, esendnick, LEVEL_MSGS)) return;
            flood_newmsg(eserver, LEVEL_MSGS, esendnick);
        }

        chan = channel_joined(eserver, esendnick);
        level = LEVEL_MSGS;
        sprintf(format, "[%%3%s%%n(%%4%s%%n)]%%n %s\n",
                 esendnick, esendaddr == NULL ? "" : esendaddr, data);
        /* if autoquery is on and the dude doesnt already have a query window goin, make one */
        if (global_settings->autoquery && chan == NULL) irccmd_query(esendnick);
    }
    drawtext(eserver, level == LEVEL_MSGS ? esendnick : channame, level, format);
}

void eirc_notice(char *data)
{
    char *channame;
    CHAN_REC *chan;

    g_return_if_fail(data != NULL);

    channame = event_get_param(&data); /* Channel or nick name */
    if (*data == ':') data++; /* notice text */

    if (esendnick == NULL) esendnick = "server";

    if (esendaddr != NULL)
    {
        if (ignore_check(eserver, esendnick, LEVEL_NOTICES)) return;
        flood_newmsg(eserver, LEVEL_NOTICES | is_channel(*channame) ? LEVEL_PUBLIC : LEVEL_MSGS, esendnick);
    }

    if (*data == 1)
    {
        /* ctcp reply */
        char *ptr;

        ptr = strchr(++data, 1);
        if (ptr != NULL) *ptr = '\0';

        ctcp_reply(esendnick, data);
    }
    else
    {
        if (esendaddr == NULL)
        {
            /* notice from server */
            chan = channel_joined(eserver, channame);
            drawtext(eserver, channame, LEVEL_SNOTES, "!%s %s\n", esendnick, data);
        }
        else
        {
            /* notice from user */
            if (is_channel(*channame))
            {
                /* notice in some channel */
                chan = channel_joined(eserver, channame);
                drawtext(eserver, channame, LEVEL_NOTICES,
                         "-%3%s%n:%4%s%n- %s\n", esendnick, channame, data);
            }
            else
            {
                /* private notice */
                chan = channel_joined(eserver, esendnick);
                drawtext(eserver, esendnick, LEVEL_NOTICES,
                         "-%3%s%n(%4%s%n)- %s\n", esendnick, esendaddr, data);
            }
        }
    }
}

/* Check if nick is joined to any channel in specified window */
static int nick_joined(WINDOW_REC *win, char *nick)
{
    GList *chan;

    g_return_val_if_fail(win != NULL, 0);
    g_return_val_if_fail(nick != NULL, 0);

    GLIST_FOREACH(chan, win->chanlist)
        if (find_nick_rec((CHAN_REC *) chan->data, nick)) return 1;

    return 0;
}

void eirc_nick(char *data)
{
    char *nick, *oldnick, temp[512];
    GList *win;

    g_return_if_fail(data != NULL);

    nick = event_get_param(&data);

    if (strcasecmp(esendnick, eserver->nick) == 0)
    {
        /* You changed your nick */
        oldnick = eserver->nick;
        eserver->nick = g_strdup(nick);
        gui_nick_change(eserver, oldnick, nick);
        g_free(oldnick);
        sprintf(temp, IRCTXT_YOUR_NICK_CHANGED, eserver->nick);

//        g_warning(temp);

        drawtext(eserver, NULL, LEVEL_CHAN, temp);
//        drawtext(eserver, NULL, LEVEL_CHAN, IRCTXT_YOUR_NICK_CHANGED, eserver->nick);
        gui_update_statusbar(NULL);
    }
    else
    {
        /* Someone else changed nick */
        GLIST_FOREACH(win, winlist)
        {
            WINDOW_REC *winrec;
            GList *chan;
            int found;

            winrec = (WINDOW_REC *) win->data;
            found = 0;
            GLIST_FOREACH(chan, winrec->chanlist)
            {
                CHAN_REC *rec;

                rec = (CHAN_REC *) chan->data;
                if (is_channel(*rec->name))
                {
                    /* channel */
                    if (!found && find_nick_rec(rec, esendnick))
                        found = 1;
                }
                else
                {
                    /* query */
                    int dcc;

                    dcc = *rec->name == '=';
                    if (strcasecmp(dcc ? rec->name+1 : rec->name, esendnick) == 0)
                    {
                        /* change query name */
                        if (!found) found = 1;
                        g_free(rec->name);
                        rec->name = g_new(char, strlen(nick)+dcc ? 2 : 1);
                        if (!dcc)
                            strcpy(rec->name, nick);
                        else
                            sprintf(rec->name, "=%s", nick);
                        gui_channel_change_name(rec, rec->name);
                    }
                }

                if (found == 1)
                {
                    /* print nick change to window.. */
                    drawtext(winrec->curchan->server, rec->name, LEVEL_CHAN,
                             IRCTXT_NICK_CHANGED, esendnick, nick);
                    found = 2;
                }
            }
        }
        gui_nick_change(eserver, esendnick, nick);
    }

    GLIST_FOREACH(win, winlist)
    {
        GList *chan;

        GLIST_FOREACH(chan, ((WINDOW_REC *) win->data)->chanlist)
        {
            CHAN_REC *ch;

            ch = (CHAN_REC *) chan->data;
            if (ch->server == eserver)
                change_nick(ch, esendnick, nick);
        }
    }
}

void eirc_join(char *data)
{
    char *channame, *tmp;
    CHAN_REC *chan;

    g_return_if_fail(data != NULL);

    channame = event_get_param(&data);
    tmp = strchr(channame, 7); /* ^G does something weird.. */
    if (tmp != NULL) *tmp = '\0';

    chan = channel_joined(eserver, channame);

    drawtext(eserver, channame, LEVEL_CHAN, IRCTXT_JOIN,
             esendnick, esendaddr, channame);
    if (chan == NULL) return;

    if (strcasecmp(esendnick, eserver->nick) != 0)
    {
        /* someone else joined channel */
        NICK_REC *rec;

        rec = insert_nick_rec(chan, esendnick);
        rec->host = g_strdup(esendaddr);
        gui_channel_join(chan, esendnick);
        
        return;
    }

    /* you joined .. send some queries to server about channel.. */

    tmp = g_new(char, strlen(chan->name)+10);
    sprintf(tmp, "MODE %s", chan->name);
    irc_send_cmd(eserver, tmp);

    sprintf(tmp, "MODE %s b", chan->name);
    irc_send_cmd(eserver, tmp);

    sprintf(tmp, "WHO %s", chan->name);
    irc_send_cmd(eserver, tmp);

    g_free(tmp);
    chan->queries = CHANQUERY_BANLIST | CHANQUERY_WHO | CHANQUERY_MODE;
}

void eirc_part(char *data)
{
    char *channame, *reason;
    CHAN_REC *chan;
    WINDOW_REC *win;

    g_return_if_fail(data != NULL);

    event_get_params(data, 2, &channame, &reason);
    chan = channel_joined(eserver, channame);

    drawtext(eserver, channame, LEVEL_CHAN, IRCTXT_PART,
             esendnick, esendaddr, channame, reason);
    if (chan == NULL) return;

    if (strcasecmp(esendnick, eserver->nick) != 0)
    {
        /* someone else left channel */
        remove_nick(chan, esendnick);
        gui_channel_part(chan, esendnick);
        return;
    }

    win = chan->window;
    if (chan == win->curchan)
        irc_select_new_channel(chan->window);
    if (win != curwin)
      {
        irc_window_focus(win);
        gui_select_channel(curwin, chan);
      }

    gui_channel_part(chan, NULL);
    irc_chan_free(chan);
/*
    if (win->chanlist == NULL &&
        g_list_first(winlist)->next != NULL)
    {
        irc_window_close(win);
    }
*/
}

void eirc_ping(char *data)
{
    int conn;
    char *tmp;

    g_return_if_fail(data != NULL);

    /* irc_send_cmd() doesn't send commands until server has sent connection
       messages but PING can be sent before connection has happened, so fake
       connection */

    tmp = g_new(char, strlen(data)+6);
    conn = eserver->connected;
    eserver->connected = 1;
    sprintf(tmp, "PONG %s", data);
    irc_send_cmd(eserver, tmp);
    eserver->connected = conn;
    g_free(tmp);
}

void eirc_pong(char *data)
{
    char *host, *reply;
    g_return_if_fail(data != NULL);

    event_get_params(data, 2, &host, &reply);

    drawtext(eserver, NULL, LEVEL_CRAP, IRCTXT_PONG, host, reply);
}

void eirc_quit(char *data)
{
    GList *win;

    g_return_if_fail(data != NULL);

    if (*data == ':') data++; /* quit message */
    GLIST_FOREACH(win, winlist)
    {
        WINDOW_REC *winrec;

        winrec = (WINDOW_REC *) win->data;
        if (nick_joined(winrec, esendnick))
        {
            drawtext(winrec->curchan->server, winrec->curchan->name,
                     LEVEL_CHAN, IRCTXT_QUIT, esendnick, esendaddr, data);
            g_list_foreach(winrec->chanlist, (GFunc) remove_nick, esendnick);
        }
    }

    gui_channel_part(NULL, esendnick);
}

void eirc_mode(char *data)
{
    char *channame;
    CHAN_REC *chan;

    g_return_if_fail(data != NULL);

    channame = event_get_param(&data);
    if (*data == ':') data++;

    if (!is_channel(*channame))
    {
        /* user mode change */
        drawtext(eserver, NULL, LEVEL_CRAP, IRCTXT_USERMODE_CHANGE, data, channame);
        return;
    }

    /* channel mode change */
    chan = channel_joined(eserver, channame);
    drawtext(eserver, channame, LEVEL_CHAN,
             IRCTXT_CHANMODE_CHANGE, channame, data, esendnick);

    if (chan != NULL) parse_channel_mode(chan, data);
}

void eirc_kick(char *data)
{
    char *channame, *nick, *reason;
    CHAN_REC *chan;

    g_return_if_fail(data != NULL);

    event_get_params(data, 3, &channame, &nick, &reason);
    chan = channel_joined(eserver, channame);

    drawtext(eserver, channame, LEVEL_CHAN, IRCTXT_KICK,
             nick, channame, esendnick, reason);
    if (chan == NULL) return;

    if (strcasecmp(nick, eserver->nick) != 0)
    {
        /* someone else was kicked */
        remove_nick(chan, nick);
        gui_channel_part(chan, nick);
    }
    else
    {
        if (chan == chan->window->curchan)
            irc_select_new_channel(chan->window);

        gui_channel_part(chan, NULL);
        irc_chan_free(chan);
    }

#ifdef USE_SCRIPT
    {
        char *tmp;

        tmp = g_new(char, strlen(channame)+strlen(nick)+strlen(reason)+3);
        sprintf(tmp, "%s %s %s", channame, nick, reason);
        script_event(eserver, esendnick, esendaddr, "KICKED", tmp);
        g_free(tmp);
    }
#endif
}

void eirc_invite(char *data)
{
    char *chan;

    g_return_if_fail(data != NULL);

    event_get_params(data, 2, NULL, &chan);
    if (*chan != '\0')
        drawtext(eserver, NULL, LEVEL_CRAP, IRCTXT_INVITE, esendnick, chan);
}

void eirc_new_topic(char *data)
{
    CHAN_REC *chan;
    char *channame, *topic;

    g_return_if_fail(data != NULL);

    event_get_params(data, 2, &channame, &topic);
    chan = channel_joined(eserver, channame);
    if (chan == NULL) return;

    if (chan->topic != NULL) g_free(chan->topic);
    chan->topic = *topic == '\0' ? NULL : g_strdup(topic);

    if (chan->topic != NULL)
    {
        drawtext(eserver, channame, LEVEL_CHAN, IRCTXT_NEW_TOPIC,
                 esendnick, chan->name, chan->topic);
    }
    else
    {
        drawtext(eserver, channame, LEVEL_CHAN, IRCTXT_TOPIC_UNSET,
                 esendnick, chan->name);
    }

    gui_change_topic(chan);
}

void eirc_error(char *data)
{
    g_return_if_fail(data != NULL);

    if (*data == ':') data++;
    drawtext(eserver, NULL, LEVEL_CRAP, IRCTXT_ERROR, data);
}

void eirc_wallops(char *data)
{
    g_return_if_fail(data != NULL);

    if (*data == ':') data++;
    drawtext(eserver, NULL, LEVEL_CRAP, IRCTXT_WALLOPS, data);
}
