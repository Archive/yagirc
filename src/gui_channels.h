#include "config.h"

#ifndef __GUI_CHANNELS_H
#define __GUI_CHANNELS_H

#include "data.h"

/* Change channel button label */
void gui_channel_change_name(CHAN_REC *chan, char *name);
/* Hilight channel button */
void gui_channel_hilight(CHAN_REC *chan);
/* Dehilight channel button */
void gui_channel_dehilight(CHAN_REC *chan);

/* Channel changed in window - draw new topic, nicks, hilight channel button */
void gui_draw_channel(WINDOW_REC *win, CHAN_REC *chan);
/* Select channel in specific window, if channel is in some other window,
   move it from there */
void gui_select_channel(WINDOW_REC *win, CHAN_REC *chan);

/* Change topic of channel */
void gui_change_topic(CHAN_REC *chan);

/* Display channel modes dialog */
void gui_channel_modes(CHAN_REC *chan);

/* Draw new channel button */
GtkWidget *gui_private_add_channel_button(GtkBox *box, CHAN_REC *chan);
/* Remove channel button */
void gui_private_remove_channel_button(CHAN_REC *chan);

/* text widget click signal */
void sigchantext_button_released(GtkWidget *widget, GdkEventButton *event, WINDOW_REC *win);
/* channel button click signal */
void sigchanbut_button_released(GtkWidget *widget, GdkEventButton *event, CHAN_REC *chan);

#endif
