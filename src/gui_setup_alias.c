/*

 gui_setup_alias.c : Aliases notebook entry for setup

    Copyright (C) 1998 Timo Sirainen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/
#include "config.h"

#include <stdio.h>
#include <string.h>

#include <gtk/gtk.h>
#include <glib.h>

#include "os.h"
#include "irc.h"
#include "gui.h"
#include "misc.h"
#include "intl.h"

static GtkWidget *alias_list, *alias_text, *command_text;
static int current_alias;

static int compare_aliases(ALIAS_REC *p1, ALIAS_REC *p2)
{
    int n;

    if (p1 == NULL) return -1;
    if (p2 == NULL) return 1;

    n = strcasecmp(p1->alias, p2->alias);
    return n;
}

/* add new server to server list */
ALIAS_REC *add_alias_list(char *alias, char *cmd)
{
    ALIAS_REC *al;
    char temp[512], *temp2;

    g_return_val_if_fail(alias != NULL, NULL);
    g_return_val_if_fail(cmd != NULL, NULL);

    g_strup(alias); 

    strncpy(temp, cmd, sizeof(temp));
    temp[sizeof(temp)-1] = '\0';
    temp2 = strchr(temp, ' ');
    if (temp2 != NULL) *temp2++ = '\0'; else temp2 = "";

    g_strup(temp);
  
    if ( (strcasecmp(temp, "MSG\0")) && (strcasecmp(temp, "ME\0")) && (strcasecmp(temp, "SAY\0")) && (strcasecmp(temp, "ECHO\0")) ) g_strup(cmd);

    al = g_new(ALIAS_REC, 1);
    al->alias = g_strdup(alias);
    al->cmd = g_strdup(cmd);

    aliases = g_list_insert_sorted(aliases, al, (GCompareFunc) compare_aliases);
    return al;
}

/* remove server from server list */
void remove_alias_list(ALIAS_REC *rec)
{
    g_return_if_fail(rec != NULL);

    aliases = g_list_remove(aliases, rec);

    g_free(rec->alias);
    g_free(rec->cmd);
    g_free(rec);
}

/* return alias record number in list */
static ALIAS_REC *alias_number(int num)
{
    GList *tmp;

    tmp = g_list_first(aliases);
    while (num > 0 && tmp != NULL)
    {
        num--;
        tmp = tmp->next;
    }

    return tmp == NULL ? NULL : (ALIAS_REC *) tmp->data;
}

void alias_box(char *alias, char *cmd, GtkSignalFunc func)
{
    GtkWidget *win;
    GtkWidget *button;

    win = gtk_dialog_new();
    gtk_grab_add (GTK_WIDGET (win));

    alias_text = gui_add_label(GTK_WIDGET(GTK_DIALOG(win)->vbox), _("Alias:"), alias);
    command_text = gui_add_label(GTK_WIDGET(GTK_DIALOG(win)->vbox), _("Command:"), cmd);

    button = gtk_button_new_with_label (_("Ok"));
    gtk_box_pack_start (GTK_BOX (GTK_DIALOG(win)->action_area), button, TRUE, FALSE, 0);
    gtk_signal_connect_object (GTK_OBJECT (button), "clicked",
                               func, GTK_OBJECT(win));
    GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT);
    gtk_widget_grab_default (button);
    gtk_widget_show (button);

    button = gtk_button_new_with_label (_("Cancel"));
    gtk_box_pack_start (GTK_BOX (GTK_DIALOG(win)->action_area), button, TRUE, FALSE, 0);
    gtk_signal_connect_object (GTK_OBJECT (button), "clicked",
                               GTK_SIGNAL_FUNC (gtk_widget_destroy),
                               GTK_OBJECT(win));
    gtk_widget_show (button);

    gtk_widget_show(win);
}

void sig_add_alias(GtkWidget *widget, GtkWidget *dialog)
{
    char *alias, *cmd;
    char *args[2];
    int pos;
    GList *list;

    gui_get_label(alias, alias_text);
    gui_get_label(cmd, command_text);

    args[0] = alias; args[1] = cmd;

    list = g_list_first(aliases);
    for (pos = 0; list != NULL; pos++, list = list->next)
        if (irc_nicks_compare(((ALIAS_REC *) list->data)->alias, alias) > 0) break;

    add_alias_list(alias, cmd);

    gtk_clist_insert(GTK_CLIST(alias_list), pos, args);

    g_free(alias);
    g_free(cmd);

    gtk_widget_destroy(dialog);
}

void add_alias(GtkWidget *widget)
{
    alias_box("", "", GTK_SIGNAL_FUNC(sig_add_alias));
}

void sig_edit_alias(GtkWidget *widget, GtkWidget *dialog)
{
    remove_alias_list(alias_number(current_alias));
    gtk_clist_remove(GTK_CLIST(alias_list), current_alias);
    current_alias = GTK_CLIST(alias_list)->selection == NULL ? -1 :
        GPOINTER_TO_INT(GTK_CLIST(alias_list)->selection->data);

    sig_add_alias(widget, dialog);
}

void edit_alias(GtkWidget *widget, GtkCList *clist)
{
    ALIAS_REC *al;

    if (current_alias == -1) return;

    al = alias_number(current_alias);
    alias_box(al->alias, al->cmd, GTK_SIGNAL_FUNC(sig_edit_alias));
}

void delete_alias(GtkWidget *widget, GtkCList *clist)
{
    if (current_alias == -1) return;

    remove_alias_list(alias_number(current_alias));
    gtk_clist_remove(clist, current_alias);

    current_alias = clist->selection == NULL ? -1 :
        GPOINTER_TO_INT(clist->selection->data);
}

void sig_alias_changed(GtkWidget *widget, gint row, gint column,
                       GdkEventButton *bevent)
{
    current_alias = row;
}

/* draw alias list section */
void gui_setup_aliases(GtkWidget *vbox)
{
    GtkWidget *hbox;
    GtkWidget *button, *sw;
    GList *tmp;
//    char *titles[] = { "alias", "command" };
    char *titles[2];
    
    titles[0] = _("alias");
    titles[1] = _("commands");
    
    current_alias = -1;

    hbox = gtk_hbox_new(FALSE, 0);
    gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);
    gtk_widget_show(hbox);

    /* create alias list widget here for buttons.. */
    alias_list = gtk_clist_new_with_titles(2, titles);
    sw = gtk_scrolled_window_new(NULL, NULL);
    gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(sw), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

    /* add/edit/delete buttons */
    button = gtk_button_new_with_label(_("Add"));
    gtk_signal_connect (GTK_OBJECT (button), "clicked",
                        GTK_SIGNAL_FUNC(add_alias), alias_list);
    gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 10);
    gtk_widget_show(button);
    button = gtk_button_new_with_label(_("Edit"));
    gtk_signal_connect (GTK_OBJECT (button), "clicked",
                        GTK_SIGNAL_FUNC(edit_alias), alias_list);
    gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 10);
    gtk_widget_show(button);
    button = gtk_button_new_with_label(_("Delete"));
    gtk_signal_connect (GTK_OBJECT (button), "clicked",
                        GTK_SIGNAL_FUNC(delete_alias), alias_list);
    gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 10);
    gtk_widget_show(button);

    /* alias list */
    gtk_clist_set_column_width (GTK_CLIST (alias_list), 0, 100);
    //gtk_list_set_selection_mode (GTK_LIST(alias_list), GTK_SELECTION_MULTIPLE);
    gtk_clist_set_selection_mode (GTK_CLIST(alias_list), GTK_SELECTION_BROWSE);
    gtk_signal_connect (GTK_OBJECT (alias_list), "select_row",
                        GTK_SIGNAL_FUNC(sig_alias_changed), NULL);
    gtk_container_add(GTK_CONTAINER(sw), alias_list);
    gtk_box_pack_start(GTK_BOX(vbox), sw, TRUE, TRUE, 10);
    gtk_widget_show(alias_list);

    gtk_clist_freeze(GTK_CLIST(alias_list));
    for (tmp = g_list_first(aliases); tmp != NULL; tmp = tmp->next)
    {
        ALIAS_REC *al;
        char *args[2];

        al = (ALIAS_REC *) tmp->data;
        args[0] = al->alias; args[1] = al->cmd;
        gtk_clist_append(GTK_CLIST(alias_list), args);
    }
    gtk_clist_thaw(GTK_CLIST(alias_list));
}
