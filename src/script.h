#include "config.h"

#ifdef USE_SCRIPT

#ifndef __SCRIPT_H
#define __SCRIPT_H

#include "data.h"

enum
{
    POPUPMENU_MAIN,
    POPUPMENU_NICK,
    POPUPMENU_CHAN,
};

void script_init(void);
void script_deinit(void);

int script_load(char *name);

int script_event(SERVER_REC *sendserver, char *sendnick, char *sendaddr, char *event, char *data);

void script_add_popups(GtkWidget *menu, int menutype, char *data);

#endif

#endif
