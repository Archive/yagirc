/*

 events-numeric.c : Functions to handle (numeric) IRC server replies

    Copyright (C) 1998 Timo Sirainen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "config.h"
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <sys/time.h>
#include <ctype.h>

#include <glib.h>

#include "os.h"
#include "irc.h"
#include "events-numeric_txt.h"
#include "gui.h"
#include "ctcp.h"
#include "events.h"
#include "script.h"
#include "misc.h"
#include "options.h"
#include "intl.h"
#include "gui_list.h"

static char tmpstr[600];

static CHAN_REC *names_chan; /* /NAMES */

/* Initialize events */
void events_init(void)
{
    names_chan = NULL;
}

void events_deinit(void)
{
}

/* Get next parameter */
char *event_get_param(char **data)
{
    char *pos;

    g_return_val_if_fail(data != NULL, NULL);
    g_return_val_if_fail(*data != NULL, NULL);

    if (**data == ':')
    {
        pos = *data;
        *data += strlen(*data);
        return pos+1; /* easy. :) */
    }

    pos = *data;
    while (**data != '\0' && **data != ' ') (*data)++;
    if (**data == ' ') *(*data)++ = '\0';

    return pos;
}

/* Get next count parameters from data */
char *event_get_params(char *data, int count, ...)
{
    char **str, *tmp;

    va_list args;
    va_start(args, count);

    while (count-- > 0)
    {
        str = (char **) va_arg(args, char **);
        tmp = event_get_param(&data);
        if (str != NULL) *str = tmp;
    }
    va_end(args);

    return data;
}

void eirc_welcome(char *data)
{
    /* first welcome message found - we're connected now! */
    eserver->connected = 1;
    return;
}

void eirc_connected(char *data)
{
    char *tempname;
    char *tptr;
    /* last welcome message found - commands can be sent to server now. */
#ifdef USE_SCRIPT
    script_event(eserver, esendnick, esendaddr, "CONNECTED", eserver->name);
#endif

    /* if on undernet, use undernet style wallops */
    tempname = g_strdup(eserver->name);
    for (tptr = tempname; *tptr != '\0'; tptr++) *tptr = tolower(*tptr);
    if (strstr(tempname, "undernet.org") != NULL) eserver->wallop_type = WALLOP_TYPE_UNET;
    else eserver->wallop_type = WALLOP_TYPE_STD;
    g_free(tempname);

    gui_connected(eserver, 1);
}

void eirc_nick_in_use(char *data)
{
    char *nick;

    if (eserver->connected)
    {
        /* Already connected, no need to handle this anymore. */
        event_get_params(data, 2, NULL, &nick);
        drawtext(eserver, NULL, LEVEL_CRAP, IRCTXT_NICK_IN_USE, nick);
        return;
    }

    /* nick already in use - need to change it .. */
    nick = eserver->nick;
    if (strlen(nick) < 9)
    {
        nick = g_new(char, strlen(nick)+2);
        sprintf(nick, "%s_", eserver->nick);
        g_free(eserver->nick);
        eserver->nick = nick;
    }
    else
    {
        int n;

        /* keep adding numbers to end of nick */
        for (n = 8; n > 0; n--)
        {
            if (nick[n] < '0' || nick[n] > '9')
            {
                nick[n] = '1';
                break;
            }
            else
            {
                if (nick[n] < '9')
                {
                    nick[n]++;
                    break;
                }
                else
                    nick[n] = '0';
            }
        }
    }

    eserver->connected = 1;
    sprintf(tmpstr, "NICK %s", nick);
    irc_send_cmd(eserver, tmpstr);

    eserver->connected = 0;
}

void eirc_names_list(char *data)
{
    char *type, *channel, *names, *ptr;

    g_return_if_fail(data != NULL);

    /* type = '=' = public, '*' = private, '@' = secret */
    event_get_params(data, 4, NULL, &type, &channel, &names);

    drawtext(eserver, NULL, LEVEL_CRAP, IRCTXT_NAMES, channel, names);

    if (names_chan == NULL)
    {
        names_chan = channel_joined(eserver, channel);
        if (names_chan == NULL) return;
        if (names_chan->nicks != NULL)
        {
            while (names_chan->nicks != NULL)
                remove_nick_rec(names_chan, names_chan->nicks->data);
        }
        names_chan->nicks = NULL;
    }

    while (*names != '\0')
    {
        while (*names == ' ') names++;
        ptr = names;
        while (*names != '\0' && *names != ' ') names++;
        if (*names != '\0') *names++ = '\0';

        insert_nick_rec(names_chan, ptr);
    }
}

void eirc_end_of_names(char *data)
{
    char *chan;

    event_get_params(data, 2, NULL, &chan);
    drawtext(eserver, NULL, LEVEL_CRAP, IRCTXT_ENDOFNAMES, chan);
    if (names_chan != NULL) gui_nicklist_redraw(names_chan->window);
    names_chan = NULL;
}

void eirc_topic(char *data)
{
    CHAN_REC *chan;
    char *channame, *topic;

    g_return_if_fail(data != NULL);

    event_get_params(data, 3, NULL, &channame, &topic);
    chan = channel_joined(eserver, channame);
    if (chan == NULL) return;

    if (chan->topic != NULL) g_free(chan->topic);
    chan->topic = *topic == '\0' ? NULL : g_strdup(topic);

    if (chan->topic != NULL)
        drawtext(eserver, channame, LEVEL_CHAN, IRCTXT_TOPIC, chan->name, chan->topic);
    gui_change_topic(chan);
}

void eirc_topic_info(char *data)
{
    CHAN_REC *chan;
    time_t t;
    struct tm *tim;
    char *timestr, *channame, *topicby, *topictime;

    g_return_if_fail(data != NULL);

    event_get_params(data, 4, NULL, &channame, &topicby, &topictime);

    chan = channel_joined(eserver, channame);
    if (chan == NULL) return;

    if (sscanf(topictime, "%lu", &t) != 1) t = 0; /* topic set date */
    tim = localtime(&t);
    timestr = g_strdup(asctime(tim));
    if (timestr[strlen(timestr)-1] == '\n') timestr[strlen(timestr)-1] = '\0';

    drawtext(eserver, channame, LEVEL_CHAN, IRCTXT_TOPIC_INFO, topicby, timestr);
    g_free(timestr);
}

void eirc_who(char *data)
{
    char *nick, *channel, *user, *host, *status, *realname, *tmp;
    CHAN_REC *chan;
    NICK_REC *rec;

    g_return_if_fail(data != NULL);

    event_get_params(data, 8, NULL, &channel, &user, &host, NULL, &nick, &status, &realname);
    /* skip hop count */
    while (*realname != '\0' && *realname != ' ') realname++;
    while (*realname == ' ') realname++;

    chan = channel_joined(eserver, channel);
    if (chan == NULL || (chan->queries & CHANQUERY_WHO) == 0)
    {
        sprintf(tmpstr, "%-10s %%_%-9s %-3s%%_ %s@%s (%%!%s%%!)\n", channel, nick, status, user, host, realname);
        drawtext(eserver, NULL, LEVEL_CRAP, tmpstr);
        return;
    }

    rec = find_nick_rec(chan, nick);
    if (rec == NULL) return;
    if (rec->host != NULL) g_free(rec->host);

    tmp = g_new(char, strlen(user)+strlen(host)+2);
    sprintf(tmp, "%s@%s", user, host);
    rec->host = tmp;
}

void eirc_end_of_who(char *data)
{
    CHAN_REC *ch;
    char *chan;

    event_get_params(data, 2, NULL, &chan);
    ch = channel_joined(eserver, chan);

    if (ch != NULL && ch->queries & CHANQUERY_WHO)
    {
        /* End of /WHO query */
        ch->queries &= ~CHANQUERY_WHO;
    }
    else
    {
        drawtext(eserver, NULL, LEVEL_CRAP, IRCTXT_END_OF_WHO);
    }
}

void eirc_ban_list(char *data)
{
    char *channame, *ban, *setby, *tims;
    long secs, tim;
    CHAN_REC *chan;

    g_return_if_fail(data != NULL);

    event_get_params(data, 5, NULL, &channame, &ban, &setby, &tims);
    chan = channel_joined(eserver, channame);

    if (sscanf(tims, "%ld", &tim) != 1) tim = (long) time(NULL);
    secs = (long) time(NULL)-tim;

    if (chan == NULL || (chan->queries & CHANQUERY_BANLIST) == 0)
    {
        drawtext(eserver, channame, LEVEL_CRAP, IRCTXT_BANLIST,
                 channame, ban, setby, secs);
        return;
    }

    add_ban(chan, ban, setby, (time_t) tim);
}

void eirc_end_of_banlist(char *data)
{
    CHAN_REC *ch;
    char *chan;

    g_return_if_fail(data != NULL);

    event_get_params(data, 2, NULL, &chan);
    ch = channel_joined(eserver, chan);

    if (ch != NULL && ch->queries & CHANQUERY_BANLIST)
        ch->queries &= ~CHANQUERY_BANLIST;
}

void eirc_ison(char *data)
{
    char *online;
    GList *tmp, *next;

    g_return_if_fail(data != NULL);

    event_get_params(data, 2, NULL, &online);

    if (eserver->ison_reqs <= 0)
    {
        /* user wrote /ISON.. */
        if (eserver->ison_reqs < 0) eserver->ison_reqs++;
        drawtext(eserver, NULL, LEVEL_CRAP, IRCTXT_ONLINE, online);
        return;
    }

    /* we're trying to find out who of the notify list members are in IRC */
    while (online != NULL && *online != '\0')
    {
        char *ptr;

        ptr = strchr(online, ' ');
        if (ptr != NULL) *ptr++ = '\0';
        eserver->ison_tempusers =
            g_list_append(eserver->ison_tempusers, g_strdup(online));
        online = ptr;
    }

    if (--eserver->ison_reqs == 0)
    {
        /* all /ISON requests got - scan through notify list to check who's
           came and who's left */

        /* first check who's came to irc */
        GLIST_FOREACH(tmp, eserver->ison_tempusers)
        {
            GList *on;

            GLIST_FOREACH(on, eserver->ison_users)
            {
                if (strcasecmp((char *) tmp->data, (char *) on->data) == 0)
                    break;
            }
            if (on == NULL)
            {
                /* not found from list, user's just joined to irc */
                gui_notify_join(tmp->data);
                eserver->ison_users =
                    g_list_append(eserver->ison_users, g_strdup(tmp->data));
            }
        }

        /* and then check who's left irc */
        GLIST_FOREACH(tmp, eserver->ison_users)
        {
            GList *on;

            next = tmp->next;
            GLIST_FOREACH(on, eserver->ison_tempusers)
            {
                if (strcasecmp((char *) tmp->data, (char *) on->data) == 0)
                    break;
            }
            if (on == NULL)
            {
                /* not found from list, user's left irc */
                gui_notify_part(tmp->data);
                g_free(tmp->data);
                eserver->ison_users =
                    g_list_remove_link(eserver->ison_users, tmp);
            }
        }

        /* free memory used by temp list */
        g_list_foreach(eserver->ison_tempusers, (GFunc) g_free, NULL);
        g_list_free(eserver->ison_tempusers);
        eserver->ison_tempusers = NULL;
    }
}

/* couldn't join to channel for some reason */
static void cannot_join(char *channel, char *reason)
{
    CHAN_REC *chan;

    g_return_if_fail(channel != NULL);

    chan = channel_joined(eserver, channel);
    drawtext(eserver, NULL, LEVEL_CRAP, IRCTXT_CANNOT_JOIN, channel, reason);

    if (chan == NULL) return;

    if (chan == chan->window->curchan)
        irc_select_new_channel(chan->window);

    gui_channel_part(chan, NULL);
    irc_chan_free(chan);
}

void eirc_nick_unavailable(char *data)
{
    char *channel;

    g_return_if_fail(data != NULL);

    event_get_params(data, 2, NULL, &channel);
    if (*channel == '#' || *channel == '&')
    {
        /* channel is unavailable. */
        cannot_join(channel, _("Channel is temporarily unavailable"));
        return;
    }

    if (!eserver->connected)
    {
        /* nick is unavailable.. */
        eirc_nick_in_use(NULL);
    }

    drawtext(eserver, NULL, LEVEL_CRAP, IRCTXT_NICK_UNAVAILABLE, channel);
}

void eirc_too_many_channels(char *data)
{
    char *channel;

    g_return_if_fail(data != NULL);

    event_get_params(data, 2, NULL, &channel);
    cannot_join(channel, _("You have joined too many channels"));
}

void eirc_channel_is_full(char *data)
{
    char *channel;

    g_return_if_fail(data != NULL);

    event_get_params(data, 2, NULL, &channel);
    cannot_join(channel, _("Channel is full"));
}

void eirc_invite_only(char *data)
{
    char *channel;

    g_return_if_fail(data != NULL);

    event_get_params(data, 2, NULL, &channel);
    cannot_join(channel, _("You must be invited"));
}

void eirc_banned(char *data)
{
    char *channel;

    g_return_if_fail(data != NULL);

    event_get_params(data, 2, NULL, &channel);
    cannot_join(channel, _("You are banned"));
}

void eirc_bad_channel_key(char *data)
{
    char *channel;

    g_return_if_fail(data != NULL);

    event_get_params(data, 2, NULL, &channel);
    cannot_join(channel, _("Bad channel key"));
}

void eirc_bad_channel_mask(char *data)
{
    char *channel;

    g_return_if_fail(data != NULL);

    event_get_params(data, 2, NULL, &channel);
    cannot_join(channel, _("Bad channel mask"));
}

void eirc_channel_mode(char *data)
{
    CHAN_REC *chan;
    char *channame;

    g_return_if_fail(data != NULL);

    data = event_get_params(data, 2, NULL, &channame);
    chan = channel_joined(eserver, channame);
    if (chan == NULL || (chan->queries & CHANQUERY_MODE) == 0)
    {
        drawtext(eserver, channame, LEVEL_CRAP, IRCTXT_CHANNEL_MODE, channame, data);
        return;
    }

    parse_channel_mode(chan, data);
    if ((chan->mode & CHANMODE_KEY) == 0 && chan->key != NULL)
    {
        g_free(chan->key);
        chan->key = NULL;
    }
    chan->queries &= ~CHANQUERY_MODE;
}

void eirc_channel_created(char *data)
{
    char *channel, *times, *timestr;
    time_t t;
    struct tm *tim;

    g_return_if_fail(data != NULL);

    event_get_params(data, 3, NULL, &channel, &times);

    if (sscanf(times, "%ld", (long *) &t) != 1) tim = 0;
    tim = localtime(&t);
    timestr = g_strdup(asctime(tim));
    if (timestr[strlen(timestr)-1] == '\n') timestr[strlen(timestr)-1] = '\0';

    drawtext(eserver, NULL, LEVEL_CRAP, IRCTXT_CHANNEL_CREATED, channel, timestr);
    g_free(timestr);
}

void eirc_away(char *data)
{
    gui_set_away(1);
    drawtext(eserver, NULL, LEVEL_CRAP, IRCTXT_AWAY);
}

void eirc_unaway(char *data)
{
    gui_set_away(0);
    drawtext(eserver, NULL, LEVEL_CRAP, IRCTXT_UNAWAY);
}

void eirc_whois(char *data)
{
    char *nick, *user, *host, *realname;

    event_get_params(data, 6, NULL, &nick, &user, &host, NULL, &realname);
    drawtext(eserver, NULL, LEVEL_CRAP, IRCTXT_WHOIS, nick, user, host, realname);
}

void eirc_whois_idle(char *data)
{
    char *nick, *secstr, *signon;
    int secs, h, m, s;
    time_t t;

    data = event_get_params(data, 3, NULL, &nick, &secstr);
    if (sscanf(secstr, "%d", &secs) == 0) secs = 0;
    t = 0;
    if (strstr(data, ", signon time") != NULL)
    {
        signon = event_get_param(&data);
        sscanf(signon, "%ld", (long *) &t);
    }

    h = secs/3600; m = (secs-h)/60; s = (secs-h)%60;
    drawtext(eserver, NULL, LEVEL_CRAP, IRCTXT_WHOIS_IDLE, nick, h, m, s);
    if (t != 0)
    {
        char *timestr;
        struct tm *tim;

        tim = localtime(&t);
        timestr = g_strdup(asctime(tim));
        if (timestr[strlen(timestr)-1] == '\n') timestr[strlen(timestr)-1] = '\0';
        drawtext(eserver, NULL, LEVEL_CRAP, IRCTXT_WHOIS_SIGNON, timestr);
    }
    drawtext(eserver, NULL, LEVEL_CRAP, "\n");
}

void eirc_whois_server(char *data)
{
    char *nick, *server, *desc;

    event_get_params(data, 4, NULL, &nick, &server, &desc);
    drawtext(eserver, NULL, LEVEL_CRAP, IRCTXT_WHOIS_SERVER, nick, server, desc);
}

void eirc_whois_oper(char *data)
{
    char *nick;

    event_get_params(data, 2, NULL, &nick);
    drawtext(eserver, NULL, LEVEL_CRAP, IRCTXT_WHOIS_OPER, nick);
}

void eirc_whois_channels(char *data)
{
    char *nick, *chans;

    event_get_params(data, 3, NULL, &nick, &chans);
    drawtext(eserver, NULL, LEVEL_CRAP, IRCTXT_WHOIS_CHANNELS, nick, chans);
}

void eirc_end_of_whois(char *data)
{
    char *nick;

    event_get_params(data, 2, NULL, &nick);
    drawtext(eserver, NULL, LEVEL_CRAP, IRCTXT_END_OF_WHOIS, nick);
}

static GList *ChanList=NULL;

void eirc_list_start(char *data)
{
   if (ChanList != NULL)
      g_list_free(ChanList);
   ChanList = g_list_alloc();

}

void eirc_list(char *data)
{
ChanListItem *newChan;

   data = strchr(data, ' ') + 1;
   newChan = chan_list_item_new(data);
   g_list_append(ChanList, newChan);

}

void eirc_list_end(char *data)
{
   gui_list_window(ChanList);

}
