#ifndef __IRC_TXT_H
#define __IRC_TXT_H

#include "gui.h"
#include "intl.h"

static char *  IRCTXT_CANT_CONNECT=N_("Unable to connect server %_%s%_ port %_%d%_\n");
static char *  IRCTXT_CONNECTING=N_("Connecting to %_%s%_ (%s) port %_%d%_\n");
static char *  IRCTXT_CONNECTION_LOST=N_("Connection lost to %_%s%_\n");
static char *  IRCTXT_NOT_CONNECTED=N_("Not connected to any server yet\n");
static char *  IRCTXT_NOT_JOINED=N_("Not joined to any channels yet\n");
static char *  IRCTXT_TALKING_IN=N_("Now talking in channel %_%s%_\n");
static char *  IRCTXT_QUERYING=N_("Now talking with %_%s%_\n");
static char *  IRCTXT_LOOKING_UP=N_("Looking up %_%s%_\n");
static char *  IRCTXT_CONNECTION_ESTABLISHED=N_("Connection to %_%s%_ established\n");

#endif