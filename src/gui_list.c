#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "data.h"
#include "events.h"
#include "gui_list.h"
#include "gui_menu.h"
#include "irc.h"

static int doubleclick, row;
static GtkWidget *window, *rchannel, *rusers, *rtopic, *rascend, *rdescend, *emin, *emax;
static GtkCList *clist=NULL;

ChanListItem *chan_list_item_new(char *data)
{
char *tmp, *temp2;
ChanListItem *newChan;
 
   newChan = g_malloc(sizeof(ChanListItem));
   newChan->name = g_strdup(data);
   tmp = strchr(newChan->name, ' ');
   if (tmp != NULL) *tmp = '\0';
   newChan->name = g_realloc(newChan->name, strlen(newChan->name)+1);

   data = strchr(data, ' ') + 1;
   temp2 = g_strdup(data);
   tmp = strchr(temp2, ' ');
   if (tmp != NULL) *tmp = '\0';
   temp2 = g_realloc(temp2, strlen(temp2)+1);
   newChan->users = atoi(temp2);
 
   data = strchr(data, ' ') + 1;
   if (*data == ':') data++;
   newChan->topic = g_malloc(strlen(data)+1);
   strcpy(newChan->topic, data);
   
   return newChan;
}

static void save_list()
{
char fname[128], *user_dir = getenv("HOME"), *net = gtk_object_get_data(GTK_OBJECT(clist), "net");
char *insert[3];
int i, j;
FILE *f;

   sprintf(fname, "%s/%s/%s", user_dir, CONFIG_DIR, net);
   if (!(f=fopen(fname, "w+t"))) return;
   fprintf(f, "' Channel listing for %s [DO NOT EDIT THIS LINE]\n", net);
   for(i=0; i<clist->rows; i++) {
      for(j=0; j<3; j++)
         gtk_clist_get_text(clist, i, j, &insert[j]);
      fprintf(f, "%s %s %s\n", insert[0], insert[1], insert[2]);
   }
   fprintf(f, "' End of list\n");
   fclose(f);
}

static GList *load_list(char *file)
{
char buf[2048];
FILE *f;
GList *ret;
ChanListItem *item;

   if (!(f=fopen(file, "r+t"))) return NULL;
   if (fgets(buf, 2048, f) == NULL || strncmp(buf, "' Channel ", 10)) return NULL;
   ret = g_list_alloc();
   while(fgets(buf, 2048, f) != NULL)
      if (buf[0]!='\'') {
         item = chan_list_item_new(buf);
	 g_list_append(ret, item);
      }
   fclose(f);
   return ret;
}

static void my_kill(GtkWidget *widget)
{
clist = NULL;
gtk_widget_destroy(widget);
}

static void radio_callback(GtkWidget *widget)
{
char *type = gtk_object_get_data(GTK_OBJECT(widget), "type");
GtkWidget *button;
int num;

   if (!strcmp(type, "channel")) {
      button = rchannel;
      num = 0;
   } else if (!strcmp(type, "users")) {
      button = rusers;
      num = 1;
   } else {
      button = rtopic;
      num = 2;
   }
   if (!GTK_IS_TOGGLE_BUTTON(widget)) {
      if (GTK_TOGGLE_BUTTON(button)->active) {
         if (GTK_TOGGLE_BUTTON(rascend)->active)
 	        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(rdescend), TRUE);
         else
	        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(rascend), TRUE);
      } else
         gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(button), TRUE);
   } else {
      gtk_clist_set_sort_column(clist, num);
      gtk_clist_sort(clist);
   }
}

static void sort_callback(GtkWidget *widget)
{
char *type = gtk_object_get_data(GTK_OBJECT(widget), "type");

  if (!strcmp(type, "descend"))
     gtk_clist_set_sort_type(clist, GTK_SORT_DESCENDING);
  else
     gtk_clist_set_sort_type(clist, GTK_SORT_ASCENDING);
  gtk_clist_sort(clist);
}

static void list_callback(GtkWidget *widget, gint nrow, gint column, GdkEventButton *event, gpointer data)
{
   if (data == NULL)
      row = nrow;
   else
      row = -1;
}

static void list_join(GtkWidget *widget)
{
char *channel;

   gtk_clist_get_text(clist, row, 0, &channel);
   irccmd_join(channel);
}

static void list_reload()
{
char tmp[100];

   sprintf(tmp, "LIST >%d,<%d\n", atoi(gtk_entry_get_text(GTK_ENTRY(emin)))-1, 
      atoi(gtk_entry_get_text(GTK_ENTRY(emax)))+1);
   irc_send_cmd(cserver, tmp);
}

static void real_load(GtkWidget *widget, GtkWidget *filesel)
{
GList *list=load_list(gtk_file_selection_get_filename(GTK_FILE_SELECTION(filesel))), *tmp;
ChanListItem *item;
char *insert[3];

   gtk_widget_destroy(filesel);
   gtk_clist_clear(clist);
   for(tmp=g_list_first(list); tmp; tmp=tmp->next)
      if (tmp->data != NULL) {
         item = (ChanListItem *)tmp->data;
	 insert[0] = item->name;
	 insert[1] = item->users;
	 insert[2] = item->topic;
         gtk_clist_append(clist, insert);
      }
}

static void list_load()
{
char def[128], *user_dir = getenv("HOME");
GtkWidget *filesel;

   sprintf(def, "%s/%s/%s", user_dir, CONFIG_DIR, (char *)gtk_object_get_data(GTK_OBJECT(clist), "net"));
   filesel = gtk_file_selection_new("Channel list file");
   gtk_file_selection_set_filename(GTK_FILE_SELECTION(filesel), def);
   gtk_signal_connect(GTK_OBJECT(GTK_FILE_SELECTION(filesel)->ok_button), "clicked",
      GTK_SIGNAL_FUNC(real_load), filesel);
   gtk_signal_connect_object(GTK_OBJECT(GTK_FILE_SELECTION(filesel)->cancel_button), "clicked",
      GTK_SIGNAL_FUNC(gtk_widget_destroy), GTK_OBJECT(filesel));
   gtk_widget_show(filesel);
}

static void list_mousedown(GtkWidget *widget, GdkEventButton *event)
{
   if (event->button == 1) {
      if (!doubleclick)
         doubleclick = 1;
      else
         doubleclick = 2;
   }
}

static void list_mouseup(GtkCList *list, GdkEventButton *event)
{
   if (doubleclick == 2) {
      list_join(NULL);
      doubleclick = 0;
   }
   if (event->button == 1)
      doubleclick = 0;
   else if ((event->button == 3) && (row != -1)) {
      GtkWidget *menu = gtk_menu_new();
      char *channel, *tmp;
      
      gtk_clist_get_text(list, row, 0, &channel);
      tmp = g_malloc(strlen(channel)+8);
      sprintf(tmp, "NAMES %s\n", channel);
      add_popup(menu, "Join", irccmd_join, channel);
      add_popup(menu, "Mode", irccmd_mode, channel);
      add_popup(menu, "Names", irccmd_quote, tmp);
      gtk_menu_popup(GTK_MENU(menu), NULL, NULL, (GtkMenuPositionFunc) posfunc, event, 0, event->time);
   }
}

gchar *findnetwork()
{
GList *tmp;
SETUP_SERVER_REC *rec;

   for(tmp=g_list_first(servers); tmp; tmp=tmp->next)
      if (tmp->data != NULL) {
         rec = (SETUP_SERVER_REC *)tmp->data;
	 if (!strcmp(rec->name, eserver->name) && rec->port == eserver->port)
	    return rec->ircnet;
      }
   return "";
}

void gui_list_window(GList *channels)
{
  GtkWidget *vbox, *vbox2, *hbox, *hbox2, *button, *label, *sw;
  GSList *radio;
  GList *tlist;
  ChanListItem *item;
  int i;
  char tmp[100], *insert[3], *net=findnetwork(eserver->name);

  if (clist != NULL) {
   char *nnet;
   if (channels != NULL) {
      gtk_clist_clear(clist);
      for(tlist=g_list_first(channels); tlist; tlist=tlist->next)
         if (tlist->data != NULL) {
            item = (ChanListItem *)tlist->data;
	    insert[0] = item->name;
	    sprintf(insert[1], "%i", item->users);
	    insert[2] = item->topic;
	    gtk_clist_append(clist, insert);
         }
   } else
      list_reload();
   nnet = gtk_object_get_data(GTK_OBJECT(clist), "net");
   if (strcmp(net, nnet)) {
      sprintf(tmp, "Channel listing - %s", net);
      gtk_window_set_title(GTK_WINDOW(window), tmp);
      gtk_object_set_data(GTK_OBJECT(clist), "net", net);
   }
  } else {
   window = gtk_window_new(GTK_WINDOW_DIALOG);
   gtk_object_set_data(GTK_OBJECT(window), "list", channels);
   gtk_signal_connect(GTK_OBJECT(window), "destroy", GTK_SIGNAL_FUNC(my_kill), NULL);
   gtk_window_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
   gtk_widget_set_usize(GTK_WIDGET(window), 500, 500);
   sprintf(tmp, "Channel listing - %s", net);
   gtk_window_set_title(GTK_WINDOW(window), tmp);
   
   vbox = gtk_vbox_new(FALSE, 5);
   gtk_container_add(GTK_CONTAINER(window), vbox);
   
   hbox = gtk_hbox_new(FALSE, 5);
   gtk_box_pack_start(GTK_BOX(vbox), hbox, TRUE, TRUE, 0);

   clist = (GtkCList *)gtk_clist_new(3);
   sw = gtk_scrolled_window_new(NULL, NULL);
   gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(sw), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

   gtk_object_set_data(GTK_OBJECT(clist), "net", net);
   gtk_signal_connect(GTK_OBJECT(clist), "button_press_event", GTK_SIGNAL_FUNC(list_mousedown), NULL);
   gtk_signal_connect(GTK_OBJECT(clist), "button_release_event", GTK_SIGNAL_FUNC(list_mouseup), NULL);
   gtk_signal_connect(GTK_OBJECT(clist), "select_row", GTK_SIGNAL_FUNC(list_callback), NULL);
   gtk_signal_connect(GTK_OBJECT(clist), "unselect_row", GTK_SIGNAL_FUNC(list_callback), "unselect");
   gtk_container_add(GTK_CONTAINER(sw), GTK_WIDGET(clist));
   gtk_box_pack_start(GTK_BOX(hbox), sw, TRUE, TRUE, 0);
   gtk_clist_set_selection_mode(clist, GTK_SELECTION_SINGLE);
   //gtk_clist_set_border(clist, GTK_SHADOW_OUT);
   gtk_clist_set_column_title(clist, 0, "Channel");
   gtk_clist_set_column_title(clist, 1, "Users");
   gtk_clist_set_column_title(clist, 2, "Topic");
   gtk_clist_set_column_width(clist, 0, 100);
   gtk_clist_set_column_width(clist, 1, 50);
   gtk_clist_set_column_width(clist, 2, 228);
   gtk_clist_set_sort_column(clist, 0);
   gtk_clist_set_auto_sort(clist, TRUE);
   gtk_clist_column_titles_show(clist);
   gtk_clist_column_titles_active(clist);
   gtk_object_set_data(GTK_OBJECT(clist->column[0].button), "type", "channel");
   gtk_object_set_data(GTK_OBJECT(clist->column[1].button), "type", "users");
   gtk_object_set_data(GTK_OBJECT(clist->column[2].button), "type", "topic");

   for(i=0; i<clist->columns; i++)
      if (clist->column[i].button)
         gtk_signal_connect(GTK_OBJECT(clist->column[i].button), "clicked",
	    GTK_SIGNAL_FUNC(radio_callback), NULL);
   if (channels == NULL) {
      insert[0] = "Not";
      insert[1] = "Yet";
      insert[2] = "Loaded";
      gtk_clist_append(clist, insert);
   } else
      for(tlist=g_list_first(channels); tlist; tlist=tlist->next)
         if (tlist->data != NULL) {
            item = (ChanListItem *)tlist->data;
   	    insert[0] = item->name;
	    sprintf(insert[1], "%i", item->users);
	    insert[2] = item->topic;
	    gtk_clist_append(clist, insert);
         }

   vbox2 = gtk_vbox_new(TRUE, 5);
   gtk_box_pack_start(GTK_BOX(hbox), vbox2, FALSE, FALSE, 0);
   label = gtk_label_new("Sort by:");
   gtk_box_pack_start(GTK_BOX(vbox2), label, FALSE, FALSE, 0);
   gtk_widget_show(label);
   rchannel = gtk_radio_button_new_with_label(NULL, "Channel");
   gtk_object_set_data(GTK_OBJECT(rchannel), "type", "channel");
   gtk_signal_connect(GTK_OBJECT(rchannel), "clicked", GTK_SIGNAL_FUNC(radio_callback), NULL);
   gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(rchannel), TRUE);
   gtk_box_pack_start(GTK_BOX(vbox2), rchannel, FALSE, FALSE, 0);
   gtk_widget_show(rchannel);
   
   radio = gtk_radio_button_group(GTK_RADIO_BUTTON(rchannel));
   
   rusers = gtk_radio_button_new_with_label(radio, "Users");
   gtk_object_set_data(GTK_OBJECT(rusers), "type", "users");
   gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(rusers), FALSE);
   gtk_signal_connect(GTK_OBJECT(rusers), "clicked", GTK_SIGNAL_FUNC(radio_callback), NULL);
   gtk_box_pack_start(GTK_BOX(vbox2), rusers, FALSE, FALSE, 0);
   gtk_widget_show(rusers);
   
   radio = gtk_radio_button_group(GTK_RADIO_BUTTON(rusers));
   
   rtopic = gtk_radio_button_new_with_label(radio, "Topic");
   gtk_object_set_data(GTK_OBJECT(rtopic), "type", "topic");
   gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(rtopic), FALSE);
   gtk_signal_connect(GTK_OBJECT(rtopic), "clicked", GTK_SIGNAL_FUNC(radio_callback), NULL);
   gtk_box_pack_start(GTK_BOX(vbox2), rtopic, FALSE, FALSE, 0);
   gtk_widget_show(rtopic);
   
   label = gtk_label_new("Order:");
   gtk_box_pack_start(GTK_BOX(vbox2), label, FALSE, FALSE, 0);
   gtk_widget_show(label);
   
   rascend = gtk_radio_button_new_with_label(NULL, "Ascending");
   gtk_object_set_data(GTK_OBJECT(rascend), "type", "ascend");
   gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(rascend), TRUE);
   gtk_signal_connect(GTK_OBJECT(rascend), "clicked", GTK_SIGNAL_FUNC(sort_callback), NULL);
   gtk_box_pack_start(GTK_BOX(vbox2), rascend, FALSE, FALSE, 0);
   gtk_widget_show(rascend);
   
   radio = gtk_radio_button_group(GTK_RADIO_BUTTON(rascend));
   
   rdescend = gtk_radio_button_new_with_label(radio, "Descending");
   gtk_object_set_data(GTK_OBJECT(rdescend), "type", "descend");
   gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(rdescend), FALSE);
   gtk_signal_connect(GTK_OBJECT(rdescend), "clicked", GTK_SIGNAL_FUNC(sort_callback), NULL);
   gtk_box_pack_start(GTK_BOX(vbox2), rdescend, FALSE, FALSE, 0);
   gtk_widget_show(rdescend);
   
   hbox2 = gtk_hbox_new(FALSE, 0);
   gtk_box_pack_start(GTK_BOX(vbox2), hbox2, FALSE, FALSE, 0);
   
   label = gtk_label_new("Min:");
   gtk_box_pack_start(GTK_BOX(hbox2), label, FALSE, FALSE, 0);
   gtk_widget_show(label);
   
   emin = gtk_entry_new_with_max_length(4);
   gtk_entry_set_text(GTK_ENTRY(emin), "1");
   gtk_entry_select_region(GTK_ENTRY(emin), 0, 1);
   gtk_widget_set_usize(GTK_WIDGET(emin), 50, 25);
   gtk_box_pack_start(GTK_BOX(hbox2), emin, FALSE, FALSE, 0);
   gtk_widget_show(emin);
   gtk_widget_show(hbox2);
   
   hbox2 = gtk_hbox_new(FALSE, 0);
   gtk_box_pack_start(GTK_BOX(vbox2), hbox2, FALSE, FALSE, 0);
   
   label = gtk_label_new("Max:");
   gtk_box_pack_start(GTK_BOX(hbox2), label, FALSE, FALSE, 0);
   gtk_widget_show(label);
   
   emax = gtk_entry_new_with_max_length(4);
   gtk_entry_set_text(GTK_ENTRY(emax), "9999");
   gtk_entry_select_region(GTK_ENTRY(emax), 0, 4);
   gtk_widget_set_usize(GTK_WIDGET(emax), 50, 25);
   gtk_box_pack_start(GTK_BOX(hbox2), emax, FALSE, FALSE, 0);
   gtk_widget_show(emax);
   gtk_widget_show(hbox2);
   
   hbox2 = gtk_hbox_new(TRUE, 5);
   gtk_box_pack_start(GTK_BOX(vbox), hbox2, FALSE, FALSE, 0);
   
   button = gtk_button_new_with_label("Join");
   gtk_signal_connect(GTK_OBJECT(button), "clicked", GTK_SIGNAL_FUNC(list_join), NULL);
   gtk_box_pack_start(GTK_BOX(hbox2), button, TRUE, TRUE, 5);
   gtk_widget_show(button);
   
   button = gtk_button_new_with_label("Reload");
   gtk_signal_connect(GTK_OBJECT(button), "clicked", GTK_SIGNAL_FUNC(list_reload), NULL);
   gtk_box_pack_start(GTK_BOX(hbox2), button, TRUE, TRUE, 5);
   gtk_widget_show(button);
   
   button = gtk_button_new_with_label("Save");
   gtk_signal_connect(GTK_OBJECT(button), "clicked", GTK_SIGNAL_FUNC(save_list), NULL);
   gtk_box_pack_start(GTK_BOX(hbox2), button, TRUE, TRUE, 5);
   gtk_widget_show(button);
   
   button = gtk_button_new_with_label("Load");
   gtk_signal_connect(GTK_OBJECT(button), "clicked", GTK_SIGNAL_FUNC(list_load), NULL);
   gtk_box_pack_start(GTK_BOX(hbox2), button, TRUE, TRUE, 5);
   gtk_widget_show(button);
   
   button = gtk_button_new_with_label("Close");
   gtk_signal_connect_object(GTK_OBJECT(button), "clicked", GTK_SIGNAL_FUNC(my_kill), GTK_OBJECT(window));
   gtk_box_pack_start(GTK_BOX(hbox2), button, TRUE, TRUE, 5);

   gtk_widget_show(button);
   gtk_widget_show(GTK_WIDGET(clist));
   gtk_widget_show(hbox2);
   gtk_widget_show(vbox2);
   gtk_widget_show(hbox);
   gtk_widget_show(vbox);
   gtk_widget_show(window);
  }
}
