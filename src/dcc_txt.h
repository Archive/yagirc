#ifndef __DCC_TXT_H
#define __DCC_TXT_H

#include "gui.h"
#include "intl.h"

static char *  IRCTXT_DCC_CHAT=N_("%!DCC%! %_CHAT%_ from %_%s%_ [%!%s port %d%!]\n");
static char *  IRCTXT_DCC_CHAT_CONNECTED =N_("%!DCC%! %_CHAT%_ connection with %_%s%_ [%!%s port %d%!] established\n");
static char *  IRCTXT_DCC_CHAT_DISCONNECTED=N_("%!DCC%! lost chat to %_%s\n");
static char *  IRCTXT_DCC_SEND=N_("%!DCC%! %_%s%_ from %_%s%_ [%!%s port %d%!]: %!%s%! [%_%ld bytes%_]\n");
static char *  IRCTXT_DCC_SEND_EXISTS=N_("%!DCC%! already sending file %_%s%_ for %_%s%_\n");
static char *  IRCTXT_DCC_UNKNOWN_CTCP=N_("%!DCC%! unknown command %_%s%_ from %_%s%_ [%!%s %d%!]: %!%s%! [%_%ld%_]\n");
static char * IRCTXT_DCC_UNKNOWN_REPLY=N_("%!DCC%! unknown reply %_%s%_ from %_%s%_: %!%s\n");
static char * IRCTXT_DCC_UNKNOWN_TYPE=N_("%!DCC%! unknown type %_%s\n");
static char *  IRCTXT_DCC_CONNECTED=N_("%!DCC%! connection established with %_%s%_\n");
static char *  IRCTXT_DCC_CONNECT_ERROR=N_("%!DCC%! can't connect to %_%s%_ port %_%d\n");
static char *  IRCTXT_DCC_GET_CONNECTED=N_("%!DCC%! receiving file %!%s%! from %_%s%_ [%!%s port %d%!]\n");
static char *  IRCTXT_DCC_CANT_CREATE=N_("%!DCC%! can't create file %!%s\n");
static char *  IRCTXT_DCC_SEND_NOT_FOUND=N_("%!DCC%! file not found: %!%s\n");
static char *  IRCTXT_DCC_SEND_CONNECTED=N_("%!DCC%! sending file %!%s%! for %_%s%_ [%!%s port %d%!]\n");
static char *  IRCTXT_DCC_REJECTED=N_("%!DCC%! %_%s%_ was rejected by %_%s%_ (%!%s%!)\n");
static char * IRCTXT_DCC_CLOSE=N_("%!DCC%! %_%s%_ close for %_%s%_ (%!%s%!)\n");
static char * IRCTXT_DCC_GET_NONE_OFFERED=N_("%!DCC%! no file offered by %_%s\n");
static char * IRCTXT_DCC_GET_NOT_FOUND=N_("%!DCC%! file %_%s%_ not offered by %_%s\n");

#endif
