/*

 gui_dcc.c : User interface for DCC

    Copyright (C) 1998 Timo Sirainen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "config.h"

#include <stdio.h>
#include <string.h>

#include <gtk/gtk.h>

#include "os.h"
#include "dcc.h"
#include "irc.h"
#include "ctcp.h"
#include "events.h"
#include "gui.h"
#include "intl.h"

static char * IRCTXT_DCC_ASK_CHAT=N_(" Accept DCC CHAT from \n %s [%s port %d] ? ");
static char * IRCTXT_DCC_ASK_GET=N_(" Accept DCC SEND from %s \n [%s port %d]: %s [%ld bytes] ? ");

/* signal: dcc window closed */
static int dcc_delete_event(GtkWidget *window, GtkWidget *t, DCC_REC *dcc)
{
    if (dcc->handle != -1)
    {
        /* transfer still going - abort */
        dcc_abort(dcc);
    }
    gui_dcc_force_close(dcc);
    g_free(dcc);

    return 0;
}

/* Create DCC transfer window */
void gui_dcc_init(DCC_REC *dcc)
{
    GtkWidget *window;
    GtkWidget *label;
    GtkWidget *vbox;
    char *tmp;

    g_return_if_fail(dcc != NULL);

    dcc->gui = g_new0(GUI_DCC_REC, 1);

    dcc->gui->mainwin = window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_signal_connect (GTK_OBJECT (window), "delete_event",
                        GTK_SIGNAL_FUNC(dcc_delete_event),
                        dcc);

    tmp = g_new(char, strlen(dcc->nick)+PROGRAM_TITLE_SIZE+20);
    sprintf(tmp, "DCC %s %s - "PROGRAM_TITLE,
            dcc->type == DCC_TYPE_SEND ? "SEND" : "GET", dcc->nick);
    gtk_window_set_title(GTK_WINDOW(window), tmp);
    gtk_container_border_width(GTK_CONTAINER(window), 20);
    g_free(tmp);

    vbox = gtk_vbox_new(FALSE, 0);
    gtk_container_add(GTK_CONTAINER(window), vbox);
    gtk_widget_show(vbox);

    label = gtk_label_new(dcc->arg);
    gtk_box_pack_start(GTK_BOX(vbox), label, FALSE, FALSE, 0);
    gtk_widget_show(label);

    dcc->gui->progbar = gtk_progress_bar_new();
    gtk_box_pack_start(GTK_BOX(vbox), dcc->gui->progbar, FALSE, FALSE, 10);
    gtk_widget_show(dcc->gui->progbar);

    dcc->gui->infotext = gtk_label_new("(waiting for connection)");
    gtk_box_pack_start(GTK_BOX(vbox), dcc->gui->infotext, FALSE, FALSE, 0);
    gtk_widget_show(dcc->gui->infotext);

    gtk_widget_show(window);
}

/* Update DCC transfer window */
void gui_dcc_update(DCC_REC *dcc)
{
    char tmp[100];
    long secs;
    static int lastsecs = 0;

    g_return_if_fail(dcc != NULL);

    secs = time(NULL)-dcc->starttime;
    if (secs == lastsecs) return;

    lastsecs = secs;
    if (secs <= 0) secs = 1;
    sprintf(tmp, _("%lu of %lu bytes %s - avg %0.2fkB/sec"), dcc->transfd, dcc->size,
            dcc->type == DCC_TYPE_SEND ? _("sent") : _("received"),
            (double) dcc->transfd/secs/1024);
    gtk_label_set(GTK_LABEL(dcc->gui->infotext), tmp);
    gtk_progress_bar_update(GTK_PROGRESS_BAR(dcc->gui->progbar), (gfloat) dcc->transfd/dcc->size);
}

/* Close DCC transfer window */
void gui_dcc_close(DCC_REC *dcc)
{
    char tmp[100];
    long secs;

    g_return_if_fail(dcc != NULL);
    g_return_if_fail(dcc->gui != NULL);

    secs = time(NULL)-dcc->starttime;
    if (dcc->transfd == dcc->size)
    {
        gtk_progress_bar_update(GTK_PROGRESS_BAR(dcc->gui->progbar), 1);
        strcpy(tmp, _("transfer complete - all"));
    }
    else
        strcpy(tmp, _("transfer aborted -"));

    if (secs <= 0) secs = 1;
    sprintf(tmp+strlen(tmp), _(" %lu bytes %s - avg %0.2fkB/sec"), dcc->transfd,
            dcc->type == DCC_TYPE_SEND ? _("sent") : _("received"),
            (double) dcc->transfd/secs/1024);
    gtk_label_set(GTK_LABEL(dcc->gui->infotext), tmp);
}

/* Destroy DCC transfer window */
void gui_dcc_force_close(DCC_REC *dcc)
{
    g_return_if_fail(dcc != NULL);
    g_return_if_fail(dcc->gui != NULL);

    gtk_widget_destroy(dcc->gui->mainwin);
    g_free(dcc->gui);
}

/* DCC chat connection established - do something.. */
void gui_dcc_chat_init(DCC_REC *dcc)
{
    g_return_if_fail(dcc != NULL);
}

/* Text received from DCC chat - write to screen */
void gui_dcc_chat_write(DCC_REC *dcc, char *str)
{
    char *tmp;

    g_return_if_fail(dcc != NULL);
    g_return_if_fail(str != NULL);

    tmp = g_new(char, strlen(dcc->nick)+2);
    sprintf(tmp, "=%s", dcc->nick);

    if (*str == 1)
    {
        /* DCC CTCP */
        char *ptr;

        /* remove the later \001 */
        ptr = strchr(++str, 1);
        if (ptr != NULL)
            *ptr = *(ptr+1) == '\0' ? '\0' : ' ';

        /* get ctcp command */
        for (ptr = str; *ptr != ' ' && *ptr != '\0'; ptr++) ;
        if (*ptr == ' ') *ptr++ = '\0';

        if (strcasecmp(str, "ACTION") == 0)
        {
            SERVER_REC *old;

            old = eserver; eserver = NULL;
            ctcp_action(dcc->nick, tmp, ptr);
            eserver = old;
        }
        else
            drawtext(NULL, tmp, LEVEL_DCC, _("%9>>> DCC CTCP received from %_%s%_: %!%s\n"), dcc->nick, str);
    }
    else
    {
        drawtext(NULL, tmp, LEVEL_DCC,
                 "[%3%s%n(%4dcc%n)]%n %s\n", dcc->nick, str);
    }
    g_free(tmp);
}

/* pop up dialog on incoming dcc requests */
void gui_dccask_dialog(DCC_REC *dcc)
{
  GtkWidget *win, *labelw, *yes_button, *no_button;
  char *labeltxt = NULL;

  win = gtk_dialog_new();
  gtk_object_set_data(GTK_OBJECT(win), "rec", (gpointer)dcc);

  switch(dcc->type) {
  case DCC_TYPE_CHAT:
    labeltxt = g_new0(char, strlen(dcc->nick)+64);
    sprintf(labeltxt, IRCTXT_DCC_ASK_CHAT, dcc->nick, dcc->addr, dcc->port);
    break;
  case DCC_TYPE_GET:
    labeltxt = g_new0(char, strlen(dcc->nick)+strlen(dcc->arg)+90);
    sprintf(labeltxt, IRCTXT_DCC_ASK_GET, dcc->nick, dcc->addr, dcc->port, dcc->arg, dcc->size);
    break;
  }

  gtk_object_set_data(GTK_OBJECT(win), "text", (gpointer)labeltxt);
  labelw = gtk_label_new(labeltxt);
  yes_button = gtk_button_new_with_label(_("Yes"));
  no_button = gtk_button_new_with_label(_("No"));

  gtk_box_pack_start(GTK_BOX(GTK_DIALOG(win)->vbox), labelw, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX(GTK_DIALOG(win)->action_area), yes_button, TRUE, TRUE, 0);
  gtk_box_pack_start(GTK_BOX(GTK_DIALOG(win)->action_area), no_button, TRUE, TRUE, 0);

  gtk_signal_connect_object(GTK_OBJECT(yes_button), "clicked", GTK_SIGNAL_FUNC(sig_dccask_yes), GTK_OBJECT(win));
  gtk_signal_connect_object(GTK_OBJECT(no_button), "clicked", GTK_SIGNAL_FUNC(sig_dccask_no), GTK_OBJECT(win));
  gtk_signal_connect(GTK_OBJECT(win), "delete_event", GTK_SIGNAL_FUNC(dccask_kill), NULL);

  gtk_widget_show(labelw);
  gtk_widget_show(yes_button);
  gtk_widget_show(no_button);
  gtk_widget_show(win);
}

/* dcc approved */
void sig_dccask_yes(GtkWidget *window)
{
  DCC_REC *dcc = gtk_object_get_data(GTK_OBJECT(window), "rec");
  char *tmpcmd;

  switch(dcc->type) {
    case DCC_TYPE_CHAT:
      dcc_chat(dcc->nick);
      break;
    case DCC_TYPE_GET:
      tmpcmd = g_new0(char, strlen(dcc->nick)+strlen(dcc->arg)+2);
      sprintf(tmpcmd, "%s %s", dcc->nick, dcc->arg);
      dcc_get(tmpcmd);
      g_free(tmpcmd);
      break;
  }

  dccask_kill(window, NULL);
}

/* no, screw off! */
void sig_dccask_no(GtkWidget *window)
{
  DCC_REC *dcc = gtk_object_get_data(GTK_OBJECT(window), "rec");

  switch(dcc->type) {
  case DCC_TYPE_CHAT:
    dcc_find_close(dcc->nick, DCC_TYPE_CHAT, dcc->arg);
    break;
  case DCC_TYPE_GET:
    dcc_find_close(dcc->nick, DCC_TYPE_GET, dcc->arg);
    break;
  }

  dccask_kill(window, NULL);
}

/* kill that dcc ask dialog */
void dccask_kill(GtkWidget *window, gpointer data)
{
  char *labeltxt = gtk_object_get_data(GTK_OBJECT(window), "text");
  gtk_widget_destroy(window);
  g_free(labeltxt);
}

/* dcc send file selection dialog */
void gui_dcc_fileselect(char *nick)
{
  GtkWidget *dlg;
  char *title;

  g_return_if_fail(nick != NULL);

  title = g_new0(char, strlen(nick)+14);
  sprintf(title, "DCC Send to %s", nick);
  dlg = gtk_file_selection_new(title);
  g_free(title);

  gtk_object_set_data(GTK_OBJECT(dlg), "nick", g_strdup(nick));

  gtk_signal_connect(GTK_OBJECT(GTK_FILE_SELECTION(dlg)->ok_button), "clicked", GTK_SIGNAL_FUNC(dcc_fileselect_ok), dlg);
  gtk_signal_connect_object(GTK_OBJECT(GTK_FILE_SELECTION(dlg)->cancel_button), "clicked", GTK_SIGNAL_FUNC(dcc_fileselect_kill), GTK_OBJECT(dlg));
  gtk_signal_connect_object(GTK_OBJECT(dlg), "delete_event", GTK_SIGNAL_FUNC(dcc_fileselect_kill), GTK_OBJECT(dlg));

  gtk_widget_show(dlg);
}

/* ok, dcc this file */
void dcc_fileselect_ok(GtkWidget *window, gpointer data)
{
  char *temp, *nick, *file;

  nick = gtk_object_get_data(GTK_OBJECT(data), "nick");
  file = gtk_file_selection_get_filename(GTK_FILE_SELECTION(data));

  temp = g_new0(char, strlen(nick)+strlen(file)+2);
  sprintf(temp, "%s %s", nick, file);

  dcc_send(temp);

  g_free(temp);

  dcc_fileselect_kill((GtkWidget *)data);
}

/* kill the dialog */
void dcc_fileselect_kill(GtkWidget *window)
{
  g_free(gtk_object_get_data(GTK_OBJECT(window), "nick"));
  gtk_widget_destroy(window);
}

