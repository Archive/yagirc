/*

 gui_nicklist.c : User interface for handling nicklist

    Copyright (C) 1998 Timo Sirainen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "config.h"

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include <gtk/gtk.h>
#include <glib.h>

#include "os.h"
#include "irc.h"
#include "dcc.h"
#include "gui.h"
#include "commands.h"
#include "script.h"
#include "intl.h"

static int doubleclick = 0;
static char *doublesel = NULL;
extern void posfunc(GtkMenu *menu, gint *x, gint *y, GdkEventButton *event);

/* Redraw nick list */
void gui_nicklist_redraw(WINDOW_REC *window)
{
    GtkWidget *listitem;
    GList *nicks;
    GList *nlist;

    g_return_if_fail(window != NULL);
    g_return_if_fail(window->gui != NULL);

    if (window->gui->parent->selected != window)
    {
        /* this window is hidden right now.. */
        return;
    }

    if (window->gui->parent->nicklistlen > 0)
    {
        /* first clear list */
        GList *tmp;

        for (tmp = GTK_LIST(window->gui->parent->nicklist)->children; tmp != NULL; tmp = tmp->next)
            g_free(gtk_object_get_data(GTK_OBJECT(tmp->data), "nick"));
        gtk_list_clear_items(GTK_LIST(window->gui->parent->nicklist), 0, window->gui->parent->nicklistlen);
        window->gui->parent->nicklistlen = 0;
    }

    if (window->curchan == NULL)
    {
        /* no channel selected in this window */
        return;
    }

    /* draw all items.. */
    nlist = NULL;
    nicks = g_list_first(window->curchan->nicks);
    while (nicks != NULL)
    {
        NICK_REC *rec;

        rec = (NICK_REC *) nicks->data;
        listitem = gtk_list_item_new_with_label(rec->nick);
        gtk_object_set_data(GTK_OBJECT(listitem), "nick", g_strdup(rec->nick));
        nlist = g_list_append(nlist, listitem);
        gtk_widget_show(listitem);
        nicks = nicks->next;
        window->gui->parent->nicklistlen++;
    }
    if (nlist != NULL)
    {
        /* some nicks.. */
        gtk_list_append_items(GTK_LIST(window->gui->parent->nicklist), nlist);
        /* causes 8 pixel upward scroll in text window */
        gtk_widget_set_usize(window->gui->parent->nickscroll, 120, -1);
    }
//    else
//    {
        /* no nicks */
//        gtk_widget_set_usize(window->gui->parent->nickscroll, 1, -1);
//    }
//
}

/* Remove nick from GUI nicklist */
static char *gui_nicklist_remove(CHAN_REC *chan, char *nick)
{
    GList *pos, *list;
    char *data;

    g_return_val_if_fail(chan != NULL, 0);
    g_return_val_if_fail(nick != NULL, 0);
    if (chan->window->curchan != chan) return NULL;

    if (isircflag(*nick)) nick++; /* Skip @ and + */

    for (pos = GTK_LIST(chan->window->gui->parent->nicklist)->children; pos != NULL; pos = pos->next)
    {
        data = gtk_object_get_data(GTK_OBJECT(pos->data), "nick");
        if (strcasecmp(isircflag(*data) ? data+1 : data, nick) == 0)
            break;
    }
    if (pos == NULL) return NULL;

    list = g_list_append(NULL, pos->data);
    gtk_list_remove_items(GTK_LIST(chan->window->gui->parent->nicklist), list);
    chan->window->gui->parent->nicklistlen--;
    g_list_free(list);
    return data;
}

/* Add nick to list */
static int gui_nicklist_add(CHAN_REC *chan, char *nick)
{
    g_return_val_if_fail(chan != NULL, 0);
    g_return_val_if_fail(nick != NULL, 0);

    if (chan->window->curchan == chan && /* same channel selected */
        chan->window == chan->window->gui->parent->selected) /* same hidden window selected */
    {
        GList *list, *nlist;
        GtkWidget *listitem;
        int pos;

        listitem = gtk_list_item_new_with_label(nick);
        gtk_object_set_data(GTK_OBJECT(listitem), "nick", g_strdup(nick));
        gtk_widget_show(listitem);

        list = g_list_append(NULL, listitem);

        nlist = GTK_LIST(chan->window->gui->parent->nicklist)->children;
        for (pos = 0; nlist != NULL; pos++, nlist = nlist->next)
        {
            char *data;

            data = gtk_object_get_data(GTK_OBJECT(nlist->data), "nick");
            if (irc_nicks_compare(data, nick) > 0) break;
        }
        gtk_list_insert_items(GTK_LIST(chan->window->gui->parent->nicklist), list, pos);

        /* causes 8 pixel scroll up, doesn't appear to hurt when commented out */
//        gtk_widget_set_usize(chan->window->gui->parent->nickscroll, 120, -1);
        chan->window->gui->parent->nicklistlen++;
    }

    return 1;
}

/* Change nick name */
void gui_nick_change(SERVER_REC *server, char *from, char *to)
{
    GList *chanlist, *win;
    char *tmp, *ptr;

    g_return_if_fail(server != NULL);
    g_return_if_fail(from != NULL);
    g_return_if_fail(to != NULL);

    tmp = g_new(char, strlen(to)+2);
    strcpy(tmp+1, to);

    /* scan all windows */
    for (win = g_list_first(winlist); win != NULL; win = win->next)
    {
        /* scan all channels */
        for (chanlist = g_list_first(((WINDOW_REC *) win->data)->chanlist); chanlist != NULL; chanlist = chanlist->next)
        {
            char *rnick;
            CHAN_REC *chan;

            chan = (CHAN_REC *) chanlist->data;
            if (chan->server != server)
            {
                /* nick wasn't changed in this server.. */
                continue;
            }

            rnick = gui_nicklist_remove(chan, from);
            if (rnick != NULL)
            {
                /* nick found from this channel */
                tmp[0] = *rnick;
                ptr = isircflag(tmp[0]) ? tmp : tmp+1;
                gui_nicklist_add(chan, ptr);
                g_free(rnick);
            }
        }
    }
    g_free(tmp);
}

/* Change nick's mode (@, +) */
void gui_change_nick_mode(CHAN_REC *chan, char *nick, char type)
{
    char *rnick, *tmp, *ptr;

    g_return_if_fail(chan != NULL);
    g_return_if_fail(nick != NULL);

    rnick = gui_nicklist_remove(chan, nick);
    if (rnick != NULL)
    {
        tmp = g_new(char, strlen(nick)+2);
        sprintf(tmp, "%c%s", type, nick);

        ptr = tmp[0] == ' ' ? tmp+1 : tmp;
        gui_nicklist_add(chan, ptr);

        g_free(tmp);
        g_free(rnick);
    }
}

/* Someone joined some channel.. If nick==NULL, you joined */
void gui_channel_join(CHAN_REC *chan, char *nick)
{
    g_return_if_fail(chan != NULL);

    if (nick == NULL)
    {
        /* You joined to channel */
        chan->gui = gui_private_add_channel_button(GTK_BOX(chan->window->gui->parent->chanbox), chan);
        gui_draw_channel(chan->window, chan);
        return;
    }

    /* someone else joined some channel */
    gui_nicklist_add(chan, nick);
}

/* Someone left some channel.. If nick==NULL, you left, if chan==NULL someone
   quit from IRC */
void gui_channel_part(CHAN_REC *chan, char *nick)
{
    GList *tmplist, *win;

    if (nick == NULL)
    {
        /* You've left some channel */

        if (chan == NULL) return;

        gui_draw_channel(chan->window, chan->window->curchan);
        gui_private_remove_channel_button(chan);
        return;
    }

    if (chan != NULL)
    {
        /* Someone left some channel */
        char *rnick = gui_nicklist_remove(chan, nick);
        if (rnick != NULL) g_free(rnick);
        return;
    }

    /* Someone quit IRC, remove nick from all channels */
    for (win = g_list_first(winlist); win != NULL; win = win->next)
    {
        WINDOW_REC *winrec;

        winrec = (WINDOW_REC *) win->data;
        for (tmplist = g_list_first(winrec->chanlist); tmplist != NULL; tmplist = tmplist->next)
        {
            char *rnick;
            rnick = gui_nicklist_remove((CHAN_REC *) tmplist->data, nick);
            if (rnick != NULL) g_free(rnick);
        }
    }
}

/* signal: button pressed */
void signick_button_pressed(GtkWidget *widget, GdkEventButton *event)
{
    GList *sel;

    g_return_if_fail(widget != NULL);
    g_return_if_fail(event != NULL);

    if (event->button == 1)
    {
        if (!doubleclick || doublesel == NULL)
        {
            sel = GTK_LIST(widget)->selection;
            if (sel != NULL)
                doublesel = gtk_object_get_data(GTK_OBJECT(sel->data), "nick");
            else
                doublesel = NULL;
        }
        if (doubleclick)
        {
            /* doubleclicked - query user */
            doubleclick = 0;

            if (doublesel != NULL)
            {
                if (isircflag(*doublesel)) doublesel++;
                irccmd_query(doublesel);
                doublesel = NULL;
            }
        }
        else
        {
            doubleclick = 1;
        }
    }
}

/* signal: button released */
void signick_button_released(GtkWidget *widget, GdkEventButton *event)
{
    g_return_if_fail(widget != NULL);
    g_return_if_fail(event != NULL);

    if (event->button == 1)
    {
        doubleclick = 0;
        doublesel = NULL;
    /* show popup menu */
    } else if (event->button == 3) nicklist_popup(GTK_LIST(widget)->selection, event);
}
/* popup menu: CTCP PING */
void nicklist_ctcp_ping(char *nick)
{
    char *tmp;

    g_return_if_fail(nick != NULL);

    tmp = g_new(char, strlen(nick)+17);
    sprintf(tmp, "%s PING", nick);
    irccmd_ctcp(tmp);
}

/* popup menu: CTCP VERSION */
void nicklist_ctcp_version(char *nick)
{
    char *tmp;

    g_return_if_fail(nick != NULL);

    tmp = g_new(char, strlen(nick)+9);
    sprintf(tmp, "%s VERSION", nick);
    irccmd_ctcp(tmp);
}

/* Kick OK */
static void kick_ok(GtkWidget *window, int (*func) (char *))
{
    char *nick, *reason, *tmp;
    GtkEntry *entry;

    nick = gtk_object_get_data(GTK_OBJECT(window), "data");
    entry = gtk_object_get_data(GTK_OBJECT(window), "entry");

    reason = gtk_entry_get_text(entry);
    if (*reason != '\0')
    {
        tmp = g_new(char, strlen(nick)+strlen(reason)+2);
        sprintf(tmp, "%s %s", nick, reason);
        func(tmp);
        g_free(tmp);
    }

    gtk_widget_destroy(window);
}

static void sig_kick_ok(GtkWidget *window)
{
    kick_ok(window, irccmd_kick);
}
static void sig_kickban_ok(GtkWidget *window)
{
    kick_ok(window, irccmd_kickban);
}
static void sig_knockout_ok(GtkWidget *window)
{
    kick_ok(window, irccmd_knockout);
}

/* popup menu: KICK */
static void nicklist_kick(char *nick)
{
    g_return_if_fail(nick != NULL);

    gui_entry_dialog(_("Kick reason:"), NULL,
                     GTK_SIGNAL_FUNC(sig_kick_ok), nick);
}

/* popup menu: KICK+BAN */
static void nicklist_kickban(char *nick)
{
    g_return_if_fail(nick != NULL);

    gui_entry_dialog(_("Kick reason:"), NULL,
                     GTK_SIGNAL_FUNC(sig_kickban_ok), nick);
}

/* popup menu: KNOCKOUT */
static void nicklist_knockout(char *nick)
{
    g_return_if_fail(nick != NULL);

    gui_entry_dialog(_("Kick reason:"), NULL,
                     GTK_SIGNAL_FUNC(sig_knockout_ok), nick);
}

/* Nicklist's popup menu */
void nicklist_popup(GList *sel, GdkEventButton *event)
{
    GtkWidget *mymenu, *ctcpmenu, *opmenu;
    char *nick;
    GList *sel2;
    int cnt;
    /* fixit note: should add a subfunction of add_popup that dynamically
     * creates and destroys the args to these popup menu items so we can do
     * away with these ass static chars */
    static char onenick[200], normlist[200], stripclist[200];

    g_return_if_fail(event != NULL);
    if (sel == NULL) return;

    sel2 = sel;

    /* onenick: first selected nick, flags stripped (for queries and other non-multinick things)*/
    nick = gtk_object_get_data(GTK_OBJECT(sel->data), "nick");
    if (isircflag(*nick)) strncpy(onenick, nick+1, sizeof(onenick)-1);
    else strncpy(onenick, nick, sizeof(onenick)-1);

    /* normlist: space separated, not stripped (for scripting)*/
    strcpy(normlist, "");
    for (cnt=strlen(nick)+1; cnt < sizeof(normlist)-1; sel2 = sel2->next) {
      if (strlen(normlist)) strcat(normlist, " ");
      strcat(normlist, nick);
      if (sel2->next == NULL) break;
      nick = gtk_object_get_data(GTK_OBJECT(sel2->next->data), "nick");
      cnt += strlen(nick)+1;
    }

    /* stripclist: comma separated, flags stripped (for ctcps n shit)*/
    nick = gtk_object_get_data(GTK_OBJECT(sel->data), "nick");
    strcpy(stripclist, "");
    for (cnt=strlen(nick)+1; cnt < sizeof(stripclist)-1; sel = sel->next) {
      if (strlen(stripclist)) strcat(stripclist, ",");
      if (isircflag(*nick)) strcat(stripclist, ++nick);
      else strcat(stripclist, nick);
      if (sel->next == NULL) break;
      nick = gtk_object_get_data(GTK_OBJECT(sel->next->data), "nick");
      cnt += strlen(nick)+1;
    }

    mymenu = gtk_menu_new();
    opmenu = gtk_menu_new();
    ctcpmenu = gtk_menu_new();

    add_popup(mymenu, "Whois", irccmd_whois, stripclist);
    add_popup(mymenu, "Query", irccmd_query, onenick);
    add_popup(opmenu, "Op", irccmd_op, onenick);
    add_popup(opmenu, "Deop", irccmd_deop, onenick);
    add_popup(opmenu, "Voice", irccmd_voice, onenick);
    add_popup(opmenu, "Devoice", irccmd_devoice, onenick);
    add_popup(opmenu, "Kick", nicklist_kick, onenick);
    add_popup(opmenu, "Ban", irccmd_ban, onenick);
    add_popup(opmenu, "Kick+ban", nicklist_kickban, onenick);
    add_popup(opmenu, "Knockout", nicklist_knockout, onenick);
    add_popup_sub(mymenu, "Op", opmenu);
    add_popup(ctcpmenu, "Ping", nicklist_ctcp_ping, stripclist);
    add_popup(ctcpmenu, "Version", nicklist_ctcp_version, stripclist);
    add_popup(ctcpmenu, "DCC Chat", dcc_chat, onenick);
    add_popup(ctcpmenu, "DCC Send", dcc_send, onenick);
    add_popup_sub(mymenu, "CTCP", ctcpmenu);
#ifdef USE_SCRIPT
    script_add_popups(mymenu, POPUPMENU_NICK, normlist);
#endif

    gtk_menu_popup (GTK_MENU(mymenu), NULL, NULL, (GtkMenuPositionFunc) posfunc, event, 0, event->time);
}
