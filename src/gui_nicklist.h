#include "config.h"

#ifndef __GUI_NICKLIST_H
#define __GUI_NICKLIST_H

#include "data.h"

/* Redraw nick list */
void gui_nicklist_redraw(WINDOW_REC *window);

/* Change nick name */
void gui_nick_change(SERVER_REC *server, char *from, char *to);
/* Change nick's mode (@, +) */
void gui_change_nick_mode(CHAN_REC *chan, char *nick, char type);
/* Someone joined some channel.. If nick==NULL, you joined */
void gui_channel_join(CHAN_REC *chan, char *nick);
/* Someone left some channel.. If nick==NULL, you left, if chan==NULL someone
   quit from IRC */
void gui_channel_part(CHAN_REC *chan, char *nick);

void nicklist_popup(GList *sel, GdkEventButton *event);
/* ctcp funcs */
void nicklist_ctcp_ping(char *nick);
void nicklist_ctcp_version(char *nick);
/* nick list signals */
void signick_button_pressed(GtkWidget *widget, GdkEventButton *event);
void signick_button_released(GtkWidget *widget, GdkEventButton *event);

#endif
