#include "config.h"

#ifndef __GUI_SETUP_SERVER_H
#define __GUI_SETUP_SERVER_H

typedef struct
{
    char *name;
    char *ircnet;
    char *location;
    int port;
}
SETUP_SERVER_REC;

extern GList *servers; /* list of selected servers */
extern GList *globalservers; /* list of global servers */
extern SETUP_SERVER_REC *default_server_rec;

void gui_setup_servers(GtkWidget *vbox);
void gui_setup_servers_accept(void);

SETUP_SERVER_REC *add_server_list(GList **servers, char *net, char *name, int port, char *location);
void remove_server_list(GList **servers, SETUP_SERVER_REC *rec);

/* private */
GtkWidget *create_local_servers(GtkWidget *vbox, int max);

#endif
