#include "config.h"

#ifndef __GUI_SETUP_SETTINGS_H
#define __GUI_SETUP_SETTINGS_H

#include <gtk/gtk.h>

void gui_setup_guisettings(GtkWidget *vbox);
void gui_setup_guisettings_accept(void);
void gui_setup_othersettings(GtkWidget *vbox);
void gui_setup_othersettings_accept(void);

#endif
