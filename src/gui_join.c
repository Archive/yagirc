/*

 gui_join.c : Join channel dialog

    Copyright (C) 1998 Timo Sirainen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "config.h"

#include <stdio.h>
#include <string.h>

#include <glib.h>

#include "os.h"
#include "gui.h"
#include "irc.h"
#include "commands.h"
#include "misc.h"
#include "intl.h"

GList *joinlist;

static GtkWidget *joindlg = NULL;
static GList *jointemp;
static int doubleclick;

/* add new entry */
static void add_entry(GtkList *list, char *chan)
{
    GtkWidget *li;
    GList *glist;
    int pos;

    while (*chan == ' ') chan++;
    if (*chan == '\0') return;

    if (*chan == '#' || *chan == '&')
        chan = g_strdup(chan);
    else
    {
        char *tmp;

        tmp = g_new(char, strlen(chan)+2);
        sprintf(tmp, "#%s", chan);
        chan = tmp;
    }

    li = gtk_list_item_new_with_label(chan);
    gtk_object_set_data(GTK_OBJECT(li), "channel", g_strdup(chan));
    gtk_widget_show(li);

    /* sort insert */
    glist = GTK_LIST(list)->children;
    for (pos = 0; glist != NULL; pos++, glist = glist->next)
    {
        char *data;

        data = gtk_object_get_data(GTK_OBJECT(glist->data), "channel");
        if (strcasecmp(data, chan) > 0) break;
    }

    glist = g_list_append(NULL, li);
    gtk_list_insert_items(list, glist, pos);
    jointemp = g_list_insert(jointemp, g_strdup(chan), pos);

    gtk_list_select_child(list, li);
}

/* signal OK: Add new channel to list */
static void sig_add_ok(GtkWidget *window)
{
    GtkEntry *entry;
    GtkList *list;
    char *chan;

    list = gtk_object_get_data(GTK_OBJECT(window), "data");
    entry = gtk_object_get_data(GTK_OBJECT(window), "entry");
    chan = gtk_entry_get_text(entry);

    if (*chan != '\0') add_entry(list, chan);

    gtk_widget_destroy(window);
}

/* signal: add button pressed */
static void sig_add_join(GtkWidget *widget, GtkList *list)
{
    g_return_if_fail(list != NULL);

    gui_entry_dialog(_("Add new channel:"), NULL,
                     GTK_SIGNAL_FUNC(sig_add_ok), list);
}

/* signal: delete button pressed */
static void sig_delete_join(GtkWidget *widget, GtkList *list)
{
    GList *glist;
    int pos;

    g_return_if_fail(list != NULL);
    if (list->selection == NULL) return;

    /* remove from join list */
    pos = gtk_list_child_position(list, list->selection->data);
    glist = g_list_nth(jointemp, pos);
    g_free(glist->data);
    jointemp = g_list_remove_link(jointemp, glist);

    /* remove from gtk list */
    g_free(gtk_object_get_data(GTK_OBJECT(list->selection->data), "channel"));
    glist = g_list_append(NULL, list->selection->data);
    gtk_list_remove_items(list, glist);
}

/* signal OK: Edit channel name */
static void sig_edit_ok(GtkWidget *window)
{
    GtkEntry *entry;
    GtkList *list;
    char *chan;

    list = gtk_object_get_data(GTK_OBJECT(window), "data");
    entry = gtk_object_get_data(GTK_OBJECT(window), "entry");
    chan = gtk_entry_get_text(entry);

    if (*chan != '\0')
    {
        GtkWidget *old, *new;

        old = list->selection->data;

        add_entry(list, chan);

        new = list->selection->data;

        gtk_list_select_child(list, old);
        sig_delete_join(NULL, list);
        gtk_list_select_child(list, new);
    }

    gtk_widget_destroy(window);
}

/* signal: edit button pressed */
static void sig_edit_join(GtkWidget *widget, GtkList *list)
{
    char *data;

    g_return_if_fail(list != NULL);
    if (list->selection == NULL) return;

    data = gtk_object_get_data(GTK_OBJECT(list->selection->data), "channel");
    gui_entry_dialog(_("Change channel name:"), data,
                     GTK_SIGNAL_FUNC(sig_edit_ok), list);
}

/* signal: join button pressed */
static void sig_join(GtkWidget *widget, GtkList *list)
{
    SERVER_REC *server;
    GtkEntry *entry;
    char *data;

    g_return_if_fail(list != NULL);

    entry = gtk_object_get_data(GTK_OBJECT(list), "entry");
    data = gtk_entry_get_text(entry);
    if (*data == '\0') return;

    server = cserver;
    cserver = gtk_object_get_data(GTK_OBJECT(list), "server");
    irccmd_join(data);
    cserver = server;
}

/* signal: list selection changed */
static void sig_changed(GtkList *list)
{
    GtkEntry *entry;
    char *data;

    entry = gtk_object_get_data(GTK_OBJECT(list), "entry");

    if (list->selection != NULL)
        data = gtk_object_get_data(GTK_OBJECT(list->selection->data), "channel");
    else
        data = "";
    gtk_entry_set_text(entry, data);
}

/* signal: cancel button pressed */
static void sig_cancel(GtkWidget *window)
{
    GtkList *list;
    GList *glist;

    g_list_foreach(jointemp, (GFunc) g_free, NULL);
    g_list_free(jointemp); jointemp = NULL;

    list = gtk_object_get_data(GTK_OBJECT(window), "list");
    GLIST_FOREACH(glist, GTK_LIST(list)->children)
        g_free(gtk_object_get_data(GTK_OBJECT(glist->data), "channel"));

    gtk_widget_destroy(window);
    joindlg = NULL;
}

/* signal: ok button pressed */
static void sig_save(GtkWidget *window)
{
    g_list_foreach(joinlist, (GFunc) g_free, NULL);
    g_list_free(joinlist);

    joinlist = jointemp;
    jointemp = NULL;

    gui_write_config();

    sig_cancel(window);
}

/* signal: mouse button pressed */
static void sig_mousedown(GtkList *list, GdkEventButton *event)
{
    if (event->button == 1)
    {
        if (!doubleclick)
            doubleclick = 1;
        else
        {
            doubleclick = 0;
            sig_join(GTK_WIDGET(list), list);
        }
    }
}

/* signal: mouse button released */
static void sig_mouseup(GtkList *list, GdkEventButton *event)
{
    if (event->button == 1)
    {
        doubleclick = 0;
    }
}

void gui_join_dialog(SERVER_REC *server)
{
    GtkWidget *window;
    GtkWidget *hbox;
    GtkWidget *scrollbox;
    GtkWidget *list;
    GtkWidget *button;
    GtkWidget *entry;
    GList *tmp;

    if (joindlg != NULL)
    {
        /* join window already created */
        gdk_window_raise(joindlg->window);
        return;
    }
    doubleclick = 0;

    list = gtk_list_new();

    joindlg = window = gtk_dialog_new();
    gtk_window_position (GTK_WINDOW (joindlg), GTK_WIN_POS_MOUSE);
    
    gtk_signal_connect (GTK_OBJECT (window), "delete_event",
                        GTK_SIGNAL_FUNC(sig_cancel), window);
    gtk_widget_set_usize(window, 250, 300);
    gtk_object_set_data(GTK_OBJECT (window), "list", list);

    hbox = gtk_hbox_new(FALSE, 0);
    gtk_box_pack_start(GTK_BOX(GTK_DIALOG(window)->vbox), hbox, FALSE, FALSE, 10);
    gtk_widget_show(hbox);

    /* add/edit/delete/join buttons */
    button = gtk_button_new_with_label(_("Add"));
    gtk_signal_connect (GTK_OBJECT (button), "clicked",
                        GTK_SIGNAL_FUNC(sig_add_join), list);
    gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 10);
    gtk_widget_show(button);
    button = gtk_button_new_with_label(_("Edit"));
    gtk_signal_connect (GTK_OBJECT (button), "clicked",
                        GTK_SIGNAL_FUNC(sig_edit_join), list);
    gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 10);
    gtk_widget_show(button);
    button = gtk_button_new_with_label(_("Delete"));
    gtk_signal_connect (GTK_OBJECT (button), "clicked",
                        GTK_SIGNAL_FUNC(sig_delete_join), list);
    gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 10);
    gtk_widget_show(button);
    button = gtk_button_new_with_label(_("Join"));
    gtk_signal_connect (GTK_OBJECT (button), "clicked",
                        GTK_SIGNAL_FUNC(sig_join), list);
    gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 10);
    gtk_widget_show(button);

    /* list box */
    scrollbox = gtk_scrolled_window_new(NULL, NULL);
    gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrollbox),
                                   GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
    gtk_box_pack_start(GTK_BOX(GTK_DIALOG(window)->vbox), scrollbox, TRUE, TRUE, 0);
    gtk_widget_show(scrollbox);

    gtk_list_set_selection_mode (GTK_LIST(list), GTK_SELECTION_BROWSE);
    gtk_signal_connect (GTK_OBJECT (list), "button_press_event",
                        GTK_SIGNAL_FUNC(sig_mousedown), NULL);
    gtk_signal_connect (GTK_OBJECT (list), "button_release_event",
                        GTK_SIGNAL_FUNC(sig_mouseup), NULL);
    gtk_signal_connect (GTK_OBJECT (list), "selection_changed",
                        GTK_SIGNAL_FUNC(sig_changed), NULL);
    // gtk_container_add(GTK_CONTAINER(scrollbox), list);
    gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(scrollbox),list);
    gtk_widget_show(list);

    /* entry */
    entry = gtk_entry_new();
    gtk_signal_connect (GTK_OBJECT (entry), "activate",
                        GTK_SIGNAL_FUNC(sig_join), list);
    gtk_box_pack_start(GTK_BOX(GTK_DIALOG(window)->vbox), entry, FALSE, FALSE, 0);
    gtk_widget_show(entry);

    /* ok / cancel */
    button = gtk_button_new_with_label (_("OK"));
    gtk_box_pack_start (GTK_BOX (GTK_DIALOG(window)->action_area), button, TRUE, TRUE, 10);
    gtk_signal_connect_object (GTK_OBJECT (button), "clicked",
                               GTK_SIGNAL_FUNC (sig_save), GTK_OBJECT(window));
    gtk_widget_show (button);

    button = gtk_button_new_with_label (_("Cancel"));
    gtk_box_pack_start (GTK_BOX (GTK_DIALOG(window)->action_area), button, TRUE, TRUE, 10);
    gtk_signal_connect_object (GTK_OBJECT (button), "clicked",
                               GTK_SIGNAL_FUNC (sig_cancel), GTK_OBJECT(window));
    gtk_widget_show (button);

    gtk_object_set_data(GTK_OBJECT(list), "server", server);
    gtk_object_set_data(GTK_OBJECT(list), "entry", entry);

    gtk_widget_grab_focus(entry);

    for (tmp = g_list_first(joinlist); tmp != NULL; tmp = tmp->next)
        add_entry(GTK_LIST(list), (char *) tmp->data);

    gtk_widget_show(window);
}
