#include "config.h"

#ifndef __DATA_H
#define __DATA_H

#include <sys/time.h>

#define CHANQUERY_BANLIST     0x01
#define CHANQUERY_WHO         0x02
#define CHANQUERY_MODE        0x04

#define CHANMODE_INVITE       0x01
#define CHANMODE_SECRET       0x02
#define CHANMODE_PRIVATE      0x04
#define CHANMODE_MODERATE     0x08
#define CHANMODE_NOMSGS       0x10
#define CHANMODE_OP_TOPIC     0x20
#define CHANMODE_KEY          0x40

#define WALLOP_TYPE_STD 0 /* nick,nick,nick type wallops */
#define WALLOP_TYPE_UNET 1 /* @#chan type wallops */

typedef struct _NICK_REC NICK_REC;
typedef struct _BAN_REC BAN_REC;
typedef struct _CHAN_REC CHAN_REC;
typedef struct _WINDOW_REC WINDOW_REC;
typedef struct _SERVER_REC SERVER_REC;
typedef struct _SCRIPT_REC SCRIPT_REC;
typedef struct _DCC_REC DCC_REC;
typedef struct _GLOBAL_SETTINGS GLOBAL_SETTINGS;
typedef struct _URL_REC URL_REC;
typedef struct _URLWIN_REC URLWIN_REC;

#include "gui.h"

typedef void (*DRAW_FUNC)(char *, void *);

struct _URLWIN_REC
{
  GList *urls; /* All the URLs in this window */

  GtkWidget *win, *mainbox, *list, *clearbutt, *closebutt, *buttonbox; /* das widgetz */
};

struct _URL_REC
{
  char *url;
  char *nick;
  char *channel;
};

struct _GLOBAL_SETTINGS
{
    int acceptdccchat; /* Always accept DCC chat requests? */
    int acceptdccsend; /* Always accept DCC send requests? */
    int autoaskondcc; /* Automatically popup response box on incoming DCC? */
    int autoquery; /* Autoquery on msg? */
    int autowin_chan; /* Auto open new win for channel? */
    int autowin_query; /* Auto open new win for queries? */
    int pager; /* Enables the pager - maybe should have toggle for Always/Away/Never? */
    char *prefix; /* Default to the defined PREFIX, but allow change */
    int sound; /* Enable sound? */
    char *soundpath; /* BASH style path to sound files */
    int invmode;
    char *fingerreply;
    int logtofile;
    char *logpath; /* logs directory path */
    int toolbar_style;

	char *icon[16];
    int urlcatch; /* catch urls? */
    char *browserpath; /* path to url browser */
    int loadscript; /* load a script on startup? */
    char *loadscriptfile; /* file to load as initial script */
    int sayaway; /* should it announce away to current channel */
};

struct _WINDOW_REC
{
    int num; /* window number */
    int level;
    GList *chanlist; /* list of channels in this window */
    CHAN_REC *curchan; /* current active channel */
    SERVER_REC *defserv;

    DRAW_FUNC drawfunc;
    void *drawfuncdata;

    int new_data; /* set on when text is written to this window */

    char textbuf[1024];
    int textbuflen;

    GUI_WINDOW_REC *gui;
};

struct _NICK_REC
{
    char *nick;
    char *host;
};

struct _BAN_REC
{
    char *ban;
    char *setby;
    time_t time;
};

struct _CHAN_REC
{
    char *name; /* channel name */
    char *topic; /* channel topic */
    int mode; /* mode flags */
    int limit; /* user limit */
    char *key; /* channel key */
    GList *cmdhist, *histpos; /* command history */
    int histlines; /* same */
    GList *nicks; /* list of nicks */
    GList *banlist; /* list of bans */
    WINDOW_REC *window; /* channel window */
    SERVER_REC *server; /* channel server */
    int queries; /* queries going on in this channel.. CHANQUERY_xxx */

    int new_data; /* set on when text is written to this channel */

    GUI_CHAN_REC *gui;
};

struct _SERVER_REC
{
    char *name; /* server address */
    int port; /* connected port */
    char *nick; /* your nick */
    int connected; /* connected to server */
    int handle; /* socket handle */
    int readtag; /* gui input tag */
    int timetag; /* gui timeout tag */
    WINDOW_REC *defwin; /* default window where to send server messages */
    int wallop_type;  /* type of wallop to use (@#chan or nick,nick,nick) */

    int ison_reqs; /* number of /ISON requests sent to server for now */
    GList *ison_users, *ison_tempusers; /* /ISON */

    /* CTCP flood protection */
    int ctcptag; /* timeout tag */
    GList *ctcpqueue; /* reply queue */

    /* Flood protection */
    int floodtag; /* timeout tag */
    GHashTable *floodlist;

    /* Auto ignore list */
    int ignoretag; /* timeout tag */
    GList *ignorelist;

    /* /knockout ban list */
    int knockouttag; /* timeout tag */
    GList *knockoutlist;

    char buf[512]; /* receive buffer */
    int bufpos;

    int nbtag;
    int nbpipe[2];
};

struct _SCRIPT_REC
{
    char *name;
    int readfd;
    int writefd;
    int pid;
    int tag;

    char buf[512]; /* receive buffer */
    int bufpos;
};

enum
{
    DCC_TYPE_CHAT = 1,
    DCC_TYPE_SEND,
    DCC_TYPE_GET,
} DCCType;

struct _DCC_REC
{
    int type;

    char *arg;
    char *nick;

    char *addr; /* address we're connected in */
    int port; /* port we're connected in */

    long size, transfd; /* file size / bytes transferred */
    int handle, tag; /* socket handle / gui tag */
    int fhandle; /* file handle */
    time_t starttime; /* transfer start time */

    char *buf; /* read buffer */
    int bufpos;

    GUI_DCC_REC *gui;
};

typedef struct
{
    char *alias;
    char *cmd;
}
ALIAS_REC;

typedef struct
{
    char *name; /* channel or nick */
    int level; /* log level in this channel / for this nick */
}
LOG_ITEM_REC;

typedef struct
{
    char *fname;
    int handle;
    int level;
    GList *items;
}
LOG_REC;

#endif
