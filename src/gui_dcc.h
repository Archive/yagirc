#include "config.h"

#ifndef __GUI_DCC_H
#define __GUI_DCC_H

#include "data.h"

/* Create DCC transfer window */
void gui_dcc_init(DCC_REC *dcc);
/* Update DCC transfer window */
void gui_dcc_update(DCC_REC *dcc);
/* Close DCC transfer window */
void gui_dcc_close(DCC_REC *dcc);
/* Destroy DCC transfer window */
void gui_dcc_force_close(DCC_REC *dcc);

/* DCC chat connection established - do something.. */
void gui_dcc_chat_init(DCC_REC *dcc);
/* Text received from DCC chat - write to screen */
void gui_dcc_chat_write(DCC_REC *dcc, char *str);

/* dcc request yes/no dialog */
void gui_dccask_dialog(DCC_REC *rec);
void sig_dccask_yes(GtkWidget *window);
void sig_dccask_no(GtkWidget *window);
void dccask_kill(GtkWidget *window, gpointer data);

/* dcc send file selection dlg */
void gui_dcc_fileselect(char *nick);
void dcc_fileselect_ok(GtkWidget *window, gpointer data);
void dcc_fileselect_kill(GtkWidget *window);

#endif
