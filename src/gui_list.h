#include <gtk/gtk.h>

typedef struct chan_list_item {
   char *name;
   int users;
   char *topic;
} ChanListItem;

extern void posfunc(GtkMenu *, gint *, gint *, GdkEventButton *);
extern int irccmd_join(char *);
extern int irccmd_mode(char *);
extern int irccmd_quote(char *);
void gui_list_window(GList *);
ChanListItem *chan_list_item_new(char *);
