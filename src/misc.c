#include "config.h"
#include <stdio.h>
#include <string.h>

#include <glib.h>

#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>

#include "data.h"
#include "irc.h"
#include "misc.h"
#include "network.h"
#include "events.h"
#include "sound.h"

int exists(char *s)
{
  struct stat st;
  if ((!s) || (!*s)) return 0;
  if (stat(s, &st) < 0) return 0;
  return 1;
}

/*
int execprog(void *params)
{
    char *sh;
    char *path;
    char exe[10240];
    
    if (!params) return 0;
    if (fork() != 0) return 0;
    setsid();
    sh = "/bin/sh";
    exe[0] = 0;
    sscanf(params, "%10240s", exe);
    if (exe[0])
      {
        path = pathtofile(params, getenv("PATH"));
        if (path != NULL) 
          {
            g_free(path);
            execl(sh, sh, "-c", path, NULL);
            exit(0);
          }
        else
          {
            g_free(path);
            execl(sh, sh, "-c", (char *)params, NULL);
            exit(0);
          }
        return 0;
      }
    execl(sh, sh, "-c", (char *)params, NULL);
    exit(0);
    return 0;        
}
*/

char *pathtofile(char *file, char *searchpath)
{
    char *tempa, *tempb, *pathtest;
    int pathlen, execlen;

    tempa = g_malloc(strlen(searchpath) + 2);
    tempb = g_malloc(strlen(searchpath) + 2);

    if ((file[0] == '/') || (!searchpath))
      {
        return file;
      }
    if (!file) return NULL;

    for ( tempa = strdup(searchpath) ; tempb != NULL ; tempb = strchr(tempa, ':') )
      {
        tempb = strchr(tempa, ':');
        *tempb = '\0';
        pathtest = strdup(tempa);

        pathlen = strlen(pathtest);
        execlen = strlen(file);

        if (pathtest[pathlen] != '/')
          {
            pathtest = g_realloc(pathtest, pathlen + 2 + execlen);
            strcat(pathtest, "/");
            strcat(pathtest, file);
          }
        if (exists(pathtest)) return pathtest;
      }
    return NULL;
}

#ifdef WE_ARE_NUTS
int fileisaudio(char *file)
{
    char buf[4];
    FILE *f;
    WAVFormatChunk fmt;

    if (exists(file)) f = fopen(file, "r");
    else return FALSE;

    fread(buf, 1, 4, f);
    if ( (buf[0] != 'R') || (buf[1] != 'I') || (buf[2] != 'F') || (buf[3] != 'F') )
      {
        fclose(f);
        return FALSE;
      }
    fread(buf, 1, 4, f); fread(buf, 1, 4, f);
    fread(&fmt, sizeof(WAVFormatChunk), 1, f);
    if ( (fmt.chunkID[0] == 'f') && (fmt.chunkID[1] == 'm') && (fmt.chunkID[2] == 't') && (fmt.chunkID[3] == ' ') )
      {
        if (fmt.wFormatTag != 1)
          {
            fclose(f);
            return FALSE;
          }
        fclose(f);
        return TRUE;
      }
    else 
      {
        fclose(f);
        return FALSE;
      }
}
#endif

GList *glist_case_find_string(GList *list, char *str)
{
    for (list = g_list_first(list); list != NULL; list = list->next)
    {
        if (strcasecmp((char *) list->data, str) == 0)
            return list;
    }

    return NULL;
}

GList *glist_find_string(GList *list, char *str)
{
    for (list = g_list_first(list); list != NULL; list = list->next)
    {
        if (strcmp((char *) list->data, str) == 0)
            return list;
    }

    return NULL;
}

/* Read line from somewhere..

    socket : 0 = read from file/pipe, 1 = read from socket
    handle : file/socket handle to read from
    str    : where to put line
    buf    : temp buffer
    bufsize: temp buffer size
    bufpos : current position in temp buffer
*/
int read_line(int socket, int handle, char *str, char *buf, int bufsize, int *bufpos)
{
    int len, pos, bufs;

    if (handle == -1 || str == NULL || buf == NULL || bufpos == NULL) return -1;

    *str = '\0';

    if (socket)
    {
        len = net_receive(handle, buf+*bufpos, bufsize-*bufpos);
    }
    else
    {
        len = read(handle, buf+*bufpos, bufsize-*bufpos);
    }
    if (len < 0 && *bufpos != 0)
    {
        /* error/connection lost but still something in buffer.. */
        for (pos = 0; pos < *bufpos; pos++)
        {
            if (buf[pos] == 13 || buf[pos] == 10)
            {
                len = 0;
                break;
            }
        }
    }

    if (len < 0)
    {
        if (errno == EAGAIN || errno == EWOULDBLOCK) return 0;
        return -1;
    }

    bufs = len+*bufpos;
    if (bufs == 0) return 0; /* nothing came.. */

    for (pos = 0; pos < bufs; pos++)
    {
        if (buf[pos] == 13 || buf[pos] == 10)
        {
            /* end of line */
            memcpy(str, buf, pos); str[pos] = '\0';

            if (buf[pos] == 13 && pos+1 < bufs && buf[pos+1] == 10) pos++;

            memmove(buf, buf+pos+1, bufs-(pos+1));
            *bufpos = bufs-(pos+1);
            return 1;
        }
    }

    if (len+*bufpos == bufsize)
    {
        /* buffer overrun... */
        memcpy(str, buf, bufsize-1); str[bufsize-1] = '\0';
        *bufpos = 0;
        return 1;
    }

    /* EOL wasn't found, wait for more data.. */
    *bufpos = bufs;
    return 0;
}

/* nick record comparision for sort functions */
static int nickrec_compare(NICK_REC *p1, NICK_REC *p2)
{
    if (p1 == NULL) return -1;
    if (p2 == NULL) return 1;

    return irc_nicks_compare(p1->nick, p2->nick);
}

/* Add new nick to list */
NICK_REC *insert_nick_rec(CHAN_REC *chan, char *nick)
{
    NICK_REC *rec;

    g_return_val_if_fail(chan != NULL, NULL);
    g_return_val_if_fail(nick != NULL, NULL);

    rec = g_new(NICK_REC, 1);
    rec->nick = g_strdup(nick);
    rec->host = NULL;

    chan->nicks = g_list_insert_sorted(chan->nicks, rec, (GCompareFunc) nickrec_compare);
    return rec;
}

/* remove nick from list */
void remove_nick_rec(CHAN_REC *chan, NICK_REC *nick)
{
    g_return_if_fail(chan != NULL);
    g_return_if_fail(nick != NULL);

    chan->nicks = g_list_remove(chan->nicks, nick);

    g_free(nick->nick);
    if (nick->host != NULL) g_free(nick->host);
    g_free(nick);
}

/* Find nick record from list */
NICK_REC *find_nick_rec(CHAN_REC *chan, char *nick)
{
    GList *tmp;

    g_return_val_if_fail(chan != NULL, NULL);
    g_return_val_if_fail(nick != NULL, NULL);

    if (isircflag(*nick)) nick++;
    for (tmp = g_list_first(chan->nicks); tmp != NULL; tmp = tmp->next)
    {
        NICK_REC *rec;

        rec = (NICK_REC *) tmp->data;
        if (strcasecmp(isircflag(*rec->nick) ? rec->nick+1 : rec->nick, nick) == 0)
            return rec;
    }

    return NULL;
}

BAN_REC *add_ban(CHAN_REC *chan, char *ban, char *nick, time_t time)
{
    BAN_REC *rec;

    rec = g_new(BAN_REC, 1);
    rec->ban = g_strdup(ban);
    rec->setby = g_strdup(nick);
    rec->time = time;

    chan->banlist = g_list_append(chan->banlist, rec);

    return rec;
}

void remove_ban(CHAN_REC *chan, char *ban)
{
    GList *tmp;

    for (tmp = g_list_first(chan->banlist); tmp != NULL; tmp = tmp->next)
    {
        BAN_REC *rec;

        rec = (BAN_REC *) tmp->data;
        if (strcasecmp(rec->ban, ban) == 0)
        {
            chan->banlist = g_list_remove(chan->banlist, rec);
            break;
        }
    }
}

/* Change nick mode in list */
static void nick_mode_change(CHAN_REC *chan, char *nick, char mode)
{
    NICK_REC *rec, *newrec;
    char *data;

    g_return_if_fail(chan != NULL);
    g_return_if_fail(nick != NULL);

    rec = find_nick_rec(chan, nick);
    if (rec == NULL) return;

    /* insert new nick to list */
    data = g_new(char, strlen(nick)+2);
    if (mode != ' ')
        sprintf(data, "%c%s", mode, nick);
    else
        strcpy(data, nick);
    newrec = insert_nick_rec(chan, data);
    if (rec->host != NULL) newrec->host = g_strdup(rec->host);
    g_free(data);

    /* remove old nick from list */
    remove_nick_rec(chan, rec);
}

/* Parse channel mode string */
void parse_channel_mode(CHAN_REC *chan, char *modestr)
{
    NICK_REC *nrec;
    char *ptr, *mode, type;
    int modenum;

    type = '+';
    for (mode = event_get_param(&modestr); *mode != '\0'; mode++)
    {
        if (*mode == '+' || *mode == '-')
        {
            type = *mode;
            continue;
        }

        switch (*mode)
        {
            case 'b':
                ptr = event_get_param(&modestr);
                if (*ptr == '\0') break;

                if (type == '+')
                    add_ban(chan, ptr, esendnick, time(NULL));
                else
                    remove_ban(chan, ptr);
                break;

            case 'v':
                ptr = event_get_param(&modestr);
                if (*ptr != '\0')
                {
		  /* dont mess with voice shit if the dude is opped */
		  if ((nrec = find_nick_rec(chan, ptr)) != NULL) 
		  {
		    if (*nrec->nick != '@') 
		    {
		      gui_change_nick_mode(chan, ptr, type == '+' ? '+' : ' ');
		      nick_mode_change(chan, ptr, type == '+' ? '+' : ' ');
		    }
		  }
                }
                break;

            case 'o':
                ptr = event_get_param(&modestr);
                if (*ptr != '\0')
                {
                    gui_change_nick_mode(chan, ptr, type == '+' ? '@' : ' ');
                    nick_mode_change(chan, ptr, type == '+' ? '@' : ' ');
                }
                break;

            case 'l':
                if (type == '-')
                    chan->limit = 0;
                else
                {
                    ptr = event_get_param(&modestr);
                    sscanf(ptr, "%d", &chan->limit);
                }
                break;
            case 'k':
                ptr = event_get_param(&modestr);
                if (*ptr != '\0' || type == '-')
                {
                    if (chan->key != NULL)
                    {
                        g_free(chan->key);
                        chan->key = NULL;
                    }
                    if (type == '+') chan->key = g_strdup(ptr);
                }
                if (type == '+')
                    chan->mode |= CHANMODE_KEY;
                else
                    chan->mode &= ~CHANMODE_KEY;
                break;

            default:
                modenum = 0;
                switch (*mode)
                {
                    case 'i':
                        modenum = CHANMODE_INVITE;
                        break;
                    case 'm':
                        modenum = CHANMODE_MODERATE;
                        break;
                    case 's':
                        modenum = CHANMODE_SECRET;
                        break;
                    case 'p':
                        modenum = CHANMODE_PRIVATE;
                        break;
                    case 'n':
                        modenum = CHANMODE_NOMSGS;
                        break;
                    case 't':
                        modenum = CHANMODE_OP_TOPIC;
                        break;
                }
                if (type == '-')
                    chan->mode &= ~modenum;
                else
                    chan->mode |= modenum;
                break;
        }
    }
}

