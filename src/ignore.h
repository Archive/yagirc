#include "config.h"

#ifndef __IGNORE_H
#define __IGNORE_H

/* Initialize auto ignoring */
void autoignore_init(SERVER_REC *server);
/* Deinitialize auto ignoring */
void autoignore_deinit(SERVER_REC *server);
/* Autoignore nick */
void autoignore_nick(SERVER_REC *server, int type, char *nick);

/* Check if nick is in ignore list */
int ignore_check(SERVER_REC *server, char *nick, int type);

#endif
