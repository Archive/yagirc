/*

 flood.c : Flood protection (see also ctcp.c)

    Copyright (C) 1998 Timo Sirainen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "config.h"
#include <stdio.h>
#include <string.h>

#include <glib.h>

#include "irc.h"
#include "gui.h"
#include "script.h"
#include "ignore.h"
#include "options.h"

typedef struct
{
    int type;
    int num;
    char *nick;
    SERVER_REC *server;
}
FLOOD_REC;

static int flood_timeout(SERVER_REC *server);

static void flood_rec_deinit(char *key, FLOOD_REC *rec)
{
    g_return_if_fail(rec != NULL);

    g_free(rec->nick);
    g_free(rec);
}

/* Initialize flood protection */
void init_flood(SERVER_REC *server)
{
    g_return_if_fail(server != NULL);

    server->floodlist = g_hash_table_new((GHashFunc) g_str_hash, (GCompareFunc) g_str_equal);
    server->floodtag = gui_timeout_new(FLOOD_TIMECHECK, (GUI_TIMEOUT_FUNC) flood_timeout, server);
}

/* Deinitialize flood protection */
void deinit_flood(SERVER_REC *server)
{
    g_return_if_fail(server != NULL);

    gui_timeout_remove(server->floodtag);
    g_hash_table_freeze(server->floodlist);
    g_hash_table_foreach(server->floodlist, (GHFunc) flood_rec_deinit, NULL);
    g_hash_table_thaw(server->floodlist);
    g_hash_table_destroy(server->floodlist);
}

/* timeout function: flood protection */
static int flood_timeout(SERVER_REC *server)
{
    g_return_val_if_fail(server != NULL, 0);

    /* remove everyone from hash table */
    g_hash_table_freeze(server->floodlist);
    g_hash_table_foreach(server->floodlist, (GHFunc) flood_rec_deinit, NULL);
    g_hash_table_thaw(server->floodlist);

    /* is this the only way to remove the whole hash table?? */
    g_hash_table_destroy(server->floodlist);
    server->floodlist = g_hash_table_new((GHashFunc) g_str_hash, (GCompareFunc) g_str_equal);

    return 1;
}

/* All messages should go through here.. */
void flood_newmsg(SERVER_REC *server, int type, char *nick)
{
    FLOOD_REC *rec;

    g_return_if_fail(server != NULL);
    g_return_if_fail(nick != NULL);

    rec = g_hash_table_lookup(server->floodlist, nick);
    if (rec != NULL)
    {
        if (++rec->num > MAX_FLOOD_MSGS)
        {
            /* flooding!!! */
#ifdef USE_SCRIPT
            int ok;

            ok = 1;
            {
                char *level, *params;

                level = irc_bits2level(rec->type);
                params = g_new(char, strlen(level)+strlen(nick)+2);
                sprintf(params, "%s %s", nick, level);
                ok = script_event(server, nick, NULL, "FLOOD", params);
                g_free(params);
            }
            if (ok)
#endif
            {
                if (type & LEVEL_MSGS)
                    autoignore_nick(server, type, nick);
            }
        }
        return;
    }

    rec = g_new(FLOOD_REC, 1);
    rec->type = type;
    rec->num = 1;
    rec->server = server;
    rec->nick = g_strdup(nick);

    g_hash_table_insert(server->floodlist, nick, rec);
}
