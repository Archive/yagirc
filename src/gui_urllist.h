#include "config.h"

#ifndef __GUI_URLLIST_H
#define __GUI_URLLIST_H

#include <glib.h>

#include "data.h"

GList *urllist;

void catalogurl(char *data, char *nick, char *channel);
char *formaturl(URL_REC *url);
void graburls(char **data, char *nick, char *channel);
int url_browse(char *url);
void gui_urllist_add(URLWIN_REC *window, URL_REC *url);
void gui_urllist_show(GList *urls);
void gui_urllist_kill(GtkWidget *widget, void *dizata);
void gui_urllist_clear(GtkWidget *widget, gpointer data);

#endif
