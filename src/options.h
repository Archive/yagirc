#include "config.h"

#ifndef __OPTIONS_H
#define __OPTIONS_H

/* max length sprintf("%lu") can give +1 .. in 32bit systems 11 is enough,
   64bit systems .. uh .. I don't know, pretty much :) */
#define INTEGER_LENGTH 20

/* time in ms between notify list checks */
#define NOTIFYLIST_TIMECHECK 30000

#define CMD_CHAR '/'

/* CTCP reply send timeout */
#define CTCP_TIMECHECK 1000
/* Max CTCP reply queue length */
#define MAX_CTCP_QUEUE 3

/* Flood check timeout */
#define FLOOD_TIMECHECK 1000
/* Max msgs in FLOOD_TIMECHECK msecs before considered as flooding */
#define MAX_FLOOD_MSGS 3

/* How often to check if there's anyone to be unignored in autoignore list */
#define AUTOIGNORE_TIMECHECK 10000
/* How many seconds to keep someone autoignored */
#define AUTOIGNORE_TIME (5*60)

/* How often to check if there's anyone to be unbanned in knockout list */
#define KNOCKOUT_TIMECHECK 10000
/* How many seconds to keep /knockouted ban */
#define KNOCKOUT_TIME (5*60)

/*#define SCRIPT_PREFIX PREFIX "/lib/yagirc/"
*/

#endif
