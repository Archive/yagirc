#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <glib.h>
#include <sys/types.h>

#include <unistd.h>
#include <pwd.h>

char *convhome(char *fname)
{
    struct passwd *pwd;
    char *tmp, *home;

    if (*fname != '~') return g_strdup(fname);

    home = getenv("HOME");
    if (home == NULL)
    {
        if ((pwd = getpwuid(getuid())) == NULL)
            home = NULL;
        else
            home = pwd->pw_dir;
    }

    if (home == NULL || *home == '\0')
        return g_strdup(fname); /* couldn't find home directory.. */

    tmp = g_new(char, strlen(fname)+strlen(home));
    sprintf(tmp, "%s%s", home, fname+1);
    return tmp;
}
