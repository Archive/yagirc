#include "config.h"

#ifndef __VERSION_H
#define __VERSION_H

#define IRC_VERSION_NUMBER VERSION
#define IRC_VERSION "yagIRC " IRC_VERSION_NUMBER

#endif
