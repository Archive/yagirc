/*

 gui_setup_dcc.c : DCC notebook entry for setup

    Copyright (C) 1998 Timo Sirainen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "config.h"

#include <stdio.h>
#include <string.h>

#include <gtk/gtk.h>
#include <glib.h>

#include "os.h"
#include "irc.h"
#include "gui.h"

/* draw alias list section */
void gui_setup_dcc(GtkWidget *vbox)
{
    GtkWidget *button;

    button = gtk_check_button_new_with_label("blahblah");
    gtk_box_pack_start(GTK_BOX(vbox), button, FALSE, FALSE, 0);
    gtk_widget_show(button);

    /*button = gtk_radio_button_new_with_label(
	         gtk_radio_button_group (GTK_RADIO_BUTTON (button)),
		 "DCC palplalpa");
    gtk_box_pack_start(GTK_BOX(vbox), button, FALSE, FALSE, 0);
    gtk_widget_show(button);*/
}
