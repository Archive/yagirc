#ifndef __COMMANDS_TXT_H
#define __COMMANDS_TXT_H

#include "gui.h"
#include "intl.h"

static char *  IRCTXT_BANLIST=N_("%!%s%!: ban %_%s%_, set by %!%s%! %_%lu%_ secs ago\n");
static char *  IRCTXT_DCC_CHAT_NOT_FOUND=N_("No DCC CHAT connection open to %_%s\n");
static char *  IRCTXT_DCC_UNKNOWN_COMMAND=N_("%!DCC%! unknown command: %!%s\n");
static char *  IRCTXT_DISCONNECTED=N_("Disconnected from %_%s%_ (%!%s%!)\n");
static char *  IRCTXT_LOG_OPENED=N_("Log opened: %_%s\n");
static char *  IRCTXT_LOG_CLOSED=N_("Log closed: %_%s\n");
static char *  IRCTXT_LOG_CANT_CREATE=N_("%!Log%! Can't create file %_%s\n");
static char *  IRCTXT_LOG_ENTRY=N_("Logging: %_%s\n");
static char *  IRCTXT_NO_QUERY=N_("No query with %_%s%_\n");
static char *  IRCTXT_NO_TOPIC=N_("No topic set for %_%s%_\n");
static char *  IRCTXT_QUERYING=N_("Now talking with %_%s%_\n");
static char *  IRCTXT_SERVER_SELECTED=N_("Server %_%d%_ (%!%s%!) selected\n");
static char *  IRCTXT_SERVER_NOT_FOUND=N_("No such server ID: %_%d\n");
static char *  IRCTXT_TALKING_IN=N_("Now talking in channel %_%s%_\n");
static char *  IRCTXT_TOPIC=N_("Topic for %_%s%_: %!%s\n");
static char *  IRCTXT_WINDOW_UNKNOWN_CMD=N_("Unknown /WINDOW command: %!%s\n");
static char *  IRCTXT_WALLOP_CMD_STD=N_("NOTICE %s :(wall/\x02%s\x02) %s");
static char *  IRCTXT_WALLOP_CMD_UNET=N_("NOTICE @%s :(wall/\x02%s\x02) %s");
static char *  IRCTXT_WALLOP_TXT=N_("-(%3wall%n/%4%_%s%_%n) %s\n");

#endif
