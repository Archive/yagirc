#include "config.h"

#ifndef __NETWORK_H
#define __NETWORK_H

/* Connect to socket */
int net_connect(char *server, int port);
/* Disconnect socket */
void net_disconnect(int fh);

/* Listen for connections on a socket */
int net_listen(char *addr, int *port);
/* Accept a connection on a socket */
int net_accept(int handle, char *addr, int *port);

/* Read data from socket */
int net_receive(int fh, char *buf, int len);
/* Transmit data */
int net_transmit(int fh, char *data, int len);

/* Get socket address/port */
int net_getsockname(int handle, char *addr, int *port);

#endif
