/*

 commands.c : Functions to all IRC /commands you can use

    Copyright (C) 1998 Timo Sirainen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "config.h"
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <time.h>

#include <glib.h>

#include "os.h"
#include "irc.h"
#include "dcc.h"
#include "network.h"
#include "commands_txt.h"
#include "gui.h"
#include "gui_list.h"
#include "script.h"
#include "sound.h"
#include "params.h"
#include "commands.h"
#include "log.h"
#include "misc.h"
#include "options.h"
#include "intl.h"

/* /KNOCKOUT */
typedef struct
{
    char *ban;
    int timeleft;
}
KNOCKOUT_REC;

/* /DCC */
typedef int (*CALL_FUNC)(char *);

struct CMD_STRUCT
{
    char *name;
    CALL_FUNC func;
} CMD_STRUCT;

static struct CMD_STRUCT dcc_cmds[] =
{
    { "CHAT", (CALL_FUNC) dcc_chat },
    { "SEND", (CALL_FUNC) dcc_send },
    { "GET", (CALL_FUNC) dcc_get },
    { "LIST", (CALL_FUNC) dcc_list },
    { "CLOSE", (CALL_FUNC) dcc_close },
    { NULL, NULL }
};

static char tmp[600];

static char *get_param(char **data)
{
    char *pos;

    g_return_val_if_fail(data != NULL, NULL);
    g_return_val_if_fail(*data != NULL, NULL);

    pos = *data;
    while (**data != '\0' && **data != ' ') (*data)++;
    if (**data == ' ') *(*data)++ = '\0';

    return pos;
}

int irccmd_say(char *data)
{
    char str[512];
    int len;

    g_return_val_if_fail(data != NULL, RET_ERR_PARAM);

    if (curwin->curchan == NULL) return RET_NOT_JOINED;

    len = sprintf(str, "%s ", curwin->curchan->name);
    strncpy(str+len, data, 510-len);
    str[510] = '\0';
  
    irccmd_msg(str);

    return RET_OK;
}

int irccmd_server(char *data)
{
    SERVER_REC *serv, *oldserv;
    char *ptr;
    int port;
    GList *tmp;

    g_return_val_if_fail(data != NULL, RET_ERR_PARAM);

    if (*data == '\0')
    {
      gui_connect_dialog();
      return RET_OK;
    }

    ptr = strchr(data, ':');
    if (ptr == NULL)
        port = 6667;
    else
    {
        *ptr = '\0';
        if (sscanf(ptr+1, "%d", &port) != 1) port = 6667;
    }

    if (curwin->defserv == NULL)
        oldserv = NULL;
    else
    {
        /* disconnect from old irc server */
        oldserv = curwin->defserv;
        irc_server_disconnect(curwin->defserv);
    }

    serv = irc_server_connect(data, port);
    if (serv == NULL) return RET_ERROR;

    serv->defwin = curwin;
    curwin->defserv = serv;

    /* put this to default server for all windows with no default servers yet */
    for (tmp = g_list_first(winlist); tmp != NULL; tmp = tmp->next)
    {
        WINDOW_REC *win;

        win = (WINDOW_REC *) tmp->data;
        if (win->defserv == NULL || win->defserv == oldserv)
            win->defserv = curwin->defserv;
    }

    irc_window_focus(curwin);

    return RET_OK;
}

int irccmd_servers(char *data)
{
    GList *tmp;
    g_return_val_if_fail(data != NULL, RET_ERR_PARAM);

    GLIST_FOREACH(tmp, servlist)
    {
        SERVER_REC *serv;

        serv = (SERVER_REC *) tmp->data;
        drawtext(NULL, NULL, LEVEL_CRAP, "server %d: %s\n", serv->handle, serv->name);
    }

    return RET_OK;
}

int irccmd_disconnect(char *data)
{
    SERVER_REC *server;
    char *sid;
    int id;

    g_return_val_if_fail(data != NULL, RET_ERR_PARAM);

    if (cserver == NULL)
    {
        /* we're not in any server yet.. */
        return RET_NOT_CONNECTED;
    }

    strcpy(tmp, "QUIT");

    sid = data; id = 0;
    while (isdigit(*sid))
    {
        id = id*10 + (*sid-'0');
        sid++;
    }

    if (sid != data && (*sid == ' ' || *sid == '\0'))
    {
        data = *sid == ' ' ? sid+1 : sid;
        server = irc_get_server(id);
        if (server == NULL)
        {
            drawtext(NULL, NULL, LEVEL_YAGNOTICE, IRCTXT_SERVER_NOT_FOUND, id);
            return RET_ERROR;
        }
    }
    else
    {
        if (sid == data && *sid == '*' && (*(sid+1) == ' ' || *(sid+1) == '\0'))
        {
            data++;
            if (*data == ' ') data++;
        }
        id = -1;
        server = cserver;
    }
    if (*data != '\0') sprintf(tmp+4, " :%s", data);

    irc_send_cmd(server, tmp);
    drawtext(NULL, NULL, LEVEL_YAGNOTICE, IRCTXT_DISCONNECTED, server->name, data);
    irc_server_disconnect(server);
    return RET_OK;
}

int irccmd_quit(char *data)
{
    SERVER_REC *old;
    char *tmp;

    g_return_val_if_fail(data != NULL, RET_ERR_PARAM);

    if (*data == '\0')
        tmp = NULL;
    else
    {
        tmp = g_new(char, strlen(data)+3);
        sprintf(tmp, "* %s", data);
    }

    /* disconnect from every server */

    /* Quit message */


    old = cserver;
    while (servlist != NULL)
    {
        cserver = (SERVER_REC *) servlist->data;
        irccmd_disconnect(tmp != NULL ? tmp : "yagIRC - http://yagirc.home.ml.org");
    }
    if (tmp != NULL) g_free(tmp);

    cserver = old;
    gui_exit();

    return RET_OK;
}

int irccmd_quote(char *data)
{
    g_return_val_if_fail(data != NULL, RET_ERR_PARAM);

    irc_send_cmd(cserver, data);
    return RET_OK;
}

int irccmd_echo(char *data)
{
    if ((data != NULL) && (*data != '\0'))
    { 
      char *temp;
      temp = data;
      while (*temp != '\0') temp++; *temp = '\n';
      drawtext(cserver, NULL, LEVEL_ALL, data);
      return RET_OK;
    } 
    else if (*data == '\0')  drawtext(cserver, NULL, LEVEL_ALL, "\n");

    return RET_OK;
}

#ifdef WE_ARE_NUTS

int irccmd_play(char *data)
{
    Sound *s;
    s = sound_load(data);
    switch (sound_play(s))
      {
        case SOUND_NOFILE:
          drawtext(cserver, NULL, LEVEL_ALL, "error, no such file\n");
          break;
        case SOUND_FMT_UNKNWN:
          drawtext(cserver, NULL, LEVEL_ALL, "error, unknown audio format\n");
          break;
        case SOUND_FAIL:
          drawtext(cserver, NULL, LEVEL_ALL, "error\n");
          break;
      }
    sound_destroy(s);
    return TRUE;
}

#endif

int irccmd_join(char *data)
{
    SERVER_REC *server;
    CHAN_REC *chan;
    char *ptr, *next, *key, *tmpp;

    tmpp = g_malloc(sizeof(char) *2);

    g_return_val_if_fail(data != NULL, RET_ERR_PARAM);

    if (*data == '\0')
    {
        if (cserver != NULL)
            gui_join_dialog(cserver);
        return RET_OK;
    }
    if (cserver == NULL || !cserver->connected) return RET_NOT_CONNECTED;

    for (next = data; next != NULL; data = next)
    {
        server = cserver;

        /* This code here seems to parse multiple joins at once */
        next = strchr(data, ',');
        if (next != NULL) *next++ = '\0';

        while (*data == ' ') data++;
        if (*data == '\0') break;

        if (isdigit(*data))
        {
            /* server id */
            char *digit = data;

            while (isdigit(*data)) data++;
            if (*data != ' ')
            {
                /* hm.. I think it was channel anyway.. */
                data = digit;
            }
            else
            {
                int tag;

                *data++ = '\0';
                if (sscanf(digit, "%d", &tag) == 1)
                {
                    server = irc_get_server(tag);
                    if (server == NULL)
                    {
                        /* no server has such id.. */
                        continue;
                    }
                }
            }
        }

        key = strchr(data, ' ');
        if (key != NULL)
        {
            /* channel key */
            *key++ = '\0';
        }

        if (*data != '&' && *data != '#')
        {
            /* No # or & at start of channel so add # there.. */
            ptr = g_new(char, strlen(data)+2);
            sprintf(ptr, "#%s", data);
            data = ptr;
        }
        else
            ptr = NULL;

        chan = channel_joined(server, data);
        if (chan != NULL)
        {
            /* already joined this channel, set it active.. */
            if (curwin->curchan != chan)
            {
                /* this channel wasn't previously selected in this window */
                drawtext(server, data, LEVEL_YAGNOTICE, IRCTXT_TALKING_IN, chan->name);
            }
            curwin->curchan = chan;
            gui_select_channel(curwin, chan);
            irc_window_focus(curwin);
            if (ptr != NULL) g_free(ptr);
            continue;
        }

        /* Send JOIN command to server */
        if (key != NULL)
            sprintf(tmp, "JOIN %s %s", data, key);
        else
            sprintf(tmp, "JOIN %s", data);
        irc_send_cmd(server, tmp);

        irc_channel_new(data, curwin, server, key);

        if (ptr != NULL) g_free(ptr);
    }

    g_free(tmpp);

    return RET_OK;
}

int irccmd_query(char *data)
{
    SERVER_REC *server;
    CHAN_REC *chan;

    g_return_val_if_fail(data != NULL, RET_ERR_PARAM);

    if (*data == '\0')
    {
        /* remove current query */
        irccmd_unquery("");
        return RET_OK;
    }

    server = cserver;
    if (isdigit(*data))
    {
        /* server id */
        char *digit = data;

        while (isdigit(*data)) data++;
        if (*data != ' ')
        {
            /* hm.. I think it was channel anyway.. */
            data = digit;
        }
        else
        {
            int tag;

            *data++ = '\0';
            if (sscanf(digit, "%d", &tag) == 1)
            {
                server = irc_get_server(tag);
                if (server == NULL)
                {
                    /* no server has such id.. */
                    drawtext(NULL, NULL, LEVEL_YAGNOTICE, IRCTXT_SERVER_NOT_FOUND, tag);
                    return RET_ERROR;
                }
            }
        }
    }

    chan = channel_joined(server, data);
    if (chan != NULL)
    {
        /* query already existed - change to query window */
        if (curwin->curchan != chan)
        {
            /* this query wasn't previously selected in this window */
            drawtext(server, data, LEVEL_YAGNOTICE, IRCTXT_QUERYING, chan->name);
        }
        curwin->curchan = chan;
        irc_window_focus(curwin);
        gui_select_channel(curwin, chan);
        return RET_OK;
    }

    irc_channel_new(data, curwin, cserver, NULL);

    return RET_OK;
}

int irccmd_unquery(char *data)
{
    CHAN_REC *chan;

    g_return_val_if_fail(data != NULL, RET_ERR_PARAM);

    if (*data == '\0')
        chan = curwin->curchan; /* remove current query */
    else
    {
        chan = channel_joined(cserver, data);
        if (chan == NULL)
        {
            drawtext(NULL, NULL, LEVEL_YAGNOTICE, IRCTXT_NO_QUERY, data);
            return RET_ERROR;
        }
    }

    /* remove query */
    if (chan == NULL || is_channel(*chan->name))
        return RET_ERROR;

    if (chan->window != curwin)
      {
        irccmd_query(chan->name);
      }

    if (chan->window != curwin)
      {
        irccmd_query(chan->name);
      }

    irc_select_new_channel(curwin);
    gui_channel_part(chan, NULL);
    irc_chan_free(chan);

    return RET_OK;
}

int irccmd_whois(char *data)
{
    g_return_val_if_fail(data != NULL, RET_ERR_PARAM);

    if (*data == '\0') return RET_NOT_ENOUGH_PARAMS;

    sprintf(tmp, "WHOIS %s", data);
    irc_send_cmd(cserver, tmp);

    return RET_OK;
}

int irccmd_part(char *data)
{
    char *channame;
    CHAN_REC *chan;

    g_return_val_if_fail(data != NULL, RET_ERR_PARAM);

    channame = get_param(&data);
    if (*channame == '\0' || strcmp(channame, "*") == 0)
    {
        if (curwin->curchan == NULL) return RET_NOT_JOINED;
        channame = curwin->curchan->name;
    }

    if (*channame == '\0') return RET_NOT_ENOUGH_PARAMS;

    chan = channel_joined(cserver, channame);
    if (chan == NULL) return RET_CHAN_NOT_FOUND;

    sprintf(tmp, "PART %s", channame);

    if (data != NULL && *data != '\0')
        sprintf(tmp+strlen(tmp), " :%s", data);

    irc_send_cmd(cserver, tmp);
    return RET_OK;
}

int irccmd_mode(char *data)
{
    char *channame;

    g_return_val_if_fail(data != NULL, RET_ERR_PARAM);

    channame = get_param(&data);
    if (strcmp(channame, "*") == 0)
    {
        if (curwin->curchan == NULL) return RET_NOT_JOINED;
        channame = curwin->curchan->name;
    }

    sprintf(tmp, "MODE %s %s", channame, data);
    irc_send_cmd(cserver, tmp);

    return RET_OK;
}

int irccmd_modes(char *data)
{
    CHAN_REC *chan;
    char *channame;

    g_return_val_if_fail(data != NULL, RET_ERR_PARAM);

    channame = get_param(&data);
    if (*channame == '\0' || strcmp(channame, "*") == 0)
    {
        if (curwin->curchan == NULL) return RET_NOT_JOINED;
        channame = curwin->curchan->name;
    }

    chan = channel_joined(cserver, channame);
    if (chan == NULL) return RET_CHAN_NOT_FOUND;

    gui_channel_modes(chan);

    return RET_OK;
}

int irccmd_topic(char *data)
{
    char *channame;

    g_return_val_if_fail(data != NULL, RET_ERR_PARAM);

    if (*data == '#' || *data == '&' || (*data == '*' && *(data+1) == '\0'))
        channame = get_param(&data);
    else
        channame = "";

    if (*channame == '\0' || strcmp(channame, "*") == 0)
    {
        if (curwin->curchan == NULL) return RET_NOT_JOINED;
        channame = curwin->curchan->name;
    }

    if (*data == '\0')
    {
        /* Display topic */
        CHAN_REC *chan;

        chan = channel_joined(cserver, channame);
        if (chan == NULL) return RET_CHAN_NOT_FOUND;

        if (chan->topic != NULL)
            drawtext(cserver, channame, LEVEL_YAGNOTICE, IRCTXT_TOPIC, chan->name, chan->topic);
        else
            drawtext(cserver, channame, LEVEL_YAGNOTICE, IRCTXT_NO_TOPIC, chan->name);
    }
    else
    {
        /* Change topic */
        sprintf(tmp, "TOPIC %s :%s", channame, data);
        irc_send_cmd(cserver, tmp);
    }

    return RET_OK;
}

int irccmd_msg(char *data)
{
    CHAN_REC *chan;
    char *target;

    g_return_val_if_fail(data != NULL, RET_ERR_PARAM);

    target = get_param(&data);
    if (*target == '\0' || *data == '\0') return RET_NOT_ENOUGH_PARAMS;

    chan = channel_joined(cserver, target);

    if (*target == '=')
    {
        /* dcc msg */
        DCC_REC *dcc;

        dcc = dcc_find_item(DCC_TYPE_CHAT, ++target, NULL);
        if (dcc == NULL)
        {
            drawtext(NULL, NULL, LEVEL_YAGNOTICE, IRCTXT_DCC_CHAT_NOT_FOUND, target);
            return RET_ERROR;
        }
        drawtext(NULL, NULL, LEVEL_DCC, "[%5dcc%n(%6%s%n)] %s\n", target, data);
        dcc_chat_write(dcc, data);
        return RET_OK;
    }

    if (cserver == NULL) return RET_NOT_CONNECTED;

    if (*target == '#' || *target == '&')
    {
        /* msg to channel */
        if (chan != NULL && chan->window->curchan != NULL &&
            strcasecmp(chan->window->curchan->name, target) == 0)
            drawtext(cserver, target, LEVEL_PUBLIC, "<%0%s%n> %s\n", cserver->nick, data);
        else
            drawtext(cserver, target, LEVEL_PUBLIC, "<%0%s%n:%2%s%n> %s\n", cserver->nick, target, data);
    }
    else
    {
        /* private message */
        drawtext(cserver, target, LEVEL_MSGS,
                 "[%5msg%n(%6%s%n)]%n %s\n", target, data);
    }

    sprintf(tmp, "PRIVMSG %s :%s", target, data);
    irc_send_cmd(cserver, tmp);

    return RET_OK;
}

int irccmd_notice(char *data)
{
    CHAN_REC *chan;
    char *target;

    g_return_val_if_fail(data != NULL, RET_ERR_PARAM);

    target = get_param(&data);
    if (*target == '\0' || *data == '\0') return RET_NOT_ENOUGH_PARAMS;

    chan = channel_joined(cserver, target);

    if (cserver == NULL) return RET_NOT_CONNECTED;

    drawtext(cserver, target, LEVEL_NOTICES,
             "-%4notice%n(%3%s%n)-%n %s\n", target, data);

    sprintf(tmp, "NOTICE %s :%s", target, data);
    irc_send_cmd(cserver, tmp);

    return RET_OK;
}

int irccmd_me(char *data)
{
    char *nick;

    g_return_val_if_fail(data != NULL, RET_ERR_PARAM);

    if (curwin->curchan == NULL) return RET_NOT_JOINED;

    nick = curwin->curchan->name;
    drawtext(curwin->curchan->server, nick, LEVEL_ACTIONS, "%7 * %s %8%s\n",
             cserver->nick, data);

    if (*nick != '=')
    {
        sprintf(tmp, "PRIVMSG %s :\001ACTION %s\001", nick, data);
        irc_send_cmd(cserver, tmp);
    }
    else
    {
        /* DCC action */
        DCC_REC *dcc;

        dcc = dcc_find_item(DCC_TYPE_CHAT, ++nick, NULL);
        if (dcc == NULL)
        {
            drawtext(NULL, NULL, LEVEL_YAGNOTICE, IRCTXT_DCC_CHAT_NOT_FOUND, nick);
            return RET_ERROR;
        }
        sprintf(tmp, "\001ACTION %s\001", data);
        dcc_chat_write(dcc, tmp);
    }

    return RET_OK;
}

int irccmd_ctcp(char *data)
{
    CHAN_REC *chan;
    char *ptr, *target;

    g_return_val_if_fail(data != NULL, RET_ERR_PARAM);

    target = get_param(&data);
    if (*target == '\0' || *target == '\0') return RET_NOT_ENOUGH_PARAMS;

    chan = channel_joined(cserver, target);

    for (ptr = data; *ptr != '\0' && *ptr != ' '; ptr++)
        *ptr = toupper(*ptr);

    if ( (!strcasecmp(data, "PING")) || (strstr(data, "PING") != NULL) ) 
      {
        sprintf(tmp, "PRIVMSG %s :\001%s %d\001", target, data, (int)time(NULL));
      }
    else
      {
        sprintf(tmp, "PRIVMSG %s :\001%s\001", target, data);
      }

    if ( (!strcasecmp(data, "PING")) || (strstr(data, "PING") != NULL) ) 
      {
        sprintf(tmp, "PRIVMSG %s :\001%s %d\001", target, data, (int)time(NULL));
      }
    else
      {
        sprintf(tmp, "PRIVMSG %s :\001%s\001", target, data);
      }

    drawtext(cserver, target, LEVEL_CTCP,
             "[%5ctcp%n(%6%s%n)]%n %s\n", target, data);

    irc_send_cmd(cserver, tmp);

    return RET_OK;
}

int irccmd_dcc(char *data)
{
    char *cmd;
    int n;

    g_return_val_if_fail(data != NULL, RET_ERR_PARAM);

    cmd = get_param(&data);
    if (*cmd == '\0') return RET_NOT_ENOUGH_PARAMS;

    for (n = 0; dcc_cmds[n].name != NULL; n++)
    {
        if (strcasecmp(dcc_cmds[n].name, cmd) == 0)
            return dcc_cmds[n].func(data);
    }

    drawtext(NULL, NULL, LEVEL_YAGNOTICE, IRCTXT_DCC_UNKNOWN_COMMAND, cmd);
    return RET_OK;
}

int irccmd_clear(char *data)
{
    g_return_val_if_fail(data != NULL, RET_ERR_PARAM);

    gui_window_clear(curwin);
    return RET_OK;
}

int irccmd_window(char *data)
{
    WINDOW_REC *win;
    char *cmd;

    g_return_val_if_fail(data != NULL, RET_ERR_PARAM);

    cmd = get_param(&data);
    if (*cmd == '\0') return RET_NOT_ENOUGH_PARAMS;

    while (*data == ' ') data++;
    if (strcasecmp(cmd, "NEW") == 0)
    {
        char *spec;

        spec = get_param(&data);
        if (*spec == '\0')
            win = irc_window_new(cserver, NULL);
        else if (strcasecmp(spec, "HIDE") == 0)
            win = irc_window_new(cserver, curwin);
    }
    else if (strcasecmp(cmd, "CLOSE") == 0)
    {
        irc_window_close(curwin);
    }
    else if (strcasecmp(cmd, "AUTORAISE") == 0)
    {
        char *spec;

        spec = get_param(&data);
        gui_set_autoraise(curwin, strcasecmp(spec, "off") == 0 ? 0 : 1);
    }
    else if (strcasecmp(cmd, "GOTO") == 0)
    {
        int num;

        if (sscanf(get_param(&data), "%d", &num) == 1 && num > 0)
        {
            GList *tmp;

            tmp = g_list_nth(winlist, num-1);
            if (tmp != NULL) gui_window_select((WINDOW_REC *) tmp->data);
        }
    }
    else if (strcasecmp(cmd, "LEVEL") == 0)
    {
        GList *tmp;

        curwin->level = irc_level2bits(data);
        GLIST_FOREACH(tmp, winlist)
        {
            WINDOW_REC *rec;

            rec = (WINDOW_REC *) tmp->data;
            if (rec == curwin || rec->defserv != curwin->defserv) continue;

            if (rec->level & curwin->level)
            {
                /* levels overlap .. */
                rec->level &= ~(rec->level & curwin->level);
            }
        }
    }
    else if (strcasecmp(cmd, "SERVER") == 0)
    {
        SERVER_REC *serv;
        char *ptr;
        int port;

        if (*data == '\0') return RET_NOT_ENOUGH_PARAMS;

        ptr = data;
        while (*ptr >= '0' && *ptr <= '9') ptr++;
        if (*ptr == '\0' && sscanf(data, "%d", &port) == 1)
        {
            /* try to change to already connected server */
            serv = irc_get_server(port);
            if (serv == NULL)
            {
                /* server not found.. */
                drawtext(NULL, NULL, LEVEL_YAGNOTICE, IRCTXT_SERVER_NOT_FOUND, port);
                return RET_ERROR;
            }

            /* server selected */
            drawtext(NULL, NULL, LEVEL_YAGNOTICE, IRCTXT_SERVER_SELECTED,
                     serv->handle, serv->name);
        }
        else
        {
            /* connect to server */
            ptr = strchr(data, ':');
            if (ptr == NULL)
                port = 6667;
            else
            {
                *ptr = '\0';
                if (sscanf(ptr+1, "%d", &port) != 1) port = 6667;
            }
            serv = irc_server_connect(data, port);
        }
        if (serv == NULL) return RET_ERROR;
        irc_select_new_server_window(curwin);
        serv->defwin = curwin;
        curwin->defserv = serv;
        irc_window_focus(curwin);
        gui_update_statusbar(curwin);
    }
    else
    {
        drawtext(NULL, NULL, LEVEL_YAGERROR, _(IRCTXT_WINDOW_UNKNOWN_CMD), cmd);
        return RET_ERROR;
    }

    return RET_OK;
}

int irccmd_ison(char *data)
{
    g_return_val_if_fail(data != NULL, RET_ERR_PARAM);
    if (*data == '\0') return RET_NOT_ENOUGH_PARAMS;

    if (cserver->ison_reqs > 0)
        /* hm.. there's already /ISON going because of notify list check,
           sending any /ISON's now would mess up notify list checks.. So,
           I don't think it's so bad to ignore it right now? :) maybe message
           to user, maybe change and make notify list differently, dunno,
           works enough well now .. */
        ;
    else
    {
        sprintf(tmp, "ISON :%s", data);
        irc_send_cmd(cserver, tmp);
    }

    return RET_OK;
}

int irccmd_load(char *data)
{
#ifdef USE_SCRIPT
    return script_load(data);
#else
    return RET_OK;
#endif
}

int irccmd_unload(char *data)
{
#ifdef USE_SCRIPT
    script_deinit();
#endif
    return RET_OK;
}

static int irccmd_modeset(char *data, char *mode)
{
    char *chan, *nick;

    g_return_val_if_fail(data != NULL, RET_ERR_PARAM);

    if (*data == '#' || *data == '&' || *data == '*')
        chan = get_param(&data);
    else
    {
        if (curwin->curchan == NULL) return RET_NOT_JOINED;
        chan = curwin->curchan->name;
    }
    if (*chan == '\0') return RET_NOT_ENOUGH_PARAMS;
    if (*chan != '#' && *chan != '&') return RET_NOT_JOINED;

    while (*data != '\0')
    {
        nick = get_param(&data);

        sprintf(tmp, "MODE %s %s %s", chan, mode, nick);
        irc_send_cmd(cserver, tmp);
    }

    return RET_OK;
}

int irccmd_op(char *data)
{
    return irccmd_modeset(data, "+o");
}

int irccmd_deop(char *data)
{
    return irccmd_modeset(data, "-o");
}

int irccmd_voice(char *data)
{
    return irccmd_modeset(data, "+v");
}

int irccmd_devoice(char *data)
{
    return irccmd_modeset(data, "-v");
}

int irccmd_ban(char *data)
{
    char *chan, *nick, *ptr;
    int banhost;

    g_return_val_if_fail(data != NULL, RET_ERR_PARAM);

    if (curwin->curchan == NULL) return RET_NOT_JOINED;
    chan = curwin->curchan->name;
    if (!is_channel(*chan)) return RET_NOT_JOINED;

    nick = get_param(&data);
    if (*nick == '\0') return RET_NOT_ENOUGH_PARAMS;

    banhost = 0;
    for (ptr = nick; *ptr != '\0'; ptr++)
    {
        if (!banhost && *ptr == '!')
            banhost = 1;
        else if (banhost == 1 && *ptr == '@')
            break;
    }
    if (*ptr == '\0') banhost = 0;

    if (!banhost)
        nick = irc_get_nickmask(curwin->curchan, nick);
    else
        nick = g_strdup(nick);

    if (nick == NULL) return RET_ERROR;

    sprintf(tmp, "MODE %s +b %s", chan, nick);
    irc_send_cmd(cserver, tmp);

    g_free(nick);
    return RET_OK;
}

int irccmd_kick(char *data)
{
    char *chan, *nick;

    g_return_val_if_fail(data != NULL, RET_ERR_PARAM);

    if (*data == '#' || *data == '&' || *data == '*')
        chan = get_param(&data);
    else
    {
        if (curwin->curchan == NULL) return RET_NOT_JOINED;
        chan = curwin->curchan->name;
    }
    if (*chan == '\0') return RET_NOT_ENOUGH_PARAMS;
    if (!is_channel(*chan)) return RET_NOT_JOINED;

    nick = get_param(&data);
    if (*nick == '\0') return RET_NOT_ENOUGH_PARAMS;

    sprintf(tmp, "KICK %s %s :%s", chan, nick, data);
    irc_send_cmd(cserver, tmp);

    return RET_OK;
}

int irccmd_kickban(char *data)
{
    char *nick, *olddata;
    int ret, ret2;

    g_return_val_if_fail(data != NULL, RET_ERR_PARAM);

    olddata = g_strdup(data);
    nick = get_param(&data);
    ret = irccmd_ban(nick);
    ret2 = irccmd_kick(olddata);
    if (ret == RET_OK) ret = ret2;
    g_free(olddata);

    return ret;
}

/* timeout function: knockout */
int knockout_timeout(SERVER_REC *server)
{
    static time_t lastcheck = 0;
    GList *tmp, *next;
    time_t t;

    g_return_val_if_fail(server != NULL, 0);

    t = time(NULL);
    if (lastcheck == 0) lastcheck = time(NULL)-KNOCKOUT_TIMECHECK;
    t -= lastcheck;

    for (tmp = g_list_first(server->knockoutlist); tmp != NULL; tmp = next)
    {
        KNOCKOUT_REC *rec;

        next = tmp->next;
        rec = (KNOCKOUT_REC *) tmp->data;
        if (rec->timeleft > t)
            rec->timeleft -= t;
        else
        {
            /* timeout, unban. */
            SERVER_REC *oldserver;

            oldserver = cserver; cserver = server;
            irccmd_modeset(rec->ban, "-b");
            cserver = oldserver;
            server->knockoutlist = g_list_remove(server->knockoutlist, rec);
            server->knockouttag = 0;
            g_free(rec->ban);
            g_free(rec);
        }
    }

    return server->knockoutlist != NULL;
}

int irccmd_knockout(char *data)
{
    int ret, ret2;
    char *nick, *knocktime, *ptr;
    int timeleft;

    g_return_val_if_fail(data != NULL, RET_ERR_PARAM);

    nick = get_param(&data);
    if (*nick == '\0') return RET_NOT_ENOUGH_PARAMS;

    if (isdigit(*data))
        knocktime = get_param(&data);
    else
        knocktime = "";

    if (*knocktime == '\0')
        timeleft = KNOCKOUT_TIME;
    else
    {
        if (sscanf(knocktime, "%d", &timeleft) != 0)
            timeleft = KNOCKOUT_TIME;
    }

    ret = irccmd_ban(nick);
    sprintf(tmp, "%s %s", nick, data);
    ptr = g_strdup(tmp);
    ret2 = irccmd_kick(ptr);
    g_free(ptr);

    if (ret == RET_OK) ret = ret2;
    if (ret == RET_OK)
    {
        KNOCKOUT_REC *rec;
        char *ban;

        rec = g_new(KNOCKOUT_REC, 1);
        rec->timeleft = timeleft;
        ban = irc_get_nickmask(curwin->curchan, nick);
        rec->ban = g_new(char, strlen(curwin->curchan->name)+strlen(ban)+2);
        sprintf(rec->ban, "%s %s", curwin->curchan->name, ban);
        g_free(ban);

        if (rec->ban == NULL)
            g_free(rec);
        else
        {
            if (cserver->knockoutlist == NULL)
                cserver->knockouttag = gui_timeout_new(KNOCKOUT_TIMECHECK, (GUI_TIMEOUT_FUNC) knockout_timeout, cserver);
            cserver->knockoutlist = g_list_append(cserver->knockoutlist, rec);
        }
    }
    return ret;
}

int irccmd_banstat(char *data)
{
    CHAN_REC *chan;
    GList *tmp;

    g_return_val_if_fail(data != NULL, RET_ERR_PARAM);

    if (*data == '*' || *data == '\0')
        chan = curwin->curchan;
    else
        chan = channel_joined(cserver, data);
    if (chan == NULL) return RET_CHAN_NOT_FOUND;

    for (tmp = g_list_first(chan->banlist); tmp != NULL; tmp = tmp->next)
    {
        BAN_REC *rec;

        rec = (BAN_REC *) tmp->data;
        drawtext(NULL, NULL, LEVEL_CRAP, IRCTXT_BANLIST,
                 chan->name, rec->ban, rec->setby, time(NULL)-rec->time);
    }

    return RET_OK;
}

int irccmd_kill(char *data)
{
    char *dest;

    g_return_val_if_fail(data != NULL, RET_ERR_PARAM);

    dest = get_param(&data);
    if (*dest == '\0') return RET_NOT_ENOUGH_PARAMS;

    sprintf(tmp, "KILL %s :%s", dest, data);
    irc_send_cmd(cserver, tmp);

    return RET_OK;
}

int irccmd_away(char *data)
{
    g_return_val_if_fail(data != NULL, RET_ERR_PARAM);

    sprintf(tmp, "AWAY :%s", data);
    irc_send_cmd(cserver, tmp);

    return RET_OK;
}

int irccmd_awayall(char *data)
{
    GList *list;
    char *awaymsg;
    int tot_sec = 0;
    static int sec_one;
    static int isaway;
    int sec_two;

    g_return_val_if_fail(data != NULL, RET_ERR_PARAM);

    sprintf(tmp, "AWAY :%s", data);
	

    GLIST_FOREACH(list, servlist)
        irc_send_cmd((SERVER_REC *) list->data, tmp);

    /* Annoucing away to channel, if desired */

    if(global_settings->sayaway)
    {

       awaymsg = malloc(1024);

       if(isaway == TRUE)
       {
          isaway = FALSE;

	  sec_two = time(NULL);
	  tot_sec = sec_two - sec_one;	
          sprintf(awaymsg, "is back. gone %d seconds.", tot_sec);
	  
       } else {
          isaway = TRUE;

	  sec_one = time(NULL);
          sprintf(awaymsg, "is away, %s", data);
       }
       irccmd_me(awaymsg);
       free(awaymsg);
    }

    return RET_OK;
}

int irccmd_wallops(char *data)
{
    char *chan;

    g_return_val_if_fail(data != NULL, RET_ERR_PARAM);

    chan = get_param(&data);

    sprintf(tmp, "WALLOPS %s :%s", chan, data);
    irc_send_cmd(cserver, tmp);

    return RET_OK;
}

int irccmd_log(char *data)
{
    LOG_REC *rec;
    char *file;

    g_return_val_if_fail(data != NULL, RET_ERR_PARAM);

    file = convhome(get_param(&data));
    rec = log_file_find(file);
    if (rec != NULL)
    {
        log_file_close(rec);
        drawtext(NULL, NULL, LEVEL_YAGNOTICE, IRCTXT_LOG_CLOSED, file);
        g_free(file);
        return RET_OK;
    }

    rec = log_file_open(file, data);
    if (rec == NULL)
    {
        drawtext(NULL, NULL, LEVEL_YAGERROR, IRCTXT_LOG_CANT_CREATE, file);
        g_free(file);
        return RET_ERROR;
    }
    g_free(file);

    drawtext(NULL, NULL, LEVEL_YAGNOTICE, IRCTXT_LOG_OPENED, rec->fname);
    return RET_OK;
}

int irccmd_logs(char *data)
{
    GList *tmp;

    GLIST_FOREACH(tmp, logs)
    {
        LOG_REC *rec;

        rec = (LOG_REC *) tmp->data;
        drawtext(NULL, NULL, LEVEL_CRAP, IRCTXT_LOG_ENTRY, rec->fname);
    }
    return RET_OK;
}

typedef struct
{
    char *user;
    SERVER_REC *server;
}
TMP_OPER_REC;

void irccmd_oper_callback(char *pass, void *data)
{
    TMP_OPER_REC *rec;

    g_return_if_fail(data != NULL);

    rec = (TMP_OPER_REC *) data;
    if (pass != NULL)
    {
        sprintf(tmp, "OPER %s %s", (char *) rec->user, pass);
        irc_send_cmd(rec->server, tmp);
    }
    g_free(rec);
}

int irccmd_oper(char *data)
{
    TMP_OPER_REC *rec;
    char *user, *pass;

    g_return_val_if_fail(data != NULL, RET_ERR_PARAM);
    if (cserver == NULL) return RET_NOT_CONNECTED;

    user = get_param(&data);
    if (*user == '\0') return RET_NOT_ENOUGH_PARAMS;

    pass = get_param(&data);
    if (*pass != '\0')
    {
        sprintf(tmp, "OPER %s %s", user, pass);
        irc_send_cmd(cserver, tmp);
        return RET_OK;
    }

    rec = g_new(TMP_OPER_REC, 1);
    rec->user = user;
    rec->server = cserver;
    gui_ask_password(_("Enter password:"), (GUI_PASSWORD_FUNC) irccmd_oper_callback, rec);

    return RET_OK;
}

/* channel wallop */
int irccmd_wallop(char *data)
{
  char *tsmsg, *nicks = NULL;
  GList *anick;
  NICK_REC *nrec;

  g_return_val_if_fail(data != NULL, RET_ERR_PARAM);

  /* if we're not in a channel, return */
  if (!is_channel(*curwin->curchan->name)) return(RET_NOT_JOINED);

  /* if the nicklist is empty, return */
  if (curwin->curchan->nicks == NULL) return(RET_OK);

  switch (cserver->wallop_type) {
  case WALLOP_TYPE_STD:
    for(anick = curwin->curchan->nicks; anick != NULL; anick = anick->next)  /* generate a comma-separated list of all the nicks in the chan */
      {
	if (anick == NULL) break;
	nrec = (NICK_REC *)anick->data;
	
	/* if the nick is yerself, dont add it (test for @/+/etc. chars on nick too) */
	if (isircflag(*nrec->nick)) {
	  if (!strcasecmp(cserver->nick, nrec->nick+1)) continue;
	} else {
	  if (!strcasecmp(cserver->nick, nrec->nick)) continue;
	}

	if (nicks == NULL) {
	  if (*nrec->nick == '@') nicks = g_strdup(nrec->nick+1);
	  else continue;

	} else {

	  if (*nrec->nick != '@') continue;
	  nicks = g_realloc(nicks, strlen(nicks)+strlen(nrec->nick)+2);
	  strcat(nicks, ",");
	  if (!isircflag(*nrec->nick)) strcat(nicks, nrec->nick);
	  else strcat(nicks, nrec->nick+1);
	}
      }
    tsmsg = g_new0(char, strlen(IRCTXT_WALLOP_CMD_STD)+strlen(curwin->curchan->name)+strlen(nicks)+strlen(data));
    sprintf(tsmsg, IRCTXT_WALLOP_CMD_STD, nicks, curwin->curchan->name, data);

    irc_send_cmd(cserver, tsmsg);
    g_free(tsmsg);

    break;

  case WALLOP_TYPE_UNET:
    tsmsg = g_new0(char, strlen(IRCTXT_WALLOP_CMD_UNET)+strlen(curwin->curchan->name)+strlen(curwin->curchan->name)+strlen(data)+1);
    sprintf(tsmsg, IRCTXT_WALLOP_CMD_UNET, curwin->curchan->name, curwin->curchan->name, data);

    irc_send_cmd(cserver, tsmsg);
    g_free(tsmsg);

    break;
  }

  drawtext(NULL, NULL, LEVEL_NOTICES, IRCTXT_WALLOP_TXT, curwin->curchan->name, data);
  g_free(nicks);

  return RET_OK;
}

/* list command */
int irccmd_list(char *data)
{
   g_return_val_if_fail(cserver != NULL, RET_NOT_CONNECTED);
   gui_list_window(NULL);
   return RET_OK;
}
