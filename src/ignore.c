/*

 ignore.c : Ignore stuff

    Copyright (C) 1998 Timo Sirainen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "config.h"
#include <stdio.h>
#include <string.h>

#include <glib.h>

#include "os.h"
#include "ignore_txt.h"
#include "irc.h"
#include "gui.h"
#include "misc.h"
#include "options.h"

typedef struct
{
    char *nick;
    int timeleft;
    int type;
}
IGNORE_REC;

static int autoignore_timeout(SERVER_REC *server);

static void autoignore_remove(SERVER_REC *server, IGNORE_REC *rec)
{
    g_return_if_fail(server != NULL);
    g_return_if_fail(rec != NULL);

    g_free(rec->nick);
    g_free(rec);

    server->ignorelist = g_list_remove(server->ignorelist, rec);
}

/* Initialize auto ignoring */
void autoignore_init(SERVER_REC *server)
{
    g_return_if_fail(server != NULL);

    server->ignoretag = gui_timeout_new(AUTOIGNORE_TIMECHECK, (GUI_TIMEOUT_FUNC) autoignore_timeout, server);
    server->ignorelist = NULL;
}

/* Deinitialize auto ignoring */
void autoignore_deinit(SERVER_REC *server)
{
    g_return_if_fail(server != NULL);

    gui_timeout_remove(server->ignoretag);

    while (server->ignorelist)
        autoignore_remove(server, (IGNORE_REC *) server->ignorelist->data);
}

/* timeout function: unignore old ignores.. */
static int autoignore_timeout(SERVER_REC *server)
{
    static time_t lastcheck = 0;
    GList *tmp, *next;
    time_t t;

    g_return_val_if_fail(server != NULL, 0);

    t = time(NULL);
    if (lastcheck == 0) lastcheck = time(NULL)-AUTOIGNORE_TIMECHECK;
    t -= lastcheck;

    for (tmp = g_list_first(server->ignorelist); tmp != NULL; tmp = next)
    {
        IGNORE_REC *rec;

        next = tmp->next;
        rec = (IGNORE_REC *) tmp->data;
        if (rec->timeleft > t)
            rec->timeleft -= t;
        else
        {
            drawtext(server, NULL, LEVEL_YAGNOTICE, IRCTXT_AUTOUNIGNORE, rec->nick);
            autoignore_remove(server, rec);
        }
    }

    return 1;
}

/* Autoignore nick */
void autoignore_nick(SERVER_REC *server, int type, char *nick)
{
    IGNORE_REC *rec;

    g_return_if_fail(server != NULL);
    g_return_if_fail(nick != NULL);

    drawtext(server, NULL, LEVEL_YAGNOTICE, IRCTXT_AUTOIGNORE, nick, AUTOIGNORE_TIME/60);

    rec = g_new(IGNORE_REC, 1);
    rec->nick = g_strdup(nick);
    rec->timeleft = AUTOIGNORE_TIME;
    rec->type = type;

    server->ignorelist = g_list_append(server->ignorelist, rec);
}

/* Check if nick is in ignore list */
int ignore_check(SERVER_REC *server, char *nick, int type)
{
    GList *tmp;

    g_return_val_if_fail(nick != NULL, 0);

    if (server != NULL)
    {
        GLIST_FOREACH(tmp, server->ignorelist)
        {
            IGNORE_REC *rec;

            rec = (IGNORE_REC *) tmp->data;
            if (rec->type & type && strcasecmp(rec->nick, nick) == 0) return 1;
        }
    }

    GLIST_FOREACH(tmp, ignores)
    {
        char *data, *ptr;

        data = g_strdup((char *) tmp->data);
        ptr = strchr(data, ' ');
        if (ptr == NULL)
        {
            g_free(data);
            continue; /* illegal ignore list entry */
        }
        *ptr++ = '\0';

        if (strcasecmp(data, nick) == 0)
        {
            if (type & irc_level2bits(ptr))
            {
                g_free(data);
                return 1;
            }

            g_free(data);
            return 0;
        }
        g_free(data);
   }

    return 0;
}
