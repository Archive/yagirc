#include "config.h"

#ifndef __OS_H
#define __OS_H

#define FILE_CREATE_MODE 0644

/* convert ~ to home dir */
char *convhome(char *fname);

#ifdef USE_GNOME
#  include <gnome.h>
#else
#  ifndef _
#    define _(a) a
#  endif
#endif

#endif
