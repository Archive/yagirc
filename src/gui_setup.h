#include "config.h"

#include "options.h"

#ifndef __GUI_SETUP_H
#define __GUI_SETUP_H

void gui_setup_init(void);
void gui_setup_deinit(void);

/* Read yagIRC's configuration file */
int gui_read_config(void);
/* Write yagIRC's configuration file */
int gui_write_config(void);

/* Run setup */
void gui_setup(void);

extern char *default_server;
extern int default_server_port;
extern char *default_nick;
extern char *user_name;
extern char *real_name;

extern char *default_text_pixmap;

/* private */
typedef void (*GUI_SETUP_FUNC)(GtkWidget *box);

#define gui_get_label(a, b) { a = g_strdup(gtk_entry_get_text(GTK_ENTRY(b))); }
GtkWidget *gui_add_label(GtkWidget *box, char *text, char *field);

#include "gui_setup_server.h"
#include "gui_setup_outlook.h"
#include "gui_setup_settings.h"

extern SETUP_THEME_REC *default_theme;

#endif
